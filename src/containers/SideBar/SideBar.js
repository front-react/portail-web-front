import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from 'react-router'
import Divider from 'material-ui/Divider'
import FaHome from 'react-icons/lib/fa/home';
import FaAlignJustify from 'react-icons/lib/fa/align-justify';
import FaAutomobile from 'react-icons/lib/fa/automobile';
import FaDashboard from 'react-icons/lib/fa/dashboard';
import FaExchange from 'react-icons/lib/fa/exchange';
import FaWrench from 'react-icons/lib/fa/wrench';
import FaExclamationTriangle from 'react-icons/lib/fa/exclamation-triangle'
import FaRoad from 'react-icons/lib/fa/road';
import FaCogs from 'react-icons/lib/fa/cogs';
import LogoutIcon from 'react-icons/lib/fa/power-off';
import IconButton from 'material-ui/IconButton'
import {blueGrey100, blueGrey900} from "material-ui/styles/colors"
import {logout} from '../../redux/actions/logout'
import Popover from 'material-ui/Popover/Popover';
import {Menu, MenuItem} from 'material-ui/Menu';
import FaDesktop from 'react-icons/lib/fa/desktop'
import FaBomb from 'react-icons/lib/fa/bomb';
import FaListAlt from 'react-icons/lib/fa/list-alt'
import FaUser from 'react-icons/lib/fa/user'

const styles = {
  divSideBar: {
    paddingTop: '10px'
  },
  logout: {
    width: '100%',
    position: 'absolute',
    bottom: 0
  },
  iconMin: {
    width: '100%',
  },
  iconButton: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: blueGrey100
  },
  popOver: {
    backgroundColor: blueGrey900,
    width: '150px',

  },
  menu: {
    maxWidth: '100%',
  },
  menuItemPopOver: {
    color: blueGrey100,
    lineHeight: '34px',
    minHeight: '34px',
    fontSize: '14px',
    FontFamily: 'Robot Light'
  },
  menuItemPopOverContent: {
    display: 'flex',
    alignItems: 'center',
    padding: '0px 10px',
  },
  menuItemPopOverIcon: {
    marginRight: '10px',
  },

};


const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
      dispatch(logout())
    }
  }
};

class SideBar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      car: false,
      admin: false,
      anchorOrigin: {
        horizontal: 'right',
        vertical: 'top',
      },
      targetOrigin: {
        horizontal: 'left',
        vertical: 'top',
      },
    };
  }

  handleTouchTap = (event, number) => {
    event.preventDefault();

    switch(number) {
      case 1:
          this.setState({
            car: true,
            anchorEl: event.currentTarget,
          });
          break;
      case 2:
          this.setState({
            admin: true,
            anchorEl: event.currentTarget,
          });
          break;
      default:
         console.log("nothing")
    }
  };

  handleRequestClose = () => {
    this.setState({
      car: false,
      admin: false
    });
  };

  setFocus = (applicationFocused) => {
    this.handleTouchTap();
  };

  handleLogout = () => {
    this.props.logout();
  };




  render() {
    const { user } = this.props;

    const carPark = user.applications.find((application) => application.applicationId === "145")

    return(
      <div>
        <div style={styles.divSideBar}>
          <IconButton style={styles.iconButton} children={<FaAlignJustify size={24}/>} onTouchTap={this.props.handleToggle}/>
          <Link to="/">
            <IconButton style={styles.iconButton} children={<FaHome size={24}/>} onTouchTap={() => this.setFocus("home")}/>
          </Link>
          {carPark &&
          <IconButton style={styles.iconButton} children={<FaAutomobile size={24}/>}  onTouchTap={(event) => this.handleTouchTap(event,1)} />
          }

          <IconButton style={styles.iconButton} children={<FaCogs size={24}  />} onTouchTap={(event) => this.handleTouchTap(event,2)} />

          <div style={styles.logout}>
            <Divider/>
            <IconButton style={styles.iconButton} children={<LogoutIcon size={24}/>} onClick={() => { this.handleLogout() }}/>
          </div>
        </div>

        <Popover
          open={this.state.car}
          anchorEl={this.state.anchorEl}
          anchorOrigin={this.state.anchorOrigin}
          targetOrigin={this.state.targetOrigin}
          onRequestClose={this.handleRequestClose}
          style={styles.popOver}
        >
          <Menu style={styles.menu}>
            <Link to="/parcauto/dashboard" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaDashboard size={16}/></span>}
                primaryText="Dashboard"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/parcauto/cars" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaAutomobile size={16}/></span>}
                primaryText="véhicules"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/parcauto/assignment" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaExchange size={16}/></span>}
                primaryText="Attribution"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/parcauto/repairs" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaWrench size={16}/></span>}
                primaryText="Réparation"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/parcauto/alertesRepairs" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaExclamationTriangle size={16}/></span>}
                primaryText="Alertes"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/parcauto/upcar" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaRoad size={16}/></span>}
                primaryText="UpCar"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/parcauto/retardsUpcar" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaBomb size={16}/></span>}
                primaryText="Retards Upcars"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/parcauto/ecodriving" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaAutomobile size={16}/></span>}
                primaryText="EcoDriving"
                onClick={this.handleRequestClose}
              />
            </Link>
          </Menu>
        </Popover>

        <Popover
          open={this.state.admin}
          anchorEl={this.state.anchorEl}
          anchorOrigin={this.state.anchorOrigin}
          targetOrigin={this.state.targetOrigin}
          onRequestClose={this.handleRequestClose}
          style={styles.popOver}
        >
          <Menu style={styles.menu}>
            <Link to="/admin/dashboard" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaDashboard size={16}/></span>}
                primaryText="Dashboard"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/user" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaUser size={16}/></span>}
                primaryText="utilisateurs"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/admin/application" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaDesktop size={16}/></span>}
                primaryText="Applications"
                onClick={this.handleRequestClose}
              />
            </Link>
            <Link to="/admin/profile" style={{ textDecoration: 'none' }}>
              <MenuItem
                style={styles.menuItemPopOver}
                innerDivStyle={styles.menuItemPopOverContent}
                children={<span style={styles.menuItemPopOverIcon}><FaListAlt size={16}/></span>}
                primaryText="Profiles"
                onClick={this.handleRequestClose}
              />
            </Link>
          </Menu>
        </Popover>
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(SideBar);