import React, { Component } from "react";
import { connect } from "react-redux";
import { Card } from "material-ui/Card";
//import Avatar from "material-ui/Avatar";
import FlatButton from "material-ui/FlatButton";

//const imgprofile = require('../Header/profil.png');
//const img = require('./welcom.png');
export const styles = {
  homepage: {
    padding: "50px 50px 50px 0",
  },
  homepageCard: {
    /*backgroundImage: "url(" + img + ")",*/
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    width: "100%",
    height: "600px",
  },
  gradientCard: {
    width: "100%",
    height: "100%",
    background:
      "linear-gradient(to left, rgba(63, 76, 107, 0.4), rgba(96, 108, 136, 0.4))",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    color: "white",
  },
  welcome: {
    marginTop: "70px",
    fontSize: "52px",
  },
  avatar: {
    border: "solid 4px white",
  },
  username: {
    fontSize: "40px",
  },
  welcomeMessage: {
    textAlign: "center",
  },
  labelButton: {},
};
const mapStateToProps = state => {
  return {
    user: state.auth.user.username,
  };
};

class Home extends Component {
  render() {
    return (
      <div style={styles.homepage}>
        <Card style={styles.homepageCard} containerStyle={styles.gradientCard}>
          <span style={styles.welcome}>Bienvenue</span>
          {/*<Avatar style={styles.avatar} size={100} src={imgprofile} />*/}
          <span style={styles.username}>{this.props.user}</span>
          <div style={styles.welcomeMessage}>
            Cette version du portail UpSell est en cours de developpement et
            peux contenir des bugs. Pour reporter ceux-ci contacter
            hotline@upsell.fr
          </div>
          <a href="https://mail.google.com/" target="_blank">
            <FlatButton
              backgroundColor="white"
              label="Envoyer un mail"
              labelStyle={styles.labelButton}
            />
          </a>
        </Card>
      </div>
    );
  }
}
export default connect(mapStateToProps)(Home);
