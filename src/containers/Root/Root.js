import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Provider} from 'react-redux';
import {Router} from 'react-router';
import routes from '../../routes';


class Root extends Component {
  render () {
    const {store, history} = this.props;

    return (
      <div>
        <Provider store={store}>
          <div>
            <Router history={history} routes={routes} />
          </div>
        </Provider>
      </div>
    )
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

export default Root
