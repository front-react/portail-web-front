import React, { Component } from "react";
import { connect } from "react-redux";
import ReactSideBar from "react-sidebar";
import { SideBar, HeaderContainer } from "../../containers";
import { Loader } from "../../components";
import { ErrorSnackBar } from "../../components/componup";
import styles from "./stylesLayout";
import changeClient from "../../redux/actions/changeClient";
import clearError from "../../redux/actions/clearError";

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    error: state.app.error,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeClient: newClient => {
      dispatch(changeClient(newClient));
    },
    clearError: () => {
      dispatch(clearError());
    },
  };
};

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      openSnackbar: false,
      error: {
        errorMessage: null,
        errorStatus: null,
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error !== null) {
      this.setState({
        error: nextProps.error,
        openSnackbar: true,
      });
    }
  }

  handleRequestClose = () => {
    this.props.clearError();
    this.setState({ openSnackbar: false });
  };

  handleToggle = event => {
    event.preventDefault();
    this.setState({ open: !this.state.open });
  };

  handleFocus = event => {
    event.preventDefault();

    this.setState({ open: !this.state.open });
  };

  changeClient = newClient => {
    this.props.changeClient(newClient);
  };

  _renderSidebarContent = () => {
    let applicationListId = [];
    for (let application in this.props.user.applications) {
      if (application) {
        applicationListId.push(application);
      }
    }

    return (
      <div style={styles.sidebar}>
        <SideBar
          handleToggle={this.handleToggle}
          applicationList={applicationListId}
          user={this.props.user}
        />
      </div>
    );
  };

  _renderSidebarContentMin = () => {
    let applicationListId = [];

    for (let application in this.props.user.applications) {
      if (application) {
        applicationListId.push(application);
      }
    }

    return (
      <div style={styles.sidebarMin}>
        <SideBar
          handleToggle={this.handleToggle}
          applicationList={applicationListId}
          user={this.props.user}
        />
      </div>
    );
  };

  render() {
    const { error, openSnackbar } = this.state;
    if (
      this.props.user.username != null &&
      this.props.user.currentClient != null &&
      this.props.user.applications != null
    ) {
      let sidebarContent = null;
      if (this.state.open === true) {
        sidebarContent = this._renderSidebarContent();
      } else {
        sidebarContent = this._renderSidebarContentMin();
      }

      return (
        <div style={styles.app}>
          <ReactSideBar
            sidebar={sidebarContent}
            docked={true}
            transitions={true}
          >
            <ErrorSnackBar
              open={openSnackbar}
              message={error.errorStatus + "  " + error.errorMessage}
              onRequestClose={this.handleRequestClose}
            />
            <HeaderContainer
              user={this.props.user}
              changeClient={this.changeClient}
              routes={this.props.routes}
              params={this.props.params}
            />
            <div style={styles.body}>{this.props.children}</div>
          </ReactSideBar>
        </div>
      );
    }

    return <Loader />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
