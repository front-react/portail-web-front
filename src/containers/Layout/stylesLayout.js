import {grey200, blueGrey900
} from "material-ui/styles/colors"

export const styles = {
  sidebar: {
    backgroundColor: blueGrey900,
    height: '100%',
    overflow: 'hidden',
    width: '200px',
  },
  sidebarMin: {
    backgroundColor: blueGrey900,
    height: '100%',
    overflow: 'hidden',
    width: '50px',
  },
  app: {
    fontFamily: 'Roboto, sans-serif',

  },
  body: {
    paddingLeft: '20px',
    paddingRight: '20px',
    paddingBottom: '10px',
    paddingTop: '10px',
    backgroundColor: grey200,
    minHeight: '100%',
  },
  snackBar: {
    top: '0',
  },
  snackBarBody: {
    height: '60px',
    backgroundColor: '#F44336',
  },
  actionSnackbar: {
    color: 'black',
  },
  errorMessage: {

  },
};
export default styles;
