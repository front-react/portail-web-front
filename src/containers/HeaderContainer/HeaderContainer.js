import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as routerActions from "react-router-redux";

import MenuItem from "material-ui/MenuItem";

import { Header } from "../../components";

const mapStateToProps = state => {
  return {
    currentLocation: state.app.currentLocation,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    routerActions: bindActionCreators(routerActions, dispatch),
  };
};

class HeaderContainer extends Component {
  handleChange = (event, index, value) => {
    this.props.changeClient(value);
  };

  prevLocation = () => {
    this.props.routerActions.goBack();
  };

  renderMenuItems = listClient => {
    let MenuItems = [];
    for (let client in listClient) {
      if (client) {
        MenuItems.push(
          <MenuItem
            key={listClient[client].clientId}
            value={listClient[client].clientId}
            primaryText={listClient[client].clientName}
          />,
        );
      }
    }

    return MenuItems;
  };

  render() {
    const { routes, user } = this.props;
    const { clients, username, currentClient } = user;
    let location = routes[routes.length - 1].name;
    const menuItems = this.renderMenuItems(clients);

    return (
      <Header
        clients={clients}
        username={username}
        currentClient={currentClient}
        location={location}
        menuItems={menuItems}
        prevLocation={this.prevLocation}
        handleChange={this.handleChange}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);
