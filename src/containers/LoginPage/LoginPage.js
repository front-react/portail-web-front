import React, {Component} from 'react'
import {connect} from 'react-redux'
import LoginForm from '../../components/LoginForm/LoginForm'
import login from '../../redux/actions/login'

const mapStateToProps = (state) => {
  return {errorMessage: state.auth.errorMessage}
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginAction: formProps => {
      dispatch(login(formProps))
    }
  }
};

class LoginPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      message: null,
      open: false,
    };
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.errorMessage !== null){
      this.setState({
        message: nextProps.errorMessage,
        open: true,
      })
    }
  }

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };


  handleFormSubmit = (formProps) => {
    this.props.loginAction(formProps);
  };

  render() {
    return <LoginForm
      open={this.state.open}
      message={this.state.message}
      handleRequestClose={this.handleRequestClose}
      handleFormSubmit={this.handleFormSubmit}
    />
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)