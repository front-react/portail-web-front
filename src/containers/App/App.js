import React, {Component} from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {orange500, orange600, grey900} from 'material-ui/styles/colors';
import injectTapEventPlugin from 'react-tap-event-plugin'


const muiTheme = getMuiTheme({
  fontFamily: 'Roboto Light, sans-serif',
  fontWeight: '200',
  palette: {
    primary1Color: orange500,
    primary2Color: orange600,
    textColor: grey900
  }
});
const style = {
  background: 'red'
};

class App extends Component {

  componentDidMount() {
    injectTapEventPlugin();
  }
  render() {

    return (
      <MuiThemeProvider style={style} muiTheme={muiTheme}>
        {this.props.children}
      </MuiThemeProvider>
    )
  }
}

export default App;