import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import authReducer from './authReducer';
import appReducer from './appReducer';
import carParkReducer from '../../modules/carPark/redux/reducers';
import rhReducer from '../../modules/rh/redux/reducers';
import adminReducer from '../../modules/admin/redux/reducers';

const rootReducer = combineReducers({
  app: appReducer,
  routing: routerReducer,
  form: formReducer,
  auth: authReducer,
  carPark: carParkReducer,
  rh: rhReducer,
  admin: adminReducer,
});

export default rootReducer
