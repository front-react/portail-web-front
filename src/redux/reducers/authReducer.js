import {
  AUTH_REQUEST,
  AUTH_SUCCESS,
  AUTH_ERROR,
  AUTH_LOGOUT,
  AUTH_GET_TOKEN_SUCCESS,
  AUTH_GET_TOKEN_ERROR,
  AUTH_GET_IDENTITY_SUCCESS,
  AUTH_GET_IDENTITY_ERROR,
  AUTH_GET_APPLICATION_SUCCESS,
  AUTH_GET_APPLICATION_ERROR,
  AUTH_GET_PRIVILEGE_SUCCESS,
  AUTH_GET_PRIVILEGE_ERROR,
  AUTH_GET_CLIENT_SUCCESS,
  AUTH_GET_CLIENT_ERROR,
  CHANGE_CLIENT,
} from '../types'

const initialState = {
  authenticated: false,
  user: {
    username: null,
    applications: null,
    privileges: null,
    clients: null,
    currentClient: null,
  },
  errorMessage: null,
};

export default (state = initialState, action) => {
  switch (action.type) {

    case AUTH_REQUEST:
      return {
        ...state,
      };

    case AUTH_SUCCESS:
      return {
        ...state,
        authenticated: true,
      };

    case AUTH_LOGOUT:
      return {
        ...state,
        authenticated: false,
        user: null
      };

    case AUTH_GET_TOKEN_SUCCESS:
      return {
        ...state,
      };

    case AUTH_ERROR:
      return {
        ...state,
      };

    case AUTH_GET_TOKEN_ERROR:
      return {
        ...state,
        authenticated: false,
        errorMessage: action.error
      };

    case AUTH_GET_IDENTITY_SUCCESS:

      return {
        ...state,
        user: {
          ...state.user,
          ...action.data,
        }
      };

    case AUTH_GET_IDENTITY_ERROR:
      return {
        ...state,
        authenticated: false,
        errorMessage: action.error
      };

    case AUTH_GET_APPLICATION_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          applications: action.data
        }
      };

    case AUTH_GET_APPLICATION_ERROR:
      return {
        ...state,
        authenticated: false,
        errorMessage: action.error
      };

    case AUTH_GET_PRIVILEGE_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          privileges: action.data
        }
      };

    case AUTH_GET_PRIVILEGE_ERROR:
      return {
        ...state,
        authenticated: false,
        errorMessage: action.error
      };

    case AUTH_GET_CLIENT_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          clients: action.data,
          currentClient: action.defaultClient
        }
      };

    case AUTH_GET_CLIENT_ERROR:
      return {
        ...state,
        authenticated: false,
        errorMessage: action.error
      };

    case CHANGE_CLIENT:
      return {
        ...state,
        user: {
          ...state.user,
          currentClient: action.data
        }
      };

    default:
      return state;
  }
}