import { LOCATION_CHANGE } from 'react-router-redux';
import { DISPLAY_ERROR, CLEAR_ERROR } from '../types';

const initial = {
  currentLocation: null,
  error: null,
};

function app(state = initial, action) {
  switch (action.type) {

    case LOCATION_CHANGE:
      return {
        ...state,
        currentLocation: action.payload.pathname
      };

    case DISPLAY_ERROR:
      return {
        ...state,
        error: action.error
      };

    case CLEAR_ERROR:
      return {
        ...state,
        error: null
      };

    default:
      return state;
  }
}

export default app;