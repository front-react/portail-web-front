import {
  CHANGE_CLIENT,
} from '../types';

export const changeClient = (idNewClient) => {

  return (dispatch, getState) => {
    const newClient = getState().auth.user.clients[idNewClient];

    return dispatch({
      type: CHANGE_CLIENT,
      data: newClient
    })
  };
}

export default changeClient;