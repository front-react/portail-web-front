import { AUTH_SUCCESS } from '../types';
import { setAuthorization } from '../../utils/api/providers';
import { Local } from 'OhMyCache';
import logout from './logout';
import requestRefreshToken from './requestRefreshToken';
import getMyAuthorization from './authorization';



/**
 * Initialise token infos from Storage
 */
/*eslint-disable consistent-return */

export const initAuth = () => {
  return dispatch => {
    const jwt = Local.get('auth-jwt');
    const refreshToken = Local.get('auth-refresh-token');

    if (jwt == null) {
      if (refreshToken != null) {
        dispatch(requestRefreshToken(refreshToken));
      } else{
        dispatch(logout());
      }

    } else {
      setAuthorization(jwt);
      dispatch(getMyAuthorization)
        .then(
          dispatch({
            type: AUTH_SUCCESS,
          }),
        )
    }
  }
};

export default initAuth