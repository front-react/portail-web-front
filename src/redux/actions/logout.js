import { AUTH_LOGOUT } from '../types';
import { Local } from 'OhMyCache';
import { browserHistory } from 'react-router';
import { unSetAuthorization } from '../../utils/api/providers';
/**
 * Logout user and clear token infos
 */
export const logout = () => {
  return dispatch => {
    Local.remove('auth-jwt');
    Local.remove('auth-refresh-token');
    unSetAuthorization();
    dispatch({type: AUTH_LOGOUT});
    browserHistory.push('/login');
  }
};

export default logout;