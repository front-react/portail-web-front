import { AUTH_REQUEST } from '../types';
import getToken from './token';

const authRequest = () => ({ type: AUTH_REQUEST });

export const login = ({username, password}) => {
  return dispatch => {
    console.log('%c login > dispatch', 'color:#000; background:#fff7af; padding:2px;');
    dispatch(authRequest());
    // get token with login informations
    return dispatch(getToken(username, password));
  }
};

export default login;