import {
  AUTH_GET_IDENTITY_SUCCESS,
  AUTH_GET_IDENTITY_ERROR,
  AUTH_GET_APPLICATION_SUCCESS,
  AUTH_GET_APPLICATION_ERROR,
  AUTH_GET_PRIVILEGE_SUCCESS,
  AUTH_GET_PRIVILEGE_ERROR,
  AUTH_GET_CLIENT_SUCCESS,
  AUTH_GET_CLIENT_ERROR,
  AUTH_SUCCESS
} from '../types';
import {authorizationApiProvider} from '../../utils/api/providers';
import displayError from './displayError';

const authSuccess = () => ({ type: AUTH_SUCCESS });

const getMyIdentity = () => {
  return dispatch => {

    return authorizationApiProvider.get('/me')
      .then(
        res => {
          dispatch(authSuccess());
          const userInformation = res.data.data;

          dispatch({
            type: AUTH_GET_IDENTITY_SUCCESS,
            data: userInformation
          });
        },
        err => {
          dispatch(displayError(err));
          dispatch({
            type: AUTH_GET_IDENTITY_ERROR,
            err
          });
        }
      )
  }
};

const getMyApplication = () => {
  return dispatch => {

    return authorizationApiProvider.get('/me/applications')
      .then(
        res => {
          const listApplication = res.data.data;
          dispatch({
            type: AUTH_GET_APPLICATION_SUCCESS,
            data: listApplication
          });
        },
        err => {
          dispatch(displayError(err));
          dispatch({
            type: AUTH_GET_APPLICATION_ERROR,
            err
          });
        }
      )
  }
};

const getMyPrivilege = () => {
  return dispatch => {

    return authorizationApiProvider.get('/me/privileges')
      .then(
        res => {
          const listPrivilege = res.data.data;
          dispatch({
            type: AUTH_GET_PRIVILEGE_SUCCESS,
            data: listPrivilege
          });
        },
        err => {
          dispatch(displayError(err));
          dispatch({
            type: AUTH_GET_PRIVILEGE_ERROR,
            err
          });
        }
      )
  }
};

const getMyClient = () => {
  return dispatch => {

    return authorizationApiProvider.get('/me/clients')
      .then(
        res => {
          const listClient = res.data.data;
          let defaultClient = listClient[Object.keys(listClient)[0]];
          for(let idClient in listClient){
            if(listClient[idClient].idDefault === 1){
              defaultClient = listClient[idClient];
            }
          }
          dispatch({
            type: AUTH_GET_CLIENT_SUCCESS,
            data: listClient,
            defaultClient
          });
        },
        err => {
          dispatch(displayError(err));
          dispatch({
            type: AUTH_GET_CLIENT_ERROR,
            err
          });
        }
      )
  }
};


const getMyAuthorization = () => {
  return dispatch => Promise.all([
    dispatch(getMyIdentity()),
    dispatch(getMyApplication()),
    dispatch(getMyPrivilege()),
    dispatch(getMyClient())
  ]);
};

export default getMyAuthorization();