import { AUTH_LOGIN, AUTH_ERROR } from '../types';
import { authApiProvider, setAuthorization } from '../../utils/api/providers';
import { Local } from 'OhMyCache';
import jwtDecode from 'jwt-decode';

const initialConfig = {
  client_id: 'upsell-web-front',
  client_secret: 'R8ZeBpIULqfi7E2e'
};

export const errorHandler = (dispatch, type, errorMessage, status) => {
  dispatch({
    type,
    errorMessage,
    status
  });
};

/**
 * Request a token with a refresh_token
 */
const requestRefreshToken = (refreshToken) => {
  return dispatch => {
    let loginInfos = {
      refresh_token: refreshToken,
      grant_type: 'refresh_token'
    };

    let params = Object.assign({}, initialConfig, loginInfos);

    authApiProvider.post('token', params)
      .then(response => {
        const {data, status} = response;
        if(data.error != null) {
          errorHandler(dispatch, AUTH_ERROR, data.error_description, status);
        }
        else{
          const {access_token, refresh_token, expires_in} = data;

          Local.set('auth-jwt',access_token, {expire: expires_in});

          Local.remove('auth-refresh-token');
          if(refresh_token != null) {
            Local.set('auth-refresh-token',refresh_token, {expire: 604800}); //7 days
          }

          const jwtDecoded = jwtDecode(access_token);

          const user = {
            username: jwtDecoded.sub,
            id: jwtDecoded.sub
          };

          dispatch({
            type: AUTH_LOGIN,
            user
          });

          setAuthorization(access_token);

          //browserHistory.push('/');
        }
      })
      .catch(response => {
        const {status, statusText} = response;
        errorHandler(dispatch, AUTH_ERROR, statusText, status);
      });

  }
};



export default requestRefreshToken;