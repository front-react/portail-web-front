import {
  AUTH_GET_TOKEN_SUCCESS,
  AUTH_GET_TOKEN_ERROR
} from '../types';
import {authApiProvider, setAuthorization} from '../../utils/api/providers';
import getMyAuthorization from './authorization';
import { Local } from 'OhMyCache';
import { browserHistory } from 'react-router';

const setToken = (data) => {
  // if login informations ok
  if (!data.error) {
    const {access_token, refresh_token, expires_in} = data;
    Local.set('auth-jwt',access_token, {expire: expires_in});
    Local.remove('auth-refresh-token');
    setAuthorization(access_token);

    if(refresh_token != null) {
      Local.set('auth-refresh-token',refresh_token, {expire: 604800}); //7 days
    }
  }
};

const getTokenError = (error) => ({ type: AUTH_GET_TOKEN_ERROR, error });
const getTokenSuccess = () => ({ type: AUTH_GET_TOKEN_SUCCESS });

export const getToken = (username, password) => {
  let loginInfos = {
    username,
    password,
    grant_type: 'password'
  };

  const initialConfig = {
    client_id: 'portail-web',
    client_secret: 'R8ZeBpIULqfi7E2e',
    headers: {
      'Accept': 'application/text',
      'Content-Type': 'application/text',
      'Authorization' : '8456123',
    }
  };

  const params = Object.assign({}, initialConfig, loginInfos);

  return dispatch => {
    return authApiProvider.post('token', params)
      .then(
        res => {
          const {data} = res;

          return Promise.resolve(setToken(data)).then(
            () => {
              dispatch(getTokenSuccess());
              // if token is ok with login informations, get user authorization
              dispatch(getMyAuthorization)
                .then(() => {
                    browserHistory.push('/');
                  }
              )
            }
          )
        },
        err => {
          dispatch(getTokenError(err || 'unknow error'));
          return err;
        }
      )
  }
};

export default getToken;