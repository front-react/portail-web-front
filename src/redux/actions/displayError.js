import {
  DISPLAY_ERROR,
} from '../types';

export const displayError = (error) => {
  const errorMessage = error.response.data.error.detail || error.response.data.message;
  const errorStatus = error.response.status;

  return dispatch => {
    return dispatch({
      type: DISPLAY_ERROR,
      error: {
        errorMessage,
        errorStatus,
      },
    })
  };
};

export default displayError;