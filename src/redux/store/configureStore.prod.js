import {createStore, applyMiddleware} from 'redux'
import reducers from '../reducers'
import reduxThunk from 'redux-thunk'
import {browserHistory} from 'react-router'
import {routerMiddleware} from 'react-router-redux'

/**
 * Configure dev store
 */
export default function configureStore(initialState) {

  let reactRouterRedux = routerMiddleware(browserHistory);
  let middleware = [reduxThunk, reactRouterRedux];
  const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

  const store = createStoreWithMiddleware(
    reducers,
    initialState,
  );

  return store;
}
