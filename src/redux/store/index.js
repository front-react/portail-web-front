import config from '../../config'

if(config.env === 'dev'){
  module.exports = require('./configureStore.dev')
}
else if(config.env === 'test'){
  module.exports = require('./configureStore.dev')
}
else if(config.env === 'stage'){
  module.exports = require('./configureStore.dev')
}
else if(config.env === 'production'){
  module.exports = require('./configureStore.prod')
}
