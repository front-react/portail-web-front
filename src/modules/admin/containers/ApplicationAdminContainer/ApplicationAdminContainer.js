import React, {Component} from 'react'
import {connect} from 'react-redux'
import {groupBy} from 'services/data/groupBy'
import ApplicationAdmin from 'components/admin/application/presentational/ApplicationAdmin'


const mapStatetoprops = (state) => {
  return {
    application: state.admin.application,
    role: state.admin.role,
    privilege: state.admin.privilege,
  }
}
@connect(mapStatetoprops)
export default class ApplicationAdminContainer extends Component {

  render() {

    const application = this.props.application.currentApplication
    const privilegeList = this.props.privilege.data
    const roleList = this.props.role.data
    const metaField = {
      idPrivilege: [],
      nomPrivilege: [],
    }
    const privilegeGroupedByType = groupBy(privilegeList, 'idTypePrivilege', 'typePrivilege', metaField)

    return <ApplicationAdmin
      privilegeGroupedByType={privilegeGroupedByType}
      roleList={roleList}
      application={application}/>
  }
}