import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import getProfile from '../../redux/actions/profile/getProfile'
import Profile from '../../components/Profile/Profile'




const mapStateToProps = (state) => {
  return {
    profile: state.admin.profile.profileSelect,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProfile: (id, filter) => { dispatch(getProfile(id, filter)) },
  }
};

class ProfileContainer extends Component {

  componentWillMount() {
    const idProfile = this.props.params.id
    this.props.getProfile(idProfile);
  }

  render() {
    const { profile } = this.props;
    const idProfile = this.props.params.id

   if(profile !== null){
      return(
        <div>
          <Profile 
          profile={profile}
          profileId={idProfile}
          />
        </div>
      );
    }
    return <Loader />
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer)