import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {ApplicationsRolesList} from '../../components'
import getRoleList from '../../redux/actions/role/getRoleList'


const mapStateToProps = (state) => {
  return {
    roleList: state.admin.role.roleList
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRoleList: (filter) => { dispatch(getRoleList(filter))},
  };
};

class ApplicationsRolesListContainer extends Component {


  componentDidMount() {
    const idApplication = this.props.params.id;
    this.props.getRoleList('_filter(applicationId-%3D)='+idApplication);
  }



  render() {
   const roleList = this.props.roleList;
    
    if (roleList != null) {
      return <ApplicationsRolesList 
        roleList={roleList}
        NameApplication={this.props.params.name} />
    }

    return (<Loader />)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationsRolesListContainer);