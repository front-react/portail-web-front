import React, { Component } from "react";
import { connect } from "react-redux";
import { FormCreateApplication } from "../../components";
import { createApplication } from "../../redux/actions/application/createApplication";

const mapDispatchToProps = dispatch => {
  return {
    createApplication: RequestBody => {
      dispatch(createApplication(RequestBody));
    },
  };
};

class CreateApplication extends Component {
  handleFormSubmit = formData => {
    this.props.createApplication(formData);
  };

  render() {
    return <FormCreateApplication handleFormSubmit={this.handleFormSubmit} />;
  }
}
export default connect(null, mapDispatchToProps)(CreateApplication);
