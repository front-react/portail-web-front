import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import getApplicationUsersList from '../../redux/actions/user/getApplicationUsersList'
import {ApplicationsUsersList} from '../../components'

const mapStateToProps = (state) => {
  return {
    ApplicationUsersList: state.admin.user.ApplicationUsersList
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getApplicationUsersList: (id) => { dispatch(getApplicationUsersList(id))},
  };
};

class ApplicationUsersListContainer extends Component {

  componentDidMount() {
    const idApplication = this.props.params.id;
    this.props.getApplicationUsersList(idApplication);
  }

  render() {

   const {ApplicationUsersList} = this.props;
    
    if (ApplicationUsersList != null) {
      return (<ApplicationsUsersList UsersList={ApplicationUsersList} />);
    }
    return (<Loader />)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationUsersListContainer);