import React, {Component} from 'react'
import {connect} from 'react-redux'
//import {Loader} from '../../../../components'
import {CreatePrivilege} from '../../components'
import {createPrivilege} from '../../redux/actions/privilege/createPrivilege'


const mapDispatchToProps = (dispatch) => {
    return {
        createPrivilege: (RequestBody, applicationId) => {dispatch(createPrivilege(RequestBody, applicationId))
      }
    }
  };

class CreatePrivilegeContainer extends Component {

    state = {
        open: false,
      };
    
      handleOpen = () => {
        this.setState({open: true});
      };
    
      handleClose = () => {
        this.setState({open: false});
      };
    
      handleFormSubmit = (requestBody) => {

        const formData = {}
        formData.privilegeName = requestBody.privilegeName
        formData.applicationId = this.props.idApplication
        formData.privilegeTypeId = requestBody.privilegeTypeId
        this.props.createPrivilege(formData, this.props.idApplication)
      }

  render() {   


      return <CreatePrivilege
        handleFormSubmit={this.handleFormSubmit} 
        createDialogStatus={this.state.open} 
        handleClose={this.handleClose} 
        handleOpen={this.handleOpen} />

  }
};

export default connect(null, mapDispatchToProps)(CreatePrivilegeContainer);