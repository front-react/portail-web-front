import React, {Component} from 'react'
import {connect} from 'react-redux'
import {CreateUser} from '../../components'
import createUser from '../../redux/actions/user/createUser'
import getPersonList from '../../../rh/redux/actions/person/getPersonList';

const mapStateToProps = (state) => {
  return {
    personList: state.rh.person.personList,
    currentUser: state.admin.user.currentUser
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    createUser: (RequestBody) => { dispatch(createUser(RequestBody)) },
    getPersonList: (filter) => {dispatch(getPersonList(filter))},
  }
}

class CreateUserContainer extends Component {

  componentDidMount(){
    this.props.getPersonList()
  }

  state = {
    person: '',
    personId: null,
    openSnackbar: true,
    password: ''
  }

  handleFormSubmit = (formData) => {

    formData.personId = this.state.personId
    formData.username = this.state.person
    formData.password = this.state.password
    
    this.props.createUser(formData)
  }
  

  searchPerson = (personName) => {
    this.props.getPersonList(`_filter(personFullName-like)=%25${personName}%25&_limit=5`);
  }
  onChangePassword = (event, newValue) => {
    this.setState({
      password: newValue
    })
  }

  selectPerson = (value) => {
    let lowerCaseValue = value.personFullName.toLowerCase()
    let replacePoint = lowerCaseValue.replace(" ", ".")
    let replacetrait = replacePoint.replace("-", ".")
    this.setState({
      person: replacetrait,
      personId: value.personId
    })
  }

  handleRequestClose = () => {
    this.setState({openSnackbar: false});
  };

  handleAfterSubmit = (dialogueEvent) => {
    if(dialogueEvent === 'next'){
      this.props.handleNext()
    }
    else if(dialogueEvent === 'close'){
      this.props.handleClose()
    }
  }

  render(){
    const { personList, currentUser } = this.props

    return(
      <CreateUser 
        handleFormSubmit={this.handleFormSubmit}
        searchPerson={this.searchPerson}
        person={this.state.person}
        password={this.state.password}
        personList={personList}
        selectPerson={this.selectPerson}
        currentUser={currentUser}
        handleAfterSubmit={this.handleAfterSubmit}
        onChangePassword={this.onChangePassword}
      />
    );
    
  }
}

export default  connect(mapStateToProps, mapDispatchToProps)(CreateUserContainer);




