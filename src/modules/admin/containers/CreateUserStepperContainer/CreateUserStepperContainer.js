import React, { Component } from "react";
import { Step, Stepper, StepLabel } from "material-ui/Stepper";
import Dialog from "material-ui/Dialog";
import RaisedButton from "material-ui/RaisedButton";
import {
  CreateUserContainer,
  AddUserProfileClientContainer,
} from "../../containers";

const styles = {
  dialogue: {
    width: "60%",
    maxWidth: "none",
  },
  titleDialogue: {
    border: "none",
  },
};
const steps = ["Generale", "Profil & Clients", "Management", "GPS", "Stats"];

class CreateUserStepperContainer extends Component {
  state = {
    open: false,
    stepIndex: 0,
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
    this.setState({ stepIndex: 0 });
  };

  handleNext = () => {
    const stepIndex = this.state.stepIndex;

    this.setState({
      stepIndex: stepIndex + 1,
    });
  };

  getStepContent = stepIndex => {
    switch (stepIndex) {
      case 0:
        return (
          <CreateUserContainer
            handleNext={this.handleNext}
            handleClose={this.handleClose}
          />
        );
      case 1:
        return (
          <AddUserProfileClientContainer
            handleNext={this.handleNext}
            handleClose={this.handleClose}
          />
        );
      case 2:
        return (
          <CreateUserContainer
            handleNext={this.handleNext}
            handleClose={this.handleClose}
          />
        );
      case 3:
        return "WIP";
      case 4:
        return "WIP";
      default:
        return "Oups, ça a bugger";
    }
  };

  render() {
    const { stepIndex } = this.state;
    const contentStyle = { margin: "0 16px" };

    return (
      <div>
        <RaisedButton
          label="Ajouter un utilisateur"
          onTouchTap={this.handleOpen}
        />
        <Dialog
          title="Ajouter un utilisateur"
          titleStyle={styles.titleDialogue}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <div>
            <Stepper activeStep={stepIndex}>
              {steps.map((step, index) => (
                <Step key={index}>
                  <StepLabel>{step}</StepLabel>
                </Step>
              ))}
            </Stepper>
            <div style={contentStyle}>
              <span>{this.getStepContent(stepIndex)}</span>
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default CreateUserStepperContainer;
