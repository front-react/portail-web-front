import React, {Component} from 'react'
import {connect} from 'react-redux'
import {UserProfile} from '../../components'
import {Loader} from '../../../../components'
import getUserInformation from '../../redux/actions/user/getUserInformation'
import refreshUser from '../../redux/actions/user/refreshUser'


const mapStateToProps = (state) => {
  return {
    currentUser: state.admin.user.currentUser
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUserInformation: (id) => { dispatch(getUserInformation(id)) },
    refreshUser: () => { dispatch(refreshUser()) },
  }
};

class UserProfileContainer extends Component {

  componentDidMount(){
    const userId = this.props.params.id;
    this.props.getUserInformation(userId)
  }

  componentWillUnmount() {
    this.props.refreshUser()
  }

  render() {
    const {currentUser} = this.props;
    if(currentUser !== null){
      return <UserProfile userInformation={currentUser}/>
    }
    return <Loader />
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileContainer);