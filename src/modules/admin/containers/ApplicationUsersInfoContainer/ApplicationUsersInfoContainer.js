import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {ApplicationUsersInfo} from '../../components'
import getCountUsersApplication from '../../redux/actions/application/getCountUsersApplication'


const mapStateToProps = (state) => {
  return {
    countUsersApplication: state.admin.application.countUsersApplication
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCountUsersApplication: (id) => { dispatch(getCountUsersApplication(id))},
  };
};

class ApplicationUsersInfoContainer extends Component {

  componentDidMount() {
    const idApplication = this.props.idApplication;
    this.props.getCountUsersApplication(idApplication);
  }



  render() {
   const {countUsersApplication, idApplication} = this.props;
    
    if (countUsersApplication != null) {
      return <ApplicationUsersInfo countUsersApplication={countUsersApplication} idApplication={idApplication} />
    }

    return (<Loader />)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationUsersInfoContainer);