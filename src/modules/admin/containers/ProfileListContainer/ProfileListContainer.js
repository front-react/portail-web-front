import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import getProfileList from '../../redux/actions/profile/getProfileList'

import { ProfilesList } from '../../components'

const mapStateToProps = (state) => {
  return {
    profileList: state.admin.profile.profileList,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProfileList: (filter) => { dispatch(getProfileList(filter)) }
  }
};

class ProfilesListContainer extends Component {

  componentWillMount() {
    this.props.getProfileList()
  }

  filterProfileList = (filterValue, field) => {
    this.props.getProfileList('_filter('+field+'-like)=%25'+filterValue+'%25')
  };


  render() {
    
    const {profileList} = this.props;

    if(profileList !== null) {
      return (
        <ProfilesList 
          profileList={profileList} 
          filterProfileList={this.filterProfileList}
         />
      );
    }
    return <Loader />
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilesListContainer)