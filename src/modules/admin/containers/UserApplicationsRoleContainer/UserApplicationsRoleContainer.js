import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {UserApplicationsRole} from '../../components'
import getUserApplicationRole from '../../redux/actions/application/getUserApplicationRole'


const mapStateToProps = (state) => {
  return {
    userApplicationRole: state.admin.application.userApplicationRole
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUserApplicationRole: (id) => { dispatch(getUserApplicationRole(id))},
  };
};

class UserApplicationsRoleContainer extends Component {

  componentDidMount() {
    const idUser = this.props.userId
    this.props.getUserApplicationRole(idUser);
  }

  render() {

   const {userApplicationRole} = this.props;
    
    if (userApplicationRole != null) {
      return <UserApplicationsRole userApplicationRole={userApplicationRole} />
    }

    return (<Loader />)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserApplicationsRoleContainer);