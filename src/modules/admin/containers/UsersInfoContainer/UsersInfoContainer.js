import React, { Component } from "react";
import { connect } from "react-redux";
import { withLoader } from "../../../../components/Hoc";
import { UsersInfo } from "../../components";
import getCountUsers from "../../redux/actions/application/getCountUsers";

const mapStateToProps = state => {
  return {
    countUsers: state.admin.application.countUsers,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCountUsers: () => {
      dispatch(getCountUsers());
    },
  };
};

class UsersInfoContainer extends Component {
  componentDidMount() {
    this.props.getCountUsers();
  }

  render() {
    const { countUsers } = this.props;
    const isLoading = countUsers == null;
    const UsersInfoWithLoading = withLoader(UsersInfo);

    return (
      <UsersInfoWithLoading isLoading={isLoading} countUsers={countUsers} />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersInfoContainer);
