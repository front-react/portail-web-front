import React, {Component} from 'react'
import {connect} from 'react-redux'
import getApplicationList from '../../redux/actions/application/getApplicationList'
import {Loader} from '../../../../components'
import {ApplicationList} from '../../components'


const mapStateToProps = (state) => {
  return {
    applicationList: state.admin.application.applicationList,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getApplicationList: (filter) => { dispatch(getApplicationList(filter)) },
  }
};

class ApplicationListContainer extends Component {

  componentDidMount() {
    this.props.getApplicationList();
  }

  filterApplicationList = (filterValue, field) => {
    this.props.getApplicationList('_filter('+field+'-like)=%25'+filterValue+'%25')
  };

  render() {
    const {applicationList} = this.props;
      
    if(applicationList !== null){
      return (
        <div>
          <ApplicationList 
            applicationList={applicationList}
            filterApplicationList={this.filterApplicationList} />
        </div>
      )
    }
    return <Loader />

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ApplicationListContainer)

/*
 <ApplicationListHeader />
 <ApplicationList applicationList={applicationList} roleList={roleList} />

 */