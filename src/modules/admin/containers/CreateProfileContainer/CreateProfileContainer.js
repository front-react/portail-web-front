import React, {Component} from 'react'
import {connect} from 'react-redux'
import {CreateProfile} from '../../components'
import {createProfile} from '../../redux/actions/profile/createProfile'


const mapDispatchToProps = (dispatch) => {
  return {
    createProfile: RequestBody => {
      dispatch(createProfile(RequestBody))
    }
  }
}


class CreateProfileContainer extends Component {

  handleFormSubmit = (formData) => {
    this.props.createProfile(formData)
  }

  render(){

    return(
      <CreateProfile handleFormSubmit={this.handleFormSubmit}/>
    )
  }
}

export default connect(null, mapDispatchToProps)(CreateProfileContainer)
