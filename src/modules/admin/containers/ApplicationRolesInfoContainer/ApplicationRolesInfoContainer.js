import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {ApplicationRolesInfo} from '../../components'
import getCountRolesApplication from '../../redux/actions/application/getCountRolesApplication'


const mapStateToProps = (state) => {
  return {
    countRolesApplication: state.admin.application.countRolesApplication
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCountRolesApplication: (id) => { dispatch(getCountRolesApplication(id))},
  };
};

class ApplicationRolesInfoContainer extends Component {

  componentDidMount() {
    const idApplication = this.props.idApplication;
    this.props.getCountRolesApplication(idApplication);
  }



  render() {
   const {countRolesApplication, idApplication} = this.props;
    
    if (countRolesApplication != null) {
      return <ApplicationRolesInfo countRolesApplication={countRolesApplication} idApplication={idApplication} applicationName={this.props.applicationName}  />
    }

    return (<Loader />)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationRolesInfoContainer);