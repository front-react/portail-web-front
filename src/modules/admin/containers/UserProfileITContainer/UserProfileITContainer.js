import React, { Component } from "react";
import { UserProfileIT } from "../../components";
import { Loader } from "../../../../components";
import { connect } from "react-redux";
import getUserInformation from "../../redux/actions/user/getUserInformation";
import setDefaultClient from "../../redux/actions/user/setDefaultClient";
import setClient from "../../redux/actions/user/setClient";
//import setClientDefault from "../../redux/actions/user/setClientDefault";

const mapStateToProps = state => {
  return {
    currentUser: state.admin.user.currentUser,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUserInformation: id => {
      dispatch(getUserInformation(id));
    },
    setDefaultClient: (userId, clientId) => {
      dispatch(setDefaultClient(userId, clientId));
    },
    setClient: (userId, bodyData) => {
      dispatch(setClient(userId, bodyData));
    },
  };
};

class UserProfileITContainer extends Component {
  componentDidMount() {
    const id = this.props.userId;
    this.props.getUserInformation(id);
  }

  toggleSetClientDefault = (clientId, profileId, isDefault) => {
    const userId = this.props.currentUser.userId;

    this.props.setDefaultClient(userId, clientId);
  };

  toggleRemoveSetClient = (clientId, profileId, isDefault) => {
    const formData = {};

    formData.clientId = clientId;
    formData.profileId = profileId;
    formData.isDefault = isDefault;

    const userId = this.props.currentUser.userId;

    this.props.setClient(userId, formData);
  };

  render() {
    const { currentUser } = this.props;

    if (currentUser !== null) {
      return (
        <UserProfileIT
          userInformation={currentUser}
          toggleSetClientDefault={this.toggleSetClientDefault}
          toggleRemoveSetClient={this.toggleRemoveSetClient}
        />
      );
    }

    return <Loader />;
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(
  UserProfileITContainer,
);
