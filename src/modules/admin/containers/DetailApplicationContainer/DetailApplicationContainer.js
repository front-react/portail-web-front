import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {DetailApplication} from '../../components'
import getApplication from '../../redux/actions/application/getApplication'


const mapStateToProps = (state) => {
  return {
    currentApplication: state.admin.application.currentApplication
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getApplication: (id) => { dispatch(getApplication(id)) }
  };
};

class DetailApplicationContainer extends Component {

  componentDidMount() {
    const idApplication = this.props.params.id;
    this.props.getApplication(idApplication);
  }

  refreshApplication = (applicationId) => {
    this.props.getApplication(applicationId);
  }

  render() {
    const {currentApplication} = this.props;

    
    if (currentApplication != null) {
      return <DetailApplication 
                application={currentApplication}   
              />
    }
    return <Loader />
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailApplicationContainer);