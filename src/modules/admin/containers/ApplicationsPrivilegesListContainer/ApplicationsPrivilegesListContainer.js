import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {ApplicationsPrivilegesList} from '../../components'
import getPrivilegeList from '../../redux/actions/privilege/getPrivilegeList'


const mapStateToProps = (state) => {
  return {
    privilegesList: state.admin.privilege.privilegesList
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPrivilegeList: (filter) => { dispatch(getPrivilegeList(filter))},
  };
};

class ApplicationsPrivilegesListContainer extends Component {


  componentDidMount() {
    const idApplication = this.props.params.id;
    this.props.getPrivilegeList(`_filter(applicationId-%3D)=${idApplication}&_limit=100000`);
  }

  render() {
   const ApplicationPrivilegesList = this.props.privilegesList;
    
    if (ApplicationPrivilegesList != null) {
      return <ApplicationsPrivilegesList 
        ApplicationPrivilegesList={ApplicationPrivilegesList}  
        />
    }

    return (<Loader />)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationsPrivilegesListContainer);