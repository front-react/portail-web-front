import React, { Component } from "react";
import { connect } from "react-redux";
import { AdminDashBoard } from "../../components";
import { Loader } from "../../../../components";
import getUsersList from "../../redux/actions/user/getUserList";
import getProfileList from "../../redux/actions/profile/getProfileList";
import getApplicationList from "../../redux/actions/application/getApplicationList";

const mapStateToProps = state => {
  return {
    userList: state.admin.user.userList,
    profileList: state.admin.profile.profileList,
    applicationList: state.admin.application.applicationList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUsersList: filter => {
      dispatch(getUsersList(filter));
    },
    getProfileList: filter => {
      dispatch(getProfileList(filter));
    },
    getApplicationList: filter => {
      dispatch(getApplicationList(filter));
    },
  };
};

class AdminDashBoardContainer extends Component {
  state = {
    userSelected: null,
    profileSelected: null,
    applicationSelected: null,
  };

  componentDidMount() {
    this.props.getUsersList("_limit=5");
    this.props.getProfileList("_limit=5");
    this.props.getApplicationList("_limit=5");
  }

  searchUser = userId => {
    this.props.getUsersList(
      "_filter(username-like)=%25" + userId + "%25&_limit=5",
    );
  };

  selectUser = userId => {
    this.setState({ userSelected: userId });
  };

  searchProfile = nameProfile => {
    this.props.getProfileList(
      "_filter(profileName-like)=%25" + nameProfile + "%25&_limit=5",
    );
  };

  selectProfile = idProfile => {
    this.setState({ profileSelected: idProfile });
  };

  searchApplications = nameApplicaion => {
    this.props.getApplicationList(
      "_filter(applicationName-like)=%25" + nameApplicaion + "%25&_limit=5",
    );
  };

  selectApplication = idApplication => {
    this.setState({ applicationSelected: idApplication });
  };

  render() {
    const { userList, profileList, applicationList } = this.props;
    const { userSelected, profileSelected, applicationSelected } = this.state;

    if (userList && profileList && applicationList) {
      return (
        <AdminDashBoard
          userList={userList}
          searchUser={this.searchUser}
          selectUser={this.selectUser}
          userSelected={userSelected}
          profileList={profileList}
          searchProfile={this.searchProfile}
          selectProfile={this.selectProfile}
          profileSelected={profileSelected}
          applicationList={applicationList}
          searchApplications={this.searchApplications}
          selectApplication={this.selectApplication}
          applicationSelected={applicationSelected}
        />
      );
    }
    return <Loader />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  AdminDashBoardContainer,
);
