import React, { Component } from "react";
import { connect } from "react-redux";
import { withLoader } from "../../../../components/Hoc";
import { ProfilesInfo } from "../../components";
import getCountProfiles from "../../redux/actions/application/getCountProfiles";

const mapStateToProps = state => {
  return {
    countProfiles: state.admin.application.countProfiles,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCountProfiles: () => {
      dispatch(getCountProfiles());
    },
  };
};

class ProfilesInfoContainer extends Component {
  componentDidMount() {
    this.props.getCountProfiles();
  }

  render() {
    const { countProfiles } = this.props;

    const isLoading = countProfiles == null;

    const ProfilesInfoWithLoader = withLoader(ProfilesInfo);
    return (
      <ProfilesInfoWithLoader
        isLoading={isLoading}
        countProfiles={countProfiles}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ProfilesInfoContainer,
);
