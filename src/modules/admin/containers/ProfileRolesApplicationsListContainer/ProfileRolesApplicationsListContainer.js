import React, { Component } from "react";
import { connect } from "react-redux";
import { Loader } from "../../../../components";
import { ProfileRolesApplicationsList } from "../../components";
import setRoleApplicationProfile from "../../redux/actions/application/setRoleApplicationProfile";
import deleteRoleApplicationProfile from "../../redux/actions/application/deleteRoleApplicationProfile";
import getRoleApplicationProfile from "../../redux/actions/application/getRoleApplicationProfile";
import getApplicationList from "../../redux/actions/application/getApplicationList";
import initialiseProfileRole from "../../redux/actions/application/initialiseProfileRole";
import getRoleList from "../../redux/actions/role/getRoleList";

const mapStateToProps = state => {
  return {
    applicationList: state.admin.application.applicationList,
    roleList: state.admin.role.roleList,
    roleApplicationProfileList:
      state.admin.application.roleApplicationProfileList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getApplicationList: filter => {
      dispatch(getApplicationList(filter));
    },
    getRoleList: filter => {
      dispatch(getRoleList(filter));
    },
    setRoleApplicationProfile: (profileId, roleId) => {
      dispatch(setRoleApplicationProfile(profileId, roleId));
    },
    deleteRoleApplicationProfile: (profileId, roleId) => {
      dispatch(deleteRoleApplicationProfile(profileId, roleId));
    },
    getRoleApplicationProfile: profileId => {
      dispatch(getRoleApplicationProfile(profileId));
    },
    initialiseProfileRole: () => {
      dispatch(initialiseProfileRole());
    },
  };
};

class ProfileRolesApplicationsListContainer extends Component {
  componentDidMount() {
    this.props.getRoleList("_limit=10000");
    this.props.getApplicationList();
    this.props.getRoleApplicationProfile(this.props.profileId);
  }
  componentWillUnmount() {
    this.props.initialiseProfileRole();
  }

  deleteProfileRoleApp = roleId => {
    let profileId = this.props.profileId;
    this.props.deleteRoleApplicationProfile(profileId, roleId);
    // this.props.getRoleList();
    // this.props.getApplicationList();
  };

  setProfileRoleApp = roleId => {
    let profileId = this.props.profileId;
    this.props.setRoleApplicationProfile(profileId, roleId);
  };

  handleChangeRole = values => {
    this.setState({ value: values });
  };

  handleChange = (event, index, value) =>
    this.setState({ value: this.state.value });

  render() {
    const {
      applicationList,
      roleList,
      roleApplicationProfileList,
    } = this.props;

    if (
      applicationList != null &&
      roleList != null &&
      roleApplicationProfileList != null
    ) {
      return (
        <ProfileRolesApplicationsList
          applicationList={applicationList}
          roleList={roleList}
          roleApplicationProfileList={roleApplicationProfileList}
          deleteProfileRoleApp={this.deleteProfileRoleApp}
          setProfileRoleApp={this.setProfileRoleApp}
        />
      );
    }

    return <Loader />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ProfileRolesApplicationsListContainer,
);
