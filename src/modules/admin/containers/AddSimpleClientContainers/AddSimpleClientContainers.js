import React, { Component } from "react";
import { connect } from "react-redux";
import { AddSimpleClient } from "../../components";
import setClient from "../../redux/actions/user/setClient";
import { Loader } from "../../../../components";
const dataSource = [
  {
    clientName: "De Longhi",
    clientId: "1",
  },
  {
    clientName: "Delta Pronatura",
    clientId: "2",
  },
  {
    clientName: "GSK",
    idClient: "3",
  },
  {
    clientName: "Laurence DUMONT",
    clientId: "4",
  },
  {
    clientName: "Quinola",
    clientId: "5",
  },
  {
    clientName: "Récylum | Éco-systèmes",
    clientId: "6",
  },
  {
    clientName: "Red Bull",
    clientId: "7",
  },
  {
    clientName: "SABMiller",
    clientId: "9",
  },
  {
    clientName: "Showa",
    clientId: "10",
  },
  {
    clientName: "Swania",
    clientId: "11",
  },
  {
    clientName: "TTI",
    clientId: "13",
  },
  {
    clientName: "Unhycos",
    clientId: "14",
  },
  {
    clientName: "Urmet",
    clientId: "15",
  },
];

// const mapStateToProps = state => {
//   return {
//     currentUser: state.admin.user.currentUser,
//   };
// };

const mapDispatchToProps = dispatch => {
  return {
    setClient: (userId, RequestBody) => {
      dispatch(setClient(userId, RequestBody));
    },
  };
};

class AddSimpleClientContainers extends Component {

  handleCheck = (event, isInputChecked, clientId) => {
    const bodyProfile = {};
    const profileId = this.props.profileId;
    const createdUserId = this.props.userId;

    if (isInputChecked) {
      bodyProfile.profileId = profileId;
      bodyProfile.clientId = clientId;
      bodyProfile.isDefault = 1;
      this.props.setClient(createdUserId, bodyProfile);
    } else {
      bodyProfile.profileId = profileId;
      bodyProfile.clientId = clientId;
      bodyProfile.isDefault = 0;
      this.props.setClient(createdUserId, bodyProfile);
    }
  };



  render() {
    //const { currentUser } = this.props;

    if (dataSource !== null) {
      return (
        <AddSimpleClient
          clients={dataSource}
          handleCheck={this.handleCheck}
        />
      );
    }
    return <Loader />;
  }
}

export default connect(null, mapDispatchToProps)(
    AddSimpleClientContainers,
);
