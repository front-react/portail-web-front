import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {ApplicationPrivilegesInfo} from '../../components'
import getCountPrivilegesApplication from '../../redux/actions/application/getCountPrivilegesApplication'


const mapStateToProps = (state) => {
  return {
    countPrivilegesApplication: state.admin.application.countPrivilegesApplication
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCountPrivilegesApplication: (id) => { dispatch(getCountPrivilegesApplication(id))},
  };
};

class ApplicationPrivilegesInfoContainer extends Component {


  componentDidMount() {
    const idApplication = this.props.idApplication;
    this.props.getCountPrivilegesApplication(idApplication);
  }



  render() {
   const {countPrivilegesApplication, idApplication} = this.props;

   
    
    if (countPrivilegesApplication != null) {
      return <ApplicationPrivilegesInfo countPrivilegesApplication={countPrivilegesApplication} idApplication={idApplication}  />
    }

    return (<Loader />)
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationPrivilegesInfoContainer);