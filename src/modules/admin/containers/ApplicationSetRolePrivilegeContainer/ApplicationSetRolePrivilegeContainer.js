import React, { Component } from "react";
import { connect } from "react-redux";
import { ApplicationSetRolePrivilege } from "../../components";
//import { Loader } from "../../../../components";
import { addRolePrivilege } from "../../redux/actions/role/addRolePrivilege";
import { deleteRolePrivilege } from "../../redux/actions/role/deleteRolePrivilege";
import getApplication from "../../redux/actions/application/getApplication";
import getRoleList from "../../redux/actions/role/getRoleList";
import getPrivilegeList from "../../redux/actions/privilege/getPrivilegeList";
import { 
  CreateRoleContainer,
  CreatePrivilegeContainer 
} from "../../containers";

const styles = {
  alignBox: {
    textAlign: "center",
  },
  alignButton: {
    width: '200px',
  }
};

const mapStateToProps = state => {
  return {
    currentApplication: state.admin.application.currentApplication,
    roleList: state.admin.role.roleList,
    privilegesList: state.admin.privilege.privilegesList,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addRolePrivilege: (idRole, idPrivilege) => {
      dispatch(addRolePrivilege(idRole, idPrivilege));
    },
    deleteRolePrivilege: (idRole, idPrivilege) => {
      dispatch(deleteRolePrivilege(idRole, idPrivilege));
    },
    getApplication: id => {
      dispatch(getApplication(id));
    },
    getRoleList: filter => {
      dispatch(getRoleList(filter));
    },
    getPrivilegeList: filter => {
      dispatch(getPrivilegeList(filter));
    },
  };
};

class ApplicationSetRolePrivilegeContainer extends Component {
  state = {
    slideIndex: 0,
  };

  componentDidMount() {
    const idApplication = this.props.idApplication;
    this.props.getApplication(idApplication);
    this.props.getRoleList(
      `_filter(applicationId-%3D)=${idApplication}&_limit=100000`,
    );
    this.props.getPrivilegeList(
      `_filter(applicationId-%3D)=${idApplication}&_limit=100000`,
    );
  }

  handleTabChange = (event, value) => {
    this.setState({
      slideIndex: event,
    });

    const idApplication = this.props.idApplication;
    this.props.getApplication(idApplication);
    this.props.getRoleList(
      `_filter(applicationId-%3D)=${idApplication}&_limit=100000`,
    );
    this.props.getPrivilegeList(
      `_filter(applicationId-%3D)=${idApplication}&_limit=100000`,
    );
  };

  // handleTabChangeIndex = value => {

  //   const idApplication = this.props.idApplication;
  //   this.props.getApplication(idApplication);
  //   this.props.getRoleList(
  //     `_filter(applicationId-%3D)=${idApplication}&_limit=100000`,
  //   );
  //   this.props.getPrivilegeList(
  //     `_filter(applicationId-%3D)=${idApplication}&_limit=100000`,
  //   );
  // };

  handleToggle = (event, isInputChecked, idRole, idPrivilege) => {
    if (isInputChecked) {
      this.props.addRolePrivilege(idRole, idPrivilege);
    } else {
      this.props.deleteRolePrivilege(idRole, idPrivilege);
    }
  };

  intialisationPrivileges = (idRole, idPrivilege) => {
    this.props.addRolePrivilege(idRole, idPrivilege);
  };

  render() {
    const {
      currentApplication,
      roleList,
      privilegesList,
      idApplication,
    } = this.props;

    const { slideIndex } = this.state;

    if (
      currentApplication !== null &&
      roleList !== null &&
      privilegesList !== null &&
      slideIndex !== null &&
      idApplication !== null
    ) {
      return (
        <ApplicationSetRolePrivilege
          currentApplication={currentApplication}
          handleTabChange={this.handleTabChange}
          handleToggle={this.handleToggle}
          slideIndex={slideIndex}
          rolePrivilegesList={roleList}
          intialisationPrivileges={this.intialisationPrivileges}
          privilegesList={privilegesList}
          applicationId={idApplication}
          //handleTabChangeIndex={this.handleTabChangeIndex}
        />
      );
    } else {
      return (
        <div style={styles.alignBox}>
          Cette application n'a pas de rôle ou de privilège:
          <strong>Il vous faut au moins un rôle et un privilège</strong>
          <span style={styles.alignButton} >
            <CreateRoleContainer idApplication={idApplication} />
          </span>
          <span style={styles.alignButton} >
            <CreatePrivilegeContainer idApplication={idApplication} />
          </span>
        </div>
      );
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(
  ApplicationSetRolePrivilegeContainer,
);
