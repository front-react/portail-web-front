import React, { Component } from "react";
import { connect } from "react-redux";
import { withLoader } from "../../../../components/Hoc";
import { ApplicationsInfo } from "../../components";
import getCountApplications from "../../redux/actions/application/getCountApplications";

const mapStateToProps = state => {
  return {
    countApplications: state.admin.application.countApplications,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCountApplications: () => {
      dispatch(getCountApplications());
    },
  };
};

class ApplicationsInfoContainer extends Component {
  componentDidMount() {
    this.props.getCountApplications();
  }

  render() {
    const { countApplications } = this.props;
    const isLoading = countApplications === null;

    const ApplicationsInfoWithLoader = withLoader(ApplicationsInfo);

    return (
      <ApplicationsInfoWithLoader
        isLoading={isLoading}
        countApplications={countApplications}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ApplicationsInfoContainer,
);
