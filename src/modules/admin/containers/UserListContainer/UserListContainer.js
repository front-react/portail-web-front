import React, { Component } from "react";
import { connect } from "react-redux";
import { withLoader } from "../../../../components/Hoc";
import { UserList } from "../../components";
import getUsersList from "../../redux/actions/user/getUserList";

const mapStateToProps = state => {
  return {
    userList: state.admin.user.userList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUsersList: filters => {
      dispatch(getUsersList(filters));
    },
  };
};

class UserListContainer extends Component {
  componentDidMount() {
    this.props.getUsersList("_limit=100000");
  }

  filterUserList = (filterValue, field) => {
    this.props.getUsersList(
      "_filter(" + field + "-like)=%25" + filterValue + "%25",
    );
  };

  render() {
    const { userList } = this.props;
    const isLoading = userList === null;
    const UserListWithLoader = withLoader(UserList);
    return (
      <UserListWithLoader
        isLoading={isLoading}
        userList={userList}
        filterUserList={this.filterUserList}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserListContainer);
