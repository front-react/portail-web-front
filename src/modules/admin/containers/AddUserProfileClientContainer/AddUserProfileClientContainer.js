import React, { Component } from "react";
import { connect } from "react-redux";
import { AddUserProfileClient } from "../../components";
import addUserProfile from "../../redux/actions/user/addUserProfile";
import setClient from "../../redux/actions/user/setClient";
import getClients from "../../redux/actions/user/getClients";
import { Loader } from "../../../../components";

const mapStateToProps = state => {
  return {
    currentUser: state.admin.user.currentUser,
    clientsList: state.admin.user.clientsList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addUserProfile: RequestBody => {
      dispatch(addUserProfile(RequestBody));
    },
    setClient: (userId, RequestBody) => {
      dispatch(setClient(userId, RequestBody));
    },
    getClients: () => {
      dispatch(getClients());
    },
  };
};

class AddUserProfileClientContainer extends Component {

  componentDidMount(){
    this.props.getClients();
  }

  state = {
    value: 1,
  };

  handleChange = (event, index, value) => this.setState({ value });

  handleFormSubmit = (responseBody, dialogueEvent) => {
    let bodyProfile = {};
    const createdUserId = this.props.currentUser.userId;

    bodyProfile.profileId = responseBody.profileId;
    bodyProfile.userId = createdUserId;
    this.props.addUserProfile(bodyProfile);

    if (dialogueEvent === "next") {
      this.props.handleNext();
    } else if (dialogueEvent === "close") {
      this.props.handleClose();
    }
  };

  handleCheck = (event, isInputChecked, clientId) => {
    const bodyProfile = {};
    const profileId = 1;
    const createdUserId = this.props.currentUser.userId;

    if (isInputChecked) {
      bodyProfile.profileId = profileId;
      bodyProfile.clientId = clientId;
      bodyProfile.isDefault = 1;
      this.props.setClient(createdUserId, bodyProfile);
    } else {
      bodyProfile.profileId = profileId;
      bodyProfile.clientId = clientId;
      bodyProfile.isDefault = 0;
      this.props.setClient(createdUserId, bodyProfile);
    }
  };

  // handleChange = (event, key, payload) => {
  //   let bodyProfile = {};

  //   const createdUserId = this.props.user.currentUser.userId;

  //   bodyProfile.userId = createdUserId;
  //   bodyProfile.profileId = key;

  //   this.props.addUserProfile(bodyProfile);
  // };

  // handleStatusForm = dialogueEvent => {
  //   if (dialogueEvent === "next") {
  //     this.props.handleNext();
  //   } else if (dialogueEvent === "close") {
  //     this.props.handleClose();
  //   }
  // };

  render() {
    const { currentUser, clientsList } = this.props;

    if (currentUser !== null && clientsList !== null) {
      return (
        <AddUserProfileClient
          handleFormSubmit={this.handleFormSubmit}
          clients={clientsList}
          valueProfile={this.state.value}
          //handleChange={this.handleChange}
          handleCheck={this.handleCheck}
          //handleStatusForm={this.handleStatusForm}
        />
      );
    }
    return <Loader />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  AddUserProfileClientContainer,
);
