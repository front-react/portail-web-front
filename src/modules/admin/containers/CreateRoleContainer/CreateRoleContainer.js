import React, {Component} from 'react'
import {connect} from 'react-redux'
import {CreateRole} from '../../components'
import {createRole} from '../../redux/actions/role/createRole'


const mapDispatchToProps = (dispatch) => {
    return {
      createRole: (RequestBody, applicationId) => {dispatch(createRole(RequestBody, applicationId))
      }
    }
  };

class CreateRoleContainer extends Component {

    state = {
        open: false,
      };
    
      handleOpen = () => {
        this.setState({open: true});
      };
    
      handleClose = () => {
        this.setState({open: false});
      };
    
      handleFormSubmit = (requestBody) => {
        requestBody.applicationId = this.props.idApplication
        this.props.createRole(requestBody, this.props.idApplication)
      }

  render() {   


      return <CreateRole
        handleFormSubmit={this.handleFormSubmit} 
        createDialogStatus={this.state.open} 
        handleClose={this.handleClose} 
        handleOpen={this.handleOpen} />

  }
};

export default connect(null, mapDispatchToProps)(CreateRoleContainer);