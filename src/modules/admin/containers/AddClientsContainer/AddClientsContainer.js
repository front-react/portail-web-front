import React, { Component } from "react";
import { connect } from "react-redux";
import { AddClients } from "../../components";
import setClient from "../../redux/actions/user/setClient";
import getClients from "../../redux/actions/user/getClients";
import deleteClient from "../../redux/actions/user/deleteClient";
import { Loader } from "../../../../components";

const mapStateToProps = state => {
  return {
    clientsList: state.admin.user.clientsList,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setClient: (userId, RequestBody) => {
      dispatch(setClient(userId, RequestBody));
    },
    getClients: () => {
      dispatch(getClients());
    },
    deleteClient: (userId, clientId) => {
      dispatch(deleteClient(userId, clientId));
    },
  };
};

class AddClientsContainer extends Component {

  componentDidMount(){
    this.props.getClients();
  }


  handleFormSubmit = (responseBody, dialogueEvent) => {
    let bodyProfile = {};

    const createdUserId = this.props.userId;

    bodyProfile.profileId = responseBody.profileId;
    bodyProfile.userId = createdUserId;
    this.props.addUserProfile(bodyProfile);

    if (dialogueEvent === "next") {
      this.props.handleNext();
    } else if (dialogueEvent === "close") {
      this.props.handleClose();
    }
  };

  handleCheck = (event, isInputChecked, clientId) => {

    const bodyProfile = {};
    const profileId = this.props.user.profileId;
    const createdUserId = this.props.user.userId;

    if (isInputChecked) {
      bodyProfile.profileId = profileId;
      bodyProfile.clientId = clientId;
      bodyProfile.isDefault = 0;
      this.props.setClient(createdUserId, bodyProfile);
    } else {
      this.props.deleteClient(createdUserId, clientId);
    }
  };

  render() {
    const { clientsList } = this.props;

    const clientUser = this.props.user.clients

    if (clientsList !== null) {
      return (
        <AddClients
          clients={clientsList}
          clientsUser={clientUser}
          handleCheck={this.handleCheck}
        />
      );
    }
    return <Loader />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
    AddClientsContainer,
);
