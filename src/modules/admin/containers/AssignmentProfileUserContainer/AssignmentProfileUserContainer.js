import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Loader} from '../../../../components'
import {AssignmentProfileUser} from '../../components'
import {addAssignmentProfileUser} from '../../redux/actions/user/addAssignmentProfileUser'
import {getProfileList} from '../../redux/actions/profile/getProfileList'


const mapStateToProps = (state) => {
    return {
			profileList: state.admin.profile.profileList,
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {
		addAssignmentProfileUser: (userId, profileId) => { dispatch(addAssignmentProfileUser(userId, profileId)) },
		getProfileList: (filter) => { dispatch(getProfileList(filter)) }
  }
}


class AssignmentProfileUserContainer extends Component {

	state = {
		open: false,
		value: this.props.profileId
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

	componentDidMount(){
		this.props.getProfileList()
	}

	addAssignedProfile = (profileId) => {
		this.setState({value: profileId});
		
		const userId = this.props.userId
		this.props.addAssignmentProfileUser(userId, profileId)
     
  }

  handleChange = (event, index, value) => this.setState({value: value});

    render(){
			
			const { profileList, userName } = this.props

			if(profileList !== null && userName !== null) {

				return(
						<AssignmentProfileUser 
							profileList={profileList}
							handleOpen={this.handleOpen}
							handleClose={this.handleClose}
							statusDialog={this.state.open}
							userName={userName}
							handleChange={this.handleChange}
							addAssignedProfile={this.addAssignedProfile}
							indexProfileId={this.state.value}
						/>
				);
			}
			return <Loader />
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentProfileUserContainer)
