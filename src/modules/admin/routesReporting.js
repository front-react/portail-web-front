import React from 'react';
import {Route, IndexRedirect, IndexRoute} from 'react-router';
import {
  CarPark,
  DashBoardContainer,
  CarListContainer,
  CarProfileContainer,
  AssignmentListContainer,
  CarAssignmentContainer,
  RepairListContainer,
  AddCarContainer,
  UpCarListContainer,
} from './containers';


export default(
  <Route name="Reporting" path="reporting" component={CarPark}>
    <IndexRedirect to="dasboard" />
    <Route name="Dashboard" path="dasboard" component={DashBoardContainer}/>
    <Route name="Liste des visites" path="list" component={DashBoardContainer}/>
  </Route>
)