/*================ USERADMIN ===========*/

//---- User
export const GET_USER_LIST_REQUEST = "admin/GET_USER_LIST_REQUEST";
export const GET_USER_LIST_SUCCESS = "admin/GET_USER_LIST_SUCCESS";
export const GET_USER_LIST_FAILURE = "admin/GET_USER_LIST_FAILURE";

export const GET_USER_REQUEST = "admin/GET_USER_LIST_REQUEST";
export const GET_USER_SUCCESS = "admin/GET_USER_SUCCESS";
export const GET_USER_FAILURE = "admin/GET_USER_FAILURE";

export const CREATE_USER_REQUEST = "admin/CREATE_USER_REQUEST";
export const CREATE_USER_SUCCESS = "admin/CREATE_USER_SUCCESS";
export const CREATE_USER_CURRENT_SUCCESS = "admin/CREATE_USER_CURRENT_SUCCESS";
export const CREATE_USER_FAILURE = "admin/CREATE_USER_FAILURE";

export const ADD_USER_PROFILE_REQUEST = "admin/ADD_USER_PROFILE_REQUEST";
export const ADD_USER_PROFILE_SUCCESS = "admin/DD_USER_PROFILE_SUCCESS";
export const ADD_USER_PROFILE_FAILURE = "admin/ADD_USER_PROFILE_FAILURE";

export const ADD_USER_CLIENT_REQUEST = "admin/ADD_USER_CLIENT_REQUEST";
export const ADD_USER_CLIENT_SUCCESS = "admin/ADD_USER_CLIENT_SUCCESS";
export const ADD_USER_CLIENT_FAILURE = "admin/ADD_USER_CLIENT_FAILURE";

export const PATCH_USER_REQUEST = "admin/PATCH_USER_REQUEST";
export const PATCH_USER_SUCCESS = "admin/PATCH_USER_SUCCESS";
export const PATCH_USER_FAILURE = "admin/PATCH_USER_FAILURE";

export const DELETE_USER_REQUEST = "admin/DELETE_USER_REQUEST";
export const DELETE_USER_SUCCESS = "admin/DELETE_USER_SUCCESS";
export const DELETE_USER_FAILURE = "admin/DELETE_USER_FAILURE";

export const ADD_ASSIGNMENT_PROFILE_USER_REQUEST =
  "admin/ADD_ASSIGNMENT_PROFILE_USER_REQUEST";
export const ADD_ASSIGNMENT_PROFILE_USER_SUCCESS =
  "admin/ADD_ASSIGNMENT_PROFILE_USER_SUCCESS";
export const ADD_ASSIGNMENT_PROFILE_USER_FAILURE =
  "admin/ADD_ASSIGNMENT_PROFILE_USER_FAILURE";

export const SET_CLIENT_DEFAULT_REQUEST = "admin/SET_CLIENT_DEFAULT_REQUEST";
export const SET_CLIENT_DEFAULT_SUCCESS = "admin/SET_CLIENT_DEFAULT_SUCCESS";
export const SET_CLIENT_DEFAULT_FAILURE = "admin/SET_CLIENT_DEFAULT_FAILURE";

export const SET_CLIENT_REQUEST = "admin/SET_CLIENT_REQUEST";
export const SET_CLIENT_SUCCESS = "admin/SET_CLIENT_SUCCESS";
export const SET_CLIENT_FAILURE = "admin/SET_CLIENT_FAILURE";

export const GET_CLIENTS_REQUEST = "admin/GET_CLIENTS_REQUEST";
export const GET_CLIENTS_SUCCESS = "admin/GET_CLIENTS_SUCCESS";
export const GET_CLIENTS_FAILURE = "admin/GET_CLIENTS_FAILURE";

export const DELETE_CLIENT_USER_REQUEST = "admin/DELETE_CLIENT_USER_REQUEST";
export const DELETE_CLIENT_USER_SUCCESS = "admin/DELETE_CLIENT_USER_SUCCESS";
export const DELETE_CLIENT_USER_FAILURE = "admin/DELETE_CLIENT_USER_FAILURE";

export const DELETE_ROLE_APPLICATION_PROFILE_REQUEST =
  "admin/DELETE_ROLE_APPLICATION_PROFILE_REQUEST";
export const DELETE_ROLE_APPLICATION_PROFILE_SUCCESS =
  "admin/DELETE_ROLE_APPLICATION_PROFILE_SUCCESS";
export const DELETE_ROLE_APPLICATION_PROFILE_FAILURE =
  "admin/DELETE_ROLE_APPLICATION_PROFILE_FAILURE";

//---- Application

export const GET_APPLICATION_LIST_REQUEST =
  "admin/GET_APPLICATION_LIST_REQUEST";
export const GET_APPLICATION_LIST_SUCCESS =
  "admin/GET_APPLICATION_LIST_SUCCESS";
export const GET_APPLICATION_LIST_FAILURE =
  "admin/GET_APPLICATION_LIST_FAILURE";

export const GET_APPLICATION_REQUEST = "admin/GET_APPLICATION_REQUEST";
export const GET_APPLICATION_SUCCESS = "admin/GET_APPLICATION_SUCCESS";
export const GET_APPLICATION_FAILURE = "admin/GET_APPLICATION_FAILURE";

export const CREATE_USER_AUTH_REQUEST = "admin/CREATE_USER_AUTH_REQUEST";
export const CREATE_USER_AUTH_SUCCESS = "admin/CREATE_USER_AUTH_SUCCESS";
export const CREATE_USER_AUTH_FAILURE = "admin/CREATE_USER_AUTH_FAILURE";

export const CREATE_APPLICATION_REQUEST = "admin/CREATE_APPLICATION_REQUEST";
export const CREATE_APPLICATION_SUCCESS = "admin/CREATE_APPLICATION_SUCCESS";
export const CREATE_APPLICATION_FAILURE = "admin/CREATE_APPLICATION_FAILURE";

export const PATCH_APPLICATION_REQUEST = "admin/PATCH_APPLICATION_REQUEST";
export const PATCH_APPLICATION_SUCCESS = "admin/PATCH_APPLICATION_SUCCESS";
export const PATCH_APPLICATION_FAILURE = "admin/PATCH_APPLICATION_FAILURE";

export const DELETE_APPLICATION_REQUEST = "admin/DELETE_APPLICATION_REQUEST";
export const DELETE_APPLICATION_SUCCESS = "admin/DELETE_APPLICATION_SUCCESS";
export const DELETE_APPLICATION_FAILURE = "admin/DELETE_APPLICATION_FAILURE";

export const ADD_APPLICATION_ROLE_SUCESS = "admin/ADD_APPLICATION_ROLE_SUCESS";
export const DELETE_APPLICATION_ROLE_SUCESS =
  "admin/DELETE_APPLICATION_ROLE_SUCESS";
export const ADD_APPLICATION_PRIVILEGE_SUCESS =
  "admin/ADD_APPLICATION_PRIVILEGE_SUCESS";
export const DELETE_APPLICATION_PRIVILEGE_SUCESS =
  "admin/DELETE_APPLICATION_PRIVILEGE_SUCESS";

export const GET_COUNT_PRIVILEGES_APPLICATION_REQUEST =
  "admin/GET_COUNT_PRIVILEGES_APPLICATION_REQUEST";
export const GET_COUNT_PRIVILEGES_APPLICATION_SUCCESS =
  "admin/GET_COUNT_PRIVILEGES_APPLICATION_SUCCESS";
export const GET_COUNT_PRIVILEGES_APPLICATION_FAILURE =
  "admin/GET_COUNT_PRIVILEGES_APPLICATION_FAILURE";

export const GET_COUNT_ROLES_APPLICATION_REQUEST =
  "admin/GET_COUNT_ROLES_APPLICATION_REQUEST";
export const GET_COUNT_ROLES_APPLICATION_SUCCESS =
  "admin/GET_COUNT_ROLES_APPLICATION_SUCCESS";
export const GET_COUNT_ROLES_APPLICATION_FAILURE =
  "admin/GET_COUNT_ROLES_APPLICATION_FAILURE";

export const GET_COUNT_USERS_APPLICATION_REQUEST =
  "admin/GET_COUNT_USERS_APPLICATION_REQUEST";
export const GET_COUNT_USERS_APPLICATION_SUCCESS =
  "admin/GET_COUNT_USERS_APPLICATION_SUCCESS";
export const GET_COUNT_USERS_APPLICATION_FAILURE =
  "admin/GET_COUNT_USERS_APPLICATION_FAILURE";

export const GET_COUNT_APPLICATIONS_REQUEST =
  "admin/GET_COUNT_APPLICATIONS_REQUEST";
export const GET_COUNT_APPLICATIONS_SUCCESS =
  "admin/GET_COUNT_APPLICATIONS_SUCCESS";
export const GET_COUNT_APPLICATIONS_FAILURE =
  "admin/GET_COUNT_APPLICATIONS_FAILURE";

export const GET_COUNT_PROFILES_REQUEST = "admin/GET_COUNT_PROFILES_REQUEST";
export const GET_COUNT_PROFILES_SUCCESS = "admin/GET_COUNT_PROFILES_SUCCESS";
export const GET_COUNT_PROFILES_FAILURE = "admin/GET_COUNT_PROFILES_FAILURE";

export const GET_COUNT_USERS_REQUEST = "admin/GET_COUNT_USERS_REQUEST";
export const GET_COUNT_USERS_SUCCESS = "admin/GET_COUNT_USERS_SUCCESS";
export const GET_COUNT_USERS_FAILURE = "admin/GET_COUNT_USERS_FAILURE";

export const GET_USER_APPLICATION_ROLE_REQUEST =
  "admin/GET_USER_APPLICATION_ROLE_REQUEST";
export const GET_USER_APPLICATION_ROLE_SUCCESS =
  "admin/GET_USER_APPLICATION_ROLE_SUCCESS";
export const GET_USER_APPLICATION_ROLE_FAILURE =
  "admin/GET_USER_APPLICATION_ROLE_FAILURE";

export const GET_APPLICATION_USERS_LIST_REQUEST =
  "admin/GET_APPLICATION_USERS_LIST_REQUEST";
export const GET_APPLICATION_USERS_LIST_SUCCESS =
  "admin/GET_APPLICATION_USERS_LIST_SUCCESS";
export const GET_APPLICATION_USERS_LIST_FAILURE =
  "admin/GET_APPLICATION_USERS_LIST_FAILURE";

export const GET_APPLICATION_ROLE_PRIVILEGES_LIST_REQUEST =
  "admin/GET_APPLICATION_ROLE_PRIVILEGES_LIST_REQUEST";
export const GET_APPLICATION_ROLE_PRIVILEGES_LIST_SUCCESS =
  "admin/GET_APPLICATION_ROLE_PRIVILEGES_LIST_SUCCESS";
export const GET_APPLICATION_ROLE_PRIVILEGES_LIST_FAILURE =
  "admin/GET_APPLICATION_ROLE_PRIVILEGES_LIST_FAILURE";

export const SET_ROLE_APPLICATION_PROFILE_REQUEST =
  "admin/SET_ROLE_APPLICATION_PROFILE_REQUEST";
export const SET_ROLE_APPLICATION_PROFILE_SUCCESS =
  "admin/SET_ROLE_APPLICATION_PROFILE_SUCCESS";
export const SET_ROLE_APPLICATION_PROFILE_FAILURE =
  "admin/SET_ROLE_APPLICATION_PROFILE_FAILURE";

export const GET_ROLE_APPLICATION_PROFILE_REQUEST =
  "admin/GET_ROLE_APPLICATION_PROFILE_REQUEST";
export const GET_ROLE_APPLICATION_PROFILE_SUCCESS =
  "admin/GET_ROLE_APPLICATION_PROFILE_SUCCESS";
export const GET_ROLE_APPLICATION_PROFILE_FAILURE =
  "admin/GET_ROLE_APPLICATION_PROFILE_FAILURE";

//----- Role

export const GET_ROLE_LIST_REQUEST = "admin/GET_ROLE_LIST_REQUEST";
export const GET_ROLE_LIST_SUCCESS = "admin/GET_ROLE_LIST_SUCCESS";
export const GET_ROLE_LIST_FAILURE = "admin/GET_ROLE_LIST_FAILURE";

export const GET_ROLE_REQUEST = "admin/GET_ROLE_REQUEST";
export const GET_ROLE_SUCCESS = "admin/GET_ROLE_SUCCESS";
export const GET_ROLE_FAILURE = "admin/GET_ROLE_FAILURE";

export const CREATE_ROLE_REQUEST = "admin/CREATE_ROLE_REQUEST";
export const CREATE_ROLE_SUCCESS = "admin/CREATE_ROLE_SUCCESS";
export const CREATE_ROLE_FAILURE = "admin/CREATE_ROLE_FAILURE";

export const ADD_ROLE_PRIVILEGE_REQUEST = "admin/ADD_ROLE_PRIVILEGE_REQUEST";
export const ADD_ROLE_PRIVILEGE_SUCCESS = "admin/ADD_ROLE_PRIVILEGE_SUCCESS";
export const ADD_ROLE_PRIVILEGE_FAILURE = "admin/ADD_ROLE_PRIVILEGE_FAILURE";

export const PATCH_ROLE_REQUEST = "admin/PATCH_ROLE_REQUEST";
export const PATCH_ROLE_SUCCESS = "admin/PATCH_ROLE_SUCCESS";
export const PATCH_ROLE_FAILURE = "admin/PATCH_ROLE_FAILURE";

export const DELETE_ROLE_REQUEST = "admin/DELETE_ROLE_REQUEST";
export const DELETE_ROLE_SUCCESS = "admin/DELETE_ROLE_SUCCESS";
export const DELETE_ROLE_FAILURE = "admin/DELETE_ROLE_FAILURE";

export const DELETE_ROLE_PRIVILEGE_REQUEST =
  "admin/DELETE_ROLE_PRIVILEGE_REQUEST";
export const DELETE_ROLE_PRIVILEGE_SUCCESS =
  "admin/DELETE_ROLE_PRIVILEGE_SUCCESS";
export const DELETE_ROLE_PRIVILEGE_FAILURE =
  "admin/DELETE_ROLE_PRIVILEGE_FAILURE";

//----- Privilege

export const GET_PRIVILEGE_LIST_REQUEST = "admin/GET_PRIVILEGE_LIST_REQUEST";
export const GET_PRIVILEGE_LIST_SUCCESS = "admin/GET_PRIVILEGE_LIST_SUCCESS";
export const GET_PRIVILEGE_LIST_FAILURE = "admin/GET_PRIVILEGE_LIST_FAILURE";

export const GET_PRIVILEGE_REQUEST = "admin/GET_PRIVILEGE_REQUEST";
export const GET_PRIVILEGE_SUCCESS = "admin/GET_PRIVILEGE_SUCCESS";
export const GET_PRIVILEGE_FAILURE = "admin/GET_PRIVILEGE_FAILURE";

export const CREATE_PRIVILEGE_REQUEST = "admin/CREATE_PRIVILEGE_REQUEST";
export const CREATE_PRIVILEGE_SUCCESS = "admin/CREATE_PRIVILEGE_SUCCESS";
export const CREATE_PRIVILEGE_FAILURE = "admin/CREATE_PRIVILEGE_FAILURE";

export const PATCH_PRIVILEGE_REQUEST = "admin/PATCH_PRIVILEGE_REQUEST";
export const PATCH_PRIVILEGE_SUCCESS = "admin/PATCH_PRIVILEGE_SUCCESS";
export const PATCH_PRIVILEGE_FAILURE = "admin/PATCH_PRIVILEGE_FAILURE";

export const DELETE_PRIVILEGE_REQUEST = "admin/DELETE_PRIVILEGE_REQUEST";
export const DELETE_PRIVILEGE_SUCCESS = "admin/DELETE_PRIVILEGE_SUCCESS";
export const DELETE_PRIVILEGE_FAILURE = "admin/DELETE_PRIVILEGE_FAILURE";

//---- Profile

export const GET_PROFILE_LIST_REQUEST = "admin/GET_PROFILE_LIST_REQUEST";
export const GET_PROFILE_LIST_SUCCESS = "admin/GET_PROFILE_LIST_SUCCESS";
export const GET_PROFILE_LIST_FAILURE = "admin/GET_PROFILE_LIST_FAILURE";

export const GET_PROFILE_REQUEST = "admin/GET_PROFILE_REQUEST";
export const GET_PROFILE_SUCCESS = "admin/GET_PROFILE_SUCCESS";
export const GET_PROFILE_FAILURE = "admin/GET_PROFILE_FAILURE";

export const CREATE_PROFILE_REQUEST = "admin/CREATE_PROFILE_REQUEST";
export const CREATE_PROFILE_SUCCESS = "admin/CREATE_PROFILE_SUCCESS";
export const CREATE_PROFILE_FAILURE = "admin/CREATE_PROFILE_FAILURE";

export const PATCH_PROFILE_REQUEST = "admin/PATCH_PROFILE_REQUEST";
export const PATCH_PROFILE_SUCCESS = "admin/PATCH_PROFILE_SUCCESS";
export const PATCH_PROFILE_FAILURE = "admin/PATCH_PROFILE_FAILURE";

export const DELETE_PROFILE_REQUEST = "admin/DELETE_PROFILE_REQUEST";
export const DELETE_PROFILE_SUCCESS = "admin/DELETE_PROFILE_SUCCESS";
export const DELETE_PROFILE_FAILURE = "admin/DELETE_PROFILE_FAILURE";
