import {
  DELETE_ROLE_PRIVILEGE_REQUEST,
  DELETE_ROLE_PRIVILEGE_SUCCESS,
  DELETE_ROLE_PRIVILEGE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const deleteRolePrivilegeRequest = (request) => {
  return {
    type: DELETE_ROLE_PRIVILEGE_REQUEST,
    request
  }
}


const deleteRolePrivilegeSucess = (id) => {

  return {
    type: DELETE_ROLE_PRIVILEGE_SUCCESS,
    data: id
  }
}
const deleteRolePrivilegeFailure = (error) => {

  return {
    type: DELETE_ROLE_PRIVILEGE_FAILURE,
    error
  }
}

const getRequest = (id, privilege) => {
  return '/roles/'+id+'/privileges/'+privilege
}


export const deleteRolePrivilege = (idRole, idPrivilege) => {
  const request = getRequest(idRole, idPrivilege)

  return dispatch => {
    dispatch(deleteRolePrivilegeRequest(request))

    /*Axios.delete() don't accept body*/
    return authorizationApiProvider.request({
      method: 'delete',
      url: request
    })
      .then(
        res => {
          dispatch(deleteRolePrivilegeSucess({
            idRole,
            idPrivilege
          }))
        },
        error => dispatch(deleteRolePrivilegeFailure(error))
      )
  }
}

export default deleteRolePrivilege
