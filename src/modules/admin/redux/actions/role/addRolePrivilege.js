import {
  ADD_ROLE_PRIVILEGE_REQUEST,
  ADD_ROLE_PRIVILEGE_SUCCESS,
  ADD_ROLE_PRIVILEGE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const addRolePrivilegeRequest = (request) => {
  return {
    type: ADD_ROLE_PRIVILEGE_REQUEST,
    request
  }
}

const addRolePrivilegeSucess = (data) => {
  return {
    type: ADD_ROLE_PRIVILEGE_SUCCESS,
    data
  }
}

const addRolePrivilegeFailure = (error) => {

  return {
    type: ADD_ROLE_PRIVILEGE_FAILURE,
    error
  }
}

const getRequest = (roleId) => {
  return '/roles/'+roleId+'/privileges'
}

export const addRolePrivilege = (roleId,privilegeId) => {

  const request = getRequest(roleId)
  
  const body = {
    data:{
      privilegeId: privilegeId
    }
  }

  return dispatch => {
    dispatch(addRolePrivilegeRequest(request))

    return authorizationApiProvider.post(request,body)
      .then(
        res => { 
          dispatch(addRolePrivilegeSucess(res.data.data)) 
        },
        error => dispatch(addRolePrivilegeFailure(error))
      )
  }
}

export default addRolePrivilege
