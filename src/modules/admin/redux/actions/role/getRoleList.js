import {
  GET_ROLE_LIST_REQUEST,
  GET_ROLE_LIST_SUCCESS,
  GET_ROLE_LIST_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getRoleListRequest = (request) => {
  return {
    type: GET_ROLE_LIST_REQUEST,
    request
  }
}

const getRoleListSucess = (data) => {
  return {
    type: GET_ROLE_LIST_SUCCESS,
    data: data
  }
}
const getRoleListFailure = (error) => {

  return {
    type: GET_ROLE_LIST_FAILURE,
    error
  }
}



const getRequest = (filter) => {
  if(filter != null) {
    return '/roles?'+filter
  }

  return '/roles'
}


export const getRoleList = (filter = null) => {
  let request = getRequest(filter)

  return dispatch => {
    dispatch(getRoleListRequest(request))

    return authorizationApiProvider.get(request)
      .then(
        res => dispatch(getRoleListSucess(res.data.data)),
        error => {
          dispatch(getRoleListFailure())

        }
      )
  }
}

export default getRoleList
