import {
  GET_ROLE_REQUEST,
  GET_ROLE_SUCCESS,
  GET_ROLE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getRoleRequest = (request) => {
  return {
    type: GET_ROLE_REQUEST,
    request
  }
}

const getRoleSucess = (data) => {
  return {
    type: GET_ROLE_SUCCESS,
    data: data.data
  }
}
const getRoleFailure = (error) => {

  return {
    type: GET_ROLE_FAILURE,
    error
  }
}



const getRequest = (id) => {
    return '/roles/'+id
}


export const getRole = (id) => {

  let request = getRequest(id)

  return dispatch => {
    dispatch(getRoleRequest(request))


    return authorizationApiProvider.get(request)
      .then(
        res => dispatch(getRoleSucess(res.data)),
        error => {
          dispatch(getRoleFailure())

        }
      )
  }
}

export default getRole
