import {
  PATCH_ROLE_REQUEST,
  PATCH_ROLE_SUCCESS,
  PATCH_ROLE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const patchRoleRequest = (request) => {
  return {
    type: PATCH_ROLE_REQUEST,
    request
  }
}

const patchRoleSucess = (data) => {

  return {
    type: PATCH_ROLE_SUCCESS,
    data: data.data
  }
}
const patchRoleFailure = (error) => {

  return {
    type: PATCH_ROLE_FAILURE,
    error
  }
}

const getRequest = (id) => {
  return '/roles/'+id
}


export const patchRole = (id, body) => {
  let request = getRequest(id)
  
  const bodyRequest = {
      data: {
        roleName: body.roleName
      }
    }

  return dispatch => {
    dispatch(patchRoleRequest(request))

    return authorizationApiProvider.patch(request, bodyRequest)
      .then(
        res => {
          dispatch(patchRoleSucess(res.data))
        },
        error => dispatch(patchRoleFailure(error))
      )
  }
}

export default patchRole
