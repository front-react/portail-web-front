import {DELETE_ROLE_REQUEST, DELETE_ROLE_SUCCESS, DELETE_ROLE_FAILURE} from '../../types'
import {authorizationApiProvider} from '../../../../../utils/api/providers'

const deleteRoleRequest = (request) => {
  return {
    type: DELETE_ROLE_REQUEST,
    request
  }
}

const deleteRoleSucess = (id) => {

  return {
    type: DELETE_ROLE_SUCCESS,
    data: id
  }
}

const deleteRoleFailure = (error) => {

  return {
    type: DELETE_ROLE_FAILURE,
    error
  }
}

const getRequest = (id) => {
  return '/roles/'+id
}

export const deleteRole = (idRole) => {
  
  let request = getRequest(idRole)

  return dispatch => {
    dispatch(deleteRoleRequest(request))

    return authorizationApiProvider.delete(request)
      .then(
        res => {
          dispatch(deleteRoleSucess(idRole))
        },
        error => dispatch(deleteRoleFailure(error))
      )
  }
}

export default deleteRole
