import {
  CREATE_ROLE_REQUEST,
  CREATE_ROLE_SUCCESS,
  CREATE_ROLE_FAILURE,
} from "../../types";
import { authorizationApiProvider } from "../../../../../utils/api/providers";

import { getCountRolesApplication } from '../application/getCountRolesApplication'

const createRoleRequest = request => {
  return {
    type: CREATE_ROLE_REQUEST,
    request,
  };
};

const createRoleSucess = data => {
  return {
    type: CREATE_ROLE_SUCCESS,
    data,
  };
};
const createRoleFailure = error => {
  return {
    type: CREATE_ROLE_FAILURE,
    error,
  };
};

const getRequest = () => {
  return "/roles";
};

export const createRole = (body, applicationId) => {
  let request = getRequest();

  const roleBody = {
    data: body,
  };

  return dispatch => {
    dispatch(createRoleRequest(request));

    return authorizationApiProvider.post(request, roleBody).then(
      res => {
        dispatch(createRoleSucess(res.data.data))
        dispatch(getCountRolesApplication(applicationId))
      },
      error => dispatch(createRoleFailure(error)),
    );
  };
};

export default createRole;
