import {
  CREATE_PROFILE_REQUEST,
  CREATE_PROFILE_SUCCESS,
  CREATE_PROFILE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider

} from '../../../../../utils/api/providers'


const createProfileRequest = (request) => {
  return {
    type: CREATE_PROFILE_REQUEST,
    request
  }
}


const createProfileSucess = (data) => {

  return {
    type: CREATE_PROFILE_SUCCESS,
    data: data.data
  }
}
const createProfileFailure = (error) => {

  return {
    type: CREATE_PROFILE_FAILURE,
    error
  }
}

const getRequest = () => {
  return '/profiles'
}


export const createProfile = (body) => {
  
  let request = getRequest()

  const profileBody = {
    data : body
  }

  return dispatch => {
    dispatch(createProfileRequest(request))

    return authorizationApiProvider.post(request,profileBody)
      .then(
        res => { dispatch(createProfileSucess(res.data)) },
        error => dispatch(createProfileFailure(error))
      )
  }
}

export default createProfile
