import {
  GET_PROFILE_LIST_REQUEST,
  GET_PROFILE_LIST_SUCCESS,
  GET_PROFILE_LIST_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getProfileListRequest = (request) => {
  return {
    type: GET_PROFILE_LIST_REQUEST,
    request
  }
};

const getProfileListSucess = (data) => {

  return {
    type: GET_PROFILE_LIST_SUCCESS,
    data
  }
};


const getProfileListFailure = (error) => {

  return {
    type: GET_PROFILE_LIST_FAILURE,
    error
  }

};

const getRequest = (filter) => {
  if(filter != null) {
    return '/profiles?'+filter
  }

  return '/profiles'
}

export const getProfileList = (filter = null) => {
  let request = getRequest(filter)

  return dispatch => {
    dispatch(getProfileListRequest(request))

    return authorizationApiProvider.get(request)
      .then(
        res => dispatch(getProfileListSucess(res.data.data)),
        error => {
          dispatch(getProfileListFailure())
        }
      )
  }
};

export default getProfileList
