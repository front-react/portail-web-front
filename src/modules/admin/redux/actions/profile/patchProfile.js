import {
    PATCH_PROFILE_REQUEST,
    PATCH_PROFILE_SUCCESS,
    PATCH_PROFILE_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const patchProfileRequest = (request) => {
    return {
      type: PATCH_PROFILE_REQUEST,
      request
    }
  }
  
  const patchProfileSucess = (data) => {
  
    return {
      type: PATCH_PROFILE_SUCCESS,
      data
    }
  }
  
  const patchProfileFailure = (error) => {
  
    return {
      type: PATCH_PROFILE_FAILURE,
      error
    }
  }
  
  const getRequest = (id) => {
  
    return '/profiles/'+id
  }
  
  
  export const patchProfile = (id, bodyData) => {
  
    let request = getRequest(id)

    const body = {
        data: bodyData
    }
  
    return (dispatch) => {
      dispatch(patchProfileRequest(request))
  
      return authorizationApiProvider.patch(request, body)
        .then(
          res => {
            dispatch(patchProfileSucess(res.data.data))
          },
          error => dispatch(patchProfileFailure(error))
        )
    }
  }
  
  export default patchProfile
  