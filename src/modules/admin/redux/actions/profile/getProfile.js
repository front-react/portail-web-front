import {
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getProfileRequest = (request) => {
  return {
    type: GET_PROFILE_REQUEST,
    request
  }
}

const getProfileSucess = (data) => {

  return {
    type: GET_PROFILE_SUCCESS,
    data
  }
}

const getProfileFailure = (error) => {

  return {
    type: GET_PROFILE_FAILURE,
    error
  }
}

const getRequest = (id) => {

  return '/profiles/'+id
}

export const getProfile = (id, filter = null) => {

  let request = getRequest(id)

  return (dispatch, getState) => {
    dispatch(getProfileRequest(request))

    return authorizationApiProvider.get(request)
      .then(
        res => {
          dispatch(getProfileSucess(res.data.data))
        },
        error => dispatch(getProfileFailure())
      )
  }
}

export default getProfile
