import {
    DELETE_PROFILE_REQUEST,
    DELETE_PROFILE_SUCCESS,
    DELETE_PROFILE_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const deleteProfileRequest = (request) => {
    return {
      type: DELETE_PROFILE_REQUEST,
      request
    }
  }
  
  
  const deleteProfileSucess = (id) => {
  
    return {
      type: DELETE_PROFILE_SUCCESS,
      data: id
    }
  }
  const deleteProfileFailure = (error) => {
  
    return {
      type: DELETE_PROFILE_FAILURE,
      error
    }
  }
  
  const getRequest = (id) => {
    return '/profiles/'+id
  }
  
  
  export const deleteProfile = (idProfile) => {

    let request = getRequest(idProfile)
  
  
    return dispatch => {
      dispatch(deleteProfileRequest(request))
  
      return authorizationApiProvider.delete(request)
        .then(
          res => {
              dispatch(deleteProfileSucess(idProfile))
          },
          error => dispatch(deleteProfileFailure(error))
        )
    }
  }
  
  export default deleteProfile
  