import {
  GET_USER_LIST_REQUEST,
  GET_USER_LIST_SUCCESS,
  GET_USER_LIST_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getUsersRequest = (request) => {
  return {
    type: GET_USER_LIST_REQUEST,
    request
  }
};

const getUsersSucess = (data) => {

  return {
    type: GET_USER_LIST_SUCCESS,
    data
  }
};

const getUsersFailure = (error) => {

  return {
    type: GET_USER_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/users?'+filter
  }

  return '/users'
};

export const getUsersList = (filter = null) => {

  let request = getRequest(filter)

  return dispatch => {
    dispatch(getUsersRequest(request))


    return authorizationApiProvider.get(request)
      .then(
        res => dispatch(getUsersSucess(res.data.data)),
        error => {
          dispatch(getUsersFailure())
        }
      )
  }
}

export default getUsersList
