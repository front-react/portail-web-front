import {
    PATCH_USER_REQUEST,
    PATCH_USER_SUCCESS,
    PATCH_USER_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const patchUserRequest = (request) => {
    return {
      type: PATCH_USER_REQUEST,
      request
    }
  }
  
  const patchUserSucess = (data) => {
  
    return {
      type: PATCH_USER_SUCCESS,
      data
    }
  }
  
  const patchUserFailure = (error) => {
  
    return {
      type: PATCH_USER_FAILURE,
      error
    }
  }
  
  const getRequest = (userId) => {
  
    return `/users/${userId}`
  }
  
  
  export const patchUser = (userId, bodyData) => {
  
    let request = getRequest(userId)

    const body = {
        data: bodyData
    }
  
    return (dispatch, getState) => {
      dispatch(patchUserRequest(request))
  
      return authorizationApiProvider.patch(request, body)
        .then(
          res => {
            dispatch(patchUserSucess(res.data.data))
          },
          error => dispatch(patchUserFailure(error))
        )
    }
  }
  
  export default patchUser
  