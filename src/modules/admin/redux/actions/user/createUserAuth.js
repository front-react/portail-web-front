import {
    CREATE_USER_AUTH_REQUEST,
    CREATE_USER_AUTH_SUCCESS,
    CREATE_USER_AUTH_FAILURE,
  } from "../../types";
  import {
    authApiProvider,
  } from "../../../../../utils/api/providers";
  
  const createUserAuthRequest = request => {
    return {
      type: CREATE_USER_AUTH_REQUEST,
      request,
    };
  };
  
  const createUserAuthSucess = data => {
    return {
      type: CREATE_USER_AUTH_SUCCESS,
      data,
    };
  };
  
  
  const createUserAuthFailure = error => {
    return {
      type: CREATE_USER_AUTH_FAILURE,
      error,
    };
  };
  

  
  export const createUserAuth = (requestBody) => {

    const request = 'http://10.33.100.40/auth/public/users';
  
    return dispatch => {
      dispatch(createUserAuthRequest(request));
      const dataBody = `username=${requestBody.username}&password=${requestBody.password}`;
      return authApiProvider.request({ 
        method: 'post',
        url: '/users',
        baseUrl: 'http://10.33.100.40/auth/public',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        data: dataBody,
        transformRequest: [(data, headers) => {
          delete headers.common.Authorization
          return data;
        }]
      }).then(
        res => {
          dispatch(createUserAuthSucess(res));
        },
        error => dispatch(createUserAuthFailure(error)),
      );
    };
  };
  
  export default createUserAuth;
  