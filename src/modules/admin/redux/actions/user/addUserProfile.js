import {
  ADD_USER_PROFILE_REQUEST,
  ADD_USER_PROFILE_SUCCESS,
  ADD_USER_PROFILE_FAILURE,
} from "../../types";
import { authorizationApiProvider } from "../../../../../utils/api/providers";

const addUserProfileRequest = request => {
  return {
    type: ADD_USER_PROFILE_REQUEST,
    request,
  };
};

const addUserProfileSucess = data => {
  return {
    type: ADD_USER_PROFILE_SUCCESS,
    data,
  };
};
const addUserProfileFailure = error => {
  return {
    type: ADD_USER_PROFILE_FAILURE,
    error,
  };
};

const getRequest = userId => {
  return `/users/${userId}/profiles`;
};

export const addUserProfile = body => {
  let request = getRequest(body.userId);

  const bodyRequest = {
    data: {
      profileId: body.profileId,
    },
  };

  return dispatch => {
    dispatch(addUserProfileRequest(request));

    return authorizationApiProvider.post(request, bodyRequest).then(
      res => {
        dispatch(addUserProfileSucess(res.data.data));
      },
      error => dispatch(addUserProfileFailure(error)),
    );
  };
};

export default addUserProfile;
