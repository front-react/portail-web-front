import {
    DELETE_USER_REQUEST,
    DELETE_USER_SUCCESS,
    DELETE_USER_FAILURE
  } from '../../types'
  //import { browserHistory } from 'react-router'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const deleteUserRequest = (request) => {
    return {
      type: DELETE_USER_REQUEST,
      request
    }
  }
  
  
  const deleteUserSucess = (id) => {
  
    return {
      type: DELETE_USER_SUCCESS,
      data: id
    }
  }
  const deleteUserFailure = (error) => {
  
    return {
      type: DELETE_USER_FAILURE,
      error
    }
  }
  
  const getRequest = (id) => {
    return `/users/${id}`
  }
  
  
  export const deleteUser = (id) => {

    let request = getRequest(id)
  
    return dispatch => {
      dispatch(deleteUserRequest(request))
  
      return authorizationApiProvider.delete(request)
        .then(
          res => { 
            //browserHistory.push('/user')
            dispatch(deleteUserSucess(id)) 
          },
          error => dispatch(deleteUserFailure(error))
        )
    }
  }
  
  export default deleteUser
  