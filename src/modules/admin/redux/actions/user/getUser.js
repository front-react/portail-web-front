import {
  GET_USER_REQUEST,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getUserRequest = (request = '/users') => {
  return {
    type: GET_USER_REQUEST,
    request
  }
}

const getUserSucess = (data) => {

  return {
    type: GET_USER_SUCCESS,
    data
  }
}

const getUserFailure = (error) => {

  return {
    type: GET_USER_FAILURE,
    error
  }
}

const getRequest = (id) => {

  return '/users/'+id
}


export const getUser = (id) => {
  let request = getRequest(id)

  return (dispatch, getState) => {
    dispatch(getUserRequest(request))
    
    return authorizationApiProvider.get(request)
      .then(
        res => {
          dispatch(getUserSucess(res.data.data))
        },
        error => dispatch(getUserFailure())
      )
  }
}

export default getUser

/*

 return dispatch => {
 dispatch(getUserRequest(request))


 return authorizationApiProvider.get(request)
 .then(
 res => dispatch(getUserSucess(res.data)),
 error => {
 dispatch(getUserFailure())

 }
 )
 }

 */