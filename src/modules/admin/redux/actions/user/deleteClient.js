import {
    DELETE_CLIENT_USER_REQUEST,
    DELETE_CLIENT_USER_SUCCESS,
    DELETE_CLIENT_USER_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const deleteClientUserRequest = (request) => {
    return {
      type: DELETE_CLIENT_USER_REQUEST,
      request
    }
  }
  
  
  const deleteClientUserSucess = (id) => {
  
    return {
      type: DELETE_CLIENT_USER_SUCCESS,
      data: id
    }
  }
  const deleteClientUserFailure = (error) => {
  
    return {
      type: DELETE_CLIENT_USER_FAILURE,
      error
    }
  }
  
  const getRequest = (userId, clientId) => {
    return `/users/${userId}/clients/${clientId}`
  }
  
  
  export const deleteClient = (userId, clientId) => {

    let request = getRequest(userId, clientId)
  
    return dispatch => {
      dispatch(deleteClientUserRequest(request))
  
      return authorizationApiProvider.request({
        method: 'delete',
        url: `/users/${userId}/clients/${clientId}`,
        baseUrl: 'http://10.33.100.40/authorization-api/public',
      })
        .then(
          res => { 
            dispatch(deleteClientUserSucess(clientId)) 
          },
          error => dispatch(deleteClientUserFailure(error))
        )
    }
  }
  
  export default deleteClient
  