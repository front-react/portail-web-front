import {
    GET_USER_SUCCESS,
  } from '../../types'
  
  
  const refreshUserSucess = (data) => {
  
    return {
      type: GET_USER_SUCCESS,
      data
    }
  };
  
  export const refreshUser = () => {

    return dispatch => {
        dispatch(refreshUserSucess(null))
    }
  };
  
  export default refreshUser
  