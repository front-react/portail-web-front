import {
  SET_CLIENT_REQUEST,
  SET_CLIENT_SUCCESS,
  SET_CLIENT_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const setClientRequest = (request) => {
    return {
      type: SET_CLIENT_REQUEST,
      request
    }
  };
  
  const setClientSucess = (data) => {
  
    return {
      type: SET_CLIENT_SUCCESS,
      data
    }
  };
  
  const setClientFailure = (error) => {
  
    return {
      type: SET_CLIENT_FAILURE,
      error
    }
  };
  
  
  
  export const setClient = (userId, bodyData) => {
  
    let request = `users/${userId}/clients`;
  
    const body = {
      data: bodyData
      }
  
    return dispatch => {
      dispatch(setClientRequest(request))
  
      return authorizationApiProvider.post(request, body)
        .then(
          res => dispatch(setClientSucess(res.data.data)),
          error => {
            dispatch(setClientFailure(error))
  
          }
        )
    }
  }
  
  export default setClient
  