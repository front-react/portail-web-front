import {
    ADD_ASSIGNMENT_PROFILE_USER_REQUEST,
    ADD_ASSIGNMENT_PROFILE_USER_SUCCESS,
    ADD_ASSIGNMENT_PROFILE_USER_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  import displayError from '../../../../../redux/actions/displayError';
  
  const addAssignmentProfileUserRequest = (request) => {
    return {
      type: ADD_ASSIGNMENT_PROFILE_USER_REQUEST,
      request
    }
  }
  
  const addAssignmentProfileUserSucess = (data) => {
  
    return {
      type: ADD_ASSIGNMENT_PROFILE_USER_SUCCESS,
      data
    }
  }
  const addAssignmentProfileUserFailure = (error) => {
  
    return {
      type: ADD_ASSIGNMENT_PROFILE_USER_FAILURE,
      error
    }
  }
  
  const getRequest = (userId) => {
    return `/users/${userId}/profiles`
  }
  
  
  export const addAssignmentProfileUser = (userId, profileId) => {


    let request = getRequest(userId)

    const bodyRequest = {
        data: {
          profileId: profileId
        }
    }
  
    return dispatch => {
      dispatch(addAssignmentProfileUserRequest(request))
  
      return authorizationApiProvider.post(request, bodyRequest)
        .then(
          res => { dispatch(addAssignmentProfileUserSucess(res.data.data)) },

          error => {
            dispatch(addAssignmentProfileUserFailure(error));
            dispatch(displayError(error));
          }
        )
    }
  }
  
  export default addAssignmentProfileUser
  