import {
    GET_CLIENTS_REQUEST,
    GET_CLIENTS_SUCCESS,
    GET_CLIENTS_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getClientsRequest = (request) => {
    return {
      type: GET_CLIENTS_REQUEST,
      request
    }
  }
  
  const getClientsSucess = (data) => {
  
    return {
      type: GET_CLIENTS_SUCCESS,
      data
    }
  }
  
  const getClientsFailure = (error) => {
  
    return {
      type: GET_CLIENTS_FAILURE,
      error
    }
  }
  
  const getRequest = () => {
  
    return `/clients`
  }
  
  
  export const getClients = () => {
  
    let request = getRequest()

    return (dispatch, getState) => {
      dispatch(getClientsRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => {
            dispatch(getClientsSucess(res.data.data))
          },
          error => dispatch(getClientsFailure(error))
        )
    }
  }
  
  export default getClients
  