import {
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_USER_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getUserInformationRequest = (request) => {
    return {
      type: GET_USER_REQUEST,
      request
    }
  };
  
  const getUserInformationSucess = (data) => {
  
    return {
      type: GET_USER_SUCCESS,
      data
    }
  };
  
  const getUserInformationFailure = (error) => {
  
    return {
      type: GET_USER_FAILURE,
      error
    }
  };
  
  const getRequest = (userId) => {
  
    return '/users/'+userId
  };
  
  
  export const getUserInformation = (userId) => {
    let request = getRequest(userId);
  
    return dispatch => {
      dispatch(getUserInformationRequest(request));
  
      return authorizationApiProvider.get(request)
        .then(
          res => {
            dispatch(getUserInformationSucess(res.data.data))
          },
          error => dispatch(getUserInformationFailure())
        )
    }
  };
  
  export default getUserInformation
  