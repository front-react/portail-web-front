import {
  ADD_USER_CLIENT_REQUEST,
  ADD_USER_CLIENT_SUCCESS,
  ADD_USER_CLIENT_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const addUserClientRequest = (request) => {
  return {
    type: ADD_USER_CLIENT_REQUEST,
    request
  }
}

const addUserClientSucess = (data) => {

  return {
    type: ADD_USER_CLIENT_SUCCESS,
    data
  }
}
const addUserClientFailure = (error) => {

  return {
    type: ADD_USER_CLIENT_FAILURE,
    error
  }
}

const getRequest = (idUser) => {
  return '/users/'+idUser+'/clients'
}


export const addUserClient = (body) => {
  let request = getRequest(body.idUser)

  return dispatch => {
    dispatch(addUserClientRequest(request))

    return authorizationApiProvider.post(request,body)
      .then(
        res => { dispatch(addUserClientSucess(res.data.data)) },
        error => dispatch(addUserClientFailure(error))
      )
  }
}

export default addUserClient
