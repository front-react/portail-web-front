import {
    GET_USER_LIST_SUCCESS,
  } from '../../types'
  

  
  const sucess = (data) => {
    return {
      type: GET_USER_LIST_SUCCESS,
      data
    }
  };
  
  export const initialiseUserList = () => {
  
    return dispatch => {
        dispatch(sucess(null))
    }
  }
  
  export default initialiseUserList
  