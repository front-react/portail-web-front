import {
  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESS,
  CREATE_USER_CURRENT_SUCCESS,
  CREATE_USER_FAILURE,
} from "../../types";
import {
  authorizationApiProvider,
} from "../../../../../utils/api/providers";

import { createUserAuth } from './createUserAuth'
import displayError from '../../../../../redux/actions/displayError';

const createUserRequest = request => {
  return {
    type: CREATE_USER_REQUEST,
    request,
  };
};

const createUserSucess = data => {
  return {
    type: CREATE_USER_SUCCESS,
    data,
  };
};

const createUsercurrentSucess = data => {
  return {
    type: CREATE_USER_CURRENT_SUCCESS,
    data,
  };
};

const createUserFailure = error => {
  return {
    type: CREATE_USER_FAILURE,
    error,
  };
};

const getRequest = () => {
  return "/users";
};

export const createUser = body => {
  let request = getRequest();

  const requestUser = {
    data: {
      username: body.username,
      personId: body.personId,
    },
  };

  return dispatch => {
    dispatch(createUserRequest(request));

    return authorizationApiProvider.post(request, requestUser).then(
      res => {
        dispatch(createUserSucess(res.data.data));
        dispatch(createUsercurrentSucess(res.data.data));
        dispatch(createUserAuth(body));
      },
      error =>{
        dispatch(createUserFailure(error));
        dispatch(displayError(error));
      },
    );
  };
};

export default createUser;
