import {
  SET_CLIENT_DEFAULT_REQUEST,
  SET_CLIENT_DEFAULT_SUCCESS,
  SET_CLIENT_DEFAULT_FAILURE,
} from "../../types";
import { authorizationApiProvider } from "../../../../../utils/api/providers";

const setClientDefaultRequest = request => {
  return {
    type: SET_CLIENT_DEFAULT_REQUEST,
    request,
  };
};

const setClientDefaultSucess = data => {
  return {
    type: SET_CLIENT_DEFAULT_SUCCESS,
    data,
  };
};

const setClientDefaultFailure = error => {
  return {
    type: SET_CLIENT_DEFAULT_FAILURE,
    error,
  };
};

export const setClientDefault = (userId, clientId) => {
  let request = `users/${userId}/clients/${clientId}/default`;

  // const bodyRequest = {
  //   data: bodyData
  // }
  return dispatch => {
    dispatch(setClientDefaultRequest(request));

    return authorizationApiProvider
      .patch(request)
      .then(
        res => dispatch(setClientDefaultSucess(res.data.data)),
        error => dispatch(setClientDefaultFailure(error)),
      );
  };
};

export default setClientDefault;
