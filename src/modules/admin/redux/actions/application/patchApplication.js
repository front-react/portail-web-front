import {
  PATCH_APPLICATION_REQUEST,
  PATCH_APPLICATION_SUCCESS,
  PATCH_APPLICATION_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'


const patchApplicationRequest = (request) => {
  return {
    type: PATCH_APPLICATION_REQUEST,
    request
  }
}

const patchApplicationSucess = (data) => {

  return {
    type: PATCH_APPLICATION_SUCCESS,
    data
  }
}

const patchApplicationFailure = (error) => {

  return {
    type: PATCH_APPLICATION_FAILURE,
    error
  }
}

const getRequest = (id) => {
  return '/applications/'+id
}


export const patchApplication = (id, body) => {
  
  let request = getRequest(id)

  const applicationPatch = {
    data:{
      applicationName: body.applicationName
    }
  }

  return dispatch => {
    dispatch(patchApplicationRequest(request))

    return authorizationApiProvider.patch(request, applicationPatch)
      .then(
        res => {
          dispatch(patchApplicationSucess(res.data.data))
        },
        error => dispatch(patchApplicationFailure(error))
      )
  }
}

export default patchApplication
