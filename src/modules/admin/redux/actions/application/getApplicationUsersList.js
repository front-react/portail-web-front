import {
    GET_APPLICATION_USERS_LIST_REQUEST,
    GET_APPLICATION_USERS_LIST_SUCCESS,
    GET_APPLICATION_USERS_LIST_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getApplicationUsersListRequest = (request) => {
    return {
      type: GET_APPLICATION_USERS_LIST_REQUEST,
      request
    }
  }
  
  const getApplicationUsersListSucess = (data) => {
  
    return {
      type: GET_APPLICATION_USERS_LIST_SUCCESS,
      data
    }
  }
  const getApplicationUsersListFailure = (error) => {
  
    return {
      type: GET_APPLICATION_USERS_LIST_FAILURE,
      error
    }
  }

  export const getApplicationUsersList = (id) => {
      
    let request = 'applications/'+id+'/users'
    
    return dispatch => {
      dispatch(getApplicationUsersListRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getApplicationUsersListSucess(res.data.data)),
          error => dispatch(getApplicationUsersListFailure(error))
        )
    }
  }
  
  export default getApplicationUsersList
  