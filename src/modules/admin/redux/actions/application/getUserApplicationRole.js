import {
    GET_USER_APPLICATION_ROLE_REQUEST,
    GET_USER_APPLICATION_ROLE_SUCCESS,
    GET_USER_APPLICATION_ROLE_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getUserApplicationRoleRequest = (request) => {
    return {
      type: GET_USER_APPLICATION_ROLE_REQUEST,
      request
    }
  }
  
  const getUserApplicationRoleSucess = (data) => {
  
    return {
      type: GET_USER_APPLICATION_ROLE_SUCCESS,
      data
    }
  }
  const getUserApplicationRoleFailure = (error) => {
  
    return {
      type: GET_USER_APPLICATION_ROLE_FAILURE,
      error
    }
  }

  export const getUsersApplicationRole = (id) => {
      
    let request = '/users/'+id+'/roles'
    
    
    return dispatch => {
      dispatch(getUserApplicationRoleRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getUserApplicationRoleSucess(res.data.data)),
          error => dispatch(getUserApplicationRoleFailure(error))
        )
    }
  }
  
  export default getUsersApplicationRole
  