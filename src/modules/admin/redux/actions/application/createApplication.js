import {
  CREATE_APPLICATION_REQUEST,
  CREATE_APPLICATION_SUCCESS,
  CREATE_APPLICATION_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'



const createApplicationRequest = (request) => {
  return {
    type: CREATE_APPLICATION_REQUEST,
    request
  }
}


const createApplicationSucess = (data) => {

  return {
    type: CREATE_APPLICATION_SUCCESS,
    data
  }
}
const createApplicationFailure = (error) => {

  return {
    type: CREATE_APPLICATION_FAILURE,
    error
  }
}

const getRequest = () => {
  return '/applications'
}


export const createApplication = (body) => {
  const applicationBody = {
    data: body
  }
  let request = getRequest()

  return dispatch => {
    dispatch(createApplicationRequest(request))

    return authorizationApiProvider.post(request,applicationBody)
      .then(
        res => dispatch(createApplicationSucess(res.data.data)),
        error => dispatch(createApplicationFailure(error))
      )
  }
}

export default createApplication
