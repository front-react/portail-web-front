import {
    DELETE_ROLE_APPLICATION_PROFILE_REQUEST,
    DELETE_ROLE_APPLICATION_PROFILE_SUCCESS,
    DELETE_ROLE_APPLICATION_PROFILE_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  
  const deleteRoleApplicationProfileRequest = (request) => {
    return {
      type: DELETE_ROLE_APPLICATION_PROFILE_REQUEST,
      request
    }
  }
  
  const deleteRoleApplicationProfileSucess = (data) => {
  
    return {
      type: DELETE_ROLE_APPLICATION_PROFILE_SUCCESS,
      data
    }
  }
  
  const deleteRoleApplicationProfileFailure = (error) => {
  
    return {
      type: DELETE_ROLE_APPLICATION_PROFILE_FAILURE,
      error
    }
  }
  
  const getRequest = (profileId, roleId) => {
    return `profiles/${profileId}/roles/${roleId}`
  }
  
  
  export const deleteRoleApplicationProfile = (profileId, roleId) => {
    
    let request = getRequest(profileId, roleId)
  
    return dispatch => {
      dispatch(deleteRoleApplicationProfileRequest(request))
  
      return authorizationApiProvider.delete(request)
        .then(
          res => {
            dispatch(deleteRoleApplicationProfileSucess(roleId))
          },
          error => dispatch(deleteRoleApplicationProfileFailure(error))
        )
    }
  }
  
  export default deleteRoleApplicationProfile
  