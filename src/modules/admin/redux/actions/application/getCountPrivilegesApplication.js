import {
    GET_COUNT_PRIVILEGES_APPLICATION_REQUEST,
    GET_COUNT_PRIVILEGES_APPLICATION_SUCCESS,
    GET_COUNT_PRIVILEGES_APPLICATION_FAILURE,
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getCountPrivilegesApplicationRequest = (request) => {
    return {
      type: GET_COUNT_PRIVILEGES_APPLICATION_REQUEST,
      request
    }
  }
  
  const getCountPrivilegesApplicationSucess = (data) => {
  
    return {
      type: GET_COUNT_PRIVILEGES_APPLICATION_SUCCESS,
      data
    }
  }
  const getCountPrivilegesApplicationFailure = (error) => {
  
    return {
      type: GET_COUNT_PRIVILEGES_APPLICATION_FAILURE,
      error
    }
  }

  export const getCountPrivilegesApplication = (id) => {
    let request = '/applications/'+id+'/privileges/counts'
  
    return dispatch => {
      dispatch(getCountPrivilegesApplicationRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getCountPrivilegesApplicationSucess(res.data.data)),
          error => dispatch(getCountPrivilegesApplicationFailure(error))
        )
    }
  }
  
  export default getCountPrivilegesApplication
  