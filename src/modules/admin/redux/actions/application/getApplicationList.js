import {
  GET_APPLICATION_LIST_REQUEST,
  GET_APPLICATION_LIST_SUCCESS,
  GET_APPLICATION_LIST_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getApplicationListRequest = (request) => {
  return {
    type: GET_APPLICATION_LIST_REQUEST,
    request
  }
}

const getApplicationListSucess = (data) => {

  return {
    type: GET_APPLICATION_LIST_SUCCESS,
    data: data.data
  }
}
const getApplicationListFailure = (error) => {

  return {
    type: GET_APPLICATION_LIST_FAILURE,
    error
  }
}

const getRequest = (filter) => {

  if(filter != null) {
    return '/applications?'+filter
  }
  return '/applications'
}


export const getApplicationList = (filter = null) => {
  let request = getRequest(filter)

  return dispatch => {
    dispatch(getApplicationListRequest(request))

    return authorizationApiProvider.get(request)
      .then(
        res => dispatch(getApplicationListSucess(res.data)),
        error => dispatch(getApplicationListFailure(error))
      )
  }
}

export default getApplicationList
