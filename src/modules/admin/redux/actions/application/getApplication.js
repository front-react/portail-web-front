import {
  GET_APPLICATION_REQUEST,
  GET_APPLICATION_SUCCESS,
  GET_APPLICATION_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getApplicationRequest = (request) => {
  return {
    type: GET_APPLICATION_REQUEST,
    request
  }
}

const getApplicationSucess = (data) => {

  return {
    type: GET_APPLICATION_SUCCESS,
    data
  }
}
const getApplicationFailure = (error) => {

  return {
    type: GET_APPLICATION_FAILURE,
    error
  }
}

const getRequest = (id) => {
    return '/applications/'+id

};




export const getApplication = (id) => {
  let request = getRequest(id)


  return dispatch => {
    dispatch(getApplicationRequest(request));

    return authorizationApiProvider.get(request)
      .then(
        res => dispatch(getApplicationSucess(res.data.data)),

        error => dispatch(getApplicationFailure(error))
      )
  }
}

export default getApplication
