import {
    GET_APPLICATION_ROLE_PRIVILEGES_LIST_REQUEST,
    GET_APPLICATION_ROLE_PRIVILEGES_LIST_SUCCESS,
    GET_APPLICATION_ROLE_PRIVILEGES_LIST_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getApplicationRolePrivilegesListRequest = (request) => {
    return {
      type: GET_APPLICATION_ROLE_PRIVILEGES_LIST_REQUEST,
      request
    }
  }
  
  const getApplicationRolePrivilegesListSucess = (data) => {
  
    return {
      type: GET_APPLICATION_ROLE_PRIVILEGES_LIST_SUCCESS,
      data
    }
  }
  const getApplicationRolePrivilegesListFailure = (error) => {
  
    return {
      type: GET_APPLICATION_ROLE_PRIVILEGES_LIST_FAILURE,
      error
    }
  }

  export const getApplicationRolePrivilegesList = (id) => {
      
    let request = 'applications/'+id
    
    return dispatch => {
      dispatch(getApplicationRolePrivilegesListRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => {
            dispatch(getApplicationRolePrivilegesListSucess(res.data.data))
          },
          error => dispatch(getApplicationRolePrivilegesListFailure(error))
        )
    }
  }
  
  export default getApplicationRolePrivilegesList
  