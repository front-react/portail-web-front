import {
    GET_APPLICATION_LIST_SUCCESS,
    GET_ROLE_LIST_SUCCESS,
    GET_ROLE_APPLICATION_PROFILE_SUCCESS
  } from '../../types'

  
  const removeApplicationListSucess = (data) => {
  
    return {
      type: GET_APPLICATION_LIST_SUCCESS,
      data
    }
  }
  const removeRoleListSucess = (data) => {
  
    return {
      type: GET_ROLE_LIST_SUCCESS,
      data
    }
  }
  const removeRoleProfileListSucess = (data) => {
  
    return {
      type: GET_ROLE_APPLICATION_PROFILE_SUCCESS,
      data
    }
  }


  export const initialiseProfileRole = () => {
    
    return dispatch => {

      dispatch(removeApplicationListSucess(null));
      dispatch(removeRoleListSucess(null));
      dispatch(removeRoleProfileListSucess(null));

    }
  }
  
  export default initialiseProfileRole
  