import {
    GET_COUNT_USERS_REQUEST,
    GET_COUNT_USERS_SUCCESS,
    GET_COUNT_USERS_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getCountUsersRequest = (request) => {
    return {
      type: GET_COUNT_USERS_REQUEST,
      request
    }
  }
  
  const getCountUsersSucess = (data) => {
  
    return {
      type: GET_COUNT_USERS_SUCCESS,
      data
    }
  }
  const getCounttUsersFailure = (error) => {
  
    return {
      type: GET_COUNT_USERS_FAILURE,
      error
    }
  }

  export const getCountUsers = () => {
      
    let request = '/users/counts'
    
    return dispatch => {
      dispatch(getCountUsersRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getCountUsersSucess(res.data.data)),
          error => dispatch(getCounttUsersFailure(error))
        )
    }
  }
  
  export default getCountUsers
  