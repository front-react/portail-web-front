import {
    GET_COUNT_ROLES_APPLICATION_REQUEST,
    GET_COUNT_ROLES_APPLICATION_SUCCESS,
    GET_COUNT_ROLES_APPLICATION_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getCountRolesApplicationRequest = (request) => {
    return {
      type: GET_COUNT_ROLES_APPLICATION_REQUEST,
      request
    }
  }
  
  const getCountRolesApplicationSucess = (data) => {
  
    return {
      type: GET_COUNT_ROLES_APPLICATION_SUCCESS,
      data
    }
  }
  const getCountRolesApplicationFailure = (error) => {
  
    return {
      type: GET_COUNT_ROLES_APPLICATION_FAILURE,
      error
    }
  }

  export const getCountRolesApplication = (id) => {
      
    let request = '/applications/'+id+'/roles/counts'
    
    return dispatch => {
      dispatch(getCountRolesApplicationRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getCountRolesApplicationSucess(res.data.data)),
          error => dispatch(getCountRolesApplicationFailure(error))
        )
    }
  }
  
  export default getCountRolesApplication
  