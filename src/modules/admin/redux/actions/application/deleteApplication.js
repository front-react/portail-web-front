import {
  DELETE_APPLICATION_REQUEST,
  DELETE_APPLICATION_SUCCESS,
  DELETE_APPLICATION_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const deleteApplicationRequest = (request) => {
  return {
    type: DELETE_APPLICATION_REQUEST,
    request
  }
}


const deleteApplicationSucess = (id) => {

  return {
    type: DELETE_APPLICATION_SUCCESS,
    data: id
  }
}
const deleteApplicationFailure = (error) => {

  return {
    type: DELETE_APPLICATION_FAILURE,
    error
  }
}

const getRequest = (id) => {
  return '/applications/'+id
}


export const deleteApplication = (id) => {
  let request = getRequest(id)


  return dispatch => {
    dispatch(deleteApplicationRequest(request))

    return authorizationApiProvider.delete(request)
      .then(
        res => {
            dispatch(deleteApplicationSucess(id))
        },
        error => dispatch(deleteApplicationFailure(error))
      )
  }
}

export default deleteApplication
