import {
    GET_COUNT_PROFILES_REQUEST,
    GET_COUNT_PROFILES_SUCCESS,
    GET_COUNT_PROFILES_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getCountProfilesRequest = (request) => {
    return {
      type: GET_COUNT_PROFILES_REQUEST,
      request
    }
  }
  
  const getCounProfilesSucess = (data) => {
  
    return {
      type: GET_COUNT_PROFILES_SUCCESS,
      data
    }
  }
  const getCountProfilesFailure = (error) => {
  
    return {
      type: GET_COUNT_PROFILES_FAILURE,
      error
    }
  }

  export const getCountProfiles = () => {
      
    let request = '/profiles/counts'
    
    return dispatch => {
      dispatch(getCountProfilesRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getCounProfilesSucess(res.data.data)),
          error => dispatch(getCountProfilesFailure(error))
        )
    }
  }
  
  export default getCountProfiles
  