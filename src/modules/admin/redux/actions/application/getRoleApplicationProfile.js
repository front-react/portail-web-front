import {
  GET_ROLE_APPLICATION_PROFILE_REQUEST,
  GET_ROLE_APPLICATION_PROFILE_SUCCESS,
  GET_ROLE_APPLICATION_PROFILE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'


const getRoleApplicationProfileRequest = (request) => {
  return {
    type: GET_ROLE_APPLICATION_PROFILE_REQUEST,
    request
  }
}

const getRoleApplicationProfileSucess = (data) => {

  return {
    type: GET_ROLE_APPLICATION_PROFILE_SUCCESS,
    data
  }
}

const getRoleApplicationProfileFailure = (error) => {

  return {
    type: GET_ROLE_APPLICATION_PROFILE_FAILURE,
    error
  }
}

const getRequest = (profileId) => {
  return `profiles/${profileId}/roles`
}


export const getRoleApplicationProfile = (profileId) => {
  
  let request = getRequest(profileId)

  return dispatch => {
    dispatch(getRoleApplicationProfileRequest(request))

    return authorizationApiProvider.get(request)
      .then(
        res => {
          dispatch(getRoleApplicationProfileSucess(res.data.data))
        },
        error => dispatch(getRoleApplicationProfileFailure(error))
      )
  }
}

export default getRoleApplicationProfile
