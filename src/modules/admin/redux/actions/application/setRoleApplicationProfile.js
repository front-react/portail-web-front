import {
  SET_ROLE_APPLICATION_PROFILE_REQUEST,
  SET_ROLE_APPLICATION_PROFILE_SUCCESS,
  SET_ROLE_APPLICATION_PROFILE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'


const setRoleApplicationProfileRequest = (request) => {
  return {
    type: SET_ROLE_APPLICATION_PROFILE_REQUEST,
    request
  }
}

const setRoleApplicationProfileSucess = (data) => {

  return {
    type: SET_ROLE_APPLICATION_PROFILE_SUCCESS,
    data
  }
}

const setRoleApplicationProfileFailure = (error) => {

  return {
    type: SET_ROLE_APPLICATION_PROFILE_FAILURE,
    error
  }
}

const getRequest = (profileId, roleId) => {
  return `profiles/${profileId}/roles/${roleId}`
}


export const setRoleApplicationProfile = (profileId, roleId) => {
  
  let request = getRequest(profileId, roleId)

  return dispatch => {
    dispatch(setRoleApplicationProfileRequest(request))

    return authorizationApiProvider.post(request)
      .then(
        res => {
          dispatch(setRoleApplicationProfileSucess(res.data.data))
        },
        error => dispatch(setRoleApplicationProfileFailure(error))
      )
  }
}

export default setRoleApplicationProfile
