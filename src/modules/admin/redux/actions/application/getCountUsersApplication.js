import {
    GET_COUNT_USERS_APPLICATION_REQUEST,
    GET_COUNT_USERS_APPLICATION_SUCCESS,
    GET_COUNT_USERS_APPLICATION_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getCountUsersApplicationRequest = (request) => {
    return {
      type: GET_COUNT_USERS_APPLICATION_REQUEST,
      request
    }
  }
  
  const getCounUsersApplicationSucess = (data) => {
  
    return {
      type: GET_COUNT_USERS_APPLICATION_SUCCESS,
      data
    }
  }
  const getCountUsersApplicationFailure = (error) => {
  
    return {
      type: GET_COUNT_USERS_APPLICATION_FAILURE,
      error
    }
  }

  export const getCountUsersApplication = (id) => {
      
    let request = '/applications/'+id+'/users/counts'
    
    return dispatch => {
      dispatch(getCountUsersApplicationRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getCounUsersApplicationSucess(res.data.data)),
          error => dispatch(getCountUsersApplicationFailure(error))
        )
    }
  }
  
  export default getCountUsersApplication
  