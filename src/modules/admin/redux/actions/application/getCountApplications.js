import {
    GET_COUNT_APPLICATIONS_REQUEST,
    GET_COUNT_APPLICATIONS_SUCCESS,
    GET_COUNT_APPLICATIONS_FAILURE
  } from '../../types'
  import {
    authorizationApiProvider
  } from '../../../../../utils/api/providers'
  
  const getCountApplicationsRequest = (request) => {
    return {
      type: GET_COUNT_APPLICATIONS_REQUEST,
      request
    }
  }
  
  const getCounApplicationsSucess = (data) => {
  
    return {
      type: GET_COUNT_APPLICATIONS_SUCCESS,
      data
    }
  }
  const getCountApplicationsFailure = (error) => {
  
    return {
      type: GET_COUNT_APPLICATIONS_FAILURE,
      error
    }
  }

  export const getCountApplications = () => {
      
    let request = '/applications/counts'
    
    return dispatch => {
      dispatch(getCountApplicationsRequest(request))
  
      return authorizationApiProvider.get(request)
        .then(
          res => dispatch(getCounApplicationsSucess(res.data.data)),
          error => dispatch(getCountApplicationsFailure(error))
        )
    }
  }
  
  export default getCountApplications
  