import {CREATE_PRIVILEGE_REQUEST, CREATE_PRIVILEGE_SUCCESS, CREATE_PRIVILEGE_FAILURE} from '../../types'
import {authorizationApiProvider} from '../../../../../utils/api/providers'
import getCountPrivilegesApplication from '../application/getCountPrivilegesApplication'

const createPrivilegeRequest = (request) => {
  return {
    type: CREATE_PRIVILEGE_REQUEST,
    request
  }
}

const createPrivilegeSucess = (data) => {

  return {
    type: CREATE_PRIVILEGE_SUCCESS,
    data
  }
}

const createPrivilegeFailure = (error) => {

  return {
    type: CREATE_PRIVILEGE_FAILURE,
    error
  }
}

const getRequest = () => {
  return '/privileges'
}


export const createPrivilege = (RequestBody, applicationId) => {

  const privilegesBody = {
    data: RequestBody
  }

  let request = getRequest()

  return dispatch => {
    dispatch(createPrivilegeRequest(request))

    return authorizationApiProvider.post(request,privilegesBody)
      .then(
        res => {
          dispatch(createPrivilegeSucess(res.data.data))
          dispatch(getCountPrivilegesApplication(applicationId))
        },
        error => dispatch(createPrivilegeFailure(error))
      )
  }
}

export default createPrivilege
