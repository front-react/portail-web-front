import {
  PATCH_PRIVILEGE_REQUEST,
  PATCH_PRIVILEGE_SUCCESS,
  PATCH_PRIVILEGE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const patchPrivilegeRequest = (request) => {
  return {
    type: PATCH_PRIVILEGE_REQUEST,
    request
  }
}

const patchPrivilegeSucess = (data) => {

  return {
    type: PATCH_PRIVILEGE_SUCCESS,
    data
  }
}
const patchPrivilegeFailure = (error) => {

  return {
    type: PATCH_PRIVILEGE_FAILURE,
    error
  }
}

const getRequest = (id) => {
  return '/privileges/'+id
}


export const patchPrivilege = (id, body) => {
  
  let request = getRequest(id)

  const privivegePatch = {
    data: body
  }
  return dispatch => {
    dispatch(patchPrivilegeRequest(request))

    return authorizationApiProvider.patch(request, privivegePatch)
      .then(
        res => {
          dispatch(patchPrivilegeSucess(res.data.data))
        },
        error => dispatch(patchPrivilegeFailure(error))
      )
  }
}

export default patchPrivilege
