import {
  GET_PRIVILEGE_LIST_REQUEST,
  GET_PRIVILEGE_LIST_SUCCESS,
  GET_PRIVILEGE_LIST_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getPrivilegeListRequest = (request) => {

  return {
    type: GET_PRIVILEGE_LIST_REQUEST,
    request
  }
}

const getPrivilegeListSucess = (data) => {

  return {
    type: GET_PRIVILEGE_LIST_SUCCESS,
    data
  }
}
const getPrivilegeListFailure = (error) => {

  return {
    type: GET_PRIVILEGE_LIST_FAILURE,
    error
  }
}



const getRequest = (filter) => {
  if(filter != null) {
    return '/privileges?'+filter
  }

  return '/privileges'
}


export const getPrivilegeList = (filter = null) => {

  let request = getRequest(filter)

  return dispatch => {
    dispatch(getPrivilegeListRequest(request))


    return authorizationApiProvider.get(request)
      .then(
        res => {
          dispatch(getPrivilegeListSucess(res.data.data))

        },
        error => {
          dispatch(getPrivilegeListFailure())

        }
      )
  }
}

export default getPrivilegeList
