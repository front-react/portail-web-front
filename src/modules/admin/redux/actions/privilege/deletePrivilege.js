import {DELETE_PRIVILEGE_REQUEST, DELETE_PRIVILEGE_SUCCESS, DELETE_PRIVILEGE_FAILURE} from '../../types'
import {authorizationApiProvider} from '../../../../../utils/api/providers'
import deleteRolePrivilege from '../role/deleteRolePrivilege'

const deletePrivilegeRequest = (request) => {
  return {
    type: DELETE_PRIVILEGE_REQUEST,
    request
  }
}

const deletePrivilegeSucess = (id) => {

  return {
    type: DELETE_PRIVILEGE_SUCCESS,
    data: id
  }
}
const deletePrivilegeFailure = (error) => {

  return {
    type: DELETE_PRIVILEGE_FAILURE,
    error
  }
}

const getRequest = (id) => {
  return '/privileges/'+id
}


export const deletePrivilege = (idPrivilege, roleList) => {

  let request = getRequest(idPrivilege)

  return dispatch => {
    dispatch(deletePrivilegeRequest(request))

    return authorizationApiProvider.delete(request)
      .then(
        res => {
          dispatch(deletePrivilegeSucess(idPrivilege))
          roleList.roles.forEach((role) => {
            dispatch(deleteRolePrivilege(role.roleId, idPrivilege))
          });
        },
        error => dispatch(deletePrivilegeFailure(error))
      )
  }
}

export default deletePrivilege
