import {
  GET_PRIVILEGE_REQUEST,
  GET_PRIVILEGE_SUCCESS,
  GET_PRIVILEGE_FAILURE,
} from '../../types'
import {
  authorizationApiProvider
} from '../../../../../utils/api/providers'

const getPrivilegeRequest = (request) => {
  return {
    type: GET_PRIVILEGE_REQUEST,
    request
  }
}

const getPrivilegeSucess = (data) => {

  return {
    type: GET_PRIVILEGE_SUCCESS,
    data: data.data
  }
}
const getPrivilegeFailure = (error) => {

  return {
    type: GET_PRIVILEGE_FAILURE,
    error
  }
}

const getRequest = (id) => {
    return '/privileges/'+id
}


export const getPrivilege = (id) => {

  let request = getRequest(id)

  return dispatch => {
    dispatch(getPrivilegeRequest(request))

    return authorizationApiProvider.get(request)
      .then(
        res => {
          dispatch(getPrivilegeSucess(res.data))
        },
        error => {
          dispatch(getPrivilegeFailure())

        }
      )
  }
}

export default getPrivilege
