import {
  GET_APPLICATION_LIST_REQUEST,
  GET_APPLICATION_LIST_SUCCESS,
  GET_APPLICATION_LIST_FAILURE,
  GET_APPLICATION_REQUEST,
  GET_APPLICATION_SUCCESS,
  GET_APPLICATION_FAILURE,
  CREATE_APPLICATION_REQUEST,
  CREATE_APPLICATION_SUCCESS,
  CREATE_APPLICATION_FAILURE,
  PATCH_APPLICATION_REQUEST,
  PATCH_APPLICATION_SUCCESS,
  PATCH_APPLICATION_FAILURE,
  DELETE_APPLICATION_REQUEST,
  DELETE_APPLICATION_SUCCESS,
  DELETE_APPLICATION_FAILURE,
  ADD_APPLICATION_ROLE_SUCESS,
  DELETE_APPLICATION_ROLE_SUCESS,
  ADD_APPLICATION_PRIVILEGE_SUCESS,
  DELETE_APPLICATION_PRIVILEGE_SUCESS,
  GET_COUNT_PRIVILEGES_APPLICATION_REQUEST,
  GET_COUNT_PRIVILEGES_APPLICATION_SUCCESS,
  GET_COUNT_PRIVILEGES_APPLICATION_FAILURE,  
  GET_COUNT_ROLES_APPLICATION_REQUEST,
  GET_COUNT_ROLES_APPLICATION_SUCCESS,
  GET_COUNT_ROLES_APPLICATION_FAILURE,
  GET_COUNT_USERS_APPLICATION_REQUEST,
  GET_COUNT_USERS_APPLICATION_SUCCESS,
  GET_COUNT_USERS_APPLICATION_FAILURE,
  GET_COUNT_APPLICATIONS_REQUEST,
  GET_COUNT_APPLICATIONS_SUCCESS,
  GET_COUNT_APPLICATIONS_FAILURE,
  GET_COUNT_PROFILES_REQUEST,
  GET_COUNT_PROFILES_SUCCESS,
  GET_COUNT_PROFILES_FAILURE,
  GET_COUNT_USERS_REQUEST,
  GET_COUNT_USERS_SUCCESS,
  GET_COUNT_USERS_FAILURE,
  GET_USER_APPLICATION_ROLE_REQUEST,
  GET_USER_APPLICATION_ROLE_SUCCESS,
  GET_USER_APPLICATION_ROLE_FAILURE,
  GET_APPLICATION_ROLE_PRIVILEGES_LIST_REQUEST,
  GET_APPLICATION_ROLE_PRIVILEGES_LIST_SUCCESS,
  GET_APPLICATION_ROLE_PRIVILEGES_LIST_FAILURE,
  SET_ROLE_APPLICATION_PROFILE_REQUEST,
  SET_ROLE_APPLICATION_PROFILE_SUCCESS,
  SET_ROLE_APPLICATION_PROFILE_FAILURE,
  GET_ROLE_APPLICATION_PROFILE_REQUEST,
  GET_ROLE_APPLICATION_PROFILE_SUCCESS,
  GET_ROLE_APPLICATION_PROFILE_FAILURE,
  DELETE_ROLE_APPLICATION_PROFILE_REQUEST,
  DELETE_ROLE_APPLICATION_PROFILE_SUCCESS,
  DELETE_ROLE_APPLICATION_PROFILE_FAILURE
} from '../types';

const initialState = {
  applicationList: null ,
  currentApplication: null,
  countPrivilegesApplication: null,
  countRolesApplication: null,
  countUsersApplication: null,
  countApplications: null,
  countProfiles: null,
  countUsers: null,
  userApplicationRole: null,
  ApplicationRolePrivilegesList: null,
  roleApplicationProfileList: null
};

export const applicationReducer = (state = initialState, action) => {

  const {data,type,request,error} = action

  switch (type) {

    case GET_APPLICATION_LIST_REQUEST:
      return {
        ...state,
        request
      };

    case GET_APPLICATION_LIST_SUCCESS:
      return {
        ...state,
        applicationList: data,
      };

    case GET_APPLICATION_LIST_FAILURE:
      return {
        ...state,
        requestError: error
      };


    case GET_APPLICATION_REQUEST:
      return {
        ...state,
        request
      };

    case GET_APPLICATION_SUCCESS:
      return {
        ...state,
        currentApplication: data
      };


    case GET_APPLICATION_FAILURE:
      return {
        ...state,
        requestError: error

      };

    case CREATE_APPLICATION_REQUEST:
      return {
        ...state,
        request

      };

    case CREATE_APPLICATION_SUCCESS:
      return {
        ...state,
        applicationList: [  ...state.applicationList, data ]
      };

    case CREATE_APPLICATION_FAILURE:
      return {
        ...state,
        requestError: error
      };


    case PATCH_APPLICATION_REQUEST:
      return {
        ...state,
        request
      };

    case PATCH_APPLICATION_SUCCESS:

        return {
          ...state,
          currentApplication: data          
        };
     

    case PATCH_APPLICATION_FAILURE:
      return {
        ...state,
        requestError: error

      };

    case DELETE_APPLICATION_REQUEST:
      return {
        ...state,
        request
      }
    case DELETE_APPLICATION_SUCCESS:

    const objApplication = state.applicationList.filter((application) => application.applicationId !== data)

      return {
        ...state,
        applicationList: objApplication
      }
    case DELETE_APPLICATION_FAILURE:
      return {
        ...state,
        requestError: error
      }

    case ADD_APPLICATION_ROLE_SUCESS:
      return {
        ...state,
        currentApplicaion: data
      }

    case ADD_APPLICATION_PRIVILEGE_SUCESS:

      return {
        ...state,
        currentApplication: data        
      }

    case DELETE_APPLICATION_PRIVILEGE_SUCESS:

      const objPrivileges = state.currentApplication.privileges.filter((privilege) => privilege.privilegeId !== data)
    
    return {
      ...state,
      currentApplication: {
        ...state.currentApplication,
        privileges : objPrivileges,
      }
    }

    case GET_COUNT_PRIVILEGES_APPLICATION_REQUEST:
      return {
        ...state,
        request
      };

    case GET_COUNT_PRIVILEGES_APPLICATION_SUCCESS:
      return {
        ...state,
        countPrivilegesApplication: data,
      };

    case GET_COUNT_PRIVILEGES_APPLICATION_FAILURE:
      return {
        ...state,
        requestError: error
      };

    case GET_COUNT_ROLES_APPLICATION_REQUEST:
      return {
        ...state,
        request
      };

    case GET_COUNT_ROLES_APPLICATION_SUCCESS:
      return {
        ...state,
        countRolesApplication: data,
      };

    case GET_COUNT_ROLES_APPLICATION_FAILURE:
      return {
        ...state,
        requestError: error
      };
    
    case GET_COUNT_USERS_APPLICATION_REQUEST:
      return {
        ...state,
        request
      };

    case GET_COUNT_USERS_APPLICATION_SUCCESS:
      return {
        ...state,
        countUsersApplication: data,
      };

    case GET_COUNT_USERS_APPLICATION_FAILURE:
      return {
        ...state,
        requestError: error
      };

    case GET_COUNT_APPLICATIONS_REQUEST:
      return {
        ...state,
        request
      };

    case GET_COUNT_APPLICATIONS_SUCCESS:
      return {
        ...state,
        countApplications: data,
      };

    case GET_COUNT_APPLICATIONS_FAILURE:
      return {
        ...state,
        requestError: error
      };

    case GET_COUNT_PROFILES_REQUEST:
      return {
        ...state,
        request
      };

    case GET_COUNT_PROFILES_SUCCESS:
      return {
        ...state,
        countProfiles: data,
      };

    case GET_COUNT_PROFILES_FAILURE:
      return {
        ...state,
        requestError: error
      };

    case GET_COUNT_USERS_REQUEST:
      return {
        ...state,
        request
      };

    case GET_COUNT_USERS_SUCCESS:
      return {
        ...state,
        countUsers: data,
      };

    case GET_COUNT_USERS_FAILURE:
      return {
        ...state,
        requestError: error
      };

    case GET_USER_APPLICATION_ROLE_REQUEST:
      return {
        ...state,
        request
      };

    case GET_USER_APPLICATION_ROLE_SUCCESS:
      return {
        ...state,
        userApplicationRole: data,
      };

    case GET_USER_APPLICATION_ROLE_FAILURE:
      return {
        ...state,
        requestError: error
      };

    case GET_APPLICATION_ROLE_PRIVILEGES_LIST_REQUEST:
      return {
        ...state,
        request
      };

    case GET_APPLICATION_ROLE_PRIVILEGES_LIST_SUCCESS:
      return {
        ...state,
        ApplicationRolePrivilegesList: data,
      };

    case GET_APPLICATION_ROLE_PRIVILEGES_LIST_FAILURE:
      return {
        ...state,
        requestError: error
      };
   
    case DELETE_APPLICATION_ROLE_SUCESS:

    const objRole = state.currentApplication.roles.filter((role) => role.roleId !== data)
    
    return {
      ...state,
      currentApplication: {
        ...state.currentApplication,
        roles : objRole,
      }
    }

    case SET_ROLE_APPLICATION_PROFILE_REQUEST:
      return {
        ...state,
        request
      };

    case SET_ROLE_APPLICATION_PROFILE_SUCCESS:

    //const objRoleProfile = data[Object.keys(data)[0]]

      return {
        ...state,
        roleApplicationProfileList: data
      };

    case SET_ROLE_APPLICATION_PROFILE_FAILURE:
      return {
        ...state,
        requestError: error
      };

    case GET_ROLE_APPLICATION_PROFILE_REQUEST:
      return {
        ...state,
        request
      };

    case GET_ROLE_APPLICATION_PROFILE_SUCCESS:
    //const objRoleAppProfile = data[Object.keys(data)[0]]
      return {
        ...state,
        roleApplicationProfileList: data,
      };

    case GET_ROLE_APPLICATION_PROFILE_FAILURE:
      return {
        ...state,
        requestError: error
      };
    
    case DELETE_ROLE_APPLICATION_PROFILE_REQUEST:
      return {
        ...state,
        request
      };

    case DELETE_ROLE_APPLICATION_PROFILE_SUCCESS:

    const objDeleteRoleProfile = state.roleApplicationProfileList.filter((roleProfile) => roleProfile.roleId !== data)

      return {
        ...state,
        roleApplicationProfileList: objDeleteRoleProfile,
      };

    case DELETE_ROLE_APPLICATION_PROFILE_FAILURE:
      return {
        ...state,
        requestError: error
      };
    default:
      return state;
  }
}


export default applicationReducer;
