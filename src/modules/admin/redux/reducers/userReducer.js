import {
  GET_USER_LIST_REQUEST,
  GET_USER_LIST_SUCCESS,
  GET_USER_LIST_FAILURE,
  GET_USER_REQUEST,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  CREATE_USER_REQUEST,
  CREATE_USER_SUCCESS,
  CREATE_USER_CURRENT_SUCCESS,
  CREATE_USER_FAILURE,
  ADD_USER_PROFILE_REQUEST,
  ADD_USER_PROFILE_SUCCESS,
  ADD_USER_PROFILE_FAILURE,
  ADD_USER_CLIENT_REQUEST,
  ADD_USER_CLIENT_SUCCESS,
  ADD_USER_CLIENT_FAILURE,
  PATCH_USER_REQUEST,
  PATCH_USER_SUCCESS,
  PATCH_USER_FAILURE,
  DELETE_USER_REQUEST,
  DELETE_USER_SUCCESS,
  DELETE_USER_FAILURE,
  ADD_ASSIGNMENT_PROFILE_USER_REQUEST,
  ADD_ASSIGNMENT_PROFILE_USER_SUCCESS,
  ADD_ASSIGNMENT_PROFILE_USER_FAILURE,
  GET_APPLICATION_USERS_LIST_REQUEST,
  GET_APPLICATION_USERS_LIST_SUCCESS,
  GET_APPLICATION_USERS_LIST_FAILURE,
  SET_CLIENT_DEFAULT_REQUEST,
  SET_CLIENT_DEFAULT_SUCCESS,
  SET_CLIENT_DEFAULT_FAILURE,
  SET_CLIENT_REQUEST,
  SET_CLIENT_SUCCESS,
  SET_CLIENT_FAILURE,
  GET_CLIENTS_REQUEST,
  GET_CLIENTS_SUCCESS,
  GET_CLIENTS_FAILURE,
  DELETE_CLIENT_USER_REQUEST,
  DELETE_CLIENT_USER_SUCCESS,
  DELETE_CLIENT_USER_FAILURE,
  CREATE_USER_AUTH_REQUEST,
  CREATE_USER_AUTH_SUCCESS,
  CREATE_USER_AUTH_FAILURE,
} from "../types";

const initialState = {
  userList: null,
  currentUser: null,
  ApplicationUsersList: null,
  clientsList: null,
};

export const userReducer = (state = initialState, action) => {
  const { data, type, request, error } = action;

  switch (type) {
    case GET_USER_LIST_REQUEST:
      return {
        ...state,
        request,
      };

    case GET_USER_LIST_SUCCESS:
      return {
        ...state,
        userList: data,
      };

    case GET_USER_LIST_FAILURE:
      return {
        ...state,
        error,
      };

    case GET_APPLICATION_USERS_LIST_REQUEST:
      return {
        ...state,
        request,
      };

    case GET_APPLICATION_USERS_LIST_SUCCESS:
      return {
        ...state,
        ApplicationUsersList: data,
      };

    case GET_APPLICATION_USERS_LIST_FAILURE:
      return {
        ...state,
        error,
      };

    case GET_USER_REQUEST:
      return {
        ...state,
        request,
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        currentUser: data,
      };
    case GET_USER_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case CREATE_USER_REQUEST:
      return {
        ...state,
        request,
      };
    case CREATE_USER_SUCCESS:
      return {
        ...state,
        userList: [...state.userList, data],
      };
    case CREATE_USER_CURRENT_SUCCESS:
      return {
        ...state,
        currentUser: data,
      };
    case CREATE_USER_FAILURE:
      return {
        ...state,
        error,
      };

    case ADD_USER_PROFILE_REQUEST:
      return {
        ...state,
        request,
      };
    case ADD_USER_PROFILE_SUCCESS:
      return {
        ...state,
        currentUser: data,
      };
    case ADD_USER_PROFILE_FAILURE:
      return {
        ...state,
        error,
      };

    case ADD_USER_CLIENT_REQUEST:
      return {
        ...state,
        request,
      };
    case ADD_USER_CLIENT_SUCCESS:
      return {
        ...state,
        currentUser: data,
      };
    case ADD_USER_CLIENT_FAILURE:
      return {
        ...state,
        error,
      };

    case PATCH_USER_REQUEST:
      return {
        ...state,
        request,
      };
    case PATCH_USER_SUCCESS:
      return {
        ...state,
        currentUser: data,
      };
    case PATCH_USER_FAILURE:
      return {
        ...state,
        error,
      };

    case DELETE_USER_REQUEST:
      return {
        ...state,
        request,
      };

    case ADD_ASSIGNMENT_PROFILE_USER_REQUEST:
      return {
        ...state,
        request,
      };

    case DELETE_USER_FAILURE:
      return{
        ...state,
        error
      };

    case ADD_ASSIGNMENT_PROFILE_USER_SUCCESS:

      return {
        ...state,
        currentUser: data,
      };

    case ADD_ASSIGNMENT_PROFILE_USER_FAILURE:
      return {
        ...state,
        error,
      };

    case SET_CLIENT_DEFAULT_REQUEST:
      return {
        ...state,
        request,
      };

    case SET_CLIENT_DEFAULT_SUCCESS:
      return {
        ...state,
        currentUser: data,
      };

    case SET_CLIENT_DEFAULT_FAILURE:
      return {
        ...state,
        error,
      };

    case SET_CLIENT_REQUEST:
      return {
        ...state,
        request,
      };

    case SET_CLIENT_SUCCESS:
      return {
        ...state,
        currentUser: data,
      };

    case SET_CLIENT_FAILURE:
      return {
        ...state,
        error,
      };
    case GET_CLIENTS_REQUEST:
      return {
        ...state,
        request,
      };

    case GET_CLIENTS_SUCCESS:
      return {
        ...state,
        clientsList: data,
      };

    case GET_CLIENTS_FAILURE:
      return {
        ...state,
        error,
      };

    case DELETE_CLIENT_USER_REQUEST:
      return {
        ...state,
        request,
      };

    case DELETE_CLIENT_USER_SUCCESS:
      const clientsUser = state.currentUser.clients.filter((client) => client.clientId !== data );
      return {
        ...state,
        currentUser:{
          ...state.currentUser,
          clients: clientsUser
        }
      };

    case DELETE_CLIENT_USER_FAILURE:
      return {
        ...state,
        error,
      };

    case CREATE_USER_AUTH_REQUEST:
      return {
        ...state,
        request,
      };

    case CREATE_USER_AUTH_SUCCESS:
      return {
        ...state,
        data,
      };

    case CREATE_USER_AUTH_FAILURE:
      return {
        ...state,
        error,
      };

    case DELETE_USER_SUCCESS:
      if(state.userList){
        const objUserList = state.userList.filter(user => user.userId !== data);

        return {
          ...state,
          userList: objUserList,
        };
      }else if(state.ApplicationUsersList){
        const objApplicationUsers = state.ApplicationUsersList.filter(
          userApp => userApp.userId !== data,
        );

        return {
          ...state,
          ApplicationUsersList: objApplicationUsers,
        };
      }
      return{
        ...state
      };

    default:
      return state;
  }
};

export default userReducer;
