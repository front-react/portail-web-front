import {combineReducers} from 'redux';
import userReducer from './userReducer';
import applicationReducer from './applicationReducer';
import profileReducer from './profileReducer';
import roleReducer from './roleReducer';
import privilegeReducer from './privilegeReducer';

const adminReducer = combineReducers({
  user: userReducer,
  application: applicationReducer,
  profile: profileReducer,
  role: roleReducer,
  privilege: privilegeReducer,
});

export default adminReducer
