import {
  GET_ROLE_LIST_REQUEST,
  GET_ROLE_LIST_SUCCESS,
  GET_ROLE_LIST_FAILURE,
  GET_ROLE_REQUEST,
  GET_ROLE_SUCCESS,
  GET_ROLE_FAILURE,
  CREATE_ROLE_REQUEST,
  CREATE_ROLE_SUCCESS,
  CREATE_ROLE_FAILURE,
  ADD_ROLE_PRIVILEGE_REQUEST,
  ADD_ROLE_PRIVILEGE_SUCCESS,
  ADD_ROLE_PRIVILEGE_FAILURE,
  PATCH_ROLE_REQUEST,
  PATCH_ROLE_SUCCESS,
  PATCH_ROLE_FAILURE,
  DELETE_ROLE_PRIVILEGE_REQUEST,
  DELETE_ROLE_PRIVILEGE_SUCCESS,
  DELETE_ROLE_PRIVILEGE_FAILURE,
  DELETE_ROLE_REQUEST,
  DELETE_ROLE_SUCCESS,
  DELETE_ROLE_FAILURE,
} from "../types";

const initialState = {
  roleList: null,
  currentRole: null,
};

export const roleReducer = (state = initialState, action) => {
  const { data, type, request, error } = action;

  switch (type) {
    case GET_ROLE_LIST_REQUEST:
      return {
        ...state,
        request: request,
      };
    case GET_ROLE_LIST_SUCCESS:
      return {
        ...state,
        roleList: data,
      };
    case GET_ROLE_LIST_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case GET_ROLE_REQUEST:
      return {
        ...state,
        request,
      };
    case GET_ROLE_SUCCESS:
      return {
        ...state,
        currentRole: data,
      };

    case GET_ROLE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case CREATE_ROLE_REQUEST:
      return {
        ...state,
        request,
      };
    case CREATE_ROLE_SUCCESS:
      const objCurrentRole = data[Object.keys(data)[0]];

      return {
        ...state,
        roleList: [...state.roleList, objCurrentRole],
      };

    case CREATE_ROLE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case ADD_ROLE_PRIVILEGE_REQUEST:
      return {
        ...state,
        request,
      };
    case ADD_ROLE_PRIVILEGE_SUCCESS:
      const lengthRole = state.roleList.length;

      const objRolePrivilege = data[Object.keys(data)[0]];

      const objListRolePrivileges = state.roleList.filter(
        role => role.roleId !== objRolePrivilege.roleId,
      );

      const objListRolePrivilegesIndex = state.roleList.findIndex(
        role => role.roleId === objRolePrivilege.roleId,
      );

      return {
        ...state,
        roleList: [
          ...objListRolePrivileges.slice(0, objListRolePrivilegesIndex),
          objRolePrivilege,
          ...objListRolePrivileges.slice(
            objListRolePrivilegesIndex,
            lengthRole - 1,
          ),
        ],
      };
    case ADD_ROLE_PRIVILEGE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case PATCH_ROLE_REQUEST:
      return {
        ...state,
        request,
      };
    case PATCH_ROLE_SUCCESS:
      const patchRole = data[Object.keys(data)[0]];

      const objPatchRole = state.roleList.filter(
        role => role.roleId !== patchRole.roleId,
      );

      return {
        ...state,
        roleList: [...objPatchRole, patchRole],
      };
    case PATCH_ROLE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case DELETE_ROLE_PRIVILEGE_REQUEST:
      return {
        ...state,
        request,
      };
    case DELETE_ROLE_PRIVILEGE_SUCCESS:
      // state.data[data.idRole].privileges.forEach((value, index) => {
      //   if(value == data.idPrivilege){
      //     state.data[data.idRole].privileges.splice(index,1)
      //   }
      // })
      return {
        ...state,
      };
    case DELETE_ROLE_PRIVILEGE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case DELETE_ROLE_REQUEST:
      return {
        ...state,
        request,
      };

    case DELETE_ROLE_SUCCESS:
      const objRole = state.roleList.filter(role => role.roleId !== data);

      return {
        ...state,
        roleList: objRole,
      };

    case DELETE_ROLE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    default:
      return state;
  }
};

export default roleReducer;
