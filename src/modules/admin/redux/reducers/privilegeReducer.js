import {
  GET_PRIVILEGE_LIST_REQUEST,
  GET_PRIVILEGE_LIST_SUCCESS,
  GET_PRIVILEGE_LIST_FAILURE,
  GET_PRIVILEGE_REQUEST,
  GET_PRIVILEGE_SUCCESS,
  GET_PRIVILEGE_FAILURE,
  CREATE_PRIVILEGE_REQUEST,
  CREATE_PRIVILEGE_SUCCESS,
  CREATE_PRIVILEGE_FAILURE,
  PATCH_PRIVILEGE_REQUEST,
  PATCH_PRIVILEGE_SUCCESS,
  PATCH_PRIVILEGE_FAILURE,
  DELETE_PRIVILEGE_REQUEST,
  DELETE_PRIVILEGE_SUCCESS,
  DELETE_PRIVILEGE_FAILURE,
} from "../types";

const initialState = {
  privilegesList: null,
  currentPrivilege: null,
};

export const privilegeReducer = (state = initialState, action) => {
  const { data, type, request, error } = action;

  switch (type) {
    case GET_PRIVILEGE_LIST_REQUEST:
      return {
        ...state,
        request,
      };

    case GET_PRIVILEGE_LIST_SUCCESS:
      return {
        ...state,
        privilegesList: data,
      };

    case GET_PRIVILEGE_LIST_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case GET_PRIVILEGE_REQUEST:
      return {
        ...state,
        request,
      };

    case GET_PRIVILEGE_SUCCESS:
      return {
        ...state,
        currentPrivilege: data,
      };

    case GET_PRIVILEGE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case CREATE_PRIVILEGE_REQUEST:
      return {
        ...state,
        request,
      };

    case CREATE_PRIVILEGE_SUCCESS:
      return {
        ...state,
        privilegesList: [...state.privilegesList, data],
      };

    case CREATE_PRIVILEGE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case PATCH_PRIVILEGE_REQUEST:
      return {
        ...state,
        request,
      };
    case PATCH_PRIVILEGE_SUCCESS:
      const objPrivileges = state.privilegesList.filter(
        privilege => privilege.privilegeId !== data.privilegeId,
      );

      return {
        ...state,
        privilegesList: [...objPrivileges, data],
      };

    case PATCH_PRIVILEGE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case DELETE_PRIVILEGE_REQUEST:
      return {
        ...state,
        request,
      };
    case DELETE_PRIVILEGE_SUCCESS:
      const objListPrivileges = state.privilegesList.filter(
        privilege => privilege.privilegeId !== data,
      );

      return {
        ...state,
        privilegesList: objListPrivileges,
      };

    case DELETE_PRIVILEGE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    default:
      return state;
  }
};

export default privilegeReducer;
