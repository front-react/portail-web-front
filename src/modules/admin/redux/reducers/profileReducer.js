import {
  GET_PROFILE_LIST_REQUEST,
  GET_PROFILE_LIST_SUCCESS,
  GET_PROFILE_LIST_FAILURE,
  GET_PROFILE_REQUEST,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILURE,
  CREATE_PROFILE_REQUEST,
  CREATE_PROFILE_SUCCESS,
  CREATE_PROFILE_FAILURE,
  PATCH_PROFILE_REQUEST,
  PATCH_PROFILE_SUCCESS,
  PATCH_PROFILE_FAILURE,
  DELETE_PROFILE_REQUEST,
  DELETE_PROFILE_SUCCESS,
  DELETE_PROFILE_FAILURE,
} from "../types";

const initialState = {
  profileList: null,
  profileSelect: null,
};

export const profileReducer = (state = initialState, action) => {
  const { data, type, request, error } = action;

  switch (type) {
    case GET_PROFILE_LIST_REQUEST:
      return {
        ...state,
        request,
      };
    case GET_PROFILE_LIST_SUCCESS:
      return {
        ...state,
        profileList: data,
      };
    case GET_PROFILE_LIST_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case GET_PROFILE_REQUEST:
      return {
        ...state,
        request,
      };
    case GET_PROFILE_SUCCESS:
      return {
        ...state,
        profileSelect: data,
      };

    case GET_PROFILE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case CREATE_PROFILE_REQUEST:
      return {
        ...state,
        request,
      };

    case CREATE_PROFILE_SUCCESS:
      return {
        ...state,
        profileList: [...state.profileList, data],
      };

    case CREATE_PROFILE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case PATCH_PROFILE_REQUEST:
      return {
        ...state,
        request,
      };

    case PATCH_PROFILE_SUCCESS:
      return {
        ...state,
        profileSelect: data,
      };

    case PATCH_PROFILE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    case DELETE_PROFILE_REQUEST:
      return {
        ...state,
        request,
      };

    case DELETE_PROFILE_SUCCESS:
      const objListProfile = state.profileList.filter(
        profile => profile.profileId !== data,
      );

      return {
        ...state,
        profileList: objListProfile,
      };

    case DELETE_PROFILE_FAILURE:
      return {
        ...state,
        requestError: error,
      };

    default:
      return state;
  }
};

export default profileReducer;
