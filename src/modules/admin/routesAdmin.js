import React from 'react';
import {Route, IndexRedirect, IndexRoute} from 'react-router';
import {
  Admin,
  ApplicationListContainer,
  DetailApplicationContainer,
  ProfileListContainer,
  ProfileContainer,
  CreateProfileContainer,
  CreateApplicationContainer,
  ApplicationsPrivilegesListContainer,
  ApplicationUsersListContainer,
  ApplicationsRolesListContainer
} from './containers';

import { AdminDashBoard } from './components'


export default(
  <Route path="/admin" component={Admin}>
    <IndexRedirect to="dashboard" />
    <Route name="Dashboard" path="dashboard" component={AdminDashBoard}/>
    <Route path="application">
      <IndexRoute name="Liste des applications" component={ApplicationListContainer}/>
      <Route name="Ajouter un nouvelle application"  path="add" component={CreateApplicationContainer}/>
      <Route name="Fiche de l'application" path=":id" component={DetailApplicationContainer} />
      <Route path="users">
        <Route name="Liste des utilisateurs de l'application" path=":id" component={ApplicationUsersListContainer} />
      </Route>
      <Route path="privileges">
        <Route name="Les privilèges de l'application" path=":id" component={ApplicationsPrivilegesListContainer} />
      </Route>
      <Route path="roles">
        <Route name="Les roles de l'application" path=":name/:id" component={ApplicationsRolesListContainer} />
      </Route>
    </Route>
    <Route path="profile" >
      <IndexRoute name="Liste des profiles" component={ProfileListContainer} />
      <Route name="Ajouter un nouveau profile"  path="add" component={CreateProfileContainer}/>
      <Route name="Fiche du profile" path=":id" component={ProfileContainer}/>
    </Route>
  </Route>
)

/*
 <Route  path=":id" component={UserProfileContainer}/>


 <Route path="/admin" component={Admin}>
 <IndexRedirect to="user" />
 <Route path="user" >
 <IndexRoute component={UserListContainer} />
 <Route  path=":id" component={UserProfileContainer}/>
 </Route>
 <Route path="application">
 <IndexRoute component={ApplicationListContainer}/>
 <Route component={ApplicationContainer}>
 <IndexRoute component={ApplicationSetPrivilegeContainer} />
 <Route path="settings" component={ApplicationAdminContainer}/>
 </Route>
 </Route>
 <Route path="profile" >
 <IndexRoute component={ProfileListContainer} />
 <Route path=":id" component={ProfileContainer}/>
 </Route>
 </Route>
 */