import React from 'react'
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider'
import {grey300, grey500, grey700, orange500} from "material-ui/styles/colors"
import moment from 'moment'
import { PatchUser } from '../'
import { AssignmentProfileUserContainer } from '../../containers'


const styles = {
  profileIT: {
    display: 'flex',
    flexDirection: 'column',
    width: '66%',
  },
  profile:{
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paperUser: {
    height: '350px',
    width: '50%',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    padding: 10,
    fontSize: '14px', 
    marginLeft: '15px'
  },
  divUserPortail: {
    width: '100%',
  },
  userPortail: {
    width: '100%',
    flexDirection: 'column',
    color: grey700,
  },
  divStats: {
    width: '70%',
    color: grey500,
    fontSize: '30px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  listStats: {
    width: '70%',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  statesName: {
    display: 'inline-block',
    margin: '0 0 0 0',
    width: 'calc(100% * (1/3) - 10px - 1px)',
  },
  paperProfile: {
    height: '190px',
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: orange500,
    color: 'white',
    marginTop: '20px'
  },
  paperProfileHeader:{
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buildIcon: {
    height: '70px',
    width: '70px',
  },
  paperClient: {
    marginTop: '20px',
    padding: '10px',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  verticalDivider: {
    height: '80%',
    borderRight: '1px solid '+ grey300,
  },
  subheader: {
    lineHeight: '30px',
    fontSize: '25px',
    fontWeight: '700',
    marginBottom: '15px'
  },
  information: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: '16px',
    marginBottom: '5px',
    marginTop: '20px',
  },
  informationLabel: {
    fontWeight: '300',
  },
  profileName: {
    fontSize: '55px',
  },
  profileLabel: {
    fontSize: '20px',
  }
}

const UserProfileInfo = ({userInformation, personId}) => {
  
    const createdDate = moment(userInformation.dateOn).format('DD MMMM YYYY')

    const defaultClient = userInformation.clients

    const oneClient = defaultClient.find((client) => client.isDefault === '1' || false)

  return (
    <div style={styles.profileIT}>
      <span style={styles.row}>
        <Paper style={styles.paperUser}>
          <div style={styles.divUserPortail}>
            <div style={styles.userPortail}>
              <Subheader style={styles.subheader}>Information User</Subheader>
              <Divider />
              <span style={styles.information}>
                <span style={styles.informationLabel}>Login</span>
                {userInformation.username}
              </span>
              <span style={styles.information}>
                <span style={styles.informationLabel}>Date de creation</span>
                {createdDate}
              </span>
            </div>
          </div>

          <div style={styles.userPortail}>
            <Subheader style={styles.informationLabel}>Client par defaut</Subheader>
            <span style={styles.information}>
              <span style={styles.informationLabel}>
                {oneClient &&
                    oneClient.clientName
                }
              </span>
            </span>
          </div>
          <div>
            <PatchUser userName={userInformation.username} userId={userInformation.userId} personId={personId} />
          </div>
        </Paper>
        <Paper style={styles.paperUser}>
          <div style={styles.divUserPortail}>
            <div style={styles.userPortail}>
              <Subheader style={styles.subheader}>Profile {userInformation.username} </Subheader>
              <Divider />
              <div style={styles.paperProfile}>
                <span style={styles.paperProfileHeader}>
                  <span style={styles.profileLabel}>Profil</span>
                </span>
                {userInformation.profileName != null
                  ? <span style={styles.profile}>
                      <span style={styles.profileName} >{userInformation.profileName}</span>
                    </span>
                  : <span style={styles.profileName} >
                      Aucun profil
                    </span>
                }
              </div>
              <div>
                {userInformation.profileId !== null
                  ?
                  <AssignmentProfileUserContainer 
                    userId={userInformation.userId} 
                    userName={userInformation.username} 
                    profileId={userInformation.profileId} />
                  :
                  <AssignmentProfileUserContainer 
                    userId={userInformation.userId} 
                    userName={userInformation.username} 
                    profileId={0} />

                }
                
              </div>
            </div>
          </div>
        </Paper>
      </span>
    </div>
  )

}

export default UserProfileInfo

