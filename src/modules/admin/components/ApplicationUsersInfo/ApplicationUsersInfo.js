import React from 'react'
import {Link} from 'react-router'
import { SimpleStatsData } from '../../../../components/componup';
import ActionAccountCircle from 'material-ui/svg-icons/action/account-circle'

const styles = {
  styleLink: {
    textDecoration: 'none',
    display: 'block',
    width: '33%',
    height: '100%',
  }
};

  const ApplicationUsersInfo = ({countUsersApplication, idApplication}) => {

  const countUsers = countUsersApplication.totalUsers


      return (
        <Link to={'/admin/application/users/'+idApplication} style={styles.styleLink} >
          <SimpleStatsData 
              icon={ActionAccountCircle} 
              dataNumber={countUsers} 
              dataLegend="Utilisateur" 
              hexaColor={"#9E9E9E"} 
              sizeImg={"48px"}
              height={"85px"}
              width={"95%"}
              numberData={"20px"}
              numberLegend={"15px"}
              titleLabel={"Utilisateurs"}
          />
        </Link>
      )
    };

export default ApplicationUsersInfo

/*
 */