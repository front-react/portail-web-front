import React, { Component } from "react";
import Subheader from "material-ui/Subheader";
import MenuItem from "material-ui/MenuItem";
import { Field, reduxForm } from "redux-form";
import { SelectField } from "redux-form-material-ui";
import Checkbox from "material-ui/Checkbox";
import FlatButton from "material-ui/FlatButton";

const styles = {
  spanProfileClient: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  divProfileClient: {
    width: "50%",
  },
  actionButton: {
    float: "right",
  },
};

class AddUserProfileClient extends Component {
  render() {
    const {
      handleSubmit,
      handleFormSubmit,
      clients,
      valueProfile,
      handleChange,
      handleCheck,
      //currentUser
    } = this.props;

    return (
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <span>
          <span style={styles.spanProfileClient}>
            <div style={styles.divProfileClient}>
              <Subheader>Choisissez un ou plusieur client...</Subheader>
              {Object.keys(clients).map((client, index) => (
                <Checkbox
                  key={clients[client].idClient}
                  name="clientId"
                  label={clients[client].raisonSocialeClient}
                  style={styles.checkbox}
                  value={clients[client].raisonSocialeClient}
                  onCheck={(event, isInputChecked) =>
                    handleCheck(event, isInputChecked, clients[client].idClient)}
                />
              ))}
            </div>
            <div style={styles.divProfileClient}>
              <Subheader>Puis selectionnez le profile et quiter</Subheader>
              <Field
                name="profileId"
                component={SelectField}
                floatingLabelText="Profil"
                value={valueProfile}
                onChange={handleChange}
              >
                <MenuItem value={1} primaryText="mdr" />
                <MenuItem value={3} primaryText="administrator" />
                <MenuItem value={4} primaryText="PPdr" />
              </Field>
            </div>
          </span>
          <div style={styles.actionButton}>
            <FlatButton
              onClick={handleSubmit(data => {
                handleFormSubmit(data, "next");
              })}
              disabled={true}
              label="Etape suivante"
            />
            <FlatButton
              onClick={handleSubmit(data => {
                handleFormSubmit(data, "close");
              })}
              label="Valider & quitter"
            />
          </div>
        </span>
      </form>
    );
  }
}

AddUserProfileClient = reduxForm({
  form: "AddUserProfileClient",
})(AddUserProfileClient);

export default AddUserProfileClient;
