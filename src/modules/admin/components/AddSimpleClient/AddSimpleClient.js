import React, { Component } from "react";
import Subheader from "material-ui/Subheader";
import Checkbox from "material-ui/Checkbox";
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog'

const styles = {
  spanProfileClient: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  divProfileClient: {
    width: "50%"
  },
  actionButton: {
    float: "right",
  },
};

class AddSimpleClient extends Component {

    state = {
        open: false,
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
     this.setState({open: false});
    };

  render() {
    const {
      clients,
      handleCheck,
    } = this.props;

    return (
        <span>
          <span style={styles.spanProfileClient}>
          <RaisedButton label="Ajouter un client" onTouchTap={this.handleOpen} />
            <Dialog
            title="Liste des clients"
            modal={true}
            open={this.state.open}>
            <div style={styles.divProfileClient}>
            <Subheader>Choisissez un ou plusieurs client...</Subheader>
              {clients.map((client, index) => (
                <Checkbox
                  key={index}
                  name="clientId"
                  label={client.clientName}
                  style={styles.checkbox}
                  value={client.clientName}
                  onCheck={(event, isInputChecked) =>
                    handleCheck(event, isInputChecked, client.clientId)}
                />
              ))}
            </div>
            <FlatButton
                label="Fermer"
                primary={true}
                onTouchTap={this.handleClose}
            />
            </Dialog>
          </span>
        </span>
    );
  }
}

export default AddSimpleClient;
