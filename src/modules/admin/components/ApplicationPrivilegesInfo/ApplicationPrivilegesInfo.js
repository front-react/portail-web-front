import React from 'react'
import {Link} from 'react-router'
import { SimpleStatsData } from '../../../../components/componup';
import ActionDoneAll  from 'material-ui/svg-icons/action/done-all'

const styles = {
  styleLink: {
    textDecoration: 'none',
    display: 'block',
    width: '33%',
    height: '100%',
  }
};

  const ApplicationPrivilegesInfo = ({countPrivilegesApplication, idApplication}) => {

  const countPrivileges = countPrivilegesApplication.totalPrivileges

      return (
        <Link to={'/admin/application/privileges/'+idApplication} style={styles.styleLink} >
          <SimpleStatsData 
          icon={ActionDoneAll} 
          dataNumber={countPrivileges} 
          dataLegend="Privilèges" 
          hexaColor={"#9E9E9E"} 
          sizeImg={"48px"}
          height={"85px"}
          width={"95%"}
          numberData={"20px"}
          numberLegend={"15px"}
          titleLabel={"Privileges"}
        />
      </Link>
      )
    };

export default ApplicationPrivilegesInfo

/*
 */