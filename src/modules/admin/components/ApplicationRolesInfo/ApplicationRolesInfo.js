import React from 'react'
import {Link} from 'react-router'
import { SimpleStatsData } from '../../../../components/componup';
import ActionVerifiedUser   from 'material-ui/svg-icons/action/verified-user'

const styles = {
  styleLink: {
    textDecoration: 'none',
    display: 'block',
    width: '33%',
    height: '100%',
  }
};

  const ApplicationRolesInfo = ({countRolesApplication, idApplication, applicationName}) => {

  const countRoles = countRolesApplication.totalRoles

      return (
        <Link to={'/admin/application/roles/'+applicationName+'/'+idApplication} style={styles.styleLink} >
          <SimpleStatsData 
              icon={ActionVerifiedUser} 
              dataNumber={countRoles} 
              dataLegend="Rôles" 
              hexaColor={"#9E9E9E"} 
              sizeImg={"48px"}
              height={"85px"}
              width={"95%"}
              numberData={"20px"}
              numberLegend={"15px"}
              titleLabel={"Roles"}
          />
        </Link>
      )
    };

export default ApplicationRolesInfo

/*
 */