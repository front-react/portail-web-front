import React, {Component} from 'react'
import {connect} from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import EditIcon from "material-ui/svg-icons/editor/mode-edit"
import {patchProfile} from '../../redux/actions/profile/patchProfile'
import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5
  },
  iconButton: {
    width: '18px',
    height: '18px',
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    patchProfile: (id, body) => {dispatch(patchProfile(id, body)) }
  }
};


class PatchProfile extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleFormSubmit = (requestBody) => {
    //requestBody.profileName = this.props.profileName
    this.props.patchProfile(this.props.profileId, requestBody);
  }

  render() {

    const { handleSubmit } = this.props

    return(
      <div>
        <IconButton
          iconStyle={styles.iconButton}
          onTouchTap={this.handleOpen}
        >
          <EditIcon />
        </IconButton>
        <Dialog
          title="Modification du profile"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <form style={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
            <div>
                <span>Ancien Profile :: <strong>{this.props.profileName}</strong></span>
            </div>
            <div>
                <Field 
                    name="profileName" 
                    component={TextField} 
                    hintText="Nouveau profile"/>
            </div>
            <div style={styles.actionButton}>
              <FlatButton onTouchTap={this.handleClose} label="Cancel"/>
              <FlatButton onTouchTap={this.handleClose} type="submit" label="Submit"/>
            </div>
          </form>
        </Dialog>
      </div>
    )
  }
}

PatchProfile = reduxForm({
  form: 'PatchProfile',
})(PatchProfile);


export default connect(null, mapDispatchToProps)(PatchProfile)