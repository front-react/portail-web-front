import React, { Component } from "react";
import Subheader from "material-ui/Subheader";
import Checkbox from "material-ui/Checkbox";
import FlatButton from "material-ui/FlatButton";
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog'

const styles = {
  spanProfileClient: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  divProfileClient: {
    width: "50%",
  },
  actionButton: {
    float: "right",
  },
};

const pushTableauClient = (clients) => {

  const userClients = []
  if(clients.length > 0){
    clients.forEach((clis) => {
      userClients.push(clis.clientId)
    });
  
    return userClients
  }
  else{
    return userClients
  }
  

}

class AddClients extends Component {

  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {
    const {
      clients,
      clientsUser,
      handleCheck,
    } = this.props;

    const tableauClients = pushTableauClient(clientsUser)

    return (
      <div>
        <RaisedButton
            label="Ajouter ou supprimer des clients"
            primary={true}
            style={styles.button}
            onClick={this.handleOpen}
        />
        <Dialog
          title="Ajouter ou supprimer d'autres clients à cette utilisateur"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
        <div>
        <span style={styles.spanProfileClient}>
          <div style={styles.divProfileClient}>
            <Subheader>Choisissez un ou plusieurs clients...</Subheader>
            {Object.keys(clients).map((client, index) => (
              <span key={clients[client].idClient}>
                {tableauClients.indexOf(clients[client].idClient) !== -1
                ? 
                  <Checkbox
                    name="clientId"
                    checked={tableauClients.indexOf(clients[client].idClient) !== -1}
                    label={clients[client].raisonSocialeClient}
                    style={styles.checkbox}
                    value={clients[client].raisonSocialeClient}
                    onCheck={(event, isInputChecked) =>
                      handleCheck(event, isInputChecked, clients[client].idClient)}
                  />
                :
                  <Checkbox
                    name="clientId"
                    checked={tableauClients.indexOf(clients[client].idClient) !== -1}
                    label={clients[client].raisonSocialeClient}
                    style={styles.checkbox}
                    value={clients[client].raisonSocialeClient}
                    onCheck={(event, isInputChecked) =>
                      handleCheck(event, isInputChecked, clients[client].idClient)}
                  />

                }
                
              </span>
            ))}
          </div>
        </span>
        <div style={styles.actionButton}>
          <FlatButton
            onClick={this.handleClose}
            label="Fermer"
          />
        </div>
        </div>
        </Dialog>
      </div>
    );
  }
}

export default AddClients;
