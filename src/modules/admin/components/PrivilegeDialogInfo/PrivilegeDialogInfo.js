import React, {Component} from 'react'
import {connect} from 'react-redux'
import IconButton from 'material-ui/IconButton';
import ActionInfo from 'material-ui/svg-icons/action/info'
import {getPrivilege} from '../../redux/actions/privilege/getPrivilege'
import ClearIcon from 'material-ui/svg-icons/content/clear';
import CircularProgress from 'material-ui/CircularProgress';
import Drawer from 'material-ui/Drawer';

const styles = {
  titleDrawer: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '24px',
    color: 'rgb(220,220,220)',
    backgroundColor: 'rgb(50,50,50)',
  },
  drawerListItem: {
    display: 'flex',
    flexDirection: 'column',
    margin: '5px 0 5px 0',
  },
  drawerLabel: {
    fontWeight: '700',
    letterSpacing: '1px',
    color: 'rgb(10,10,10)',
    backgroundColor: 'rgb(150,150,150)',
    textIndent: '10px',
    padding : '2px',
  },
  repairDetailList: {
    padding : '5px',
    display: 'flex',
    flexDirection: 'column',
  },
  repairLabel: {
    padding : '10px',
    color: 'rgb(50,50,50)',
    backgroundColor: 'rgb(220,220,220)',
    borderBottom: '2px solid rgb(150,150,150)'
  },
  colorButton: {
    backgroundColor: 'rgb(250,250,250)',
  },
  titleRepair: {
    textIndent: '20px',
  },
};

const mapStateToProps = (state) => {
    return {
        currentPrivilege: state.admin.privilege.currentPrivilege,
    }
  };

const mapDispatchToProps = (dispatch) => {
    return {
        getPrivilege: (id) => {dispatch(getPrivilege(id))}
    }
};


class PrivilegeDialogInfo extends Component {

    state = {
        open: false,
    };

    handleOpen = () => {
        this.setState({open: true});
        const privilegeId = this.props.privilegeId
        this.props.getPrivilege(privilegeId);
    };

    handleClose = () => {
        this.setState({open: false});
    };

  render() {
    const {currentPrivilege} = this.props;

			return(
				<div>
					<IconButton
						iconStyle={styles.iconButton}
						onTouchTap={this.handleOpen}
					>
						<ActionInfo />
					</IconButton>
					<Drawer
						open={this.state.open}
            openSecondary={true}
            docked={false}
            width={400}
					>
						{currentPrivilege
							? <div>
                  <div style={styles.titleDrawer}>
                    <IconButton 
                      onTouchTap={this.handleClose}
                      style={styles.colorButton} 
                    >
                      <ClearIcon />
                    </IconButton>
                    <span style={styles.titleRepair}>Privilege: {currentPrivilege.privilegeName}</span>
                  </div>

                  <div style={styles.repairDetailList}>

                    <div style={styles.drawerListItem}>
                      <span style={styles.drawerLabel}>ID Privilege : {currentPrivilege.privilegeId}</span>
                      <span style={styles.repairLabel}>Privilege: {currentPrivilege.privilegeName}</span>
                    </div>

                    <div style={styles.drawerListItem}>
                      <span style={styles.drawerLabel}>Ce privilege appartient à l'application</span>
                      <span style={styles.repairLabel}>{currentPrivilege.applicationName} (ID: {currentPrivilege.applicationId})</span>
                    </div>

                    <div style={styles.drawerListItem}>
                      <span style={styles.drawerLabel}>Ce privilege appartient à la catégorie</span>
                      <span style={styles.repairLabel}>{currentPrivilege.privilegeTypeName}</span>
                    </div>

                    <div style={styles.drawerListItem}>
                      <span style={styles.drawerLabel}>Il intervient dans: </span>
                      <span style={styles.repairLabel}>{currentPrivilege.roles.length} Roles</span>
                      {Object.keys(currentPrivilege.roles).map((role, index) =>
                        <span key={index} style={styles.repairLabel}>{currentPrivilege.roles[role].roleName}</span>
                      )}
                    </div>

                  </div>
							  </div>
              : <CircularProgress />
						}
					</Drawer>
				</div>	
			);

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(PrivilegeDialogInfo);