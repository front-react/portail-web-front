import React from "react";
import Paper from "material-ui/Paper";

import { PatchApplication } from "../../components";

import { grey300, grey700 } from "material-ui/styles/colors";

import {
  ApplicationPrivilegesInfoContainer,
  ApplicationRolesInfoContainer,
  ApplicationUsersInfoContainer,
  ApplicationSetRolePrivilegeContainer,
  CreateRoleContainer,
  CreatePrivilegeContainer,
} from "../../containers";

const styles = {
  PaperDivTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: grey300,
    color: grey700,
    padding: 10,
  },
  PaperTitle: {
    fontSize: "24px",
  },
  PaperActionsTitle: {
    display: "flex",
    marginLeft: "auto",
  },
  bodyPaper: {
    padding: 10,
  },
  dashBoardGrid: {
    display: "flex",
    flexWrap: "wrap",
    marginTop: "15px",
    justifyContent: "space-around",
  },
  createButton: {
    margin: "10px 0",
    display: "flex",
    justifyContent: "left",
  },
};

const DetailleApplication = ({ application, refreshApplication }) => {
  const applicationId = application.applicationId;

  const applicationName = application.applicationName;

  return (
    <div>
      <Paper>
        <div style={styles.PaperDivTitle}>
          <span style={styles.PaperTitle}>{application.applicationName}</span>

          <PatchApplication idApplication={applicationId} />

        </div>
      </Paper>
      <div style={styles.dashBoardGrid}>
        <ApplicationPrivilegesInfoContainer idApplication={applicationId} />

        <ApplicationRolesInfoContainer
          idApplication={applicationId}
          applicationName={applicationName}
        />

        <ApplicationUsersInfoContainer idApplication={applicationId} />
      </div>
      <div style={styles.createButton}>
        <CreateRoleContainer idApplication={applicationId} />

        <CreatePrivilegeContainer idApplication={applicationId} />
      </div>
      <Paper>
        <ApplicationSetRolePrivilegeContainer idApplication={applicationId} />
      </Paper>
    </div>
  );
};

export default DetailleApplication;

/*
 */
