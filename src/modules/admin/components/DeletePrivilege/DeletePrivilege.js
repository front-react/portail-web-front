import React, {Component} from 'react'
import {connect} from 'react-redux'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import DeleteIcon from "material-ui/svg-icons/action/delete"
import {deletePrivilege} from '../../redux/actions/privilege/deletePrivilege'
import {getPrivilege} from '../../redux/actions/privilege/getPrivilege'
import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5
  },
  AlignText: {
      textAlign: 'center'
  },
  roleList: {
    marginTop: '10px',
    marginBottom: '10px',
  }
};

const mapStateToProps = (state) => {
  return {
    currentPrivilege: state.admin.privilege.currentPrivilege
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    deletePrivilege: (id, roleList) => { dispatch(deletePrivilege(id, roleList)) },
    getPrivilege: id => { dispatch(getPrivilege(id)) },
  }
};

class DeletePrivilege extends Component {

  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
    const privilegeId = this.props.privilege.privilegeId
    this.props.getPrivilege(privilegeId)
  };

  handleClose = (roleList) => {
    this.setState({open: false});
  };

  handleDelete = (roleList) => {
    const privilegeId = this.props.privilege.privilegeId
    this.props.deletePrivilege(privilegeId, roleList)
  }

  render() {
  
    
    return(
      <div>
        <IconButton
          onTouchTap={this.handleOpen}
        >
          <DeleteIcon />
        </IconButton>
        <Dialog
          title="Confirmer la supression ?"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <div style={styles.AlignText}>
            <div>
                <span>
                    Vous êtes sur le point de supprimer definitivement :
                </span>
            </div>
            <div>
                <span>
                    <strong>{this.props.privilege.privilegeName}</strong>
                </span>
            </div>
            <div>
                <span>
                   Avec les roles qui lui sont associées :
                </span>
            </div>
            {this.props.currentPrivilege &&
              <div>
                {Object.keys(this.props.currentPrivilege.roles).map( (role, index) =>
                  <div key={index} style={styles.roleList} >
                      <strong>{ this.props.currentPrivilege.roles[role].roleName }</strong>
                  </div>
                )}  
              </div>
            }
            {this.props.currentPrivilege &&
              <div style={styles.actionButton}>
                  <FlatButton onTouchTap={this.handleClose} label="NON"/>
                  <FlatButton onTouchTap={this.handleClose} label="SUPRESSSION !" onClick={() => this.handleDelete(this.props.currentPrivilege)}/>
              </div>
            }
          </div>
        </Dialog>
      </div>
    )
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DeletePrivilege)