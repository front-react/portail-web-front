import React from 'react'

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import { grey300, grey700} from "material-ui/styles/colors"

import {
  PatchPrivilege,
  DeletePrivilege,
  PrivilegeDialogInfo
} from '../'

const styles = {
    PaperDivTitle: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: grey300,
      color: grey700,
      padding: '10px'
    },
    PaperTitle: {
      fontSize: '24px'
    }
  }

const ApplicationsPrivilegesList = ({ApplicationPrivilegesList}) => {

  const listPrivileges = ApplicationPrivilegesList

    return (
        <div>
        <div style={styles.PaperDivTitle}>
            <span style={styles.PaperTitle}>Liste des privileges</span>
        </div>
        <Table selectable={false}>
        <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
          <TableRow selectable={false}>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Nom privileges</TableHeaderColumn>
            <TableHeaderColumn>Type</TableHeaderColumn>
            <TableHeaderColumn>Nom Type</TableHeaderColumn>
            <TableHeaderColumn>Update privilège</TableHeaderColumn>
            <TableHeaderColumn>Info privilège</TableHeaderColumn>
            <TableHeaderColumn>Delete privilège</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {Object.keys(listPrivileges).map((Privileges, index) =>
            <TableRow key={index} selectable={false}>
              <TableRowColumn>{listPrivileges[Privileges].privilegeId}</TableRowColumn>
              <TableRowColumn>{listPrivileges[Privileges].privilegeName}</TableRowColumn>
              <TableRowColumn>{listPrivileges[Privileges].privilegeTypeId}</TableRowColumn>
              <TableRowColumn>{listPrivileges[Privileges].privilegeTypeName}</TableRowColumn>
              <TableRowColumn>
                <PatchPrivilege privilege={listPrivileges[Privileges]} />
              </TableRowColumn>
              <TableRowColumn>
                <PrivilegeDialogInfo privilegeId={listPrivileges[Privileges].privilegeId} />
              </TableRowColumn>
              <TableRowColumn>
                <DeletePrivilege privilege={listPrivileges[Privileges]} />
              </TableRowColumn>
            </TableRow>
          )}
        </TableBody>
      </Table>
      </div>
    );
}

export default ApplicationsPrivilegesList

