import React, {Component} from 'react'
import {Field, reduxForm} from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Dialog from 'material-ui/Dialog'


const styles = {
  button: {
    marginRight: '20px',
  },
}

class FormCreateApplication extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  render() {

    const {handleSubmit, handleFormSubmit} = this.props;

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        type="submit"
        onClick={handleSubmit((data) => { handleFormSubmit(data) }) }
        onTouchTap={this.handleClose}
      />,
    ];

    return(
    <form onSubmit={handleSubmit(handleFormSubmit)}>
        <div>
            <RaisedButton label="Ajouter un application" style={styles.button} onTouchTap={this.handleOpen} />
            <Dialog
            title="Creation d'une application"
            actions={actions}
            modal={true}
            open={this.state.open}>
            <Field name="applicationName" component={TextField} hintText="Nom du application"/>
            </Dialog>
        </div>
    </form>
    )
  }
}

FormCreateApplication = reduxForm({
    form: 'FormCreateApplication',
  })(FormCreateApplication);

export default FormCreateApplication
