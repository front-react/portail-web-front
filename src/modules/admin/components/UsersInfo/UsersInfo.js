import React from 'react'
import {Link} from 'react-router'
import { SimpleStatsData } from '../../../../components/componup'
import ActionAccountCircle from 'material-ui/svg-icons/action/account-circle'

const styles = {
  styleLink: {
    textDecoration: 'none',
    display: 'block',
    width: '33%',
    height: '100%',
  }
};

  const UsersInfo = ({countUsers}) => {

  const usersCount = countUsers.totalUsers

      return (
        <Link to={'/user'} style={styles.styleLink} >
          <SimpleStatsData 
            icon={ActionAccountCircle} 
            dataNumber={usersCount} 
            dataLegend="Utilisateurs" 
            hexaColor={"#618934"} 
            sizeImg={"80px"}
            height={"120px"}
            width={"450px"}
            numberData={"42px"}
            numberLegend={"20px"}
            titleLabel={"Utilisateurs"}
          />
        </Link>
      )
    };

export default UsersInfo

/*
 */