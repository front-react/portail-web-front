import React, {Component} from 'react'
import {connect} from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import EditIcon from "material-ui/svg-icons/editor/mode-edit"
import {patchApplication} from '../../redux/actions/application/patchApplication'
import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5
  },
  iconButton: {
    width: '18px',
    height: '18px',
  }
};

const form = reduxForm({form: 'CreateApplication'});
const mapDispatchToProps = (dispatch) => {
  return {
    patchApplication: (id, body) => {
      dispatch(patchApplication(id, body))
    }
  }
};


class PatchApplication extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleFormSubmit = (requestBody) => {
    this.props.patchApplication(this.props.idApplication, requestBody);
  }

  render() {
    const {handleSubmit} = this.props;

    return(
      <div>
        <IconButton
          iconStyle={styles.iconButton}
          tooltip="Editer"
          tooltipPosition="top-center"
          onTouchTap={this.handleOpen}
        >
          <EditIcon />
        </IconButton>
        <Dialog
          title="Modification du nom de l'aplication"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <form style={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
            <Field name="applicationName" component={TextField} hintText="Nom de l'apllication"/>
            <div style={styles.actionButton}>
              <FlatButton onTouchTap={this.handleClose} label="Cancel"/>
              <FlatButton onTouchTap={this.handleClose} type="submit" label="Submit"/>
            </div>
          </form>
        </Dialog>
      </div>
    )
  }
}

PatchApplication = reduxForm({
  form: 'patchApplication',
})(PatchApplication);


export default connect(null, mapDispatchToProps)(PatchApplication)