import React, { Component } from "react";
import Dialog from "material-ui/Dialog";
import { reduxForm, Field } from "redux-form";
import { TextField } from "redux-form-material-ui";
import AddIcon from "material-ui/svg-icons/content/add";
import FlatButton from "material-ui/FlatButton";
//import SelectField from 'material-ui/SelectField';
//import MenuItem from 'material-ui/MenuItem';

const styles = {
  form: {
    display: "flex",
    flexDirection: "column",
  },
  actionButton: {
    alignSelf: "flex-end",
  },
  button: {
    marginRight: 5,
    backgroundColor: "#ffffff",
  },
  hidden: {
    visibility: "hidden",
  },
};

const privilegesTypes = [
  {
    privilegeTypeName: "Lecture",
    privilegeTypeId: "2",
  },
  {
    privilegeTypeName: "Administration",
    privilegeTypeId: "3",
  },
  {
    privilegeTypeName: "Ecriture",
    privilegeTypeId: "4",
  },
  {
    privilegeTypeName: "Divers",
    privilegeTypeId: "5",
  },
];

class CreatePrivilege extends Component {
  render() {
    const {
      handleSubmit,
      handleFormSubmit,
      createDialogStatus,
      handleClose,
      handleOpen,
    } = this.props;

    return (
      <div>
        <FlatButton
          style={styles.button}
          label="add privilege"
          onTouchTap={handleOpen}
          icon={<AddIcon />}
        />
        <Dialog
          title="Creation d'une privilege"
          modal={false}
          open={createDialogStatus}
          onRequestClose={handleClose}
          autoScrollBodyContent={true}
        >
          <span>Référence des types du privilege disponible : </span>
          {privilegesTypes.map((privilegeType, index) => (
            <div key={privilegeType.privilegeTypeId}>
              <span>{privilegeType.privilegeTypeId} : </span>
              <span>{privilegeType.privilegeTypeName}</span>
            </div>
          ))}
          <form style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>
            <Field
              name="privilegeName"
              component={TextField}
              hintText="Nom du privilège"
            />
            <Field
              name="privilegeTypeId"
              component={TextField}
              hintText="Référence des types du privilege disponible"
            />

            <div style={styles.actionButton}>
              <FlatButton onTouchTap={handleClose} label="Cancel" />
              <FlatButton
                onTouchTap={handleClose}
                type="submit"
                label="Submit"
              />
            </div>
          </form>
        </Dialog>
      </div>
    );
  }
}

export default (CreatePrivilege = reduxForm({
  form: "CreatePrivilege",
})(CreatePrivilege));
