import React from 'react'
import {Link} from 'react-router'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import IconButton from 'material-ui/IconButton'
import IconSearch from 'material-ui/svg-icons/action/search'
import {grey300, grey700} from "material-ui/styles/colors"
import Divider from 'material-ui/Divider'
import { SearchBar } from '../../../../components/componup'

import {CreateUserStepperContainer} from '../../containers'

import { DeleteUser } from '../'


const styles = {
  PaperDivTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: grey300,
    color: grey700,
    padding: 10,
  },
  PaperTitle: {
    fontSize: '24px'
  },
  PaperActionsTitle: {
    display: 'flex',
    marginLeft: 'auto'
  },
  bodyPaper: {
    padding: 10
  },
  actionBAr: {
    paddingTop: '10px',
    display: 'flex',
    alignItems: 'center',
  },
  spanSearchBar: {
    display: 'flex',
    width: '400px',
    justifyContent: 'space-between',
    marginRight: '20px',
  }
};

const UserList = ({userList, filterUserList, deleteUser, refreshUser}) => {

  return (
    <Paper>
      <div style={styles.PaperDivTitle}>
        <span style={styles.PaperTitle}>Liste des utilisateurs</span>
      </div>
      <Divider />
      <div style={styles.PaperDivTitle}>
        {filterUserList &&
          <div style={styles.actionBAr}>
              <span style={styles.spanSearchBar}>
                <SearchBar onChange={(value) => filterUserList(value, "username")}  hintText="utilisateur"/>
              </span>
          </div>
        }
        <div style={styles.actionBAr}>
          <CreateUserStepperContainer />
        </div>
      </div>
      <div>
        <Table selectable={false}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow selectable={false}>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Nom</TableHeaderColumn>
              <TableHeaderColumn>Nombres Clients</TableHeaderColumn>
              <TableHeaderColumn>Voir la fiche IT</TableHeaderColumn>
              <TableHeaderColumn>Delete User</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {Object.keys(userList).map((user) =>
              <TableRow key={user} selectable={false}>
                <TableRowColumn>{userList[user].userId}</TableRowColumn>
                <TableRowColumn>{userList[user].username}</TableRowColumn>
                <TableRowColumn>{userList[user].clients.length || "non renseigné"}</TableRowColumn>
                <TableRowColumn>
                  <Link to={{pathname: "/user/"+userList[user].userId}}>
                  <IconButton>
                    <IconSearch />
                  </IconButton>
                  </Link>
                </TableRowColumn>
                <TableRowColumn>
                  <DeleteUser 
                    idUser={userList[user].userId} 
                  />
                </TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    </Paper>
  )
};


export default UserList