import React from 'react'
import Paper from 'material-ui/Paper';
import {Tabs, Tab} from 'material-ui/Tabs';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {grey200, grey500, orange500} from "material-ui/styles/colors"
import IconButton from 'material-ui/IconButton';
import DeleteIcon from "material-ui/svg-icons/action/delete"
import ActionAccountBox from "material-ui/svg-icons/action/account-box"
import {grey300, grey700} from "material-ui/styles/colors"
import {Link} from 'react-router'

import {
  PatchProfile
} from '../../components'

import {ProfileRolesApplicationsListContainer} from '../../containers'


const styles = {
  headerProfile: {
    height: '150px',
    padding: '20px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  nameProfile: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '40px',
  },
  buildIcon: {
    height: '70px',
    width: '70px',
    marginRight: '40px',
  },
  numberUser: {
    height: '80px',
    width: '28%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    fontSize: '24px',
    color: orange500,
  },
  tabsProfile: {
    marginTop: '20px',
  },
  tabItemContainerProfile: {
    backgroundColor: grey200,
    borderBottom: '1px solid '+grey500 ,
  },
  tabContainerProfile: {
    marginTop: 20,
  },
  tabProfile: {
    width: '100px',
    //backgroundColor: grey200,
    color: grey500,
  },
  tabButtonProfile: {
    textTransform: 'none',
    fontWeight: '300',
  },
  inkBar: {
    width: '100px',
    backgroundColor: orange500
  },
  PaperDivTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: grey300,
    color: grey700,
    padding: 10,
  },
  PaperTitle: {
    fontSize: '24px'
  },
  PaperActionsTitle: {
    display: 'flex',
    marginLeft: 'auto'
  },
}

const Profile = ({profile, profileId}) => {

  return (
    <div>

      <Paper>
        <div style={styles.PaperDivTitle}>
          <span style={styles.PaperTitle}>{profile.profileName}</span>

            <PatchProfile profileId={profile.profileId} profileName={profile.profileName} />

        </div>
        
      </Paper>

      <Tabs
        style={styles.tabsProfile}
        inkBarStyle={styles.inkBar}
        contentContainerStyle={styles.tabContainerProfile}
        tabItemContainerStyle={styles.tabItemContainerProfile}>
        <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Application" >
          <div style={styles.bodyPaper} >
            <ProfileRolesApplicationsListContainer profileId={profileId} />
          </div>
        </Tab>
        <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Membres" >
          <Paper style={styles.paperDevice}>
            <div style={styles.PaperDivTitle}>
            </div>
            <div style={styles.bodyPaper}>
              <Table selectable={false}>
                <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                  <TableRow selectable={false}>
                    <TableHeaderColumn>ID</TableHeaderColumn>
                    <TableHeaderColumn>User</TableHeaderColumn>
                    <TableHeaderColumn>Delete</TableHeaderColumn>
                    <TableHeaderColumn>Voir User</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                  {profile.users.map((user) =>
                    <TableRow key={user.userId}>
                      <TableRowColumn >{user.userId}</TableRowColumn>
                      <TableRowColumn>{user.username}</TableRowColumn>
                      <TableRowColumn>
                        <IconButton>
                          <DeleteIcon />
                        </IconButton>
                      </TableRowColumn>
                      <TableRowColumn>
                          <Link to={"/user/"+user.userId}>
                            <ActionAccountBox />
                          </Link>
                      </TableRowColumn>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
          </Paper>
        </Tab>
        
      </Tabs>


    </div>
  )
}

export default Profile


/*


 */