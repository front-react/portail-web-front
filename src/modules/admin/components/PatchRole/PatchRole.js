import React, {Component} from 'react'
import {connect} from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import EditIcon from "material-ui/svg-icons/editor/mode-edit"
import {patchRole} from '../../redux/actions/role/patchRole'
import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5
  },
  iconButton: {
    width: '18px',
    height: '18px',
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    patchRole: (id, body) => {dispatch(patchRole(id, body)) }
  }
};


class PatchRole extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleFormSubmit = (requestBody) => {
    const roleId = this.props.role.roleId
    this.props.patchRole(roleId, requestBody);
  }

  render() {
    const {handleSubmit} = this.props;

    return(
      <div>
        <IconButton
          iconStyle={styles.iconButton}
          onTouchTap={this.handleOpen}
        >
          <EditIcon />
        </IconButton>
        <Dialog
          title="Modification du nom du role"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <form style={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
            <div>
                <span>Ancien role :: <strong>{this.props.role.roleName}</strong></span>
            </div>
            <div>
                <Field 
                    name="roleName" 
                    component={TextField} 
                    hintText="Nouveau role"/>
            </div>
            <div style={styles.actionButton}>
              <FlatButton onTouchTap={this.handleClose} label="Cancel"/>
              <FlatButton onTouchTap={this.handleClose} type="submit" label="Submit"/>
            </div>
          </form>
        </Dialog>
      </div>
    )
  }
}

PatchRole = reduxForm({
  form: 'PatchRole',
})(PatchRole);


export default connect(null, mapDispatchToProps)(PatchRole)