import React from 'react'
import {Link} from 'react-router'
import { SimpleStatsData } from '../../../../components/componup'
import HardwareLaptopWindows from 'material-ui/svg-icons/hardware/laptop-windows'

const styles = {
  styleLink: {
    textDecoration: 'none',
    display: 'block',
    width: '33%',
    height: '100%',
  }
};

  const ApplicationsInfo = ({countApplications}) => {

  const applicationsCount = countApplications.totalApplications

      return (
        <Link to={'/admin/application'} style={styles.styleLink} >
          <SimpleStatsData 
            icon={HardwareLaptopWindows} 
            dataNumber={applicationsCount} 
            dataLegend="Applications" 
            hexaColor={"#9E9E9E"} 
            sizeImg={"80px"}
            height={"120px"}
            width={"450px"}
            numberData={"42px"}
            numberLegend={"20px"}
            titleLabel={"Applications"}
          />
        </Link>
      )
    };

export default ApplicationsInfo

/*
 */