import React, { Component } from 'react'
import Dialog from 'material-ui/Dialog'
import { reduxForm, Field } from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import FlatButton from 'material-ui/FlatButton'
import AddIcon from "material-ui/svg-icons/content/add"

const styles = {
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  actionButton: {
    alignSelf: 'flex-end'
  },
  button: {
    marginRight: 5,
    backgroundColor: '#ffffff'
  },
  hidden: {
    visibility: 'hidden',
  }
}

class CreateRole extends Component {

  render() {

    const {handleSubmit, handleFormSubmit, createDialogStatus, handleClose, handleOpen} = this.props

    return (
      <div>
        <FlatButton style={styles.button} label="add role" onTouchTap={handleOpen} icon={<AddIcon/>} />
        <Dialog
                title="Création d'une role"
                modal={false}
                open={createDialogStatus}
                onRequestClose={handleClose}
                autoScrollBodyContent={true}
                >
                <form style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>
                <Field name="roleName" component={TextField} hintText="Nom du role"/>
                <div style={styles.actionButton}>
                    <FlatButton onTouchTap={handleClose} label="Cancel"/>
                    <FlatButton onTouchTap={handleClose} type="submit" label="Submit"/>
                </div>
                </form>
        </Dialog>
      </div>
    );
  }
}

export default CreateRole = reduxForm({
    form: 'CreateRole',
  })(CreateRole);
