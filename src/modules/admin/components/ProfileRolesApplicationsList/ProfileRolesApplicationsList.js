import React from 'react';

import { 
  SelectButton 
} from '../../components'
import Paper from 'material-ui/Paper'
import {grey300, grey700} from "material-ui/styles/colors"

const styles = {
  globalApp: {
    padding: '0px 5%',
  },
  boxApp: {
    width: 'calc(100% * (1/3) - 40px)',
    display: 'inline-block',
    margin: '15px 10px',
    textAlign: 'center'
  },
  PaperDivTitle: {
    display: 'flex',
    alignItems: 'center',
    backgroundColor: grey300,
    color: grey700,
    padding: '10px',
  },
  PaperTitle: {
    fontSize: '24px',
    textAlign: 'center',
    width: '100%'
  },
};

const ProfileRolesApplicationsList = ({applicationList, roleList, roleApplicationProfileList, deleteProfileRoleApp, setProfileRoleApp}) => {

  return (
    <div>
     {roleList.length > 0
        ? <Paper style={styles.globalApp} >
          {applicationList.map((application, index) => 
            <div key={application.applicationId} style={styles.boxApp}>
              <div style={styles.PaperDivTitle} >
                <span style={styles.PaperTitle} > {application.applicationName } </span>
              </div>

              <SelectButton
                rolesGroupedByApplication={roleList}
                rolesAssignedGroupedByApplication={roleApplicationProfileList}
                application={application}
                deleteProfileRoleApp={deleteProfileRoleApp}
                setProfileRoleApp={setProfileRoleApp}
               />
              
            </div>
          )}
          </Paper>
        : <span>Vos Applications n'ont pas de role</span>
      } 
    </div>
  );
}

export default ProfileRolesApplicationsList