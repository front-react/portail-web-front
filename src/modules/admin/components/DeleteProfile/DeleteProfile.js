import React, {Component} from 'react'
import {connect} from 'react-redux'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import DeleteIcon from "material-ui/svg-icons/action/delete"
import {deleteProfile} from '../../redux/actions/profile/deleteProfile'
import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    deleteProfile: id => {
      dispatch(deleteProfile(id))
    }
  }
};

class DeleteProfile extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleDelete = () => {
    this.props.deleteProfile(this.props.idProfile)
  }

  render() {

    return(
      <div>
        <IconButton
          tooltip="Suprimer"
          tooltipPosition="top-center"
          onTouchTap={this.handleOpen}
        >
          <DeleteIcon />
        </IconButton>
        <Dialog
          title="Confirmer la supression ?"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <div style={styles.actionButton}>
            <FlatButton onTouchTap={this.handleClose} label="NON"/>
            <FlatButton onTouchTap={this.handleClose} label="SUPRESSSION !" onClick={() => this.handleDelete()}/>
          </div>
        </Dialog>
      </div>
    )
  }
}
export default connect(null, mapDispatchToProps)(DeleteProfile)