import React from "react";
import { Link } from "react-router";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from "material-ui/Table";
import Paper from "material-ui/Paper";
import IconButton from "material-ui/IconButton";
import IconSearch from "material-ui/svg-icons/action/search";
import { grey300, grey700 } from "material-ui/styles/colors";

import { SearchBar } from "../../../../components/componup";
import Divider from "material-ui/Divider";

import { CreateProfileContainer } from "../../containers";

import { 
  ProfileDialogInfo,
  DeleteProfile
 } from "../../components";

const styles = {
  PaperDivTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: grey300,
    color: grey700,
    padding: "10px",
  },
  PaperTitle: {
    fontSize: "24px",
  },
  PaperActionsTitle: {
    display: "flex",
    marginLeft: "auto",
  },
  bodyPaper: {
    padding: "10px",
  },
  actionBAr: {
    paddingTop: "10px",
    display: "flex",
    alignItems: "center",
  },
  spanSearchBar: {
    display: "flex",
    width: "400px",
    justifyContent: "space-between",
    marginRight: "20px",
  },
};

const ProfilesList = ({ profileList, filterProfileList }) => {

  return (
    <Paper>
      <div style={styles.PaperDivTitle}>
        <span style={styles.PaperTitle}>Liste des Profils</span>
      </div>
      <Divider />
      <div style={styles.PaperDivTitle}>
        {filterProfileList && (
          <div style={styles.actionBAr}>
            <span style={styles.spanSearchBar}>
              <SearchBar
                onChange={value => filterProfileList(value, "profileName")}
                hintText="Recherchez un profile"
              />
            </span>
          </div>
        )}
        <div style={styles.actionBAr}>
          <CreateProfileContainer />
        </div>
      </div>
      <div>
        <Table selectable={false}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow selectable={false}>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Nom</TableHeaderColumn>
              <TableHeaderColumn>Voir la fiche Profile</TableHeaderColumn>
              <TableHeaderColumn>Infos Profile</TableHeaderColumn>
              <TableHeaderColumn>Supprimer</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {Object.keys(profileList).map(profileId => (
              <TableRow key={profileId} selectable={false}>
                <TableRowColumn>
                  {profileList[profileId].profileId}
                </TableRowColumn>
                <TableRowColumn>
                  {profileList[profileId].profileName}
                </TableRowColumn>
                <TableRowColumn>
                  <Link
                    to={{
                      pathname:
                        "/admin/profile/" + profileList[profileId].profileId,
                    }}
                  >
                    <IconButton>
                      <IconSearch />
                    </IconButton>
                  </Link>
                </TableRowColumn>
                <TableRowColumn>
                  <ProfileDialogInfo
                    profileId={profileList[profileId].profileId}
                  />
                </TableRowColumn>
                <TableRowColumn>
                  <DeleteProfile profileId={profileList[profileId].profileId}/>
                </TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </Paper>
  );
};

export default ProfilesList;
