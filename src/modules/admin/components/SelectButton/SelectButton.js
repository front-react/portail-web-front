import React from 'react'
import { 
  SelectButtonProfileAssigned,
  SelectButtonProfileNotAssigned,
} from '../../components'



const SelectButton = ({rolesGroupedByApplication, rolesAssignedGroupedByApplication, application, deleteProfileRoleApp, setProfileRoleApp}) => {

	const rolesGroupedByApplications = rolesGroupedByApplication.filter((roles) => roles.applicationId === application.applicationId )

	const rolesAssignedGroupedByApplications = rolesAssignedGroupedByApplication.filter((roleAssigned) => roleAssigned.applicationId === application.applicationId)

  return (
    <div>
			{Object.keys(rolesAssignedGroupedByApplications)[0] !== undefined
					?
					<SelectButtonProfileAssigned
					rolesGroupedByApplication={rolesGroupedByApplications}
					rolesAssignedGroupedByApplication={rolesAssignedGroupedByApplications}
					application={application}
					deleteProfileRoleApp={deleteProfileRoleApp}
					setProfileRoleApp={setProfileRoleApp}
				/>
					:
					<SelectButtonProfileNotAssigned
					rolesGroupedByApplication={rolesGroupedByApplications}
					rolesAssignedGroupedByApplication={0}
					application={application}
					deleteProfileRoleApp={deleteProfileRoleApp}
					setProfileRoleApp={setProfileRoleApp}
				/>
				}
				
    </div>
  )
};


export default SelectButton