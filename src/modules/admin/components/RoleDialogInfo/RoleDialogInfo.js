import React, {Component} from 'react'
import {connect} from 'react-redux'
import IconButton from 'material-ui/IconButton';
import ActionInfo from 'material-ui/svg-icons/action/info'
import {getRole} from '../../redux/actions/role/getRole'
import ClearIcon from 'material-ui/svg-icons/content/clear';
import CircularProgress from 'material-ui/CircularProgress';
import Drawer from 'material-ui/Drawer';

const styles = {
  titleDrawer: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '24px',
    color: 'rgb(220,220,220)',
    backgroundColor: 'rgb(50,50,50)',
  },
  drawerListItem: {
    display: 'flex',
    flexDirection: 'column',
    margin: '5px 0 5px 0',
  },
  drawerLabel: {
    fontWeight: '700',
    letterSpacing: '1px',
    color: 'rgb(10,10,10)',
    backgroundColor: 'rgb(150,150,150)',
    textIndent: '10px',
    padding : '2px',
  },
  repairDetailList: {
    padding : '5px',
    display: 'flex',
    flexDirection: 'column',
  },
  repairLabel: {
    padding : '10px',
    color: 'rgb(50,50,50)',
    backgroundColor: 'rgb(220,220,220)',
    borderBottom: '2px solid rgb(150,150,150)'
  },
  colorButton: {
    backgroundColor: 'rgb(250,250,250)',
  },
  titleRepair: {
    textIndent: '20px',
  },
};

const mapStateToProps = (state) => {
    return {
      currentRole: state.admin.role.currentRole,
    }
  };

const mapDispatchToProps = (dispatch) => {
  return {
    getRole: (id) => {dispatch(getRole(id))}
  }
};


class RoleDialogInfo extends Component {

    state = {
        open: false,
    };

    handleOpen = () => {
      this.setState({open: true});
      const roleId = this.props.roleId
      this.props.getRole(roleId);
    };

    handleClose = () => {
      this.setState({open: false});
    };

  render() {

    const {currentRole} = this.props;

    return(
      <div>
        <IconButton
          iconStyle={styles.iconButton}
          onTouchTap={this.handleOpen}
        >
          <ActionInfo />
        </IconButton>
        <Drawer
          open={this.state.open}
          openSecondary={true}
          docked={false}
          width={400}
        >
          {currentRole 
          
            ? <div>
              {Object.keys(currentRole).map((role, index) => 
            
                <span key={index}>
                <div style={styles.titleDrawer}>
                  <IconButton 
                    onTouchTap={this.handleClose}
                    style={styles.colorButton} 
                  >
                    <ClearIcon />
                  </IconButton>
                  <span style={styles.titleRepair}>Role {currentRole[role].roleName}</span>
                </div>

                <div style={styles.repairDetailList}>

                  <div style={styles.drawerListItem}>
                    <span style={styles.drawerLabel}>ID Role : {currentRole[role].roleId}</span>
                    <span style={styles.repairLabel}>Role: {currentRole[role].roleName}</span>
                  </div>

                  <div style={styles.drawerListItem}>
                    <span style={styles.drawerLabel}>Nombre de privileges</span>
                    <span style={styles.repairLabel}>{currentRole[role].privileges.length}</span>
                  </div>

                  <div style={styles.drawerListItem}>
                    <span style={styles.drawerLabel}>Date de création</span>
                    <span style={styles.repairLabel}>{new Date(currentRole[role].dateOn).toLocaleDateString("fr-FR") || 'Inconnu'}</span>
                  </div>

                </div>
                </span>
              )}
              </div>
            : <CircularProgress />  
          }
        </Drawer>
      </div>
    );

  }
}


export default connect(mapStateToProps, mapDispatchToProps)(RoleDialogInfo);