import React, {Component} from 'react'
import {connect} from 'react-redux'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from "material-ui/svg-icons/action/delete"
import FlatButton from 'material-ui/FlatButton'
import {deleteUser} from '../../redux/actions/user/deleteUser'


const styles = {
    actionButton: {
      marginTop: 24,
      display: 'flex',
      justifyContent: 'center',
    },
    button: {
      marginRight: 5
    }
  }


const mapDispatchToProps = (dispatch) => {
    return {
      deleteUser: (id) => {dispatch(deleteUser(id))},
      }
    };

class DeleteUser extends Component {

  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleDelete = () => {
    this.props.deleteUser(this.props.idUser)
  }

  render() {

    return (
      <div>
      <IconButton
        onTouchTap={this.handleOpen}
      >
        <DeleteIcon />
      </IconButton>
      <Dialog
        title="Confirmer la supression de l'utilisateur ?"
        modal={false}
        open={this.state.open}
        onRequestClose={this.handleClose}
        autoScrollBodyContent={true}
      >
        <div style={styles.actionButton}>
          <FlatButton onTouchTap={this.handleClose} label="NON"/>
          <FlatButton onTouchTap={this.handleClose} label="SUPRESSSION !" onClick={() => this.handleDelete()}/>
        </div>
      </Dialog>
    </div>
    );
  }

}

export default connect(null, mapDispatchToProps)(DeleteUser);

