import React from 'react'
import {Link} from 'react-router'
import { SimpleStatsData } from '../../../../components/componup'
import ActionAssignment from 'material-ui/svg-icons/action/assignment'

const styles = {
  styleLink: {
    textDecoration: 'none',
    display: 'block',
    width: '33%',
    height: '100%',
  }
};

  const ProfilesInfo = ({countProfiles}) => {

  const profilesCount = countProfiles.totalProfiles

      return (
        <Link to={'/admin/profile'} style={styles.styleLink} >
          <SimpleStatsData 
            icon={ActionAssignment} 
            dataNumber={profilesCount}
            dataLegend="Profiles" 
            hexaColor={"#229185"} 
            sizeImg={"80px"}
            height={"120px"}
            width={"450px"}
            numberData={"42px"}
            numberLegend={"20px"}
            titleLabel={"Profiles"}
          />
        </Link>
      )
    };

export default ProfilesInfo

/*
 */