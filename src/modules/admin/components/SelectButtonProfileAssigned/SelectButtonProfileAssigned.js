import React, { Component } from "react";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";

class SelectButtonProfileAssigned extends Component {
  state = {
    value: this.props.rolesAssignedGroupedByApplication[
      Object.keys(this.props.rolesAssignedGroupedByApplication)[0]
    ].roleId,
  };

  handleChangeRole = (roleId, values) => {
    if (roleId) {
      this.props.setProfileRoleApp(values);
      this.setState({ value: values });
    } else {
      this.props.deleteProfileRoleApp(values);
      this.setState({ value: 0 });
    }
  };

  handleChange = (event, index, value) => this.setState({ value: value });

  render() {
    const {
      rolesGroupedByApplication,
      rolesAssignedGroupedByApplication,
    } = this.props;
    return (
      <div>
        <div>
          {rolesAssignedGroupedByApplication !== 0 && (
            <span>
              <strong>Profile assigné :</strong>{" "}
              {
                rolesAssignedGroupedByApplication[
                  Object.keys(rolesAssignedGroupedByApplication)[0]
                ].roleName
              }
            </span>
          )}
        </div>
        <div>
          <SelectField
            floatingLabelText="Role par defaut"
            floatingLabelFixed={true}
            value={this.state.value}
            onChange={() => this.handleChange}
          >
            {rolesAssignedGroupedByApplication !== 0 && (
              <MenuItem
                value={0}
                primaryText="Aucun role"
                onClick={() => this.handleChangeRole(false, this.state.value)}
              />
            )}

            {Object.keys(
              rolesGroupedByApplication,
            ).map((roleListApp, index) => (
              //<span key={index} > {rolesGroupedByApplication[application.applicationId][roleListApp].roleId} </span>
              <MenuItem
                key={index}
                value={rolesGroupedByApplication[roleListApp].roleId}
                primaryText={rolesGroupedByApplication[roleListApp].roleName}
                checked={
                  this.props.rolesAssignedGroupedByApplication[
                    Object.keys(this.props.rolesAssignedGroupedByApplication)[0]
                  ].roleId === rolesGroupedByApplication[roleListApp].roleId
                }
                onClick={() =>
                  this.handleChangeRole(
                    true,
                    rolesGroupedByApplication[roleListApp].roleId,
                  )}
              />
            ))}
          </SelectField>
        </div>
      </div>
    );
  }
}

export default SelectButtonProfileAssigned;
