import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs';
import {UserProfileITContainer} from '../../containers'
import {grey200, grey500, orange500} from "material-ui/styles/colors"
import{PersonInfoHeaderContainer} from '../../../rh/containers'
import {
  UserProfileInfo
} from '../'
import {UserApplicationsRoleContainer} from '../../containers'

const styles = {
  userProfile: {
    padding: '40px',
  },
  PaperPerson: {
    display: 'flex',
    alignItems: 'center',
    padding: '40px 15px',
  },
  PaperPersonProfile: {
    display: 'flex',
    alignItems: 'center',
    width: '50%',
    borderRight: 'solid #E0E0E0 1px'
  },
  PaperPersonInformation: {
    height: '120px',
    display: 'flex',
    flexDirection: 'column',
  },
  PaperPersonInformationName: {
    fontSize: '28px',
    marginBottom: '5px',
  },
  PaperPersonInformationJob: {
    fontSize: '20px',
    color: '#757575',
    marginBottom: '10px',
  },
  PaperPersonInformationContactPro: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '16px',
    color: '#757575',
    marginBottom: '3px',
  },
  contactIcon: {
    color:"#757575",
    width: '16px',
    height: '16px',
    marginRight: '5px'
  },
  avatar: {
    marginRight: '20px',
  },


  tabsProfile: {
    marginTop: 20,
  },
  tabItemContainerProfile: {
    backgroundColor: grey200,
    borderBottom: '1px solid '+grey500 ,
  },
  tabContainerProfile: {
    marginTop: 20,
  },
  tabProfile: {
    color: grey500,
  },
  tabButtonProfile: {
    textTransform: 'none',
    fontWeight: '300',
  },
  inkBar: {
    backgroundColor: orange500
  },
  profileMain: {
    // display: 'flex',
    // flexDirection: 'row',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
};


const UserProfile = ({userInformation}) => {
  const personId = userInformation.personId;
  const userId = userInformation.userId;
  const userRole = userInformation.userId;

  return (
    <div style={styles.userProfile}>
      <div style={styles.profileMain} >
        <div style={styles.row} >
          <PersonInfoHeaderContainer personId={userInformation.personId} />
          <UserProfileInfo userInformation={userInformation} personId={personId} />
        </div>
      </div>
      <Tabs
        style={styles.tabsProfile}
        inkBarStyle={styles.inkBar}
        contentContainerStyle={styles.tabContainerProfile}
        tabItemContainerStyle={styles.tabItemContainerProfile}>
        <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Applications" >
          <UserApplicationsRoleContainer userId={userRole} />
        </Tab>
        <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="IT">
          <UserProfileITContainer userId={userId} />
        </Tab>
      </Tabs>
    </div>
  )
}

export default UserProfile