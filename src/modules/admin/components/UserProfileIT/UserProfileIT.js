import React from 'react'
import Paper from 'material-ui/Paper';
import Toggle from 'material-ui/Toggle';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {grey300, grey500, grey700, orange500} from "material-ui/styles/colors"

import { 
  AddSimpleClientContainers,
  AddClientsContainer 
} from '../../containers'

const styles = {
  profileIT: {
    display: 'flex',
    flexDirection: 'column',
  },
  profile:{
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paperUser: {
    height: '150px',
    width: '68%',
    display: 'flex',
    alignItems: 'center',
    padding: 10,
    fontSize: '14px',
  },
  divUserPortail: {
    width: '30%',
  },
  userPortail: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    color: grey700,
  },
  divStats: {
    width: '70%',
    color: grey500,
    fontSize: '30px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  listStats: {
    width: '70%',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  statesName: {
    display: 'inline-block',
    margin: '0 0 0 0',
    width: 'calc(100% * (1/3) - 10px - 1px)',
  },
  paperProfile: {
    height: '150px',
    width: '28%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: orange500,
    color: 'white',
  },
  paperProfileHeader:{
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buildIcon: {
    height: '70px',
    width: '70px',
  },
  paperClient: {
    marginTop: '20px',
    padding: '10px',
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  verticalDivider: {
    height: '80%',
    borderRight: '1px solid '+ grey300,
  },
  subheader: {
    lineHeight: '30px',
  },
  information: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: '16px',
    marginBottom: '5px',
  },
  informationLabel: {
    margin: '4px 0',
    fontWeight: '300',
  },
  positionLisClient: {
    width: '100%',
    display: 'inline-table'
  },
  addClient: {
    textAlign: 'center',
  }
}

const UserProfileIT = ({userInformation, toggleSetClientDefault, toggleRemoveSetClient}) => {

  return (
    <div style={styles.profileIT}>
      <div style={styles.addClient}>
        {userInformation.profileId === null 
        ? <span>Attribuer un profil a cet utilisateur avant de lui ajouter des clients</span>
        : <AddClientsContainer user={userInformation} />
        }
      </div>
     
      
      <Paper style={styles.paperClient}>
      {userInformation.clients.length !== 0
      ?
      <Table selectable={false}>
        <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
          <TableRow selectable={false}>
            <TableHeaderColumn>id</TableHeaderColumn>
            <TableHeaderColumn>Nom</TableHeaderColumn>
            <TableHeaderColumn>Client Principale</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        
        <TableBody displayRowCheckbox={false}>
          
          {userInformation.clients.map((client) =>
            <TableRow key={client.clientId}>
              <TableRowColumn>{client.clientId}</TableRowColumn>
              <TableRowColumn>{client.clientName}</TableRowColumn>
              <TableRowColumn>
              {client.isDefault === "1"
                ? <Toggle
                  defaultToggled={true}
                  onToggle={() => toggleRemoveSetClient(client.clientId, userInformation.profileId, 0)}
                  />
                : <Toggle
                  defaultToggled={false}
                  onToggle={() => toggleSetClientDefault(client.clientId, userInformation.profileId, 1)}
                  />
              }
                
              </TableRowColumn>
            </TableRow>
          )}
          
          
        </TableBody>
        
      </Table>
      : <div style={styles.addClient} >
        {userInformation.profileId !== null &&
        <span>
          <div>Vous n'avez aucun client: <strong>Ajouter des clients</strong></div>
          <div style={styles.addClient} >
            <AddSimpleClientContainers userId={userInformation.userId} profileId={userInformation.profileId} />
          </div> 
        </span> 
        }
        </div>
        }
      </Paper>
    </div>
  )

}

export default UserProfileIT