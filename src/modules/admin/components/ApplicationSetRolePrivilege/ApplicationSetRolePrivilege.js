import React from "react";
import { Tabs, Tab } from "material-ui/Tabs";
import SwipeableViews from "react-swipeable-views";
import {
  orange500,
  grey300,
  grey700,
  grey200,
} from "material-ui/styles/colors";
import { PrivilegesGroupeList } from "../index";
import RaisedButton from "material-ui/RaisedButton";

import { CreateRoleContainer } from "../../containers";

const styles = {
  stylesPaperApp: {
    padding: 10,
  },
  inkBar: {
    backgroundColor: orange500,
  },
  tabItemContainerProfile: {
    backgroundColor: grey200,
    borderBottom: "2px solid " + grey200,
  },
  tabContainerProfile: {
    marginTop: 20,
  },
  PaperDivTitle: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: "5px",
  },
  PaperTitle: {
    fontSize: "24px",
  },
  PaperActionsTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  tabs: {
    marginBottom: 20,
  },
  buttonTab: {
    color: grey700,
  },
  tab: {
    borderBottom: "1px solid" + grey300,
    backgroundColor: "white",
    color: "black",
  },
  tabContent: {
    margin: "10px 0px",
  },
  subTitle: {
    fontSize: "20px",
  },
  toggle: {
    padding: "5px 20px",
    width: "30%",
  },
  componentAlign: {
    textAlign: "center",
  },
};

const groupByTypePrivileges = currentApplication => {
  const typeId = [2, 3, 4, 5];

  let groupPrivileges = {};

  typeId.forEach(id => {
    groupPrivileges[id] = [];
  });
  currentApplication.forEach(privileges => {
    groupPrivileges[privileges.privilegeTypeId].push(privileges);
  });

  return groupPrivileges;
};

const ApplicationSetRolePrivilege = ({
  currentApplication,
  handleTabChange,
  handleToggle,
  slideIndex,
  rolePrivilegesList,
  intialisationPrivileges,
  privilegesList,
  handleTabChangeIndex,
  applicationId,
}) => {
  const PrivilegesList = privilegesList;

  //const rolePrivilegesAndPrivilegesList = rolePrivilegesList && PrivilegesList;

  const privilegeGroupedByType = groupByTypePrivileges(PrivilegesList);

  //const idRole = slideIndex;

  //const rolePrivileges = rolePrivilegesList.find((role) => role.roleId === ''+idRole+'')
  const rolePrivileges =
    rolePrivilegesList[Object.keys(rolePrivilegesList)[slideIndex]];

  return (
    <div>
      {rolePrivilegesList.length > 0 ? (
        <div>
          <Tabs
            style={styles.tabs}
            inkBarStyle={styles.inkBar}
            contentContainerStyle={styles.tabContainerProfile}
            tabItemContainerStyle={styles.tabItemContainerProfile}
            onChange={handleTabChange}
            value={slideIndex}
          >
            {Object.keys(rolePrivilegesList).map((role, index) => (
              <Tab
                style={styles.tab}
                buttonStyle={styles.buttonTab}
                key={index}
                label={rolePrivilegesList[role].roleName}
                value={index}
                //onClick={() => handleTabChangeIndex(index)}
              />
            ))}
          </Tabs>
          <SwipeableViews index={slideIndex} onChangeIndex={handleTabChange}>
            {rolePrivilegesList.map((roleId, index) => (
              <div key={index} style={styles.tabContent}>
                {rolePrivileges ? (
                  <div>
                    <div>
                      <PrivilegesGroupeList
                        privilegeGrouped={privilegeGroupedByType[2]}
                        nameGroup={"Lecture"}
                        roleId={rolePrivileges.roleId}
                        handleToggle={handleToggle}
                        rolePrivilegesList={rolePrivilegesList}
                        rolePrivileges={rolePrivileges}
                      />
                    </div>
                    <div>
                      <PrivilegesGroupeList
                        privilegeGrouped={privilegeGroupedByType[3]}
                        nameGroup={"Adminitration"}
                        roleId={rolePrivileges.roleId}
                        handleToggle={handleToggle}
                        rolePrivilegesList={rolePrivilegesList}
                        rolePrivileges={rolePrivileges}
                      />
                    </div>
                    <div>
                      <PrivilegesGroupeList
                        privilegeGrouped={privilegeGroupedByType[4]}
                        nameGroup={"Ecriture"}
                        roleId={rolePrivileges.roleId}
                        handleToggle={handleToggle}
                        rolePrivilegesList={rolePrivilegesList}
                        rolePrivileges={rolePrivileges}
                      />
                    </div>
                    <div>
                      <PrivilegesGroupeList
                        privilegeGrouped={privilegeGroupedByType[5]}
                        nameGroup={"Divers"}
                        roleId={rolePrivileges.roleId}
                        handleToggle={handleToggle}
                        rolePrivilegesList={rolePrivilegesList}
                        rolePrivileges={rolePrivileges}
                      />
                    </div>
                  </div>
                ) : (
                  <div style={styles.componentAlign}>
                    <div style={styles.tabContent}>
                      Ce Role n'a pas de privileges !
                    </div>
                    <div style={styles.tabContent}>
                      Initialiser les privileges pour{" "}
                      <strong>{rolePrivileges.roleName}</strong>
                    </div>
                    <div style={styles.tabContent}>
                      <RaisedButton
                        label="Initialiser les privilèges"
                        onClick={() =>
                          intialisationPrivileges(rolePrivileges.roleId, 1)}
                      />
                    </div>
                  </div>
                )}
              </div>
            ))}
          </SwipeableViews>
        </div>
      ) : (
        <div>
          Cette Application n'as pas encore de rôle:
          <strong>Ajouter un role !!!</strong>
          <div>
            <CreateRoleContainer idApplication={applicationId} />
          </div>
        </div>
      )}
    </div>
  );
};

export default ApplicationSetRolePrivilege;
