import React, { Component } from "react";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";

class SelectButtonProfileNotAssigned extends Component {
  state = {
    value: this.props.rolesAssignedGroupedByApplication,
  };

  handleChangeRole = (roleId, values) => {
    this.setState({ value: values });

    if (roleId) {
      this.props.setProfileRoleApp(values);
    } else {
      this.props.deleteProfileRoleApp(values);
    }
  };

  handleChange = (event, index, value) => this.setState({ value: value });

  render() {
    const {
      rolesGroupedByApplication,
      rolesAssignedGroupedByApplication,
    } = this.props;

    return (
      <div>
        <div>
          {rolesAssignedGroupedByApplication === 0 && (
            <span>
              <strong>Profile assigné :</strong> Aucun Role
            </span>
          )}
        </div>
        <div>
          <SelectField
            floatingLabelText="Role par defaut"
            floatingLabelFixed={true}
            value={this.state.value}
            onChange={() => this.handleChange}
          >
            {rolesAssignedGroupedByApplication === 0 && (
              <MenuItem
                value={0}
                primaryText="Aucun role"
                checked={true}
                onClick={() => this.handleChangeRole(false, this.state.value)}
              />
            )}

            {Object.keys(
              rolesGroupedByApplication,
            ).map((roleListApp, index) => (
              //<span key={index} > {rolesGroupedByApplication[application.applicationId][roleListApp].roleId} </span>
              <MenuItem
                key={rolesGroupedByApplication[roleListApp].roleId}
                value={rolesGroupedByApplication[roleListApp].roleId}
                primaryText={rolesGroupedByApplication[roleListApp].roleName}
                onClick={() =>
                  this.handleChangeRole(
                    true,
                    rolesGroupedByApplication[roleListApp].roleId,
                  )}
              />
            ))}
          </SelectField>
        </div>
      </div>
    );
  }
}

export default SelectButtonProfileNotAssigned;
