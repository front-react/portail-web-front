import React from "react";
import Paper from "material-ui/Paper";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from "material-ui/Table";
import { CreateApplicationContainer } from "../../containers";
import IconButton from "material-ui/IconButton";
import { SearchBar } from "../../../../components/componup";
import IconSearch from 'material-ui/svg-icons/action/search'
import Divider from "material-ui/Divider";
import { grey300, grey700 } from "material-ui/styles/colors";
import { 
  ApplicationDialogInfo,
  DeleteApplication
 } from "../../components";

const styles = {
  PaperDivTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: grey300,
    color: grey700,
    padding: "10px",
  },
  PaperTitle: {
    fontSize: "24px",
  },
  PaperActionsTitle: {
    display: "flex",
    marginLeft: "auto",
  },
  bodyPaper: {
    padding: "10px",
  },
  actionBAr: {
    paddingTop: "10px",
    display: "flex",
    alignItems: "center",
  },
  spanSearchBar: {
    display: "flex",
    width: "600px",
    justifyContent: "space-between",
    marginRight: "20px",
  },
};

const ApplicationList = ({ applicationList, filterApplicationList }) => {
  return (
    <Paper>
      <div style={styles.PaperDivTitle}>
        <span style={styles.PaperTitle}>Liste des applications</span>
      </div>
      <Divider />
      <div style={styles.PaperDivTitle}>
        {filterApplicationList && (
          <div style={styles.actionBAr}>
            <span style={styles.spanSearchBar}>
              <SearchBar
                onChange={value =>
                  filterApplicationList(value, "applicationName")}
                hintText="Recherchez une application"
              />
            </span>
          </div>
        )}
        <div style={styles.actionBAr}>
          <CreateApplicationContainer />
        </div>
      </div>
      <div>
        <Table selectable={false}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow selectable={false}>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Nom</TableHeaderColumn>
              <TableHeaderColumn>Voir l'application</TableHeaderColumn>
              <TableHeaderColumn>Info sur l'application</TableHeaderColumn>
              <TableHeaderColumn>Supprimer</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {Object.keys(applicationList).map(application => (
              <TableRow key={application} selectable={false}>
                <TableRowColumn>
                  {applicationList[application].applicationId}
                </TableRowColumn>
                <TableRowColumn>
                  {applicationList[application].applicationName}
                </TableRowColumn>
                <TableRowColumn>
                  <IconButton
                    href={
                      "/admin/application/" +
                      applicationList[application].applicationId
                    }
                  >
                    <IconSearch />
                  </IconButton>
                </TableRowColumn>
                <TableRowColumn>
                  <ApplicationDialogInfo
                    applicationId={applicationList[application].applicationId}
                  />
                </TableRowColumn>
                <TableRowColumn>
                  <DeleteApplication idApplication={applicationList[application].applicationId} />
                </TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    </Paper>
  );
};

export default ApplicationList;
