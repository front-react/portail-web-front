import React, { Component } from "react";
import IconButton from "material-ui/IconButton";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from "material-ui/Table";
import { grey300, grey700 } from "material-ui/styles/colors";
import { UserDialogInfo } from "../";
import IconSearch from "material-ui/svg-icons/action/search";

const styles = {
  PaperDivTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: grey300,
    color: grey700,
    padding: "10px",
  },
  PaperTitle: {
    fontSize: "24px",
  },
};

class ApplicationsUsersList extends Component {
  render() {
    const { UsersList } = this.props;
    return (
      <div>
        <div style={styles.PaperDivTitle}>
          <span style={styles.PaperTitle}>Liste des Utilisateurs</span>
        </div>
        <Table selectable={false}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow selectable={false}>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>User name</TableHeaderColumn>
              <TableHeaderColumn>profile</TableHeaderColumn>
              <TableHeaderColumn>Role</TableHeaderColumn>
              <TableHeaderColumn>Voir utilisateur</TableHeaderColumn>
              <TableHeaderColumn>Info utilisateur</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {Object.keys(UsersList).map((users, index) => (
              <TableRow key={index} selectable={false}>
                <TableRowColumn>{UsersList[users].userId}</TableRowColumn>
                <TableRowColumn>{UsersList[users].username}</TableRowColumn>
                <TableRowColumn>{UsersList[users].profileName}</TableRowColumn>
                <TableRowColumn>{UsersList[users].roleName}</TableRowColumn>
                <TableRowColumn>
                  <IconButton href={"/user/" + UsersList[users].userId}>
                    <IconSearch />
                  </IconButton>
                </TableRowColumn>
                <TableRowColumn>
                  <UserDialogInfo userId={UsersList[users].userId} />
                </TableRowColumn>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    );
  }
}

export default ApplicationsUsersList;
