ApplicationRolePrivilege
import React from 'react'
import {Tabs, Tab} from 'material-ui/Tabs'
import SwipeableViews from 'react-swipeable-views'
import {orange500, grey300, grey700} from "material-ui/styles/colors"
import Toggle from 'material-ui/Toggle'
import Subheader from 'material-ui/Subheader';


const styles = {
  stylesPaperApp: {
    padding: 10
  },
  inkBar: {
    backgroundColor: orange500
  },
  PaperDivTitle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '5px'
  },
  PaperTitle: {
    fontSize: '24px'
  },
  PaperActionsTitle: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  tabs: {
    marginBottom: 20,
  },
  buttonTab: {
    color: grey700,
  },
  tab: {
    borderBottom: '1px solid'+ grey300,
    backgroundColor: 'white',
    color: 'black'
  },
  tabContent: {
    margin: '10px 0px',
  },
  subTitle: {
    fontSize: '20px',
  },
  toggle: {
    padding: '5px 20px',
    width: '30%'
  },
}



const ApplicationSetPrivilege = ({application}) => {

  return (
    <div>
      { application.roles.length > 0
        ? <div>
          <Tabs style={styles.tabs} inkBarStyle={styles.inkBar} onChange={handleTabChange} value={slideIndex}>
            {application.roles.map((role, index) =>
              <Tab style={styles.tab} buttonStyle={styles.buttonTab} key={role.idRole} label={role.nameRole} value={index} />
            )}
          </Tabs>
          2    Visualisation
          3    Administration
          4    Modification
          5    Divers
          <SwipeableViews index={slideIndex} onChangeIndex={handleTabChange}>
            {application.roles.map((role, index) =>
              <div key={role.idRole} style={styles.tabContent}>
                {application.privilege.length > 0
                  ? <div>
                    {application.privilege.map((privilege) =>
                      <span>
                        <span>{privilege.namePrivilege}</span>
                      </span>
                    )
                    }
                  </div>
                  : <span>Cette application n'a pas encore de privilège, ajouté en un !</span>
                }
              </div>
            )}
          </SwipeableViews>
        </div>
        : <span>Cette application n'a pas encore de rôle, ajouter en un ! </span>
      }
    </div>
  )
}

export default ApplicationSetPrivilege

