import React, { Component } from "react";
import { reduxForm } from "redux-form";
import { TextField } from "redux-form-material-ui";
import FlatButton from "material-ui/FlatButton";
import { grey700 } from "material-ui/styles/colors";
import { SearchBarAutoComplete } from '../../../../components/componup';

const styles = {
  textField: {
    marginRight: "40px",
  },
  actionButton: {
    float: "right",
  },
  boxPerson:{
    alignItems: "center",
    padding: "10px",
    minHeight:"10px",
    marginTop: '20px',
    border: '1px solid #000000',
    width: '100%',
    height: '150px'
  },
  liPerson: {
    display:'inline-block',
    width: '100%',
    fontSize: '20px',
    margin: '5px 0px',
    paddind: '5px 0px',
    color: '#ffffff',
    backgroundColor: grey700,
    cursor: 'pointer'
  }
};

class CreateUser extends Component {
  render() {
    const { handleSubmit, handleFormSubmit, onChangePassword, person, personList, selectPerson, currentUser, handleAfterSubmit, password, searchPerson  } = this.props;

    return (
      <div style={styles.heightBox} >
      <form onSubmit={handleSubmit(handleFormSubmit)}>
        <span>
          <span>
            <TextField
              name="username"
              value={person}
              style={styles.textField}
              placeholder="Cherchez un utilisateur"
            />
            <TextField
              name="password"
              value={password}
              placeholder="Entrer un mot de passe"
              onChange={(event, newValue) => onChangePassword(event, newValue)}
            />
          </span>
          {personList &&
          <div style={styles.boxPerson} >
            <strong>Selectionnez une personne.</strong>
            <SearchBarAutoComplete 
              fullWidth={true} 
              onUpdateInput={searchPerson} 
              onNewRequest={(person) => selectPerson(person)} 
              data={personList} 
              value={573} 
              textField="personFullName" 
              valueField="personId" />
          </div>
          }
          <div style={styles.actionButton}>
            {currentUser === null &&
              <div>
                <FlatButton
                  type="submit"
                  onSubmit={handleSubmit((data) => { handleFormSubmit(data); })}
                  label="valider avant de passer à la suite"
                />
              </div>
            }

            {currentUser !== null &&
              <div>
                <FlatButton
                  type="submit"
                  onClick={ () => {handleAfterSubmit("next"); }}
                  label="Etape suivante"
                />
                <FlatButton
                  type="submit"
                  onClick={ () => {handleAfterSubmit("close"); }}
                  label="Valider & quitter"
                />
              </div>
            }
            
          </div>
        </span>
      </form>
      </div>
    );
  }
}

export default (CreateUser = reduxForm({
  form: "CreateUser",
})(CreateUser));
