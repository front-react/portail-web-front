import React, {Component} from 'react'
import {connect} from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import EditIcon from "material-ui/svg-icons/editor/mode-edit"
import {patchPrivilege} from '../../redux/actions/privilege/patchPrivilege'
import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5
  },
  iconButton: {
    width: '18px',
    height: '18px',
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    patchPrivilege: (id, body) => {dispatch(patchPrivilege(id, body)) }
  }
};


class PatchPrivilege extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleFormSubmit = (requestBody) => {
    requestBody.privilegeTypeId = this.props.privilege.privilegeTypeId
    this.props.patchPrivilege(this.props.privilege.privilegeId, requestBody);
    //this.props.refreshPrivileges();
  }

  render() {
    const {handleSubmit} = this.props;

    return(
      <div>
        <IconButton
          iconStyle={styles.iconButton}
          onTouchTap={this.handleOpen}
        >
          <EditIcon />
        </IconButton>
        <Dialog
          title="Modification du nom du privilege"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <form style={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
            <div>
                <span>Ancien Privilege :: <strong>{this.props.privilege.privilegeName}</strong></span>
            </div>
            <div>
                <Field 
                    name="privilegeName" 
                    component={TextField} 
                    hintText="Nouveau privilege"/>
            </div>
            <div style={styles.actionButton}>
              <FlatButton onTouchTap={this.handleClose} label="Cancel"/>
              <FlatButton onTouchTap={this.handleClose} type="submit" label="Submit"/>
            </div>
          </form>
        </Dialog>
      </div>
    )
  }
}

PatchPrivilege = reduxForm({
  form: 'PatchPrivilege',
})(PatchPrivilege);


export default connect(null, mapDispatchToProps)(PatchPrivilege)