import React, {Component} from 'react'
import {connect} from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import Dialog from 'material-ui/Dialog'
import RaisedButton from 'material-ui/RaisedButton';
import EditIcon from "material-ui/svg-icons/editor/mode-edit"
import {patchUser} from '../../redux/actions/user/patchUser'
import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5,
    marginTop: 5,
    width: '100%'
  },
  iconButton: {
    width: '18px',
    height: '18px',
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    patchUser: (id, body) => {dispatch(patchUser(id, body)) }
  }
};


class PatchUser extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleFormSubmit = (requestBody) => {
    requestBody.personId = this.props.personId
    this.props.patchUser(this.props.userId, requestBody);
  }

  render() {

    const { handleSubmit } = this.props

    return(
      <div>
        <RaisedButton
            label="Modifier"
            primary={true}
            icon={<EditIcon />}
            style={styles.button}
            onClick={this.handleOpen}
        />
        <Dialog
          title="Modification le nom d'utilisateur"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <form style={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
            <div>
                <span>Ancien nom d'utilisateur :: <strong>{this.props.userName}</strong></span>
            </div>
            <div>
                <Field 
                    name="username" 
                    component={TextField} 
                    hintText="Modifier nom d'utilisateur"/>
            </div>
            <div style={styles.actionButton}>
              <FlatButton onTouchTap={this.handleClose} label="Cancel"/>
              <FlatButton onTouchTap={this.handleClose} type="submit" label="Submit"/>
            </div>
          </form>
        </Dialog>
      </div>
    )
  }
}

PatchUser = reduxForm({
  form: 'PatchUser',
})(PatchUser);


export default connect(null, mapDispatchToProps)(PatchUser)