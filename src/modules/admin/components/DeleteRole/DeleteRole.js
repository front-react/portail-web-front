import React, {Component} from 'react'
import {connect} from 'react-redux'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import DeleteIcon from "material-ui/svg-icons/action/delete"
import {deleteRole} from '../../redux/actions/role/deleteRole'

import FlatButton from 'material-ui/FlatButton'

const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5
  },
  iconButton: {
    width: '18px',
    height: '18px',
  },
  AlignText: {
    textAlign: 'center'
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteRole: (id) => { dispatch(deleteRole(id)) }
  }
}


class DeleteRole extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleDelete = (role) => {
    const roleId = role.roleId
    this.props.deleteRole(roleId)
  }

  render() {

    return(
      <div>
        <IconButton
          iconStyle={styles.iconButton}
          onTouchTap={this.handleOpen}
        >
          <DeleteIcon />
        </IconButton>
        <Dialog
          title="Confirmer la supression ?"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
        <div style={styles.AlignText}>
            <div>
                <span>
                    Vous êtes sur le point de supprimer definitivement :
                </span>
            </div>
            <div>
                <span>
                    <strong>{this.props.role.roleName}</strong>
                </span>
            </div>
          </div>
          <div style={styles.actionButton}>
            <FlatButton onTouchTap={this.handleClose} label="NON"/>
            <FlatButton onTouchTap={this.handleClose} label="SUPRESSSION !" onClick={() => this.handleDelete(this.props.role)}/>
          </div>
        </Dialog>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(DeleteRole)