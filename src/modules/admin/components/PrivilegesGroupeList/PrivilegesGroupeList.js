import React from 'react'
import Toggle from 'material-ui/Toggle'
import { grey200} from 'material-ui/styles/colors'


const styles = {
  toggle: {
    padding: '10px 30px',
    margin: '5px 10px',
    backgroundColor: grey200,
    width: '75%',
    fontWeight: '700',
  },
  titrePrivilege: {
    fontWeight: '700',
    textIndent: '5px',
    fontSize: '25px',
    padding: '15px 20px',
    marginBottom: '20px',
  },
  AlignToggle: {
    display: 'flex',
    flexWrap: 'wrap',
    marginTop: '15px',
    marginLeft: '15px'
  },
  widthToggle: {
    width: '25%'
  },
  groupPrivileges: {
    margin: '15px 0px'
  }
}


const PrivilegesGroupeList = ({privilegeGrouped, nameGroup, roleId, handleToggle, rolePrivilegesList, indexPage, rolePrivileges}) => {

  const valideRole = roleId

  return (
    <div style={styles.groupPrivileges} >

      <span style={styles.titrePrivilege} >Categorie {nameGroup}</span>
      <div style={styles.AlignToggle} >
      {Object.keys(privilegeGrouped).map((privileges, index) =>
        <div key={index} style={styles.widthToggle}>
        {rolePrivileges !== null &&
          <div>
            {rolePrivileges.privileges.indexOf(privilegeGrouped[privileges].privilegeId) !== -1 
              ? <Toggle
                defaultToggled={rolePrivileges.privileges.indexOf(privilegeGrouped[privileges].privilegeId) !== -1}
                label={privilegeGrouped[privileges].privilegeName}
                onToggle={(event, isInputChecked) => handleToggle(event, isInputChecked, valideRole, privilegeGrouped[privileges].privilegeId)}
                style={styles.toggle}/>
              : <Toggle
                label={privilegeGrouped[privileges].privilegeName}
                toggled={rolePrivileges.privileges.indexOf(privilegeGrouped[privileges].privilegeId) !== -1}
                onToggle={(event, isInputChecked) => handleToggle(event, isInputChecked, valideRole, privilegeGrouped[privileges].privilegeId)}
                style={styles.toggle}/>
            }

          </div>
        }
        </div>
      )}
      </div>
    </div>
  );
}

export default PrivilegesGroupeList
