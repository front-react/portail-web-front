import React, {Component} from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import {grey300, grey700} from "material-ui/styles/colors"

import { RoleDialogInfo } from '../'

import { 
  DeleteRole,
  PatchRole 
} from '../'

const styles = {
    PaperDivTitle: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: grey300,
      color: grey700,
      padding: '10px'
    },
    PaperTitle: {
      fontSize: '24px'
    }
  }

class ApplicationsRolesList extends Component {

  render() {

    const {roleList, NameApplication} = this.props

    const RolesList = roleList

    return(
      <div>
      <div style={styles.PaperDivTitle}>
          <span style={styles.PaperTitle}>Liste des roles pour {NameApplication}</span>
      </div>
      <Table selectable={false}>
      <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
        <TableRow selectable={false}>
          <TableHeaderColumn>ID</TableHeaderColumn>
          <TableHeaderColumn>Role name</TableHeaderColumn>
          <TableHeaderColumn>Update role</TableHeaderColumn>
          <TableHeaderColumn>Info role</TableHeaderColumn>
          <TableHeaderColumn>Delete role</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody displayRowCheckbox={false}>
        {Object.keys(RolesList).map((role, index) =>
          <TableRow key={index} selectable={false}>
            <TableRowColumn>{RolesList[role].roleId}</TableRowColumn>
            <TableRowColumn>{RolesList[role].roleName}</TableRowColumn>
            <TableRowColumn>
              <PatchRole role={RolesList[role]} />
            </TableRowColumn>
            <TableRowColumn>
              <RoleDialogInfo roleId={RolesList[role].roleId} />
            </TableRowColumn>
            <TableRowColumn>
              <DeleteRole role={RolesList[role]} />
            </TableRowColumn>
          </TableRow>
        )}
      </TableBody>
    </Table>
    </div>
    );
  }
}

export default ApplicationsRolesList
