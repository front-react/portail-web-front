import React from "react";

import {
  ApplicationsInfoContainer,
  ProfilesInfoContainer,
  UsersInfoContainer,
  UserListContainer,
} from "../../containers";

const styles = {
  dashBoardGrid: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
  },
  dashBoardCard: {
    padding: "15px",
    marginBottom: "10px",
    width: "49%",
    height: "250px",
    display: "flex",
    flexDirection: "column",
  },
  dashBoardCardHeader: {
    fontSize: "24px",
    marginBottom: "15px",
  },
  dashBoardCardLine: {
    display: "flex",
    alignItems: "center",
    marginBottom: "10px",
    marginLeft: "auto",
    marginRight: "auto",
  },
  button: {
    marginRight: "10px",
  },
  searchBar: {
    width: "200px",
  },
  usersListSection: {
    width: "100%",
    margin: "20px 0 0px 0",
  },
};

const AdminDashBoard = () => {
  return (
    <div style={styles.dashBoardGrid}>
      <UsersInfoContainer />

      <ProfilesInfoContainer />

      <ApplicationsInfoContainer />

      <div style={styles.usersListSection}>
        <UserListContainer />
      </div>
    </div>
  );
};

export default AdminDashBoard;
