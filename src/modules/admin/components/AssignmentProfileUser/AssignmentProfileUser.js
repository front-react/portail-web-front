import React, {Component} from 'react'
import Dialog from 'material-ui/Dialog'
import RaisedButton from 'material-ui/RaisedButton';
import ActionAssignmentInd from "material-ui/svg-icons/action/assignment-ind"
import FlatButton from 'material-ui/FlatButton'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


const styles = {
  actionButton: {
    marginTop: 24,
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginRight: 5,
    marginTop: 5,
    width: '100%'
  },
  iconButton: {
    width: '18px',
    height: '18px',
  }
};


class AssignmentProfileUser extends Component {


  render() {

    const {handleOpen, handleClose, statusDialog, userName, profileList, addAssignedProfile, handleChange, indexProfileId} = this.props;

    return(
      <div>
        <RaisedButton
            label="Attibuer"
            primary={true}
            icon={<ActionAssignmentInd />}
            style={styles.button}
            onClick={handleOpen}
        />
        <Dialog
          title="Attribuer un profil à cette utilisateur"
          modal={false}
          open={statusDialog}
          onRequestClose={handleClose}
          autoScrollBodyContent={true}
        >
          <div>
            <span>Choisissez un profile pour :: <strong>{userName}</strong></span>
          </div>

          <div> 
            <SelectField
              floatingLabelText="Profile par defaut"
              floatingLabelFixed={true}
              value={indexProfileId}
              onChange={() => handleChange}
            >
            {indexProfileId === 0 &&
              <MenuItem 
                value={0} primaryText="Aucun Profile"
              />
            }

            {Object.keys(profileList).map((profileListApp, index) => 

            //<span key={index} > {rolesGroupedByApplication[application.applicationId][roleListApp].roleId} </span>
              <MenuItem 
                key={index}
                value={profileList[profileListApp].profileId} 
                primaryText={profileList[profileListApp].profileName}
                checked={this.props.profileId === profileList[profileListApp].profileId}
                onClick={() => addAssignedProfile(profileList[profileListApp].profileId)} 
              />
              
            )}
            </SelectField>
          </div>
          <div style={styles.actionButton}>
            <FlatButton onTouchTap={handleClose} label="Fermer"/>
          </div>
        </Dialog>
      </div>
    )
  }
}

export default AssignmentProfileUser