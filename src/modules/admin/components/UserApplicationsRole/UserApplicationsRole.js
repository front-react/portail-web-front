import React from 'react'

import Paper from 'material-ui/Paper';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Link} from 'react-router'
import SearchIcon from 'material-ui/svg-icons/action/search'
import IconButton from 'material-ui/IconButton';
import {grey700} from 'material-ui/styles/colors'

const styles = {
  searchIcon: {
    marginRight: 5
  },
}

  const UserApplicationsRole = ({userApplicationRole}) => {

      return (

        <Paper>
          <Table selectable={false}>
            <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
              <TableRow selectable={false}>
                <TableHeaderColumn>ID</TableHeaderColumn>
                <TableHeaderColumn>Nom</TableHeaderColumn>
                <TableHeaderColumn>Role</TableHeaderColumn>
                <TableHeaderColumn>Voir la fiche role/application</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {Object.keys(userApplicationRole).map((role) =>
                <TableRow key={role} selectable={false}>
                  <TableRowColumn>{userApplicationRole[role].applicationId}</TableRowColumn>
                  <TableRowColumn>{userApplicationRole[role].applicationName}</TableRowColumn>
                  <TableRowColumn>{userApplicationRole[role].roleName}</TableRowColumn>
                  <TableRowColumn>
                    <Link to={{pathname: "/admin/application/"+userApplicationRole[role].applicationId}}>
                    <IconButton>
                      <SearchIcon color={grey700} style={styles.searchIcon}/>
                    </IconButton>
                    </Link>
                  </TableRowColumn>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Paper>
      )
    };

export default UserApplicationsRole

/*
 */