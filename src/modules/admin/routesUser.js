import React from 'react';
import {Route, IndexRoute} from 'react-router';

import {
  UserListContainer,
  UserProfileContainer,
  //CreateUserStepperContainer
} from './containers';


export default(
  <Route path="user" >
    <IndexRoute name="Tous les utilisateurs" component={UserListContainer} />
    <Route name="Fiche utilisateur"  path=":id" component={UserProfileContainer}/>
  </Route>
)