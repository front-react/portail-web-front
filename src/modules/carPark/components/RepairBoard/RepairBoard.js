import React from "react";
import Paper from "material-ui/Paper";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from "material-ui/Table";
import "./style.css";

const filterActiveRepair = repair => {
  return (
    repair.repairStatusId === "1" ||
    repair.repairStatusId === "2" ||
    repair.repairStatusId === "3" ||
    repair.repairStatusId === "4"
  );
};

const RepairBoard = ({ repair }) => {
  const activeRepair = repair.filter(repair => filterActiveRepair(repair));
  activeRepair.sort((a, b) => {
    return parseFloat(a.repairTypeId) - parseFloat(b.repairTypeId);
  });

  return (
    <Paper className="repairBoard">
      <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow selectable={false}>
            <TableHeaderColumn>Categorie</TableHeaderColumn>
            <TableHeaderColumn>Dernière réparation</TableHeaderColumn>
            <TableHeaderColumn>Alerte dans</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {activeRepair.map(repair => {
            const countKm = new Intl.NumberFormat("fr-FR").format(
              +repair.targetKm - +repair.counterKm,
            );
            const countDays = new Intl.NumberFormat("fr-FR").format(
              +repair.targetDays - +repair.counterDays,
            );
            const alertMsg =
              repair.repairStatusId === "2"
                ? "Alerte en cours"
                : repair.targetDays && repair.targetKm
                  ? "dans " +
                    countKm +
                    " Km ( ou dans" +
                    +repair.targetDays +
                    " jours)"
                  : repair.targetKm
                    ? "dans " + countKm + " Km"
                    : "dans " + countDays + " jours";

            if (repair.repairTypeId !== "12") {
              return (
                <TableRow
                  key={repair.repairId}
                  selectable={false}
                  className={
                    alertMsg === "Alerte en cours" ? (
                      "alertInProgress row"
                    ) : (
                      "row"
                    )
                  }
                >
                  <TableRowColumn className="rowColumn">
                    {repair.repairTypeName}
                  </TableRowColumn>
                  <TableRowColumn className="rowColumn">
                    {repair.lastRepairKm ? (
                      `${new Intl.NumberFormat("fr-FR").format(
                        repair.lastRepairKm,
                      )} Km`
                    ) : (
                      "Aucune"
                    )}
                  </TableRowColumn>
                  <TableRowColumn className="rowColumn">
                    {alertMsg}
                  </TableRowColumn>
                </TableRow>
              );
            }
            return <TableRow key={repair.repairId} className="hidden" />;
          })}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default RepairBoard;
