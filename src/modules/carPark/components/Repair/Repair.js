import React from 'react';
import Paper from 'material-ui/Paper'
import SearchIcon from 'material-ui/svg-icons/action/search';
import IconButton from 'material-ui/IconButton'
import { AddRepairContainer } from '../../containers';
import { FixRepairTargetContainer } from '../../containers';
import RaisedButton from 'material-ui/RaisedButton';


const styles = {
  repairRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: '10px',

  },
  repairCard: {
    width: 'calc(50% - 5px)',
    padding: '15px',
    display: 'flex',
    flexDirection: 'column',
  },
  cardHeader: {
    fontWeight: '600',
    marginBottom: '20px',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'space-between',
  },
  dataRow: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '10px',
  },
  commentSection: {
    marginTop: '5px',
    display: 'flex',
    flexDirection: 'column',
  },
  commentHeader: {
    textDecoration: 'underline',
    marginBottom: '10px',
  },
  comment: {
    textAlign: 'center',
  },
  actionSection: {
    marginTop: '20px',
    display: 'flex',
    justifyContent: 'space-around',
  },
  noRepair: {
    height: '100px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
};

const Repair = ({ repair, currentKm, currentDriver, repairType, intialisationReparation, repairList, togglePneu }) => {
  const repairObject = repair[0];


  const firstRepair = repairList.shift()


  if (repairObject) {
    const countKm = new Intl.NumberFormat("fr-FR").format(repairObject.targetKm - repairObject.counterKm);
    const countDays = new Intl.NumberFormat("fr-FR").format(repairObject.targetDays - repairObject.counterDays);
    const alertMsg = (repairObject.repairStatusId === "2")
      ? "Alerte en cours"
      : (repairObject.targetDays && repairObject.targetKm)
        ? "dans " + countKm + " Km ( ou dans" + +repairObject.targetDays + " jours)"
        : (repairObject.targetKm)
          ? "dans " + countKm + " Km"
          : "dans " + countDays + " jours";

    const control = (repairObject.lastControlKm !== null)
      ? `Controlé le ${new Date(repairObject.lastControlDate).toLocaleDateString("fr-FR")} à ${new Intl.NumberFormat("fr-FR").format(repairObject.lastControlKm)} Km`
      : 'jamais controlé';
    const comments = (repairObject.comments === null) ? 'aucun commentaire' : repairObject.comments;
    const lastRepairComments = (repairObject.lastRepairComments === null) ? 'aucun commentaire' : repairObject.lastRepairComments;

    return (
      <div style={styles.repairRow}>
        <Paper style={styles.repairCard}>
          <span style={styles.cardHeader}>dernier(e) {repairObject.repairTypeName}
            <IconButton>
              <SearchIcon />
            </IconButton>
          </span>
          <span style={styles.dataRow}>
            <span>Kilometrage du devis</span>
            <span>{repairObject.lastRepairKm ? new Intl.NumberFormat("fr-FR").format(repairObject.lastRepairKm) : 'Aucun'}</span>
          </span>
          <span style={styles.dataRow}>
            <span>Date du devis</span>
            <span>{repairObject.lastRepairDate ? new Date(repairObject.lastRepairDate).toLocaleDateString("fr-FR") : 'Aucun'}</span>
          </span>
          <div style={styles.commentSection}>
            <span style={styles.commentHeader}>Commentaire:</span>
            <span style={styles.comment}>{lastRepairComments}</span>
          </div>
        </Paper>

        <Paper style={styles.repairCard}>
          <span style={styles.cardHeader}>Prochain(e) {repairObject.repairTypeName}</span>

          <span style={styles.dataRow}>
            <span>Declenchement de l'alerte</span>
            <span>{alertMsg}</span>
          </span>
          <span style={styles.dataRow}>
            <span>contrôlé à</span>
            <span>{control}</span>
          </span>
          <div style={styles.commentSection}>
            <span style={styles.commentHeader}>Commentaire:</span>
            <span style={styles.comment}>{comments}</span>
          </div>
          <div style={styles.actionSection}>
            <span>
              {togglePneu === undefined
                ? <AddRepairContainer
                  dataLocation={"currentCarRepair"}
                  repairId={repairObject}
                  carId={repairObject.carId}
                  carName={repairObject.carName}
                  personId={currentDriver.personId}
                  personName={currentDriver.personName}
                  repairTypeId={repairObject.repairTypeId}
                  currentKm={currentKm}
                  lastRepairDay={repairObject.lastRepairDate}
                />
                : <div>
                  {togglePneu === "0" && (repairType === "6" || repairType === "7") &&
                    <AddRepairContainer
                      dataLocation={"currentCarRepair"}
                      repairId={repairObject}
                      carId={repairObject.carId}
                      carName={repairObject.carName}
                      personId={currentDriver.personId}
                      personName={currentDriver.personName}
                      repairTypeId={repairObject.repairTypeId}
                      currentKm={currentKm}
                      lastRepairDay={repairObject.lastRepairDate}
                    />
                  }
                  {togglePneu === "1" && (repairType === "8" || repairType === "9") &&
                    <AddRepairContainer
                      dataLocation={"currentCarRepair"}
                      repairId={repairObject}
                      carId={repairObject.carId}
                      carName={repairObject.carName}
                      personId={currentDriver.personId}
                      personName={currentDriver.personName}
                      repairTypeId={repairObject.repairTypeId}
                      currentKm={currentKm}
                      lastRepairDay={repairObject.lastRepairDate}
                    />
                  }
                </div>
              }
            </span>
            <span>
              {togglePneu === undefined
                ? <FixRepairTargetContainer
                  repair={repairObject}
                  currentKm={currentKm}
                  lastControlDay={repairObject.lastControlDate} />
                : <div>
                  {togglePneu === "1" && (repairType === "8" || repairType === "9") &&
                    <FixRepairTargetContainer
                      repair={repairObject}
                      currentKm={currentKm}
                      lastControlDay={repairObject.lastControlDate} />
                  }
                  {togglePneu === "0" && (repairType === "6" || repairType === "7") &&
                    <FixRepairTargetContainer
                      repair={repairObject}
                      currentKm={currentKm}
                      lastControlDay={repairObject.lastControlDate} />
                  }
                </div>
              }
            </span>
          </div>
        </Paper>
      </div>
    )
  }

  return (
    <div>
      <div style={styles.noRepair}>Cette voiture n'a pas de réparation en cours pour ce type</div>
      <div style={styles.noRepair}>
        {repairType === "1" &&
          <RaisedButton
            label="Initialiser les réparations pour la vidange"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "11" &&
          <RaisedButton
            label="Initialiser les réparations pour la courroie"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "2" &&
          <RaisedButton
            label="Initialiser les réparations pour des contrôles techniques"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "10" &&
          <RaisedButton
            label="Initialiser les réparations pour des contrôles anti-pollution"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "3" &&
          <RaisedButton
            label="Initialiser les réparations les plaquettes de frein avant"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "4" &&
          <RaisedButton
            label="Initialiser les réparations la machoire arrière"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "5" &&
          <RaisedButton
            label="Initialiser les réparations les disques avant"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "6" && repairObject === undefined && togglePneu === "0" &&
          <RaisedButton
            label="Initialiser les réparations les Pneus Avant"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "7" && repairObject === undefined && togglePneu === "0" &&
          <RaisedButton
            label="Initialiser les réparations les Pneus Arrière"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "8" && repairObject === undefined && togglePneu === "1" &&
          <RaisedButton
            label="Initialiser les réparations lesPneus Avant Neige"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
        {repairType === "9" && repairObject === undefined && togglePneu === "1" &&
          <RaisedButton
            label="Initialiser les réparations les Pneus Arrière Neige"
            onClick={() => intialisationReparation(firstRepair)}
          />
        }
      </div>
    </div>

  )
};

export default Repair;

