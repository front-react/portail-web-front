import React from 'react';
import IconButton from 'material-ui/IconButton'
import ClearIcon from 'material-ui/svg-icons/content/clear';
import CircularProgress from 'material-ui/CircularProgress';
import Drawer from 'material-ui/Drawer';


const styles = {
  titleDrawer: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '24px',
    color: 'rgb(220,220,220)',
    backgroundColor: 'rgb(50,50,50)',
  },
  drawerListItem: {
    display: 'flex',
    flexDirection: 'column',
    margin: '5px 0 5px 0',
  },
  drawerLabel: {
    fontWeight: '700',
    letterSpacing: '1px',
    color: 'rgb(10,10,10)',
    backgroundColor: 'rgb(150,150,150)',
    textIndent: '10px',
    padding : '2px',
  },
  repairDetailList: {
    padding : '5px',
    display: 'flex',
    flexDirection: 'column',
  },
  repairLabel: {
    padding : '10px',
    color: 'rgb(50,50,50)',
    backgroundColor: 'rgb(220,220,220)',
    borderBottom: '2px solid rgb(150,150,150)'
  },
  colorButton: {
    backgroundColor: 'rgb(250,250,250)',
  },
  titleRepair: {
    textIndent: '20px',
  },
};

const DrawerRepair = ({handleOpen, open, repair}) => {
  return (
    <Drawer
      open={open}
      openSecondary={true}
      docked={false}
      width={400}
      onRequestChange={handleOpen}>
      {repair
        ?
        <div>
          <div style={styles.titleDrawer}>
            <IconButton 
              onTouchTap={handleOpen}
              style={styles.colorButton} 
            >
              <ClearIcon />
            </IconButton>
            <span style={styles.titleRepair}>Réparation n° {repair.repairId}</span>
          </div>

          <div style={styles.repairDetailList}>
            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Véhicule</span>
              <span style={styles.repairLabel}>{repair.carName} (id :{repair.carId})</span>
            </div>

            {repair.personId &&
            <div style={styles.drawerListItem}>
              <span style={styles.repairLabel}>Effectué par ({repair.personId}) {repair.personName}</span>
            </div>
            }


            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Type de la réparation</span>
              <span style={styles.repairLabel}>{repair.repairTypeName}</span>
            </div>

            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Date de la réparation</span>
              <span style={styles.repairLabel}>{new Date(repair.repairDate).toLocaleDateString("fr-FR") || 'en cours'}</span>
            </div>

            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Km lors de la réparation</span>
              <span style={styles.repairLabel}>{repair.repairKm ? new Intl.NumberFormat("fr-FR").format(repair.repairKm)+' Km' : 'en cours'}</span>
            </div>

            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Date du dernier controle</span>
              <span style={styles.repairLabel}>{repair.lastControlDate ? new Date(repair.lastControlDate).toLocaleDateString("fr-FR") : '-'}</span>
            </div>

            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Km du dernier controle</span>
              <span style={styles.repairLabel}>{repair.lastControlKm  ? new Intl.NumberFormat("fr-FR").format(repair.lastControlKm)+' Km' : '-'}</span>
            </div>

            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Prestataire</span>
              <span style={styles.repairLabel}>{repair.serviceProviderId ? `(id ${repair.serviceProviderId}) ${repair.serviceProviderName}` : '-'}</span>
            </div>

            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Prix</span>
              <span style={styles.repairLabel}>{repair.dtPrice ? `HT: ${repair.dtPrice} (TVA: ${repair.vat})` : " - "}</span>
            </div>

            <div style={styles.drawerListItem}>
              <span style={styles.drawerLabel}>Commentaire</span>
              <span style={styles.repairLabel}>{repair.comments || '-'}</span>
            </div>
          </div>
        </div>
        : <CircularProgress />
      }
    </Drawer>
  )
};
export default DrawerRepair;



