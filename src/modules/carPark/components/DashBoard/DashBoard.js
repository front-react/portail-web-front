import React from 'react';
import { SimpleStatsData } from '../../../../components/componup';
import { CarPieChart, EcoDrivingRanking, CarAvailableList } from '../../components';
import { CarListContainer } from '../../containers';
import FaAutomobile from 'react-icons/lib/fa/automobile';
import FaRoad from 'react-icons/lib/fa/road';
import FaBomb from 'react-icons/lib/fa/bomb';
import FaExclamationTriangle from 'react-icons/lib/fa/exclamation-triangle';
import {orange500, red500, grey500, lightGreen500} from "material-ui/styles/colors";
import {Link} from 'react-router'

const styles = {
  Dashboard: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: '10px 0 0px 0',
  },
  dataSection: {
    width: '69%',
  },
  rankSection: {
    width: '29%',
  },
  carListSection: {
    width: '100%',
    margin: '20px 0 0px 0',
  },
  SimpleDataViewLine: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: '0px 0 20px 0',
  },
  chartLine: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  divPieChart: {
    width: '40%',
    height: '350px',
  },
  divCarAvailableList: {
    paddingLeft: '20px',
    width: '60%',
    height: '350px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
};

const DashBoard = ({carStats, topEcodriver, botEcodriver, alertCount, carAvailable, upCarStats, retardsUpcars}) => {
  const countRetardsUpcars = retardsUpcars.length;
  return (
    <span>
      <div style={styles.Dashboard}>
        <div style={styles.dataSection}>
          <span style={styles.SimpleDataViewLine}>
            <Link to="/parcauto/cars" style={{ textDecoration: 'none' }}>
            <SimpleStatsData 
              icon={FaAutomobile} 
              dataNumber={carStats.total} 
              dataLegend="Véhicules" 
              hexaColor={lightGreen500} 
              sizeImg={"38px"}
              height={"80px"}
              width={"200px"}
              numberData={"26px"}
              numberLegend={"16px"}
              titleLabel={"Véhicules"}
            />
            </Link>
            <Link to="/parcauto/alertesRepairs" style={{ textDecoration: 'none' }}>
              <SimpleStatsData 
                icon={FaExclamationTriangle} 
                dataNumber={alertCount} 
                dataLegend="Alertes" 
                hexaColor={red500}
                sizeImg={"38px"}
                height={"80px"}
                width={"200px"}
                numberData={"26px"}
                numberLegend={"16px"}
                titleLabel={"Alertes"}
              />
            </Link>
            <Link to="/parcauto/retardsUpcar" style={{ textDecoration: 'none' }}>
              <SimpleStatsData 
                icon={FaBomb} 
                dataNumber={countRetardsUpcars} 
                dataLegend="retard Upcar" 
                hexaColor={orange500}
                sizeImg={"38px"}
                height={"80px"}
                width={"200px"}
                numberData={"26px"}
                numberLegend={"16px"}
                titleLabel={"retard Upcar"}
              />
            </Link>
            <Link to="/parcauto/upcar" style={{ textDecoration: 'none' }}>
              <SimpleStatsData 
                icon={FaRoad} 
                dataNumber={upCarStats.waiting} 
                dataLegend="Upcar à valider" 
                hexaColor={grey500}
                sizeImg={"38px"}
                height={"80px"}
                width={"200px"}
                numberData={"26px"}
                numberLegend={"16px"}
                titleLabel={"Upcar à valider"}
              />
            </Link>
          </span>
          <span style={styles.chartLine}>
            <div style={styles.divPieChart}>
              <CarPieChart carStats={carStats} />
            </div>

            <div style={styles.divCarAvailableList}>
              <CarAvailableList carAvailable={carAvailable}/>
            </div>
          </span>
        </div>
          <div style={styles.rankSection}>
            <EcoDrivingRanking topEcodriver={topEcodriver} botEcodriver={botEcodriver}/>
          </div>
        </div>
      <div style={styles.carListSection}>
        <CarListContainer />
      </div>
    </span>
  )
};

export default DashBoard;

