import React from 'react';
import Paper from 'material-ui/Paper'
import { Scrollbars } from 'react-custom-scrollbars';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';

const styles = {
  board:{
    width: '100%',
    height: '100%',
  },
  graphTilte:{
    padding: '10px',
  },
  secondaryText: {
    marginRight: '20px',
  }
};
const CarAvailableList = ({carAvailable}) => {
  return (
    <Paper style={styles.board}>
      <Subheader>{carAvailable.length} Vehicules disponibles</Subheader>
      <Scrollbars style={{ width: '100%', height: 'calc(100% - 50px)' }}>
        <List>
          {carAvailable.map((car) =>
            <span key={car.carId}>
              <ListItem primaryText={car.carName} secondaryText={
                <span>
                  <span style={styles.secondaryText}>Km : {car.currentKm}</span>
                  <span style={styles.secondaryText}>Date d'achat : {new Date(car.purchaseDate).toLocaleDateString("fr-FR")}</span>
              </span>
              }
              />
              <Divider />
            </span>
          )}
        </List>
      </Scrollbars>
    </Paper>
  );
};

export default CarAvailableList;


