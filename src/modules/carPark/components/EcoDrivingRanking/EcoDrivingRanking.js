import React from "react";
import Paper from "material-ui/Paper";
import Avatar from "material-ui/Avatar";
import Divider from "material-ui/Divider";
import Badge from "material-ui/Badge";
import Subheader from 'material-ui/Subheader';
import {List, ListItem} from 'material-ui/List';
import {Link} from 'react-router';


const styles = {
  paper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#FBFCFE',
  },
  topPaper: {
    display: 'flex',
    flexDirection: 'column',
    height: '50%'
  },
  title: {
    fontWeight: '200',
    fontSize: '24px',
    marginBottom: '20px'
  },
  bestList: {
    maxHeight: '50%'
  },
  worstList: {
    maxHeight: '50%'
  },
  bestListItem: {
    backgroundColor: "#DCEDC8"
  },
  worstListItem: {
    backgroundColor: "#FFCDD2"
  },
  secondaryText: {
    fontSize: "12px"
  },
  rightIconHack: {
    height: '100%',
    right: '4px',
    width: 'auto',
    marginTop: '0px',
    marginBottom: '0px',
  },
  divNote: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',

  },
  textNote: {
    fontSize: '12px'
  },
  note: {
    fontSize: '24px'
  },
  rankDiv:{
    height: '50px',
    width: '10%',
  },
  place:{
    border: '1px black solid',
    borderRadius: '50%',
  },
  topRankList: {
    display: 'flex',
    height: '140px',
    justifyContent: 'space-around',
    marginBottom: '20px',
    alignItems: 'flex-end',
  },
  topRankContainer: {
    display: 'flex',
  },

  topOne: {
    display: 'flex',
    flexDirection: 'column',
    height: '90px',
    alignSelf: 'flex-start',
    order: 0,
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: '0 10px'
  },
  topTwo: {
    display: 'flex',
    flexDirection: 'column',
    height: '100px',
    order: -1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  topThree: {
    display: 'flex',
    flexDirection: 'column',
    height: '100px',
    order: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rankGoodNote: {
    fontSize: '16px',
    fontWeight: '700px',
    color: '#8BC34A',
  },
  rankBadNote: {
    fontSize: '14px',
    color: '#F44336',
  },
  rankName: {
    fontSize: '12px',
    marginBottom: '5px',
  },
  badgeStyleContainer: {
    padding: '0',
    marginBottom: '20px'
  },
  badgeStyleOne: {
    top: 'auto',
    bottom: '-10px',
    right: '0',
    left: '0',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#FFD200'
  },
  badgeStyleTwo: {
    top: 'auto',
    bottom: '-10px',
    right: '0',
    left: '0',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#FF696A'
  },
  badgeStyleThree: {
    top: 'auto',
    bottom: '-10px',
    right: '0',
    left: '0',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#60C4BA'
  },
  lineRank: {
    display: 'flex',
    height: '40px',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '5px 20px'
  },
  userLineRank: {
    width: '50%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  linkEcodringList:{
    cursor: 'pointer',
    textDecoration: 'none',
    color: 'red'
  },
  listBot:{
    height: '50%'
  },
  listItem:{
    display: 'flex',
    justifyContent: 'space-between'
  }
};
const random = () => {
  return Math.floor(Math.random() * 99) + 1;
};

const EcoDrivingRanking = ({topEcodriver, botEcodriver}) => {

  return (
    <Link to="/parcauto/ecodriving" style={styles.linkEcodringList}>
      <Paper style={styles.paper}>
        <Paper style={styles.topPaper}>
          <Subheader>Classement Ecodriving</Subheader>
          <div style={styles.topRankList}>
            {topEcodriver.map((ecodriver, index) =>
                index === 0
                  ?
                  <span key={index} style={styles.topOne}>
              <Badge
                badgeContent={1}
                primary={true}
                style={styles.badgeStyleContainer}
                badgeStyle={styles.badgeStyleOne}
              >
                <Avatar src={"https://randomuser.me/api/portraits/men/" + random() + ".jpg"} size={80} />
              </Badge>
              <span style={styles.rankName}>{ecodriver.fullName}</span>
              <span style={styles.rankGoodNote}>{ecodriver.ecoDrivingNote}</span>
            </span>
                  : index === 1
                  ?
                  <span key={index} style={styles.topTwo}>
              <Badge
                badgeContent={2}
                primary={true}
                style={styles.badgeStyleContainer}
                badgeStyle={styles.badgeStyleTwo}
              >
                <Avatar src={"https://randomuser.me/api/portraits/men/" + random() + ".jpg"} size={60}/>
              </Badge>
              <span style={styles.rankName}>{ecodriver.fullName}</span>
              <span style={styles.rankGoodNote}>{ecodriver.ecoDrivingNote}</span>
            </span>
                  : index === 2
                    ?
                    <span key={index} style={styles.topThree}>
                <Badge
                  badgeContent={3}
                  primary={true}
                  style={styles.badgeStyleContainer}
                  badgeStyle={styles.badgeStyleThree}
                >
                <Avatar src={"https://randomuser.me/api/portraits/men/" + random() + ".jpg"} size={60} />
              </Badge>
                <span style={styles.rankName}>{ecodriver.fullName}</span>
                <span style={styles.rankGoodNote}>{ecodriver.ecoDrivingNote}</span>
               </span>
                    : <span></span>
            )}
          </div>
        </Paper>
        <List style={styles.listBot}>
          <ListItem
            primaryText="..."
          />
          <Divider />
          {botEcodriver.map((ecodriver, index) =>
              <span key={index}>
                <ListItem
                  primaryText={
                    <span style={styles.listItem}>
                      <span>{ecodriver.fullName}</span>
                      <span style={styles.rankBadNote}>{ecodriver.ecoDrivingNote}</span>
                  </span>
                  }
                  leftAvatar={<Avatar src={"https://randomuser.me/api/portraits/men/" + random() + ".jpg"} />}
                />
                {index !== 3 &&
                <Divider />
                }
          </span>
          )}
        </List>
      </Paper>
    </Link>
  );
};
export default EcoDrivingRanking;