import React from 'react';
import Paper from 'material-ui/Paper';
import { FormAddCar } from '../../components';

const styles = {
  board:{
    padding: '15px'
  },
};

const AddCar = (props) => {
  const {
    handleFormSubmit,
    closeDialog,
    brandList,
    modelList,
  } = props;

  return (
    <Paper style={styles.board}>
        <FormAddCar
          handleFormSubmit={handleFormSubmit}
          handleClose={closeDialog}
          brandList={brandList}
          modelList={modelList}
        />
    </Paper>
  )

};

export default AddCar;