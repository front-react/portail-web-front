import React from "react";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from "material-ui/Table";
import SearchIcon from "material-ui/svg-icons/action/search";
import IconButton from "material-ui/IconButton";
import { Link } from "react-router";
import { SearchBar } from "../../../../components/componup";
import Paper from "material-ui/Paper";
import RaisedButton from "material-ui/RaisedButton";
import { getStripedStyle } from "../../../../utils/style/color";
import NotificationDriveEta from "material-ui/svg-icons/notification/drive-eta";
import ActionAccountBox from "material-ui/svg-icons/action/account-box.js";
import "./style.css";

const styles = {
  board: {
    height: "100%",
    padding: "15px",
  },
  actionBAr: {
    paddingTop: "10px",
    display: "flex",
    alignItems: "center",
  },
  spanSearchBar: {
    display: "flex",
    width: "400px",
    justifyContent: "space-between",
    marginRight: "20px",
  },
};

const CarList = ({ carList, filterCarList }) => {
  return (
    <Paper style={styles.board}>
      <span style={styles.titleHeader}>Liste des véhicules</span>
      {filterCarList && (
        <div style={styles.actionBAr}>
          <span style={styles.spanSearchBar}>
            <SearchBar
              onChange={value => filterCarList(value, "carName")}
              hintText="immatriculation"
              iconSearch={NotificationDriveEta}
            />
            <SearchBar
              onChange={value => filterCarList(value, "driverName")}
              hintText="Conducteur"
              iconSearch={ActionAccountBox}
            />
          </span>
          <Link to="/parcauto/cars/add">
            <RaisedButton label="Ajouter un vehicule" />
          </Link>
        </div>
      )}

      <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow selectable={false}>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Conducteur</TableHeaderColumn>
            <TableHeaderColumn>Marque</TableHeaderColumn>
            <TableHeaderColumn>Modèle</TableHeaderColumn>
            <TableHeaderColumn>Immatriculation</TableHeaderColumn>
            <TableRowColumn>ACTIONS</TableRowColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {carList.map((car, index) => (
            <TableRow
              key={index}
              selectable={false}
              style={{ ...getStripedStyle(index) }}
            >
              <TableRowColumn>{car.carId}</TableRowColumn>
              {car.outDate === null
              ? <TableRowColumn>{car.driverName || "Disponible"}</TableRowColumn>
              : <TableRowColumn>{"Vendu / Épave"}</TableRowColumn>
              }
              <TableRowColumn>{car.brandName}</TableRowColumn>
              <TableRowColumn>{car.modelName}</TableRowColumn>
              <TableRowColumn>{car.registration}</TableRowColumn>
              <TableRowColumn>
                <Link to={"/parcauto/cars/" + car.carId}>
                  <IconButton>
                    <SearchIcon />
                  </IconButton>
                </Link>
              </TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default CarList;
