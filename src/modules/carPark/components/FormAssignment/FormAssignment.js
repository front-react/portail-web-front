import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from 'react-redux'
import {DatePicker, TextField} from 'redux-form-material-ui'
import FlatButton from 'material-ui/FlatButton';
import { DateTimeFormat } from '../../../../utils/data/date';
import { required} from '../../../../utils/data/regex';

const styles = {
  hiddenField: {
    display: 'none'
  }
};

const mapStateToProps = (state, ownProps) => {
  return {
    initialValues: {
      carId: ownProps.carId,
      personId: ownProps.personId,
      connectedPersonId: state.auth.user.idPersonne
    }
  }
};

class FormAssignment extends React.Component {

  render(){
    const {handleSubmit, handleFormSubmit,handleClose, assignmentType, personForAssignmentFullName, personForTransactionFullName} = this.props

    return (
      <div>
        <form onSubmit={handleSubmit(handleFormSubmit)}>
          <Field
            style={styles.hiddenField}
            name="carId"
            component={TextField}
            hintText="carId"
            floatingLabelText="carId"
          />
          <Field
            style={styles.hiddenField}
            name="personId"
            component={TextField}
            hintText="personId"
            floatingLabelText="personId"
          />
          <Field
            style={styles.hiddenField}
            name="connectedPersonId"
            component={TextField}
            hintText="connectedPersonId"
            floatingLabelText="connectedPersonId"
          />

          {assignmentType === 'restitution' || assignmentType === 'transaction' ?
            <Field
              DateTimeFormat={DateTimeFormat}
              locale="fr"
              cancelLabel="Annuler"
              okLabel="Valider"
              name="restitutionDate"
              floatingLabelText={"Date de restitution pour "+personForTransactionFullName+" ?"}
              component={DatePicker}
              autoOk={true}
              validate={required}
              format={null}
              fullWidth={true}
            />
            : <span></span>
          }

          {assignmentType === 'assignment' || assignmentType === 'transaction' ?
            <Field
              DateTimeFormat={DateTimeFormat}
              locale="fr"
              cancelLabel="Annuler"
              okLabel="Valider"
              name="attributionDate"
              floatingLabelText={"Date d'attribution pour "+personForAssignmentFullName+" ?"}
              component={DatePicker}
              autoOk={true}
              validate={required}
              format={null}
              fullWidth={true}
            />
            : <span></span>

          }

          <FlatButton
            label="Annuler"
            primary={true}
            onTouchTap={handleClose}
          />
          <FlatButton
            label="Valider"
            type="submit"
            primary={true}
            keyboardFocused={true}
          />
        </form>
      </div>
    )
  };
}


FormAssignment = reduxForm({
  form: 'fromAssignment',
})(FormAssignment);



export default connect(mapStateToProps)(FormAssignment);
