import React from 'react';
import {Link} from 'react-router'
import Paper from 'material-ui/Paper'
import { PieChart, Pie, Cell, Legend, Tooltip, ResponsiveContainer } from 'recharts';
import FlatButton from 'material-ui/FlatButton';
import Subheader from 'material-ui/Subheader';

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
const styles = {
  paper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    position: 'relative',
  },
  graphTilte:{
    padding: '10px',
  },
  divActionButton: {
    width: '100%',
  },
  actionButton: {
    width: '100%',
  },
  centerLabel: {
    position: 'absolute',
    left: '0',
    right: '0',
    top: '160px',
    margin: '0 auto',
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
  },
};


const CarPieChart = ({carStats}) => {
  let {total, available, assigned} = carStats;
  assigned = parseInt(assigned, 10);
  available = parseInt(available, 10);
  const data = [{name: 'Véhicules attribués', value: assigned}, {name: 'Véhicules disponibles', value: available}];

  return (
    <Paper style={styles.paper}>
      <Subheader>Vehicules du parc</Subheader>
      <div style={styles.centerLabel}>
        <span>{total}</span>
        <span>Véhicules</span>
      </div>
      <ResponsiveContainer  width="100%" height="100%" >
        <PieChart>
          <Pie
            data={data}
            innerRadius={60}
            outerRadius={80}
            fill="#8884d8"
          >
            {
              data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]}/>)
            }
          </Pie>
          <Legend align="center"/>
          <Tooltip/>
        </PieChart>
      </ResponsiveContainer>
      <div style={styles.divActionButton}>
        <Link to="/parcauto/assignment/add">
          <FlatButton style={styles.actionButton} label="Attribuer/Restituer" />
        </Link>
      </div>
      </Paper>
  );
};

export default CarPieChart;