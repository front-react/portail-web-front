import React from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import { SearchBar } from '../../../../components/componup';
import { getStripedStyle } from '../../../../utils/style/color';
import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton';
import FileFileDownload from 'material-ui/svg-icons/file/file-download';
import {Link} from 'react-router';
import CircularProgress from 'material-ui/CircularProgress';
import NotificationDriveEta from "material-ui/svg-icons/notification/drive-eta";
import ActionAccountBox from "material-ui/svg-icons/action/account-box.js";

const styles = {
  board:{
    height: '100%',
    padding: '15px'
  },
  titleHeader:{
    fontWeight: '700',
  },
  actionBAr: {
    paddingTop: '10px',
    display: 'flex',
    alignItems: 'center',
  },
  spanSearchBar: {
    display: 'flex',
    width: '400px',
    justifyContent: 'space-between',
    marginRight: '20px',
  },
  downloadFile:{
    cursor: 'pointer'
  }
};



const AssignmentList = ({assignmentList, filterAssignmentList, getCurrentAsssignment, fileLoading, attributionIdClick}) => {

  return (
    <Paper style={styles.board}>
      <span style={styles.titleHeader}>Historique des attributions</span>
      {filterAssignmentList &&
      <div style={styles.actionBAr}>
        <span style={styles.spanSearchBar}>
            <SearchBar onChange={(value) => filterAssignmentList(value, "carName")}  hintText="immatriculation" iconSearch={NotificationDriveEta}/>
            <SearchBar onChange={(value) => filterAssignmentList(value, "fullName")} hintText="Conducteur" iconSearch={ActionAccountBox}/>
          </span>
        <Link to="/parcauto/assignment/add">
          <RaisedButton label="Attribuer/Restituer un véhicule"/>
        </Link>
      </div>
      }

      <Table selectable={false} >
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow selectable={false}>
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Voiture</TableHeaderColumn>
            <TableHeaderColumn>A Qui</TableHeaderColumn>
            <TableHeaderColumn>Le</TableHeaderColumn>
            <TableHeaderColumn>Par</TableHeaderColumn>
            <TableHeaderColumn>Restitué le</TableHeaderColumn>
            <TableHeaderColumn>par</TableHeaderColumn>
            <TableHeaderColumn>Ville</TableHeaderColumn>
            <TableHeaderColumn>Décharge</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {assignmentList.map((assignment, index) =>
            <TableRow key={index} selectable={false} style={{...getStripedStyle(index)}}>>
              <TableRowColumn>{assignment.attributionId}</TableRowColumn>
              <TableRowColumn>{assignment.carName}</TableRowColumn>
              <TableRowColumn>{assignment.firstName} {assignment.lastName} </TableRowColumn>
              <TableRowColumn>{assignment.attributionDate? new Date(assignment.attributionDate).toLocaleDateString("fr-FR") : '-'}</TableRowColumn>
              <TableRowColumn>{assignment.attributionPersonName }</TableRowColumn>
              <TableRowColumn>{assignment.restitutionDate ? new Date(assignment.restitutionDate).toLocaleDateString("fr-FR") : '-'}</TableRowColumn>
              <TableRowColumn>{assignment.restitutionPersonName || '-'}</TableRowColumn>
              <TableRowColumn>{assignment.city || '-'}</TableRowColumn>
              <TableRowColumn>
                  <FileFileDownload style={styles.downloadFile}
                    onTouchTap={() => getCurrentAsssignment(assignment.carId, assignment.firstName, assignment.lastName, assignment.city, assignment.restitutionDate, assignment.attributionDate, assignment.attributionId)}
                  />
                  {fileLoading && attributionIdClick === assignment.attributionId &&
                    <CircularProgress size={20} />
                  }
              </TableRowColumn>
            </TableRow>
          )}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default AssignmentList

