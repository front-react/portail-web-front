import React, {Component} from 'react'
import {connect} from 'react-redux'
import { reduxForm, Field } from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import Dialog from 'material-ui/Dialog'
import IconButton from 'material-ui/IconButton';
import {dispatchMail} from '../../redux/actions/upcar/dispatchMail'
import FlatButton from 'material-ui/FlatButton'
import CommunicationEmail from 'material-ui/svg-icons/communication/email'


const styles = {
    actionButton: {
      marginTop: 24,
      display: 'flex',
      justifyContent: 'center',
    },
    button: {
      marginRight: 5
    },
    iconButton: {
      width: '18px',
      height: '18px',
    }
  };

//const form = reduxForm({form: 'FormMailUpcar'});
const mapDispatchToProps = (dispatch) => {
  return {
    dispatchMail: (id, body) => {
      dispatch(dispatchMail(id, body))
    }
  }
};


class FormMailUpcar extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
  };

  handleFormSubmit = (requestBody) => {
    const countDays = Math.ceil((new Date(this.props.currentDay).getTime() - new Date(this.props.kmStatementDate).getTime()) / (1000 * 3600 * 24))
    const personId = this.props.personId
    requestBody.countDays = countDays
    this.props.dispatchMail(personId, requestBody);
  }

  render() {
    const {handleSubmit} = this.props;

    return(
      <div>
        <IconButton
          iconStyle={styles.iconButton}
          onTouchTap={this.handleOpen}
        >
            <CommunicationEmail  />
        </IconButton>
        <Dialog
          title="Tapez votre message"
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
        >
          <form style={styles.form} onSubmit={handleSubmit(this.handleFormSubmit)}>
            <Field 
            name="alertMsg" 
            component={TextField} 
            hintText="Tapez votre message"
            multiLine
            rows={5}
            />
            <div style={styles.actionButton}>
              <FlatButton onTouchTap={this.handleClose} primary={true} label="Cancel"/>
              <FlatButton onTouchTap={this.handleClose} secondary={true} type="submit" label="Submit"/>
            </div>
          </form>
        </Dialog>
      </div>
    )
  }
}

FormMailUpcar = reduxForm({
  form: 'FormMailUpcar',
})(FormMailUpcar);


export default connect(null, mapDispatchToProps)(FormMailUpcar)