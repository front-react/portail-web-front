import React, { Component } from "react";
import { connect } from "react-redux";
import Paper from "material-ui/Paper";
import Divider from "material-ui/Divider";
import { UserProfile } from "../../../../components/componup";
import RaisedButton from "material-ui/RaisedButton";
import CheckIcon from "material-ui/svg-icons/navigation/check";
import { Link } from "react-router";
import ContentCreate from "material-ui/svg-icons/content/create";
//import Dialog from "material-ui/Dialog";
import Drawer from 'material-ui/Drawer';
import { FormAutoRadio } from "../../components";
import patchCar from "../../redux/actions/car/patchCar";
import { CarInfoEcoDrivingContainer } from "../../containers";

const styles = {
  paperInfo: {
    padding: "15px",
  },
  sectionHeader: {
    fontSize: "20px",
    marginBottom: "10px",
  },
  section: {
    display: "flex",
    flexDirection: "row",
    marginTop: "10px",
    marginBottom: "20px",
  },
  emptySection: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "40px",
    marginBottom: "40px",
  },
  user: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
  },
  information: {
    fontSize: "14px",
    marginBottom: "5px",
  },
  bold: {
    fontWeight: "600",
  },
  ecoConduiteRanking: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-around",
    width: "50%",
  },
  ecoConduiteLabel: {
    fontSize: "18px",
  },
  ecoConduiteScore: {
    fontSize: "30px",
    color: "#9CCC65",
  },
  ecoConduiterank: {
    color: "#9CCC65",
  },
  mainInformation: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
  },
  details: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
  },
  spanAvailable: {
    textAlign: "center",
    justifyContent: "center",
    marginBottom: "10px",
  },
  available: {
    color: "#8BC34A",
    fontSize: "24pxs",
  },
  attributionButton: {
    width: "100%",
  },
};

const mapDispatchToProps = dispatch => {
  return {
    patchCar: (data, id) => {
      dispatch(patchCar(data, id));
    },
  };
};

class CarInfo extends Component {
  state = {
    currentCarId: null,
    dialogIsOpen: false,
  };

  componentDidMount() {}

  handleFormSubmit = (formData, formCarId) => {
    this.props.patchCar(formData, formCarId);
    this.setState({ dialogIsOpen: false });
  };

  openDialog = CarId => {
    this.setState({
      dialogIsOpen: !this.state.dialogIsOpen,
      currentCarId: CarId,
    });
  };

  render() {
    const { car } = this.props;

    const picture = Math.floor(Math.random() * 99) + 1;

    for (let carField in car) {
      if (typeof car[carField] === "boolean") {
        if (car[carField]) {
          car[carField] = "Oui";
        } else {
          car[carField] = "Non";
        }
      }
    }
    return (
      <Paper style={styles.paperInfo}>
        <div style={styles.sectionHeader}>Conducteur</div>
        <Divider />
        {car.personId ? (
          <div style={styles.section}>
            <div style={styles.user}>
              <UserProfile
                picture={
                  "https://randomuser.me/api/portraits/men/" + picture + ".jpg"
                }
                name={car.driverName}
                job={car.driverJobs || "Non renseigné"}
              />
              <span style={styles.information}>
                <span style={styles.bold}>Ville : </span>
                {car.city || "Non renseigné"} ({car.department})
              </span>
            </div>
            <CarInfoEcoDrivingContainer carId={car.carId} />
          </div>
        ) : (
          <div style={styles.emptySection}>
            {car.outDate ? (
              <span style={styles.spanAvailable}>
                <div>Véhicule est sorti du parc Auto</div>
                <div>{car.comments}</div>
              </span>
            ) : (
              <span>
                <span style={styles.spanAvailable}>
                  <CheckIcon color={"#8BC34A"} />
                  <span style={styles.available}>Disponible</span>
                </span>
                <Link to="/parcauto/assignment/add">
                  <RaisedButton
                    label="Attribuer ce vehicule"
                    style={styles.attributionButton}
                  />
                </Link>
              </span>
            )}
          </div>
        )}

        <div style={styles.sectionHeader}>Informations générales</div>
        <Divider />
        <div style={styles.section}>
          <div style={styles.mainInformation}>
            <span style={styles.information}>
              <span style={styles.bold}>Id : </span>
              {car.carId}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Code balise gps : </span>
              {car.gpsTagCode}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Marque : </span>
              {car.brandName}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Modèle : </span>
              {car.modelName}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Immatricualtion : </span>
              {car.registration}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Date MEC : </span>
              {car.circulationDate ? (
                new Date(car.circulationDate).toLocaleDateString("fr-FR")
              ) : (
                "-"
              )}
            </span>
          </div>
          <div style={styles.details}>
            <span style={styles.information}>
              <span style={styles.bold}>Couleur : </span>
              {car.color}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Régulateur : </span>
              {car.regulator}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Climatisation : </span>
              {car.airConditioner}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Code AutoRadio : </span>
              {car.autoradioCode}
            </span>
          </div>
        </div>

        <div style={styles.sectionHeader}>Achat/Vente</div>
        <Divider />
        <div style={styles.section}>
          <div style={styles.mainInformation}>
            <span style={styles.information}>
              <span style={styles.bold}>Date d'achat : </span>
              {car.purchaseDate ? (
                new Date(car.purchaseDate).toLocaleDateString("fr-FR")
              ) : (
                "-"
              )}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Date de Sortie : </span>
              {car.outDate ? (
                new Date(car.outDate).toLocaleDateString("fr-FR")
              ) : (
                "-"
              )}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Prix HT : </span>
              {new Intl.NumberFormat("fr-FR").format(car.dtPrice)}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>TVA: </span>
              {car.vat}
            </span>
            <span style={styles.information}>
              <span style={styles.bold}>Km à l'achat : </span>
              {new Intl.NumberFormat("fr-FR").format(car.purchaseKm)}
            </span>
          </div>
        </div>
        <div style={styles.section}>
          {car.carId !== null && (
            <RaisedButton
              style={styles.attributionButton}
              label="Modifier les Informations générales"
              icon={<ContentCreate color={"#6F6E79"} />}
              onClick={() => this.openDialog(car.carId)}
            />
          )}
          <Drawer
            open={this.state.dialogIsOpen}
            openSecondary={true}
            docked={false}
            width={500}
            onRequestChange={this.openDialog}
          >
            <FormAutoRadio
              handleFormSubmit={data => this.handleFormSubmit(data, car.carId)}
              handleClose={this.openDialog}
              autoradioCode={car}
            />
          </Drawer>
        </div>
      </Paper>
    );
  }
}

export default connect(null, mapDispatchToProps)(CarInfo);