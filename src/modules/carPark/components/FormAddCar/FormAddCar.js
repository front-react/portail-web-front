import React from "react";
import { connect } from "react-redux";
import { Field, reduxForm, formValueSelector } from "redux-form";
import {
  TextField,
  SelectField,
  DatePicker,
  Checkbox,
} from "redux-form-material-ui";
import MenuItem from "material-ui/MenuItem";
import FlatButton from "material-ui/FlatButton";
import { DateTimeFormat } from "../../../../utils/data/date";
import {
  requiredFloat,
  required,
  requiredInt,
} from "../../../../utils/data/regex";

const styles = {
  hiddenField: {
    display: "none",
  },
  form: {
    display: "flex",
    flexDirection: "column",
  },
  driver: {
    marginBottom: "10px",
  },
  divCheckBox: {
    width: "100%",
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
  },
};

class FormAddCar extends React.Component {
  render() {
    const { handleSubmit, handleFormSubmit, brandList, brandId } = this.props;
    let modelList = this.props.modelList.filter(
      model => model.brandId === brandId,
    );

    return (
      <div>
        <form style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>
          <Field
            name="brandId"
            component={SelectField}
            floatingLabelText="Marque du véhicule"
            validate={required}
            fullWidth={true}
          >
            {brandList.map(brand =>
              <MenuItem
                key={brand.brandId}
                value={brand.brandId}
                primaryText={brand.brandName}
              />,
            )}
          </Field>

          <Field
            name="modelId"
            component={SelectField}
            floatingLabelText="Modele du véhicule"
            fullWidth={true}
          >
            {brandId
              ? modelList.map(model =>
                  <MenuItem
                    key={model.modelId}
                    value={model.modelId}
                    primaryText={model.modelName}
                  />,
                )
              : <MenuItem
                  value={null}
                  primaryText={"Veulliez selectioner une marque"}
                />}
          </Field>

          <Field
            DateTimeFormat={DateTimeFormat}
            locale="fr"
            cancelLabel="Annuler"
            okLabel="Valider"
            name="purchaseDate"
            component={DatePicker}
            autoOk={true}
            format={null}
            validate={required}
            hintText="Date d'achat"
            fullWidth={true}
          />

          <Field
            name="dtPrice"
            component={TextField}
            floatingLabelText="Prix HT"
            validate={requiredFloat}
            fullWidth={true}
          />

          <Field
            name="registration"
            component={TextField}
            floatingLabelText="Immatriculation"
            validate={required}
            fullWidth={true}
          />

          <Field
            name="purchaseKm"
            component={TextField}
            floatingLabelText="Kilometrage"
            validate={requiredInt}
            fullWidth={true}
          />

          <Field
            name="color"
            component={TextField}
            floatingLabelText="Couleur"
            fullWidth={true}
          />

          <div style={styles.divCheckBox}>
            <div>
              <Field name="regulator" component={Checkbox} label="Regulateur" />
            </div>
            <div>
              <Field
                name="airConditioner"
                component={Checkbox}
                label="Climatisation"
              />
            </div>
            <div>
              <Field
                name="withSnowTire"
                component={Checkbox}
                label="Pneu Neige"
              />
            </div>
          </div>

          <Field
            name="gpsTagCode"
            component={TextField}
            floatingLabelText="idQuartix"
            fullWidth={true}
          />

          <Field
            DateTimeFormat={DateTimeFormat}
            locale="fr"
            cancelLabel="Annuler"
            okLabel="Valider"
            name="circulationDate"
            component={DatePicker}
            autoOk={true}
            format={null}
            validate={required}
            hintText="Date de mise en circulation"
            fullWidth={true}
          />

          <Field
            name="autoradioCode"
            component={TextField}
            floatingLabelText="Code auto radio"
            fullWidth={true}
          />

          <FlatButton
            label="Valider"
            type="submit"
            primary={true}
            keyboardFocused={true}
          />
        </form>
      </div>
    );
  }
}

FormAddCar = reduxForm({
  form: "formAddCar",
})(FormAddCar);

const selector = formValueSelector("formAddCar");

FormAddCar = connect(state => {
  const brandId = selector(state, "brandId");
  return {
    brandId,
  };
})(FormAddCar);

export default FormAddCar;
