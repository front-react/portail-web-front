import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {TextField, DatePicker} from 'redux-form-material-ui'
import FlatButton from 'material-ui/FlatButton';
import { DateTimeFormat } from '../../../../utils/data/date';
import { required, requiredInt} from '../../../../utils/data/regex';

const styles = {
  hiddenField: {
    display: 'none'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  driver: {
    marginBottom: '10px'
  }
};


class FormFixRepairTarget extends React.Component {

  render(){
    const {handleSubmit, handleFormSubmit,handleClose, repair} = this.props;

    return (
      <div style={{ width: '100%'}}>
        <form  style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>

          <Field
            DateTimeFormat={DateTimeFormat}
            locale="fr"
            cancelLabel="Annuler"
            okLabel="Valider"
            name="lastControlDate"
            component={DatePicker}
            autoOk={true}
            format={null}
            validate={required}
            hintText="Date du controle"
          />

          <Field
            name="lastControlKm"
            component={TextField}
            floatingLabelText="Kilometrage lors du controle"
            validate={requiredInt}
            fullWidth={true}
          />

          {repair.targetKm &&
          <Field
            name="addToTargetKm"
            component={TextField}
            floatingLabelText={"Dans combien de kilometres voulez vous modifier l'alerte ?"}
            fullWidth={true}
            validate={requiredInt}
          />
          }

          {repair.targetDays &&
          <Field
            name="addToTargetDays"
            component={TextField}
            floatingLabelText={"Rentrer la nouvelle règle pour l'alerte (actuelement "+repair.targetDays+" jours)"}
            fullWidth={true}
          />
          }
          <Field
            name="comments"
            component={TextField}
            floatingLabelText="Commentaire"
            multiLine
            rows={2}
            fullWidth={true}
          />

          <FlatButton
            label="Annuler"
            primary={true}
            onTouchTap={handleClose}
          />

          <FlatButton
            label="Valider"
            type="submit"
            onClick={handleSubmit((data) => { handleFormSubmit(data, repair.repairTypeId) })}
            primary={true}
            keyboardFocused={true}
          />
        </form>
      </div>
    )
  };
}


export default FormFixRepairTarget = reduxForm({
  form: 'formFixRepairTarget',
})(FormFixRepairTarget);

