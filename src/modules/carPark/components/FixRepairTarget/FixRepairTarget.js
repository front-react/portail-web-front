import React from 'react';
import Dialog from 'material-ui/Dialog';
import { FormFixRepairTarget } from '../../components';


import RaisedButton from 'material-ui/RaisedButton';
const styles = {
  assignmentBox:{
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
  },
  dialogue:{
    overflowY: 'auto',
  },
};

const FixRepairTarget = (props) => {
  const {
    handleFormSubmit,
    openDialog,
    closeDialog,
    dialogIsOpen,
    repair
  } = props;
  const title = `Décaler la réparation : ${repair.repairTypeName}`
  return (
    <div style={styles.assignmentBox}>
      <RaisedButton label="Décaler la réparation" onTouchTap={openDialog}/>
      <Dialog
        title={title}
        modal={false}
        open={dialogIsOpen}
        onRequestClose={closeDialog}
        style={styles.dialogue}
      >
        <FormFixRepairTarget
          handleFormSubmit={handleFormSubmit}
          handleClose={closeDialog}
          repair={repair}
        />
      </Dialog>
    </div>
  )

};

export default FixRepairTarget;