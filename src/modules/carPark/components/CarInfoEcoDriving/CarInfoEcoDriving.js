import React from "react";
import "./style.css";

const CarInfoEcoDriving = ({
  carId,
  ecoDriverList,
  ecoDrivingNote,
  classement,
  totalEcoDriver,
}) => {
  return (
    <div className="econduitRanking">
      <span className="econduitLabel">Econduite</span>
      <span className="econduitScore">{ecoDrivingNote}</span>
      <span className="econduitClassement">
        {classement}ème/{totalEcoDriver}
      </span>
      <div />
    </div>
  );
};

export default CarInfoEcoDriving;
