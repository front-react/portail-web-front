import React from "react";

import { SimpleDataView } from "../../../../components/componup";
import FaExclamationTriangle from "react-icons/lib/fa/exclamation-triangle";
import { red500 } from "material-ui/styles/colors";

const UpCarListDash = ({ upCarList }) => {
  if (upCarList !== null) {
    const alertCount = upCarList.length;
    return (
      <div className="repairAlertDash">
        <SimpleDataView
          icon={FaExclamationTriangle}
          dataNumber={alertCount}
          dataLegend="Alertes"
          hexaColor={red500}
        />
      </div>
    );
  }
};

export default UpCarListDash;
