import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import {grey200, grey500, orange500, lightBlue100} from "material-ui/styles/colors"
import { Repair, RepairList } from '../../components';
import {Card, CardHeader, CardText} from "material-ui/Card";
import FrostIcon from 'material-ui/svg-icons/places/ac-unit';
import PlacesBeachAccess from 'material-ui/svg-icons/places/beach-access';
import RaisedButton from 'material-ui/RaisedButton';
import { withLoader } from "../../../../components/Hoc";

const styles = {

  tabsProfile: {
    marginTop: 20,
  },
  tabItemContainerProfile: {
    backgroundColor: grey200,
    borderBottom: '1px solid '+grey500 ,
  },
  tabContainerProfile: {
    marginTop: 20,
  },
  tabProfile: {
    color: grey500,
  },
  tabButtonProfile: {
    textTransform: 'none',
    fontWeight: '300',
  },
  inkBar: {
    backgroundColor: orange500
  },
  frostIcon: {
    height: '20px'
  },
  titleCard: {
    paddingRight: '0',
  },
  attributionButton: {
    width: "100%",
    marginTop: "25px",
    marginBottom: "40px",
  },
};


const groupByTypeId = (repair) => {
  const typeId = [1,2,3,4,5,6,7,8,9,10,11,12];

  let repairGrouped = {};
  typeId.forEach((id) => {
    repairGrouped[id] = [];
  });
  repair.forEach((repair) => {
    repairGrouped[repair.repairTypeId].push(repair)
  });

  return repairGrouped
};


const filterActiveRepair = (repair) => {
  return repair.repairStatusId === "1" || repair.repairStatusId === "2" || repair.repairStatusId === "3" || repair.repairStatusId === "4"
};

const RepairTabs = ({repair, currentKm, currentDriver, InitRepairType, repairType, intialisationReparation, toggleSnowSummer, togglePneu, onExpandChange, togglePneuArriere, togglePneuAvant, toggleParrHiver, togglePavHiver, toggleDisqueAvant, toggleMachoireArr, toggleFreinAvant, withSnowTire, loaderIn}) => {
  
  const repairGrouped = groupByTypeId(repair);

  const PneuHiver = () => {
    return (
      <RaisedButton
        style={styles.attributionButton}
        label="Passer en pneu hiver"
        icon={<FrostIcon color={lightBlue100} />}
        onClick={() => toggleSnowSummer()}
      />
    );
    
  }
   
  const PneuEte = () => {
    return (
      <RaisedButton
        style={styles.attributionButton}
        label="Passer en pneu été"
        icon={<PlacesBeachAccess color={orange500} />}
        onClick={() => toggleSnowSummer()}
      />
    );
  }

  const AssignPneuEte = withLoader(PneuEte);
  const AssignPneuHiver = withLoader(PneuHiver);
  
  return (
    <Tabs
      style={styles.tabsProfile}
      inkBarStyle={styles.inkBar}
      contentContainerStyle={styles.tabContainerProfile}
      tabItemContainerStyle={styles.tabItemContainerProfile}>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Toutes" >
        <div>
          <RepairList repairList={repair} />
        </div>
      </Tab>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Vidange" onClick={() => InitRepairType("1")}>
        <div>
          <Repair repairList={repairGrouped['1']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('1', firstRepair)}  repair={repairGrouped['1'].filter((repair) => filterActiveRepair(repair))} />
          <RepairList repairList={repairGrouped['1']} />
        </div>
      </Tab>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Courroie" onClick={() => InitRepairType("11")} >
        <div>
          <Repair repairList={repairGrouped['11']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('11', firstRepair)}  repair={repairGrouped['11'].filter((repair) => filterActiveRepair(repair))} />
          <RepairList repairList={repairGrouped['11']} />
        </div>
      </Tab>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="CT" onClick={() => InitRepairType("2")} >
        <div>
          <Repair repairList={repairGrouped['2']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('2', firstRepair)}  repair={repairGrouped['2'].filter((repair) => filterActiveRepair(repair))} />
          <RepairList repairList={repairGrouped['2']} />
        </div>
      </Tab>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="CAP" onClick={() => InitRepairType("10")} >
        <div>
          <Repair repairList={repairGrouped['10']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('10', firstRepair)}  repair={repairGrouped['10'].filter((repair) => filterActiveRepair(repair))} />
          <RepairList repairList={repairGrouped['10']} />
        </div>
      </Tab>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Pneus" onClick={() => InitRepairType()}  >
        <div style={styles.titleDrawer}>
          {togglePneu === "0" &&
            <AssignPneuHiver
              isLoading={loaderIn}
            />
          }
          {togglePneu === "1" &&
            <AssignPneuEte
              isLoading={loaderIn}
            />
          }
          
        </div>
        <Card expanded={togglePneuAvant} initiallyExpanded={false} onExpandChange={(newExpandedState) => onExpandChange(newExpandedState, "6")}>
          <CardHeader
            title="Pneus Avant"
            actAsExpander={true}
            showExpandableButton={true}
            onClick={() => InitRepairType("6")}
          />
          <CardText expandable={true}>
            <Repair repairList={repairGrouped['6']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('6', firstRepair)} repair={repairGrouped['6'].filter((repair) => filterActiveRepair(repair))} togglePneu={togglePneu} />
            <RepairList repairList={repairGrouped['6']} />
          </CardText>
        </Card>
        <Card expanded={togglePneuArriere} initiallyExpanded={false} onExpandChange={(newExpandedState) => onExpandChange(newExpandedState, "7")}>
          <CardHeader
            title="Pneus Arrière"
            actAsExpander={true}
            showExpandableButton={true}
            onClick={() => InitRepairType("7")}
          />
          <CardText expandable={true}>
            <Repair repairList={repairGrouped['7']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('7', firstRepair)} repair={repairGrouped['7'].filter((repair) => filterActiveRepair(repair))} togglePneu={togglePneu} />
            <RepairList repairList={repairGrouped['7']} />
          </CardText>
        </Card>
        <Card expanded={togglePavHiver} initiallyExpanded={false} onExpandChange={(newExpandedState) => onExpandChange(newExpandedState, "8")}>
          <CardHeader
            title="Pneus Avant Neige"
            titleStyle={styles.titleCard}
            children={<FrostIcon color={lightBlue100} style={styles.frostIcon}/>}
            actAsExpander={true}
            showExpandableButton={true}
            onClick={() => InitRepairType("8")}
          />
          <CardText expandable={true}>
            <Repair repairList={repairGrouped['8']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType}  intialisationReparation={(firstRepair) => intialisationReparation('8', firstRepair)} repair={repairGrouped['8'].filter((repair) => filterActiveRepair(repair))} togglePneu={togglePneu} />
            <RepairList repairList={repairGrouped['8']} />
          </CardText>
        </Card>
        <Card expanded={toggleParrHiver} initiallyExpanded={false} onExpandChange={(newExpandedState) => onExpandChange(newExpandedState, "9")}>
          <CardHeader
            title="Pneus Arrière Neige"
            children={<FrostIcon color={lightBlue100} style={styles.frostIcon}/>}
            actAsExpander={true}
            showExpandableButton={true}
            onClick={() => InitRepairType("9")}
          />
          <CardText expandable={true}>
            <Repair repairList={repairGrouped['9']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType}  intialisationReparation={(firstRepair) => intialisationReparation('9', firstRepair)} repair={repairGrouped['9'].filter((repair) => filterActiveRepair(repair))} togglePneu={togglePneu} />
            <RepairList repairList={repairGrouped['9']} />
          </CardText>
        </Card>
      </Tab>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Freins" >
        <Card expanded={toggleFreinAvant} initiallyExpanded={false} onExpandChange={(newExpandedState) => onExpandChange(newExpandedState, "3")}>
          <CardHeader
            title="Plaquette de frein avant"
            actAsExpander={true}
            showExpandableButton={true}
            onClick={() => InitRepairType("3")}
          />
          <CardText expandable={true}>
            <Repair repairList={repairGrouped['3']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType}  intialisationReparation={(firstRepair) => intialisationReparation('3', firstRepair)} repair={repairGrouped['3'].filter((repair) => filterActiveRepair(repair))} />
            <RepairList repairList={repairGrouped['3']} />
          </CardText>
        </Card>
        <Card expanded={toggleMachoireArr} initiallyExpanded={false} onExpandChange={(newExpandedState) => onExpandChange(newExpandedState, "4")}>
          <CardHeader
            title="Machoire arrière"
            actAsExpander={true}
            showExpandableButton={true}
            onClick={() => InitRepairType("4")}
          />
          <CardText expandable={true}>
            <Repair repairList={repairGrouped['4']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('4', firstRepair)} repair={repairGrouped['4'].filter((repair) => filterActiveRepair(repair))} />
            <RepairList repairList={repairGrouped['4']} />
          </CardText>
        </Card>
        <Card expanded={toggleDisqueAvant} initiallyExpanded={false} onExpandChange={(newExpandedState) => onExpandChange(newExpandedState, "5")}>
          <CardHeader
            title="Disque avant"
            actAsExpander={true}
            showExpandableButton={true}
            onClick={() => InitRepairType("5")}
          />
          <CardText expandable={true}>
            <Repair repairList={repairGrouped['5']} currentKm={currentKm} currentDriver={currentDriver} repairType={repairType} intialisationReparation={(firstRepair) => intialisationReparation('5', firstRepair)} repair={repairGrouped['5'].filter((repair) => filterActiveRepair(repair))} />
            <RepairList repairList={repairGrouped['5']} />
          </CardText>
        </Card>
      </Tab>
      <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Autres" onClick={() => InitRepairType("12")} >
        <RepairList repairList={repairGrouped['12']} repairType={repairType} intialisationReparation={() => intialisationReparation('12')} />
      </Tab>
    </Tabs>
  )
};
export default RepairTabs;

