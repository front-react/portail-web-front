import React from "react";
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from "material-ui/Table";
import Paper from "material-ui/Paper";
import FaBomb from 'react-icons/lib/fa/bomb';
import Slider from 'material-ui/Slider';
import { SimpleStatsData } from '../../../../components/componup'
import {orange500, grey500} from "material-ui/styles/colors";
import moment from 'moment'
//import IconButton from "material-ui/IconButton";
//import Dialog from 'material-ui/Dialog'

import {FormMailUpcar} from '../../components'
const styles = {
  board: {
    height: "100%",
    padding: "15px",
  },
  actionBar: {
    paddingTop: "10px",
    textAlign: "center",
  },
  spanSearchBar: {
    display: "flex",
    width: "400px",
    justifyContent: "space-between",
    marginRight: "20px",
  },
  actionData: {
    display: 'flex'
  },
  titleHeader: {
    fontSize: '24px',
    display: 'block',
    marginBottom: '12px'
  },
  titleHeaderTab:{
    color: 'rgb(50,50,50)'
  },
  actionerror:{
    fontSize: '25px',
    textAlign: 'center'
  }
};

const DelayUpcars = ({ retardsUpcars, onDragStop, currentUpcarDate, handleSlider }) => {

  const countRetardsUpcars = retardsUpcars.length;

  const currentDay = moment().format("YYYY-MM-DD");

  return (
    <Paper style={styles.board}>
      <span style={styles.titleHeader}>Liste des retards UPCARS: plus de <strong>{currentUpcarDate}</strong> derniers jours</span>

       <div style={styles.actionBar}>
        <div style={styles.actionData}>
          <SimpleStatsData
            icon={FaBomb} 
            dataNumber={countRetardsUpcars} 
            dataLegend="retards Upcar" 
            hexaColor={orange500}
            sizeImg={"55px"}
            height={"120px"}
            width={"50%"}
            numberData={"35px"}
            numberLegend={"20px"}
            titleLabel={"retards Uppcar"}
          />
          <SimpleStatsData
            icon={FaBomb} 
            dataNumber={currentUpcarDate} 
            dataLegend="Jours" 
            hexaColor={grey500}
            sizeImg={"55px"}
            height={"120px"}
            width={"50%"}
            numberData={"35px"}
            numberLegend={"20px"}
            titleLabel={"retards par période"}
          />
        </div>
       <Slider
          min={0}
          max={200}
          step={1}
          value={currentUpcarDate}
          onChange={handleSlider}
          onDragStop={(event) => onDragStop(event, currentUpcarDate)}
        />
        </div>

      <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow selectable={false} >
            <TableHeaderColumn>ID</TableHeaderColumn>
            <TableHeaderColumn>Nom véhicule</TableHeaderColumn>
            <TableHeaderColumn>Personne assigné</TableHeaderColumn>
            <TableHeaderColumn>Jours écoulés</TableHeaderColumn>
            <TableHeaderColumn>Date du dernier UPCAR</TableHeaderColumn>
            <TableHeaderColumn>Envoi mail</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {Object.keys(retardsUpcars).map((retards) => 
            <TableRow
              key={retards}
              selectable={false}
            >
            <TableRowColumn>{retardsUpcars[retards].carId}</TableRowColumn>
              <TableRowColumn>{retardsUpcars[retards].carName}</TableRowColumn>
              <TableRowColumn>{retardsUpcars[retards].drivername}</TableRowColumn>
              <TableRowColumn>{ Math.ceil((new Date(currentDay).getTime() - new Date(retardsUpcars[retards].kmStatementDate).getTime()) / (1000 * 3600 * 24)) + " Jours" }</TableRowColumn>
              <TableRowColumn>{new Date(retardsUpcars[retards].kmStatementDate).toLocaleDateString("fr-FR")}</TableRowColumn>
              <TableRowColumn>
                <FormMailUpcar 
                  personId={retardsUpcars[retards].personId} 
                  currentDay={currentDay}
                  kmStatementDate={retardsUpcars[retards].kmStatementDate}
                />
              </TableRowColumn>
            </TableRow>
          )}
        </TableBody>
      </Table>
    {retardsUpcars.length === 0 &&
      <Paper>
        <div style={styles.actionerror}>Il n'y a pas de retards UPCARS pour les {currentUpcarDate} derniers jours</div>
      </Paper>
    }
    </Paper>
  );
};

export default DelayUpcars;
