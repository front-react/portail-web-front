import React from "react";
import { Field, reduxForm } from "redux-form";
import { TextField } from "redux-form-material-ui";
import FlatButton from "material-ui/FlatButton";
import IconButton from 'material-ui/IconButton'
import ClearIcon from 'material-ui/svg-icons/content/clear';

const styles = {
  hiddenField: {
    display: "none",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    marginTop: "20px"
  },
  driver: {
    marginBottom: "10px",
  },
  colorButton: {
    backgroundColor: 'rgb(250,250,250)',
  },
  titleRepair: {
    textIndent: '20px',
  },
  titleDrawer: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '24px',
    color: 'rgb(220,220,220)',
    backgroundColor: 'rgb(50,50,50)',
  },
};

class FormAutoRadio extends React.Component {
  render() {
    const {
      handleSubmit,
      handleFormSubmit,
      handleClose,
      autoradioCode,
    } = this.props;

    return (
      <div>
        <div style={styles.titleDrawer}>
          <IconButton 
            onTouchTap={handleClose}
            style={styles.colorButton} 
          >
            <ClearIcon />
          </IconButton>
          <span style={styles.titleRepair}>Modifier les informations générales</span>
        </div>
        <div style={{ width: "85%", textAlign: "center", margin:" 0 7%" }}>
          <form style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>
            <Field
              name="gpsTagCode"
              hintText="Modifer le code balise gps"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.gpsTagCode}
              fullWidth={true}
            />

            <Field
              name="circulationDate"
              hintText="Modifer la date de mise en cirsulation (MEC)"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.circulationDate}
              fullWidth={true}
            />

            <Field
              name="dtPrice"
              hintText="Prix HT"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.dtPrice}
              fullWidth={true}
            />

            <Field
              name="outDate"
              hintText={"Modifier date de Sortie"}
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.outDate}
              fullWidth={true}
            />

            <Field
              name="purchaseDate"
              hintText="Modifier date d'achat"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.purchaseDate}
              fullWidth={true}
            />

            <Field
              name="purchaseKm"
              hintText="Modifier le Km à l'achat"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.purchaseKm}
              fullWidth={true}
            />

            <Field
              name="color"
              hintText="Modifer la couleur du vehicule"
              component={TextField}
              multiLine
              value={"Noir"}
              rows={2}
              placeholder={autoradioCode.color}
              fullWidth={true}
            />

            <Field
              name="autoradioCode"
              hintText="Modifer le code AutoRadio"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.autoradioCode}
              fullWidth={true}
            />

            <Field
              name="brandName"
              hintText="Modifer la marque du véhicule"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.brandName}
              fullWidth={true}
            />

            <Field
              name="modelName"
              hintText="Modifer le modèle du véhicule"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.modelName}
              fullWidth={true}
            />

            <Field
              name="registration"
              hintText="Modifer la plaque d'immatriculation"
              component={TextField}
              multiLine
              rows={2}
              placeholder={autoradioCode.registration}
              fullWidth={true}
            />

            <FlatButton
              label="Valider"
              type="submit"
              primary={true}
              onTouchTap={handleClose}
              keyboardFocused={true}
            />

            <FlatButton
              label="Annuler"
              primary={true}
              onTouchTap={handleClose}
            />
          </form>
      </div>
      </div>
      
    );
  }
}

export default (FormAutoRadio = reduxForm({
  form: "FormAutoRadio",
})(FormAutoRadio));