import React, { Component } from "react";
import RaisedButton from "material-ui/RaisedButton";
import { Link } from "react-router";

class AddButtonRedirect extends Component {
  render() {
    return (
      <div>
        <Link to={"/parcauto/cars"}>
          <RaisedButton label={"Finish"} primary={true} />
        </Link>
      </div>
    );
  }
}
export default AddButtonRedirect;
