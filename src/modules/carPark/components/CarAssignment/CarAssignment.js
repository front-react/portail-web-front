import React from 'react';
import Paper from 'material-ui/Paper'
import {Tabs, Tab} from 'material-ui/Tabs';
import Dialog from 'material-ui/Dialog';
import { SearchBarAutoComplete } from '../../../../components/componup';
import { FormAssignment, SortableCarList } from '../../components';
import {grey50, grey200, grey500, orange500} from "material-ui/styles/colors"

const styles = {
  assignmentBox:{
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
  },
  availableBox:{
    width: '49%',
    height:'auto',
    padding: '15px',
  },
  personBox:{
    width: '49%',
    height:'auto',
    padding: '15px',
  },
  sortable:{
    minHeight: '50px',
    backgroundColor: grey50,
    border: '1px dashed '+grey200,
    padding: '15px',
    borderRadius: '5px'
  },
  sortableItem:{
    backgroundColor: 'white',
    listStyleType: 'none',
    marginBottom: '10px',
  },
  sortableItemPaper:{
    padding: '5px',
  },
  sortableItemTitle: {
    textAlign: 'center',
    fontSize: '20px',
    marginBottom: '5px',
  },
  sortableItemInformationList: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  titleCard:{
    padding: '15px',
    fontSize: '24px',
  },
  tabItemContainerProfile: {
    backgroundColor: 'white',
    borderBottom: '1px solid '+grey500 ,
  },
  tabProfile: {
    color: grey500,
  },
  tabButtonProfile: {
    textTransform: 'none',
    fontWeight: '300',
  },
  inkBar: {
    backgroundColor: orange500
  },
};

const CarAssignment = (props) => {
  const {
    carAvailable,
    searchPerson,
    searchCarPerson,
    handleFormSubmit,
    personList,
    personForAssignment,
    personForTransaction,
    openDialog,
    closeDialog,
    dialogIsOpen,
    assignmentType,
    to,
    from = to,
    carId,
  } = props;

  return (
    <div style={styles.assignmentBox}>
      <Paper style={styles.availableBox}>
        <div style={styles.titleCard}>Vehicule à attribuer/restituer</div>
        <Tabs
          inkBarStyle={styles.inkBar}
          tabItemContainerStyle={styles.tabItemContainerProfile}>
          <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Vehicules disponibles">
            <div>
              <SortableCarList carList={carAvailable} handleOpen={openDialog} listId="carAvailable"/>
            </div>
          </Tab>
          <Tab style={styles.tabProfile} buttonStyle={styles.tabButtonProfile} label="Vehicules déja attribués">
            <div>
              <div style={styles.titleCard}>Rechercher un Conducteur</div>
              <SearchBarAutoComplete fullWidth={true} onUpdateInput={searchPerson} onNewRequest={(personId) => searchCarPerson(personId, 'personForTransaction')} data={personList} value={573} textField="personFullName" valueField="personId" />
              {personForTransaction.person  != null &&
              <div>
                <span>Vehicule</span>
                <SortableCarList carList={personForTransaction.assignment} handleOpen={openDialog} listId="personForTransaction"/>
              </div>
              }
            </div>
          </Tab>
        </Tabs>
      </Paper>

      <Paper style={styles.personBox}>
        <div style={styles.titleCard}>Rechercher un Conducteur</div>
        <SearchBarAutoComplete fullWidth={true} onUpdateInput={searchPerson} onNewRequest={(personId) => searchCarPerson(personId, 'personForAssignment')} data={personList} value={573} textField="personFullName" valueField="personId" />
        {personForAssignment.person  != null &&
        <div>
          <span>Vehicule</span>
          <SortableCarList carList={personForAssignment.assignment} handleOpen={openDialog} listId="personForAssignment"/>
        </div>
        }

      </Paper>
      <Dialog
        title="Restitution/Attribution"
        modal={false}
        open={dialogIsOpen}
        onRequestClose={closeDialog}>
        { carId != null && assignmentType != null && to != null &&
        <FormAssignment
          carId={carId}
          personId={props[to].person.personId}
          assignmentType={assignmentType}
          handleFormSubmit={handleFormSubmit}
          handleClose={closeDialog}
          personForAssignmentFullName={props[to].person.personFullName}
          personForTransactionFullName={props[from].person.personFullName}
        />
        }
      </Dialog>
    </div>
  )

};

export default CarAssignment;