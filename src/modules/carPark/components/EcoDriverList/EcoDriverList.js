import React from 'react';

import {Card, CardHeader} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import NotificationDriveEta from "material-ui/svg-icons/notification/drive-eta";
import { SearchBar } from '../../../../components/componup';

const styles ={
  cardStyle:{
    width: 'calc(25% - 20px)',
    display:'inline-block',
    marginRight: '20px',
    marginBottom: '20px'
  },
  marginBlock: {
    margin:'auto 2%'
  },
  board:{
    width: '100%',
  },
};

const random = () => {
  return Math.floor(Math.random() * 99) + 1;
};

const EcoDriverList = ({ecoDriverList, filterListEcodriver}) => {

    return (
      <div style={styles.marginBlock}>
        {filterListEcodriver &&
          <div style={styles.board}>
            <SearchBar onChange={(value) => filterListEcodriver(value, "fullName")} hintText="EcoDrive Conducteur" iconSearch={NotificationDriveEta}/>
          </div>
        }
        {ecoDriverList.map((ecodriver, index) =>
            <Card key={index}  style={styles.cardStyle}>
              <CardHeader
                title={ecodriver.fullName}
                subtitle={ecodriver.carName}
                avatar={"https://randomuser.me/api/portraits/men/" + random() + ".jpg"}
              />
              <List>
                <ListItem primaryText="Classement" secondaryText={ecodriver.classement} />
                <ListItem primaryText="DrivingNote" secondaryText={ecodriver.ecoDrivingNote} />
                <ListItem primaryText="AccelerationNote" secondaryText={ecodriver.accelerationNote} />
                <ListItem primaryText="DecelerationNote" secondaryText={ecodriver.decelerationNote} />
              </List>
            </Card>
        )}
      </div>
    );

}

export default EcoDriverList;