import DashBoard from "./DashBoard/DashBoard";
import CarPieChart from "./CarPieChart/CarPieChart";
import CarList from "./CarList/CarList";
import EcoDrivingRanking from "./EcoDrivingRanking/EcoDrivingRanking";
import CarAvailableList from "./CarAvailableList/CarAvailableList";
import CarProfile from "./CarProfile/CarProfile";
import CarInfo from "./CarInfo/CarInfo";
import KmCpt from "./KmCpt/KmCpt";
import RepairBoard from "./RepairBoard/RepairBoard";
import RepairTabs from "./RepairTabs/RepairTabs";
import Repair from "./Repair/Repair";
import RepairList from "./RepairList/RepairList";
import CarAssignment from "./CarAssignment/CarAssignment";
import FormAssignment from "./FormAssignment/FormAssignment";
import AssignmentList from "./AssignmentList/AssignmentList";
import SortableCarList from "./SortableCarList/SortableCarList";
import AddRepair from "./AddRepair/AddRepair";
import FormAddRepair from "./FormAddRepair/FormAddRepair";
import FixRepairTarget from "./FixRepairTarget/FixRepairTarget";
import FormFixRepairTarget from "./FormFixRepairTarget/FormFixRepairTarget";
import AddCar from "./AddCar/AddCar";
import FormAddCar from "./FormAddCar/FormAddCar";
import DrawerRepair from "./DrawerRepair/DrawerRepair";
import UpCarList from "./UpCarList/UpCarList";
import EcoDriverList from "./EcoDriverList/EcoDriverList";
import FormComment from "./FormComment/FormComment";
import RepairAlertList from "./RepairAlertList/RepairAlertList";
import ImageZoom from "./ImageZoom/ImageZoom";
import FormAutoRadio from "./FormAutoRadio/FormAutoRadio";
import CarInfoEcoDriving from "./CarInfoEcoDriving/CarInfoEcoDriving";
import RepairAlertListDash from "./RepairAlertListDash/RepairAlertListDash";
import KmHistory from "./KmHistory/KmHistory";
import UpCarListDash from "./UpCarListDash/UpCarListDash";
import FormOutCar from "./FormOutCar/FormOutCar";
import DelayUpcars from "./DelayUpcars/DelayUpcars";
import FormMailUpcar from "./FormMailUpcar/FormMailUpcar";

export {
  DashBoard,
  CarPieChart,
  CarList,
  EcoDrivingRanking,
  CarAvailableList,
  CarProfile,
  CarInfo,
  KmCpt,
  RepairBoard,
  RepairTabs,
  Repair,
  RepairList,
  RepairAlertList,
  CarAssignment,
  FormAssignment,
  AssignmentList,
  SortableCarList,
  AddRepair,
  FixRepairTarget,
  FormAddRepair,
  FormFixRepairTarget,
  AddCar,
  FormAddCar,
  DrawerRepair,
  UpCarList,
  EcoDriverList,
  FormComment,
  ImageZoom,
  FormAutoRadio,
  CarInfoEcoDriving,
  RepairAlertListDash,
  KmHistory,
  UpCarListDash,
  FormOutCar,
  DelayUpcars,
  FormMailUpcar
};
