import React from "react";
import FaExclamationTriangle from "react-icons/lib/fa/exclamation-triangle";
import { Card } from "material-ui/Card";
import { List, ListItem } from "material-ui/List";
import FlatButton from "material-ui/FlatButton";
import { Link } from "react-router";
import { AddRepairContainer } from "../../containers";
import { FixRepairTargetContainer } from "../../containers";
import Avatar from "material-ui/Avatar";

const styles = {
  alerteCardContainer: {
    display: "flex",
    flexWrap: "wrap",
  },
  alerteCard: {
    width: "calc(25% - 40px)",
    margin: "20px 20px",
    padding: "15px",
    display: "flex",
    flexDirection: "column",
  },
  alerteCardHeader: {
    display: "flex",
    color: "#F44336",
    fontSize: "24px",
    alignItems: "center",
  },
  alerteCardHeaderIcon: {
    marginRight: "20px",
  },
  alerteCardHeaderInformation: {
    display: "flex",
    flexDirection: "column",
  },
  alerteCardDriver: {
    height: "30px",
    display: "flex",
    alignItems: "center",
  },
  alerteCardDriverAvatar: {
    marginRight: "10px",
  },
  carAction: {
    marginTop: "auto",
  },
  board: {
    width: "100%",
  },
  buttonAlignRepair: {
    width: "100%",
    display: "flex",
    justifyContent: "space-around",
  },
  buttonAlignTarget: {
    marginTop: "20px",
    width: "100%",
    margin: "auto 10%",
  },

  buttonAlignCar: {
    textAlign: "center",
    width: "100%",
  },
};

const random = () => {
  return Math.floor(Math.random() * 99) + 1;
};

const AlertRepairList = ({ repairAlertList }) => {
  repairAlertList = repairAlertList.slice(0, 16);

  return (
    <div style={styles.alerteCardContainer}>
      {repairAlertList.map((alerts, index) => {
        return (
          <Card key={index} style={styles.alerteCard}>
            <span style={styles.alerteCardHeader}>
              <FaExclamationTriangle
                size={38}
                style={styles.alerteCardHeaderIcon}
              />
              <span style={styles.alerteCardHeaderInformation}>
                <span>{alerts.carName}</span>
                <span>{alerts.repairTypeName}</span>
              </span>
            </span>

            <List>
              <ListItem
                disabled={true}
                primaryText="conducteur"
                secondaryText={
                  alerts.personId ? (
                    <span style={styles.alerteCardDriver}>
                      <Avatar
                        src={
                          "https://randomuser.me/api/portraits/men/" +
                          random() +
                          ".jpg"
                        }
                        size={30}
                        style={styles.alerteCardDriverAvatar}
                      />
                      <span>{alerts.personName}</span>
                    </span>
                  ) : (
                    <span>Véhicule non attribuer</span>
                  )
                }
              />
              <ListItem
                disabled={true}
                primaryText="Identifiant de la réparation"
                secondaryText={alerts.repairId}
              />
            </List>
            <div style={styles.carAction}>
              <div style={styles.buttonAlignRepair}>
                <AddRepairContainer
                  dataLocation={"repairAlert"}
                  repairId={alerts.repairId}
                  carId={alerts.carId}
                  carName={alerts.carName}
                  personId={alerts.personId}
                  personName={alerts.personName}
                  repairTypeId={alerts.repairTypeId}
                />
                <FixRepairTargetContainer repair={alerts} />
              </div>
              <Link to={"parcauto/cars/" + alerts.carId}>
                <FlatButton
                  style={styles.buttonAlignCar}
                  label="Détailles du véhicule"
                  secondary={true}
                />
              </Link>
            </div>
          </Card>
        );
      })}
    </div>
  );
};

export default AlertRepairList;
