import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import FlatButton from 'material-ui/FlatButton';


const styles = {
  hiddenField: {
    display: 'none'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  driver: {
    marginBottom: '10px'
  }
};


class FormComment extends React.Component {

  render(){
    
    const {handleSubmit, handleFormSubmit,handleClose} = this.props;

    return (
      
      <div style={{ width: '100%'}}>
        <form  style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>
        
          <Field
            name="comments"
            component={TextField}
            floatingLabelText="Commentaire"
            multiLine
            rows={2}
            fullWidth={true}
          />

          <span>
          <FlatButton
            label="Annuler"
            primary={true}
            onTouchTap={handleClose}
          />

          <FlatButton
            label="Valider"
            type="submit"
            primary={true}
            keyboardFocused={true}
            onTouchTap={handleClose}
          />
          </span>
        </form>
      </div>
    )
  };
}


export default FormComment = reduxForm({
  form: 'formComment',
})(FormComment);

