import React from 'react';
import { ProfileTitle } from '../../../../components/componup';
import { CarInfo, KmCpt, RepairBoard, RepairTabs, FormOutCar } from '../../components';
import RaisedButton from 'material-ui/RaisedButton';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import Dialog from 'material-ui/Dialog';

const styles = {
  row:{
    display: 'flex',
    flexDirection: 'row',
  },
  header:{
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  carInfo: {
    width: '50%',
    padding: '5px'
  },
  carKmAndRepair: {
    width: '50%',
    display: 'flex',
    flexDirection: 'column',
    padding: '5px'
  },
  kmCpt: {
    paddingBottom: '5px'
  },
  repairBoard: {
    paddingTop: '5px',
    flex: '1',
  },
};

const CarProfile = ({car, repair, openDialog, dialogIsOpen, handleFormSubmit, InitRepairType, repairType, intialisationReparation, repairtypeid, toggleSnowSummer, togglePneu, onExpandChange, togglePneuArriere, togglePneuAvant, toggleParrHiver, togglePavHiver, toggleDisqueAvant, toggleMachoireArr, toggleFreinAvant, loaderIn}) => {

  return (
    <span>
      <div style={styles.header}>
        <ProfileTitle title={car.carName}/>
        <div>
          { car.outDate === null &&
          <RaisedButton
            label="Sortir le vehicule du parc"
            icon={<ClearIcon color={"#F44336"}/>}
            onClick={openDialog}
            disabled={car.personId !== null ? true : false}
          />
          }
         <Dialog
           title="Un commentaire ?"
           modal={false}
           open={dialogIsOpen}
           onRequestClose={openDialog}
         >
         <FormOutCar
           handleFormSubmit={handleFormSubmit} handleClose={openDialog}/>
         </Dialog>
        </div>

      </div>
      <div style={styles.row}>
        <div style={styles.carInfo}>
           <CarInfo car={car} />
        </div>
        <div style={styles.carKmAndRepair}>
          <div style={styles.kmCpt}>
            <KmCpt km={car.currentKm} carId={car.carId} />
          </div>
          <div style={styles.repairBoard}>
            <RepairBoard repair={repair} currentKm={car.currentKm} />
          </div>
        </div>
      </div>
      <RepairTabs 
        currentKm={car.currentKm} 
        currentDriver={{personId: car.personId, personName: car.driverName}} 
        repair={repair} 
        InitRepairType={InitRepairType} 
        repairType={repairType} 
        intialisationReparation={(repairtypeid, firstRepair) => intialisationReparation(repairtypeid, firstRepair, car)} 
        toggleSnowSummer={toggleSnowSummer} 
        togglePneu={togglePneu} 
        onExpandChange={onExpandChange}
        togglePneuArriere={togglePneuArriere}
        togglePneuAvant={togglePneuAvant}
        toggleParrHiver={toggleParrHiver}
        togglePavHiver={togglePavHiver}
        toggleDisqueAvant={toggleDisqueAvant}
        toggleMachoireArr={toggleMachoireArr}
        toggleFreinAvant={toggleFreinAvant}
        withSnowTire={car.withSnowTire}
        loaderIn={loaderIn}
      />
    </span>
  )
};

export default CarProfile;
