import React, {Component} from 'react';

import FlatButton from 'material-ui/FlatButton';
import DoneIcon from 'material-ui/svg-icons/action/done';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import ImageCropRotate from 'material-ui/svg-icons/image/crop-rotate';
import Paper from 'material-ui/Paper'
import {Card, CardActions, CardHeader, CardMedia, CardTitle} from 'material-ui/Card';

import { FormComment, UpCarListDash } from '../../components';
import { ImageZoom } from '../../components';
import Dialog from 'material-ui/Dialog';

const styles = {
  upCarCardContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  upCarCard: {
    width: 'calc(25% - 15px)',
    marginRight: '15px',
    marginBottom: '10px'
  },
  upCarImg: {
    width: '100%',
  },
  buttons: {
    marginTop: '10px',
    display: 'flex',
    justifyContent: 'space-between'
  },
  button: {
    width: '30%',
  },
  buttonRotate: {
    width: '80px',
  },
  subtitle:{
    display: 'flex',
    flexDirection: 'column'
  },
  cardActions:{
    display: 'flex',
    justifyContent: 'space-around'
  },
  divNoUpCar:{
    padding: '15px',
    display: 'flex',
    justifyContent: 'center'
  },
  noUpCar:{
    fontStyle: 'italic',
  },
  kmValue:{
    fontSize: '26px',
    padding: '5px',
    alignSelf: 'center',
  },
  textRotation: {
    textAlign: 'center',
  },
  defaultStyle: {
    padding: '0'
  },
  actionCount: { 
    paddingTop: "10px",
    paddingBottom: "20px",
    textAlign: "center",
    margin: "0 40%",
  }

};

//window.onload=getExif;


class UpCarList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      rotation: 0,
      carAlertId: null
    };
  };

  addRotation = (AlertId) => {

    this.setState({carAlertId: AlertId});

    if(this.state.rotation === 360){
      this.setState({rotation: 0});
    }
    else{
      this.setState({rotation: this.state.rotation+90});
    }

  };

  render() {
    const {upCarList, changeUpCarStatus, deleteUpCar, dialogIsOpen, openDialog, closeDialog, handleFormSubmit} = this.props;
    
    const rotationstyle = {
      transform: 'rotate('+this.state.rotation+'deg)'
    }

  if(upCarList.length === 0){
    return (
      <Paper style={styles.divNoUpCar}>
        <span style={styles.noUpCar}>Il n'y a pas d'UPCAR à valider</span>
      </Paper>
    )
  }
    return (
      <div>
        <div style={styles.actionCount}>
          <UpCarListDash upCarList={upCarList} />
        </div>
      <div style={styles.upCarCardContainer}>
        {upCarList.map((upCar) =>
          <Card  style={styles.upCarCard} key={upCar.kmStatementId}>
            <CardHeader
              title={"UpCar n°"+upCar.kmStatementId}
              subtitle={"fait le "+new Date(upCar.kmStatementDate).toLocaleDateString("fr-FR") }
            />
            <CardMedia>

            <div style={upCar.kmStatementId === this.state.carAlertId ? rotationstyle : styles.defaultStyle}>
              <ImageZoom
                image={{
                  src: upCar.imageFileName,
                  alt: 'img',
                  style: styles.upCarImg,
                }}
                zoomImage={{
                  src: upCar.imageFileName,
                  alt: 'img',
                }}
                rotation={upCar.kmStatementId === this.state.carAlertId ? this.state.rotation : '0'}
                personName={upCar.fullName}
                carName={upCar.carName}
                kmValue={upCar.kmValue}
                date={upCar.kmStatementDate}
                validerOrNot={upCar.kmStatementId}
                changeUpCarStatus={changeUpCarStatus}
                openDialog={openDialog}
              />
            </div>
            </CardMedia>
            
            <CardTitle subtitle={
              <span style={styles.subtitle}>
              <span>conducteur :{upCar.fullName}</span>
              <span>{upCar.carName}</span>
              <span style={styles.kmValue}>{new Intl.NumberFormat("fr-FR").format(upCar.kmValue)}Km</span>
            </span>
            } />

            <CardActions style={styles.textRotation}>
              <FlatButton 
                style={styles.buttonRotate}
                icon={<ImageCropRotate  color={"#4CAF50"}/>} 
                onClick={() => this.addRotation(upCar.kmStatementId)} 
                label={"Rotation"}
              />
            </CardActions>
            
            <CardActions style={styles.cardActions}>
              <FlatButton
                style={styles.button}
                icon={<DoneIcon color={"#4CAF50"} />}
                onClick={() => changeUpCarStatus(upCar.kmStatementId,1)}
              />

              <FlatButton
                label="à refaire"
                onTouchTap={() => openDialog(upCar.kmStatementId)}
              />

              <FlatButton
                style={styles.button}
                icon={<ClearIcon color={"#F44336"} />}
                onClick={() => deleteUpCar(upCar.kmStatementId)}
              />
            </CardActions>
          </Card>

        )}
                  <Dialog
                title="Ajouter un commentaire"
                modal={false}
                open={dialogIsOpen}
                onRequestClose={closeDialog}
              >
                <FormComment 
                  handleClose={closeDialog}
                  handleFormSubmit={handleFormSubmit}
                />
              </Dialog>
      </div>
      </div>
    );
  };

}

export default UpCarList

/*

 <Paper style={styles.upCarCard} key={upCar.kmStatementId}>
 <span>Upcar {upCar.kmStatementId}</span>
 <span>{upCar.carName}</span>
 <span>par {upCar.fullName}</span>

 <span style={styles.buttons}>

 </span>
 </Paper>
 */