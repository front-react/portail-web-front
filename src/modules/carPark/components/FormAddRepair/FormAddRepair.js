import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {connect} from 'react-redux'
import {DatePicker, TextField, SelectField} from 'redux-form-material-ui'
import MenuItem from 'material-ui/MenuItem';
import {AutoComplete as MUIAutoComplete} from 'material-ui';
import FlatButton from 'material-ui/FlatButton';
import { Scrollbars } from 'react-custom-scrollbars';
import { DateTimeFormat } from '../../../../utils/data/date';
import {  requiredFloat, required, requiredInt} from '../../../../utils/data/regex';

const styles = {
  hiddenField: {
    display: 'none'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  driver: {
    marginBottom: '10px'
  },
  doubleFieldLine: {
    display: 'flex',
    alignItems: 'flex-end',
  },
  marginRight: {
    marginRight: '40px'
  }
};

const mapStateToProps = (state, ownProps) => {

  return {
    initialValues: {
      carId: ownProps.carId,
      personId: ownProps.personId,
      repairTypeId: ownProps.repairTypeId,
    }
  }
};

class FormAddRepair extends React.Component {

  componentDidMount(){
    this.refs.scrollbars.scrollToTop()
  };

  autoCompleteCar = (searchText) => {
    this.props.getCarList('_filter(carName-like)=%25'+searchText+'%25&_limit=5')
  };

  autoCompleteDriver = (searchText) => {
    this.props.getPersonList('_filter(personFullName-like)=%25'+searchText+'%25&_limit=5')
  };

  autoCompleteServiceProvider = (searchText) => {
    this.props.getServiceProviderList('_filter(serviceProviderName-like)=%25'+searchText+'%25&_limit=5')
  };

  renderAutocomplete = (props) => {
    const {filter, dataSource, dataSourceConfig, onUpdateInput, floatingLabelText, input, validate} = props;

    return (
      <MUIAutoComplete
        floatingLabelText={floatingLabelText}
        onUpdateInput={onUpdateInput}
        dataSourceConfig={dataSourceConfig}
        dataSource={dataSource}
        filter={filter}
        errorText={validate}
        onNewRequest={value => input.onChange(value[dataSourceConfig.value])}
        openOnFocus={true}
        fullWidth={true}
      />
    )
  };

  render(){
    const {handleSubmit, handleFormSubmit,handleClose, repairTypeList, carName, carId, personId, personName} = this.props;
    let {carList, personList, serviceProviderList} = this.props;

    return (
      <div style={{ width: '100%', height: '500px' }}>
        <Scrollbars style={{ width: '100%', height: '100%' }} ref="scrollbars">
        <form  style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>
          {carId
            ? <span style={styles.driver}>Véhicule : {carName}</span>
            :
            <Field
              name="carId"
              component={this.renderAutocomplete}
              floatingLabelText="Rechercher un Véhicules"
              openOnFocus
              onUpdateInput={this.autoCompleteCar}
              filter={MUIAutoComplete.fuzzyFilter}
              dataSourceConfig={{text: 'carName', value: 'carId'}}
              dataSource={carList}
              validate={required}
            />
          }

          {personId
            ? <span>Conducteur : {personName}</span>
            : <Field
              name="personId"
              component={this.renderAutocomplete}
              floatingLabelText="Conducteur lors de la reparation"
              openOnFocus
              onUpdateInput={this.autoCompleteDriver}
              filter={MUIAutoComplete.fuzzyFilter}
              dataSourceConfig={{text: 'personFullName', value: 'personId'}}
              dataSource={personList}
            />
          }

          <Field
            name="repairTypeId"
            component={SelectField}
            floatingLabelText="Type de la réparation"
            validate={required}
            fullWidth={true}
          >
            {repairTypeList.map((repairType) =>
              <MenuItem key={repairType.repairTypeId} value={repairType.repairTypeId} primaryText={repairType.repairTypeName} />
            )}
          </Field>

          <Field
            name="serviceProviderId"
            component={this.renderAutocomplete}
            floatingLabelText="Garage"
            onUpdateInput={this.autoCompleteServiceProvider}
            openOnFocus
            filter={MUIAutoComplete.fuzzyFilter}
            dataSourceConfig={{text: 'serviceProviderName', value: 'serviceProviderId'}}
            dataSource={serviceProviderList}
          />

          <span style={styles.doubleFieldLine}>

          <Field
            style={styles.marginRight}
            name="dtPrice"
            component={TextField}
            validate={requiredFloat}
            floatingLabelText="prix Hors taxe"
          />

          <Field
            DateTimeFormat={DateTimeFormat}
            locale="fr"
            cancelLabel="Annuler"
            okLabel="Valider"
            name="repairDate"
            component={DatePicker}
            autoOk={true}
            format={null}
            validate={required}
            hintText="Date de la réparation"
          />
          </span>

          <Field
            name="repairKm"
            component={TextField}
            floatingLabelText="Kilometrage lors de la réparation"
            validate={requiredInt}
            fullWidth={true}
          />

          <Field
            name="comments"
            component={TextField}
            floatingLabelText="Commentaire"
            multiLine
            rows={2}
            fullWidth={true}
          />

          <FlatButton
            label="Annuler"
            primary={true}
            onTouchTap={handleClose}
          />

          <FlatButton
            label="Valider"
            type="submit"
            primary={true}
            keyboardFocused={true}
          />
        </form>
        </Scrollbars>
      </div>
    )
  };
}


FormAddRepair = reduxForm({
  form: 'formAddRepair',
})(FormAddRepair);



export default connect(mapStateToProps)(FormAddRepair);
