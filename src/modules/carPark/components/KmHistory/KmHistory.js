import React, {Component} from 'react';
import Paper from 'material-ui/Paper'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';

const styles = {
  upCarCardContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  divNoUpCar:{
    padding: '15px',
    display: 'flex',
    justifyContent: 'center'
  },
  noUpCar:{
    fontStyle: 'italic',
  },
  PaperDivTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: 'rgb(200,200,200)',
    color: 'rgb(50,50,50)',
    padding: "10px",
  },
  PaperTitle: {
    fontSize: "24px",
  },
  actionBAr: {
    paddingTop: '10px',
    display: 'flex',
    alignItems: 'center',
    marginLeft: '25px'
  },
  

};


class KmHistory extends Component {

  render() {
  const {kmHistory} = this.props;
    
  if(kmHistory.length === 0){
    return (
      <Paper style={styles.divNoUpCar}>
        <span style={styles.noUpCar}>Il n'y a pas d'historique de UPCAR pour cette voiture</span>
      </Paper>
    )
  }
  else{
    const firstHistory = kmHistory.slice(0,1);
    const carId =  firstHistory[Object.keys(firstHistory)[0]].carId
    return (
      <div style={styles.upCarCardContainer}>
          <Paper>
            <div style={styles.PaperDivTitle}>
              <span style={styles.PaperTitle}>historique pour le véhicule: {carId}</span>
              <span style={styles.actionBAr}>
                <RaisedButton
                  label="Fiche véhicule"
                  primary={true}
                  href={"/parcauto/cars/" + carId }
                />
              </span>
            </div>
            <div style={styles.bodyPaper}>
              <Table selectable={false}>
                <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
                  <TableRow selectable={false}>
                    <TableHeaderColumn>Numero</TableHeaderColumn>
                    <TableHeaderColumn>ID UPCAR</TableHeaderColumn>
                    <TableHeaderColumn>Date du contrôle</TableHeaderColumn>
                    <TableHeaderColumn>Contrôler à</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                {kmHistory.map((upCar, index) =>
                    <TableRow key={upCar.kmStatementId}>
                      <TableRowColumn >{index}</TableRowColumn>
                      <TableRowColumn>{upCar.kmStatementId}</TableRowColumn>
                      <TableRowColumn>{new Date(upCar.kmStatementDate).toLocaleDateString("fr-FR") }</TableRowColumn>
                      <TableRowColumn>{new Intl.NumberFormat("fr-FR").format(upCar.kmValue)}Km</TableRowColumn>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
          </Paper>
      </div>
    );
  }  
  };

}

export default KmHistory
