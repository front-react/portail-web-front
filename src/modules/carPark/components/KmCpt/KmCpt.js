import React from 'react';
import Paper from 'material-ui/Paper'
import { darkerColor } from '../../../../utils/style/color';
import IconButton from "material-ui/IconButton";
import ActionHistory from 'material-ui/svg-icons/action/history'
import {white} from 'material-ui/styles/colors';


const KmCpt = ({km, carId}) => {
  const mainColor = "#FF9800";
  const secondColor = darkerColor(mainColor, -0.05);
  const thirdColor = darkerColor(mainColor, -0.1);

  const styles = {
    paper: {
      color: 'white',
      height: 80,
      textAlign: 'center',
      display: 'flex',
    },
    iconSpan: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '20%',
      height: '100%',
      backgroundColor: mainColor,
    },
    dataSpan: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      width: '60%',
      height: '100%',
      backgroundColor: secondColor,
    },
    historySpan: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      width: '20%',
      height: '100%',
      backgroundColor: thirdColor,
    },
    dataNumber: {
      fontSize: '46px',
    },
    dataColor: {
      color: '#ffffff',
    },
  };

  return (
    <Paper style={styles.paper}>
      <div style={styles.iconSpan}>
        <span style={styles.dataNumber}>Km</span>
      </div>
      <div style={styles.dataSpan}>
        <span style={styles.dataNumber}>{new Intl.NumberFormat("fr-FR").format(km)}</span>
      </div>
      <div style={styles.historySpan}>
      <IconButton
        tooltip="UPCAR History"
        tooltipPosition="top-center"
        href={
          "/parcauto/km-history/" + carId
        }
      >
        <ActionHistory color={white} />
      </IconButton>
      </div>
    </Paper>
  )
};
export default KmCpt;

