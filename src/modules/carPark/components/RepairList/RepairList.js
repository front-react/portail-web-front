import React, {Component} from 'react';
import Paper from 'material-ui/Paper';
import { SearchBar } from '../../../../components/componup';
import { DrawerRepair } from '../../components';
import { AddRepairContainer } from '../../containers';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import SearchIcon from 'material-ui/svg-icons/action/search';
import IconButton from 'material-ui/IconButton'
import { getStripedStyle } from '../../../../utils/style/color';
import NotificationDriveEta from "material-ui/svg-icons/notification/drive-eta";

const styles = {
  board:{
    height: '100%',
    padding: '15px',
  },
  titleHeader:{
    fontWeight: '700',
  },
  actionBAr: {
    paddingTop: '10px',
    display: 'flex',
    alignItems: 'center',
  },
  spanSearchBar: {
    display: 'flex',
    width: '200px',
    justifyContent: 'space-between',
    marginRight: '20px',
  },
};

class RepairList extends Component {

  constructor(props) {
    super(props);
    this.state = {open: false};
    this.state = {repairIdDetail: null};
  };

  handleOpen = (repairIdDetail = null) => {
    this.setState({open: !this.state.open});
    this.setState({repairIdDetail: repairIdDetail});
  };

  render() {
    const {repairList, filterRepairList} = this.props;
    const { open, repairIdDetail} = this.state;
   
    const repairDetail = repairList.find((item) => {
        return item.repairId === repairIdDetail
      }) || null;

    return (
      <Paper style={styles.board}>
        <span style={styles.titleHeader}>Historique des réparations</span>

        <div style={styles.actionBAr}>
          {filterRepairList &&
            <span style={styles.spanSearchBar}>
              <SearchBar onChange={(value) => filterRepairList(value, "carName")}  hintText="immatriculation" iconSearch={NotificationDriveEta}/>
            </span>
          }
          <AddRepairContainer dataLocation={"repairList"} />
        </div>
        
        <Table selectable={false}>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow selectable={false}>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>status</TableHeaderColumn>
              <TableHeaderColumn>Categorie</TableHeaderColumn>
              <TableHeaderColumn>Date du Devis</TableHeaderColumn>
              <TableHeaderColumn>Km du Devis</TableHeaderColumn>
              <TableHeaderColumn>Voiture</TableHeaderColumn>
              <TableHeaderColumn>Prestataire</TableHeaderColumn>
              <TableHeaderColumn>Actions</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {repairList.map((repair, index) =>
              <TableRow key={repair.repairId} selectable={false} style={{...getStripedStyle(index)}}>
                <TableRowColumn>{repair.repairId}</TableRowColumn>
                <TableRowColumn>{repair.repairStatusName}</TableRowColumn>
                <TableRowColumn>{repair.repairTypeName}</TableRowColumn>
                <TableRowColumn>{repair.repairDate ? new Date(repair.repairDate).toLocaleDateString("fr-FR") : '-'}</TableRowColumn>
                <TableRowColumn>{repair.repairKm || '-'}</TableRowColumn>
                <TableRowColumn>{repair.carName}</TableRowColumn>
                <TableRowColumn>{repair.serviceProviderName || '-'}</TableRowColumn>
                <TableRowColumn>
                  <IconButton onTouchTap={ () => this.handleOpen(repair.repairId)}>
                    <SearchIcon />
                  </IconButton>
                </TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
        <DrawerRepair
          handleOpen={this.handleOpen}
          open={open}
          repair={repairDetail}
        />
      </Paper>
    )
  };
}
export default RepairList;

/*
 */