import React from 'react';
import Paper from 'material-ui/Paper'
import Sortable from 'react-sortablejs';
import {grey50, grey200} from "material-ui/styles/colors"
import moment from 'moment'

const styles = {
  sortable:{
    minHeight: '510px',
    backgroundColor: grey50,
    border: '1px dashed '+grey200,
    padding: '15px',
    borderRadius: '5px'
  },
  sortableItem:{
    backgroundColor: 'white',
    listStyleType: 'none',
    marginBottom: '10px',
  },
  sortableItemPaper:{
    padding: '5px',
  },
  sortableItemTitle: {
    textAlign: 'center',
    fontSize: '20px',
    marginBottom: '5px',
  },
  sortableItemInformationList: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
};
const SortableCarList = ({ carList, handleOpen, listId}) => {
  carList = carList || [];

  return (
    <Sortable
      style={styles.sortable}
      id={listId}
      options={{
        group: 'shared',
        onAdd: (event) => {
          let listEvent = {
            from: event.from.id,
            to: event.target.id
          };
          handleOpen(event.item.dataset.id, listEvent,);
        },
      }}
      tag="ul"
      onChange={(order, sortable, evt) => {
      }}
    >
      {carList.map((car, index) =>
        <li style={styles.sortableItem} key={index} data-id={car.carId} data-attributionId={car.attributionId}>
          <Paper style={styles.sortableItemPaper}>
            <div style={styles.sortableItemTitle}>{car.carName}</div>
            <span style={styles.sortableItemInformationList}>
            <span>id: <strong>{car.carId}</strong></span>
            <span>Kilomètrage: <strong>{car.currentKm} Km</strong></span>
              {car.attributionDate &&
              <span>Attribuer le: <strong>{moment(car.attributionDate).format('DD MMMM YYYY')}</strong></span>
              }
              {car.inDate &&
              <span>Date d'achats: {moment(car.inDate).format('DD MMMM YYYY')}</span>
              }


          </span>
          </Paper>
        </li>
      )}
    </Sortable>
  );
};

export default SortableCarList;

