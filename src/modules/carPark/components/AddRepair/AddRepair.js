import React from 'react';
import Dialog from 'material-ui/Dialog';
import { FormAddRepair } from '../../components';


import RaisedButton from 'material-ui/RaisedButton';

const styles = {
  assignmentBox:{
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
  },
  dialogue:{
    overflowY: 'auto',
  },
};

const AddRepair = (props) => {
  const {
    handleFormSubmit,
    openDialog,
    closeDialog,
    dialogIsOpen,
    getCarList,
    getServiceProviderList,
    getPersonList,
    carList,
    personList,
    repairTypeList,
    serviceProviderList,
    carId,
    carName,
    personId,
    personName,
    repairTypeId,
  } = props;

  return (
    <div style={styles.assignmentBox}>
      <RaisedButton label="Rentrer un devis" onTouchTap={openDialog}/>
      <Dialog
        title="Nouveau devis"
        modal={false}
        open={dialogIsOpen}
        onRequestClose={closeDialog}
        style={styles.dialogue}
      >
        <FormAddRepair
          serviceProviderList={serviceProviderList}
          repairTypeList={repairTypeList}
          personList={personList}
          carList={carList}
          getCarList={getCarList}
          getPersonList={getPersonList}
          getServiceProviderList={getServiceProviderList}
          handleFormSubmit={handleFormSubmit}
          handleClose={closeDialog}
          carId={carId}
          carName={carName}
          personId={personId}
          personName={personName}
          repairTypeId={repairTypeId}
        />
      </Dialog>
    </div>
  )

};

export default AddRepair;