import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import FlatButton from 'material-ui/FlatButton';
import { DateTimeFormat } from '../../../../utils/data/date';
import {required} from '../../../../utils/data/regex';
import {DatePicker} from 'redux-form-material-ui';


const styles = {
  hiddenField: {
    display: 'none'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  driver: {
    marginBottom: '10px'
  }
};


class FormOutCar extends React.Component {

  render(){
    
    const {handleSubmit, handleFormSubmit, handleClose} = this.props;

    return (
      
      <div style={{ width: '100%'}}>
        <form  style={styles.form} onSubmit={handleSubmit(handleFormSubmit)}>
        
          <Field
            name="comments"
            component={TextField}
            floatingLabelText="Commentaire"
            multiLine
            rows={2}
            fullWidth={true}
          />

          <Field
            DateTimeFormat={DateTimeFormat}
            locale="fr"
            cancelLabel="Annuler"
            okLabel="Valider"
            name="outDate"
            component={DatePicker}
            autoOk={true}
            format={null}
            validate={required}
            hintText="Date de sortie"
            fullWidth={true}
          />

          <span>
          <FlatButton
            label="Annuler"
            primary={true}
            onTouchTap={handleClose}
          />

          <FlatButton
            label="Valider"
            type="submit"
            primary={true}
            keyboardFocused={true}
          />
          </span>
        </form>
      </div>
    )
  };
}


export default FormOutCar = reduxForm({
  form: 'FormOutCar',
})(FormOutCar);

