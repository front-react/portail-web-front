import React from "react";
import FaExclamationTriangle from "react-icons/lib/fa/exclamation-triangle";
import { Card } from "material-ui/Card";
import { List, ListItem } from "material-ui/List";
import FlatButton from "material-ui/FlatButton";
import { Link } from "react-router";
import { AddRepairContainer } from "../../containers";
import { FixRepairTargetContainer } from "../../containers";
import Avatar from "material-ui/Avatar";
import { RepairAlertListDash } from "../../components";

import { SearchBar } from "../../../../components/componup";
import Paper from "material-ui/Paper";
import NotificationDriveEta from "material-ui/svg-icons/notification/drive-eta";
import ActionAccountBox from "material-ui/svg-icons/action/account-box.js";
import "./style.css";

const styles = {
  alerteCardContainer: {
    display: "flex",
    flexWrap: "wrap",
  },
  alerteCard: {
    width: "calc(33% - 40px)",
    margin: "20px 20px",
    padding: "15px",
    display: "flex",
    flexDirection: "column",
  },
  alerteCardHeader: {
    display: "flex",
    color: "#F44336",
    fontSize: "24px",
    alignItems: "center",
  },
  alerteCardHeaderIcon: {
    marginRight: "20px",
  },
  alerteCardHeaderInformation: {
    display: "flex",
    flexDirection: "column",
  },
  alerteCardDriver: {
    height: "30px",
    display: "flex",
    alignItems: "center",
  },
  alerteCardDriverAvatar: {
    marginRight: "10px",
  },
  carAction: {
    marginTop: "auto",
  },
  board: {
    width: "100%",
  },
  buttonAlignRepair: {
    width: "100%",
    display: "flex",
    justifyContent: "space-around",
  },
  buttonAlignTarget: {
    marginTop: "20px",
    width: "100%",
    margin: "auto 10%",
  },
  buttonAlignCar: {
    textAlign: "center",
    width: "100%",
  },
  actionBAr: {
    paddingTop: "10px",
    display: "flex",
    alignItems: "center",
  },
  spanSearchBar: {
    display: "flex",
    width: "400px",
    justifyContent: "space-between",
    marginRight: "20px",
  },
  boardCar: {
    height: "100%",
    padding: "15px",
  },
  titleHeader: {
    fontWeight: "700",
  },
};

const random = () => {
  return Math.floor(Math.random() * 99) + 1;
};

const AlertRepairList = ({ repairAlertList, filterAlertList }) => {
  const repairAlert = repairAlertList

  return (
    <Paper style={styles.boardCar}>
      <span className="titleHeader">liste des alertes en cours</span>
      {filterAlertList && (
        <div className="actionBar">
          <span className="spanSearchBar">
            <SearchBar
              onChange={value => filterAlertList(value, "carName")}
              hintText="immatriculation"
              iconSearch={NotificationDriveEta}
            />
            <SearchBar
              onChange={value => filterAlertList(value, "personName")}
              hintText="Conducteur"
              iconSearch={ActionAccountBox}
            />
          </span>
          <RepairAlertListDash repairAlertList={repairAlertList} />
        </div>
      )}
      <div style={styles.alerteCardContainer}>
        {repairAlert.map((alerts, index) => {
          return (
            <Card key={index} style={styles.alerteCard}>
              <span style={styles.alerteCardHeader}>
                <FaExclamationTriangle
                  size={38}
                  style={styles.alerteCardHeaderIcon}
                />
                <span style={styles.alerteCardHeaderInformation}>
                  <span>{alerts.carName}</span>
                  <span>{alerts.repairTypeName}</span>
                </span>
              </span>

              <List>
                <ListItem
                  disabled={true}
                  primaryText="conducteur"
                  secondaryText={
                    alerts.driverName ? (
                      <span style={styles.alerteCardDriver}>
                        <Avatar
                          src={
                            "https://randomuser.me/api/portraits/men/" +
                            random() +
                            ".jpg"
                          }
                          size={30}
                          style={styles.alerteCardDriverAvatar}
                        />
                        <span>{alerts.driverName}</span>
                      </span>
                    ) : (

                        <span>Véhicule non attribué</span>
                      )
                  }
                />
                <ListItem
                  disabled={true}
                  primaryText="Identifiant de la réparation"
                  secondaryText={alerts.repairId}
                />

              </List>
              <div style={styles.carAction}>
                <div style={styles.buttonAlignRepair}>
                  <AddRepairContainer
                    dataLocation={"repairAlert"}
                    repairId={alerts.repairId}
                    carId={alerts.carId}
                    carName={alerts.carName}
                    personId={alerts.personId}
                    personName={alerts.personName}
                    repairTypeId={alerts.repairTypeId}
                  />
                  <FixRepairTargetContainer repair={alerts} />
                </div>
                <Link to={"parcauto/cars/" + alerts.carId}>
                  <FlatButton
                    style={styles.buttonAlignCar}
                    label="Détaille véhicule"
                    secondary={true}
                  />
                </Link>
              </div>
            </Card>
          );
        })}
      </div>
    </Paper>
  );
};

export default AlertRepairList;
