import {
  GET_CAR_REPAIR_REQUEST,
  GET_CAR_REPAIR_SUCCESS,
  GET_CAR_REPAIR_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getCarRepairsRequest = (request) => {
  return {
    type: GET_CAR_REPAIR_REQUEST,
    request
  }
};

const getCarRepairsSuccess = (data) => {

  return {
    type: GET_CAR_REPAIR_SUCCESS,
    data
  }
};
const getCarRepairsFailure = (error) => {

  return {
    type: GET_CAR_REPAIR_FAILURE,
    error
  }
};

export const getCarRepairs = (id) => {
  let request = '/cars/'+id+'/repairs';

  

  return dispatch => {
    dispatch(getCarRepairsRequest(request));

    return carsApiProvider.get(request)
      .then(
        res => {
          dispatch(getCarRepairsSuccess(res.data.data))
        },
        error => {
          dispatch(displayError(error));
          dispatch(getCarRepairsFailure(error));
        }
      )
  }
};

export default getCarRepairs;
