import {
  GET_CAR_AVAILABLE_REQUEST,
  GET_CAR_AVAILABLE_SUCCESS,
  GET_CAR_AVAILABLE_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getCarAvailableRequest = (request) => {
  return {
    type: GET_CAR_AVAILABLE_REQUEST,
    request
  }
};

const getCarAvailableSuccess = (data) => {

  return {
    type: GET_CAR_AVAILABLE_SUCCESS,
    data
  }
};
const getCarAvailableFailure = (error) => {

  return {
    type: GET_CAR_AVAILABLE_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/cars/available?'+filter
  }

  return '/cars/available'
};


export const getCarAvailable = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getCarAvailableRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getCarAvailableSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getCarAvailableFailure(error))
        }
      )
  }
};

export default getCarAvailable;
