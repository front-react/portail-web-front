import {
  GET_CAR_MODEL_REQUEST,
  GET_CAR_MODEL_SUCCESS,
  GET_CAR_MODEL_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getCarModelRequest = (request = '/carList') => {
  return {
    type: GET_CAR_MODEL_REQUEST,
    request
  }
};

const getCarModelSuccess = (data) => {

  return {
    type: GET_CAR_MODEL_SUCCESS,
    data
  }
};
const getCarModelFailure = (error) => {

  return {
    type: GET_CAR_MODEL_FAILURE,
    error
  }
};



export const getCarModel = () => {
  let request = '/models';

  return dispatch => {
    dispatch(getCarModelRequest(request));

    return carsApiProvider.get(request)
      .then(
        res => dispatch(getCarModelSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getCarModelFailure(error))
        }
      )
  }
};

export default getCarModel;
