import {
  ADD_CAR_REQUEST,
  ADD_CAR_SUCCESS,
  ADD_CAR_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const addCarRequest = (request) => {
  return {
    type: ADD_CAR_REQUEST,
    request
  }
};

const addCarSuccess = (data, to) => {
  return {
    type: ADD_CAR_SUCCESS,
    data,
    to
  }
};
const addCarFailure = (error) => {

  return {
    type: ADD_CAR_FAILURE,
    error
  }
};



export const addCar = (formData, to) => {
  let request = '/cars';
  const body = {
    data: {
      car: formData
    }
  };

  return dispatch => {
    dispatch(addCarRequest(request));

    return carsApiProvider.post(request, body)
      .then(
        res => dispatch(addCarSuccess(res.data.data, to)),
        error => {
          dispatch(displayError(error));
          dispatch(addCarFailure(error))
        }
      )
  }
};

export default addCar;
