import {
  RESET_CURRENTCAR,
} from '../../types';

export const resetCurrentCart = () => {

  return dispatch => {
    dispatch({
      type: RESET_CURRENTCAR
    });
  };
};

export default resetCurrentCart;
