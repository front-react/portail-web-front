import {
  GET_CAR_BRAND_REQUEST,
  GET_CAR_BRAND_SUCCESS,
  GET_CAR_BRAND_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getCarBrandRequest = (request = '/carList') => {
  return {
    type: GET_CAR_BRAND_REQUEST,
    request
  }
};

const getCarBrandSuccess = (data) => {

  return {
    type: GET_CAR_BRAND_SUCCESS,
    data
  }
};
const getCarBrandFailure = (error) => {

  return {
    type: GET_CAR_BRAND_FAILURE,
    error
  }
};



export const getCarBrand = () => {
  let request = '/brands';

  return dispatch => {
    dispatch(getCarBrandRequest(request));

    return carsApiProvider.get(request)
      .then(
        res => dispatch(getCarBrandSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getCarBrandFailure(error))
        }
      )
  }
};

export default getCarBrand;
