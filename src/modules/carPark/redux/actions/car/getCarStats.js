import {
  GET_CAR_STATS_REQUEST,
  GET_CAR_STATS_SUCCESS,
  GET_CAR_STATS_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getCarStatsRequest = (request = '/carStats') => {
  return {
    type: GET_CAR_STATS_REQUEST,
    request
  }
};

const getCarStatsSuccess = (data) => {

  return {
    type: GET_CAR_STATS_SUCCESS,
    data: data.data
  }
};
const getCarStatsFailure = (error) => {

  return {
    type: GET_CAR_STATS_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/cars/stats?'+filter
  }

  return '/cars/stats'
};


export const getCarStats = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getCarStatsRequest(request));

    return carsApiProvider.get(request)
      .then(
        res => dispatch(getCarStatsSuccess(res.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getCarStatsFailure(error))
        }
      )
  }
};

export default getCarStats;
