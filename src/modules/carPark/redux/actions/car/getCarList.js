import {
  GET_CAR_LIST_REQUEST,
  GET_CAR_LIST_SUCCESS,
  GET_CAR_LIST_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getCarListRequest = (request = '/carList') => {
  return {
    type: GET_CAR_LIST_REQUEST,
    request
  }
};

const getCarListSuccess = (data) => {

  return {
    type: GET_CAR_LIST_SUCCESS,
    data
  }
};
const getCarListFailure = (error) => {

  return {
    type: GET_CAR_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/cars?'+filter
  }

  return '/cars'
};


export const getCarList = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getCarListRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getCarListSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getCarListFailure(error))
        }
      )
  }
};

export default getCarList;
