import {
  PATCH_CAR_REQUEST,
  PATCH_CAR_SUCCESS,
  PATCH_CAR_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const patchCarRequest = (request) => {
  return {
    type: PATCH_CAR_REQUEST,
    request
  }
};

const patchCarSuccess = (data) => {
  return {
    type: PATCH_CAR_SUCCESS,
    data
  }
};
const patchCarFailure = (error) => {

  return {
    type: PATCH_CAR_FAILURE,
    error
  }
};



export const patchCar = (bodyData, carId) => {
  let request = '/cars/'+carId;

  const body = {
    data: {
      car: bodyData
    }
  };

  return dispatch => {
    dispatch(patchCarRequest(request));

    return carsApiProvider.patch(request, body)
      .then(
        res => dispatch(patchCarSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(patchCarFailure(error))
        }
      )
  }
};

export default patchCar;
