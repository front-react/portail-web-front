import {
  GET_CAR_REQUEST,
  GET_CAR_SUCCESS,
  GET_CAR_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getCarRequest = (request = '/carList') => {
  return {
    type: GET_CAR_REQUEST,
    request
  }
};

const getCarSuccess = (data) => {

  return {
    type: GET_CAR_SUCCESS,
    data
  }
};
const getCarFailure = (error) => {

  return {
    type: GET_CAR_FAILURE,
    error
  }
};



export const getCar = (id) => {
  let request = '/cars/'+id;

  return (dispatch, getState) => {
    dispatch(getCarRequest(request));

    if(getState().carPark.car.carList){
      const dataInStore = getState().carPark.car.carList.find((item) => item.carId === id);
      if(dataInStore){
        return dispatch(getCarSuccess(dataInStore))
      }
    }

    return carsApiProvider.get(request)
      .then(
        res => dispatch(getCarSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getCarFailure(error));
        }
      )
  }
};

export default getCar;
