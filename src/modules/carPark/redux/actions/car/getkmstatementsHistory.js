import {
    GET_KM_HISTORY_REQEST,
    GET_KM_HISTORY_SUCCESS,
    GET_KM_HISTORY_FAILURE,
  } from '../../types';
  import {
    carsApiProvider
  } from '../../../../../utils/api/providers';
  
  const getKmHistoryRequest = (request) => {
    return {
      type: GET_KM_HISTORY_REQEST,
      request
    }
  };
  
  const getKmHistorySuccess = (data) => {
  
    return {
      type: GET_KM_HISTORY_SUCCESS,
      data
    }
  };
  const getKmHistoryFailure = (error) => {
  
    return {
      type: GET_KM_HISTORY_FAILURE,
      error
    }
  };
  
  const getRequest = (filter) => {

      return '/kmstatements/history?'+filter

  };
  
  
  export const getkmstatementsHistory = (filter) => {
  
    let request = getRequest(filter);
  
    return dispatch => {
      dispatch(getKmHistoryRequest(request));
      return carsApiProvider.get(request)
        .then(
          res => dispatch(getKmHistorySuccess(res.data.data)),
          error => {
            dispatch(getKmHistoryFailure(error))
          }
        )
    }
  };
  
  export default getkmstatementsHistory;
  