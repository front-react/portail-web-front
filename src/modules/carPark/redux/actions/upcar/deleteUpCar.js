import {
  DELETE_UPCAR_REQUEST,
  DELETE_UPCAR_SUCCESS,
  DELETE_UPCAR_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const deleteUpCarRequest = (request = '/carList', id) => {
  return {
    type: DELETE_UPCAR_REQUEST,
    id,
    request
  }
};

const deleteUpCarSuccess = () => {

  return {
    type: DELETE_UPCAR_SUCCESS,
  }
};
const deleteUpCarFailure = (error) => {

  return {
    type: DELETE_UPCAR_FAILURE,
    error
  }
};



export const deleteUpCar = (id) => {
  let request = '/kmstatements/'+id;

  return dispatch => {
    dispatch(deleteUpCarRequest(request, id));


    return carsApiProvider.delete(request)
      .then(
        res => dispatch(deleteUpCarSuccess()),
        error => {
          dispatch(displayError(error));
          dispatch(deleteUpCarFailure(error))
        }
      )
  }
};

export default deleteUpCar;
