import {
    GET_RETARDS_UPCAR_REQUEST,
    GET_RETARDS_UPCAR_SUCCESS,
    GET_RETARDS_UPCAR_FAILURE,
  } from '../../types';
  import {
    carsApiProvider
  } from '../../../../../utils/api/providers';
  import displayError from '../../../../../redux/actions/displayError';
  
  const getRetardsUpcarRequest = (request = '/carList') => {
    return {
      type: GET_RETARDS_UPCAR_REQUEST,
      request
    }
  };
  
  const getRetardsUpcarSuccess = (data) => {
  
    return {
      type: GET_RETARDS_UPCAR_SUCCESS,
      data
    }
  };
  const getRetardsUpcarFailure = (error) => {
  
    return {
      type: GET_RETARDS_UPCAR_FAILURE,
      error
    }
  };

  const getRequest = (idUpcar) => {
    if(idUpcar === null) {
      return `/kmstatements/retards/${7}`
    }
  
    return `/kmstatements/retards/${idUpcar}`
  };
  
  export const getRetardsUpcars = (idUpcar = null) => {

    let request = getRequest(idUpcar)
  
    return dispatch => {
      dispatch(getRetardsUpcarRequest(request));
  
      return carsApiProvider.get(request)
        .then(
          res => dispatch(getRetardsUpcarSuccess(res.data.data)),
          error => {
            dispatch(displayError(error));
            dispatch(getRetardsUpcarFailure(error));
          }
        )
    }
  };
  
  export default getRetardsUpcars;
  