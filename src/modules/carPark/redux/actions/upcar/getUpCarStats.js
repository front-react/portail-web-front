import {
  GET_UPCAR_STATS_REQUEST,
  GET_UPCAR_STATS_SUCCESS,
  GET_UPCAR_STATS_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getUpcarStatsRequest = (request) => {
  return {
    type: GET_UPCAR_STATS_REQUEST,
    request
  }
};

const getUpcarStatsSuccess = (data) => {

  return {
    type: GET_UPCAR_STATS_SUCCESS,
    data: data.data
  }
};
const getUpcarStatsFailure = (error) => {

  return {
    type: GET_UPCAR_STATS_FAILURE,
    error
  }
};



export const getUpcarStats = () => {

  let request ='/kmstatements/stats';

  return dispatch => {
    dispatch(getUpcarStatsRequest(request));

    return carsApiProvider.get(request)
      .then(
        res => dispatch(getUpcarStatsSuccess(res.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getUpcarStatsFailure(error))
        }
      )
  }
};

export default getUpcarStats;
