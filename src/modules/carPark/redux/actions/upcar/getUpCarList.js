import {
  GET_UPCAR_LIST_REQUEST,
  GET_UPCAR_LIST_SUCCESS,
  GET_UPCAR_LIST_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getUpCarListRequest = (request) => {
  return {
    type: GET_UPCAR_LIST_REQUEST,
    request
  }
};

const getUpCarListSuccess = (data) => {

  return {
    type: GET_UPCAR_LIST_SUCCESS,
    data
  }
};
const getUpCarListFailure = (error) => {

  return {
    type: GET_UPCAR_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/kmstatements?'+filter
  }

  return '/kmstatements'
};


export const getUpCarList = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getUpCarListRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getUpCarListSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getUpCarListFailure(error));
        }
      )
  }
};

export default getUpCarList;
