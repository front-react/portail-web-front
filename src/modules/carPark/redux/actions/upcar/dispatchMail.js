import {
    DISPATCH_MAIL_REQUEST,
    DISPATCH_MAIL_SUCCESS,
    DISPATCH_MAIL_FAILURE,
  } from '../../types';
  import {
    carsApiProvider
  } from '../../../../../utils/api/providers';
  import displayError from '../../../../../redux/actions/displayError';
  
  const dispatchMailRequest = (request) => {
    return {
      type: DISPATCH_MAIL_REQUEST,
      request
    }
  };
  
  const dispatchMailSuccess = (data) => {
  
    return {
      type: DISPATCH_MAIL_SUCCESS,
      data
    }
  };
  const dispatchMailFailure = (error) => {
  
    return {
      type: DISPATCH_MAIL_FAILURE,
      error
    }
  };

  const getRequest = (personId) => {
      return `/kmstatements/retardskmstatements/${personId}`

  };
  
  export const dispatchMail = (personId, bodyData) => {

    let request = getRequest(personId);

    const body = {
        data: bodyData
    }
  
    return dispatch => {
      dispatch(dispatchMailRequest(request));
  
      return carsApiProvider.post(request, body)
        .then(
          res => dispatch(dispatchMailSuccess(res.data.data)),
          error => {
            dispatch(displayError(error));
            dispatch(dispatchMailFailure(error));
          }
        )
    }
  };
  
  export default dispatchMail;
  