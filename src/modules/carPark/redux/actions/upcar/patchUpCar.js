import {
  PATCH_UPCAR_REQUEST,
  PATCH_UPCAR_SUCCESS,
  PATCH_UPCAR_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const patchUpCarRequest = (request, id) => {
  return {
    type: PATCH_UPCAR_REQUEST,
    id,
    request
  }
};

const patchUpCarSuccess = () => {

  return {
    type: PATCH_UPCAR_SUCCESS,
  }
};
const patchUpCarFailure = (error) => {

  return {
    type: PATCH_UPCAR_FAILURE,
    error
  }
};




export const patchUpCar = (id, statusId, validationPersonId, refuseComments) => {

  let request = '/kmstatements/'+id;

  const body = {
    data: {
      kmStatement:{
        kmStatementStatusId: statusId,
        validationPersonId: validationPersonId,
        refuseComments
      }
    }
  };

  return dispatch => {
    dispatch(patchUpCarRequest(request, id));

    return carsApiProvider.patch(request, body)
      .then(
        res => dispatch(patchUpCarSuccess()),
        error => {
          dispatch(displayError(error));
          dispatch(patchUpCarFailure(error))
        }
      )
  }
};

export default patchUpCar;
