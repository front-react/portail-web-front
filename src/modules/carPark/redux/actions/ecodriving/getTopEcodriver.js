import {
  GET_TOP_ECODRIVER_REQUEST,
  GET_TOP_ECODRIVER_SUCCESS,
  GET_TOP_ECODRIVER_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getTopEcodriverRequest = (request = '/assignment') => {
  return {
    type: GET_TOP_ECODRIVER_REQUEST,
    request
  }
};

const getTopEcodriverSuccess = (data) => {

  return {
    type: GET_TOP_ECODRIVER_SUCCESS,
    data
  }
};
const getTopEcodriverFailure = (error) => {

  return {
    type: GET_TOP_ECODRIVER_FAILURE,
    error
  }
};


export const getTopEcodriver = () => {

  let request = "/ecodrivings/ranks/top";

  return dispatch => {
    dispatch(getTopEcodriverRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getTopEcodriverSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getTopEcodriverFailure(error))
        }
      )
  }
};

export default getTopEcodriver;
