import {
  GET_BOT_ECODRIVER_REQUEST,
  GET_BOT_ECODRIVER_SUCCESS,
  GET_BOT_ECODRIVER_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getBotEcodriverRequest = (request = '/assignment') => {
  return {
    type: GET_BOT_ECODRIVER_REQUEST,
    request
  }
};

const getBotEcodriverSuccess = (data) => {

  return {
    type: GET_BOT_ECODRIVER_SUCCESS,
    data
  }
};
const getBotEcodriverFailure = (error) => {

  return {
    type: GET_BOT_ECODRIVER_FAILURE,
    error
  }
};


export const getBotEcodriver = () => {

  let request = "/ecodrivings/ranks/bot";

  return dispatch => {
    dispatch(getBotEcodriverRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getBotEcodriverSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getBotEcodriverFailure(error))
        }
      )
  }
};

export default getBotEcodriver;
