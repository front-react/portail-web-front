import {
  GET_ECODRIVER_REQUEST,
  GET_ECODRIVER_FAILURE,
  GET_ECODRIVER_SUCCESS,
  RESET_ECODRIVER,
} from "../../types";

import { carsApiProvider } from "../../../../../utils/api/providers";
import displayError from "../../../../../redux/actions/displayError";

const getEcodriverRequest = request => {
  return {
    type: GET_ECODRIVER_REQUEST,
    request,
  };
};

const getEcodriverFailure = error => {
  return {
    type: GET_ECODRIVER_FAILURE,
    error,
  };
};

const getEcodriverSuccess = data => {
  return {
    type: GET_ECODRIVER_SUCCESS,
    data,
  };
};

const resetEcodriverSuccess = () => {
  return {
    type: RESET_ECODRIVER,
  };
};

const getRequest = filter => {
  if (filter != null) {
    return "/ecodrivings/ranks?" + filter;
  }
  return "/ecodrivings/ranks";
};

export const getEcodriver = (filter = null) => {
  let request = getRequest(filter);
  return dispatch => {
    dispatch(getEcodriverRequest(request));
    return carsApiProvider.get(request).then(
      res => dispatch(getEcodriverSuccess(res.data.data[0])),
      error => {
        dispatch(displayError(error));
        dispatch(getEcodriverFailure(error));
      },
    );
  };
};

export const resetEcodriver = () => {
  return dispatch => {
    return dispatch(resetEcodriverSuccess(null));
  };
};
