import {
	GET_LIST_ECODRIVER_REQUEST,
	GET_LIST_ECODRIVER_SUCCESS,
	GET_LIST_ECODRIVER_FAILURE
} from '../../types';

import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getListEcodriverRequest = (request) => {
  return {
    type: GET_LIST_ECODRIVER_REQUEST,
    request
  }
};

const getListEcodriverSuccess = (data) => {

  return {
    type: GET_LIST_ECODRIVER_SUCCESS,
    data
  }
};
const getListEcodriverFailure = (error) => {

  return {
    type: GET_LIST_ECODRIVER_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null){
    return '/ecodrivings/ranks?'+filter
  }
  return '/ecodrivings/ranks'
}

export const getListEcodriver = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getListEcodriverRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getListEcodriverSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getListEcodriverFailure(error))
        }
      )
  }
};

export default getListEcodriver;
