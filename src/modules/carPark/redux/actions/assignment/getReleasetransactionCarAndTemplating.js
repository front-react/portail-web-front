import getCar from '../car/getCar';
import createFileTemplating from './createFileTemplating';



export const getReleasetransactionCarAndTemplating = (idCar, nom, prenom, ville, restitution, attibution) => {
    return (dispatch, getState) => {

        return dispatch(getCar(idCar))
        .then(() => {
            const {modelName, brandName, registration} = getState().carPark.car.currentCar

            if(restitution == null){
                const file ='https://d2shof7u5dhewv.cloudfront.net/parc-materiel/Decharge_vehicule.docx'
                const name = 'Decharge_vehicule'
                const etat = attibution;
                return dispatch(createFileTemplating(file, modelName, brandName, nom, prenom, ville, name, registration, attibution, etat))
            }
            else{
                const file ='https://d2shof7u5dhewv.cloudfront.net/parc-materiel/Restitution_vehicule.docx'
                const etat = restitution;
                const name = 'Restitution_vehicule'
                return dispatch(createFileTemplating(file, modelName, brandName, nom, prenom, ville, name, registration, restitution, etat))
            }
            
        } )
    }
        
};

export default getReleasetransactionCarAndTemplating;