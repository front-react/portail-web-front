import {
  GET_ASSIGNMENT_LIST_REQUEST,
  GET_ASSIGNMENT_LIST_SUCCESS,
  GET_ASSIGNMENT_LIST_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getAssignmentListRequest = (request = '/assignment') => {
  return {
    type: GET_ASSIGNMENT_LIST_REQUEST,
    request
  }
};

const getAssignmentListSuccess = (data) => {

  return {
    type: GET_ASSIGNMENT_LIST_SUCCESS,
    data
  }
};
const getAssignmentListFailure = (error) => {

  return {
    type: GET_ASSIGNMENT_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/attributions?'+filter
  }

  return '/attributions'
};


export const getAssignmentList = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getAssignmentListRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getAssignmentListSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getAssignmentListFailure(error))
        }
      )
  }
};

export default getAssignmentList;
