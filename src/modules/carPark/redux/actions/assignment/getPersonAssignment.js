import {
  GET_PERSON_ASSIGNMENT_REQUEST,
  GET_PERSON_ASSIGNMENT_SUCCESS,
  GET_PERSON_ASSIGNMENT_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getPersonAssignmentRequest = (request = '/assignment') => {
  return {
    type: GET_PERSON_ASSIGNMENT_REQUEST,
    request
  }
};

const getPersonAssignmentSuccess = (data, person, to) => {

  return {
    type: GET_PERSON_ASSIGNMENT_SUCCESS,
    data: {
      [to]: {
        person,
        assignment: data
      }
    }
  }
};
const getPersonAssignmentFailure = (error) => {

  return {
    type: GET_PERSON_ASSIGNMENT_FAILURE,
    error
  }
};

const getRequest = (id, filter) => {
  if(filter != null) {
    return '/persons/'+id+'/cars?'+filter
  }

  return '/persons/'+id+'/cars'
};


export const getPersonAssignment = (person, to, filter = null) => {
  const id = person.personId
  let request = getRequest(id, filter);

  return dispatch => {
    dispatch(getPersonAssignmentRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getPersonAssignmentSuccess(res.data.data, person, to)),
        error => {
          dispatch(displayError(error));
          dispatch(getPersonAssignmentFailure(error))
        }
      )
  }
};

export default getPersonAssignment;
