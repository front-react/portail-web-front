import {
  CREATE_FILE_TEMPLATING_REQUEST,
  CREATE_FILE_TEMPLATING_SUCCESS,
  CREATE_FILE_TEMPLATING_FAILURE,
} from '../../types';
import {
  AssignmentApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const createFileTemplatingRequest = (request) => {
  return {
    type: CREATE_FILE_TEMPLATING_REQUEST,
    request
  }
};

const createFileTemplatingSuccess = (data) => {

  return {
    type: CREATE_FILE_TEMPLATING_SUCCESS,
    data
  }
};
const createFileTemplatingFailure = (error) => {

  return {
    type: CREATE_FILE_TEMPLATING_FAILURE,
    error
  }
};




export const createFileTemplating = (file, modelCar, brandCar, nom, prenom, ville, name, registration, etat) => {


  let request ='convert/docxtopdf';
   const body = {
     file: file,
     data: {
        NOM: nom,
        PRENOM: prenom,
        MARQUE: brandCar,
        MODELE: modelCar,
        VILLE: ville,
        IMMATRICULATION: registration
     }
   }
const fileName = nom +'-'+ prenom + '-'+registration + '-' + etat + '-' + name
const reqBody = JSON.stringify(body)

  return (dispatch, setState)=> {
    dispatch(createFileTemplatingRequest(request));
    return AssignmentApiProvider.post(request, reqBody)
      .then(
        res => {
           dispatch(createFileTemplatingSuccess(res));  

                let a = document.createElement("a");
                document.body.appendChild(a);
                a.style = "display: none";

                let blob = new Blob([res.data], { type: 'application/pdf' }),
                url = window.URL.createObjectURL(blob);

                a.href = url;
                a.download = fileName;
                a.click();
                window.URL.revokeObjectURL(url);

           },
        error => {
          dispatch(displayError(error));
          dispatch(createFileTemplatingFailure(error))
        }
      )
  }
};

export default createFileTemplating;
