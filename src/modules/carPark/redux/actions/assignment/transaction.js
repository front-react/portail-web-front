import restitution from './restitution'
import createAssignment from './createAssignment'


export const transaction = (restitutionBody, attributionId, from, assignmentBody, to) => {
  return dispatch => {
    return dispatch(restitution(restitutionBody, attributionId, from))
      .then(
        res => {
          return dispatch(createAssignment(assignmentBody, to))
        })
  }
};

export default transaction;