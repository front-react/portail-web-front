import {
  RESTITUTION_REQUEST,
  RESTITUTION_SUCCESS,
  RESTITUTION_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const restitutionRequest = (request = '/assignment') => {
  return {
    type: RESTITUTION_REQUEST,
    request
  }
};

const restitutionSuccess = (data, from) => {

  return {
    type: RESTITUTION_SUCCESS,
    data,
    from
  }
};
const restitutionFailure = (error) => {

  return {
    type: RESTITUTION_FAILURE,
    error
  }
};



export const restitution = (restitutionBody, attributionId, from) => {
  let request = '/attributions/'+attributionId+'/restitutions';
  const body = {
    data: {
      restitution: restitutionBody
    }
  };

  return dispatch => {
    dispatch(restitutionRequest(request));

    return carsApiProvider.patch(request, body)
      .then(
        res => dispatch(restitutionSuccess(res.data.data, from)),
        error => {
          dispatch(displayError(error));
          dispatch(restitutionFailure(error))
        }
      )
  }
};

export default restitution;
