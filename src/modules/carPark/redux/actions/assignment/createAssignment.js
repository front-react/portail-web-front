import {
  CREATE_ASSIGNMENT_REQUEST,
  CREATE_ASSIGNMENT_SUCCESS,
  CREATE_ASSIGNMENT_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const createAssignmentRequest = (request) => {
  return {
    type: CREATE_ASSIGNMENT_REQUEST,
    request
  }
};

const createAssignmentSuccess = (data, to) => {
  return {
    type: CREATE_ASSIGNMENT_SUCCESS,
    data,
    to
  }
};
const createAssignmentFailure = (error) => {

  return {
    type: CREATE_ASSIGNMENT_FAILURE,
    error
  }
};



export const createAssignment = (formData, to) => {
  let request = '/attributions';
  const body = {
    data: {
      attribution: formData
    }
  };

  return dispatch => {
    dispatch(createAssignmentRequest(request));

    return carsApiProvider.post(request, body)
      .then(
        res => dispatch(createAssignmentSuccess(res.data.data, to)),
        error => {
          dispatch(displayError(error));
          dispatch(createAssignmentFailure(error))
        }
      )
  }
};

export default createAssignment;
