import {
  RESET_ASSIGNMENT,
} from '../../types';

export const resetAssignment = () => {

  return dispatch => {
    dispatch({
      type: RESET_ASSIGNMENT
    });
  };
};

export default resetAssignment;
