import {
  GET_REPAIR_LIST_REQUEST,
  GET_REPAIR_LIST_SUCCESS,
  GET_REPAIR_LIST_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getRepairListRequest = (request) => {
  return {
    type: GET_REPAIR_LIST_REQUEST,
    request
  }
};

const getRepairListSuccess = (data) => {

  return {
    type: GET_REPAIR_LIST_SUCCESS,
    data
  }
};
const getRepairListFailure = (error) => {

  return {
    type: GET_REPAIR_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/repairs?'+filter
  }

  return '/repairs'
};


export const getRepairList = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getRepairListRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getRepairListSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getRepairListFailure(error))
        }
      )
  }
};

export default getRepairList;
