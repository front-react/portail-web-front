import {
  GET_REPAIR_ALERTES_LIST_REQUEST,
  GET_REPAIR_ALERTES_LIST_SUCCESS,
  GET_REPAIR_ALERTES_LIST_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getRepairAlertListRequest = (request) => {
  return {
    type: GET_REPAIR_ALERTES_LIST_REQUEST,
    request
  }
};

const getRepairAlertListSuccess = (data) => {

  return {
    type: GET_REPAIR_ALERTES_LIST_SUCCESS,
    data
  }
};
const getRepairAlertListFailure = (error) => {

  return {
    type: GET_REPAIR_ALERTES_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/repairs?'+filter
  }

  return '/repairs'
};


export const getRepairAlertList = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getRepairAlertListRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getRepairAlertListSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getRepairAlertListFailure(error))
        }
      )
  }
};

export default getRepairAlertList;
