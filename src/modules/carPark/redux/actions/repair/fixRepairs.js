import {
  FIX_REPAIR_REQUEST,
  FIX_REPAIR_SUCCESS,
  FIX_REPAIR_FAILURE,
  FIX_REPAIR_ALERT_SUCCESS,
  FIX_CURRENTCAR_REPAIR_SUCCESS,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const fixRepairRequest = (request) => {
  return {
    type: FIX_REPAIR_REQUEST,
    request
  }
};

const fixRepairSuccess = (data, dataLocation) => {
  if(dataLocation === "currentCarRepair"){
    return {
      type: FIX_CURRENTCAR_REPAIR_SUCCESS,
      data
    }

  }
  else if(dataLocation === "repairList"){
    return {
      type: FIX_REPAIR_SUCCESS,
      data
    }
  }
  else{
     return {
      type: FIX_REPAIR_ALERT_SUCCESS,
      data
    }
  }

};
const fixRepairFailure = (error) => {

  return {
    type: FIX_REPAIR_FAILURE,
    error
  }
};


const getRequest = (repairId, carId, repairTypeId) => {
  if(repairId !== null){
    return '/repairs/'+repairId+'/fix'
  }
  else if(carId !== null && repairTypeId !== null){
    return '/repairs/fix/'+carId+'/'+repairTypeId
  }
};

export const fixRepair = (bodyData, repairId = null, carId = null, repairTypeId = null, dataLocation) => {
  let request = getRequest(repairId, carId, repairTypeId);

  const body = {
    data: {
      repair: bodyData
    }
  };

  return dispatch => {
    dispatch(fixRepairRequest(request));
    return carsApiProvider.patch(request, body)
      .then(
        res => dispatch(fixRepairSuccess(res.data.data, dataLocation)),
        error => {
          dispatch(displayError(error));
          dispatch(fixRepairFailure(error))
        }
      )
  }
};

export default fixRepair;
