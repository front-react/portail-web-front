 import fixRepair from './fixRepairs'
import createRepair from './createRepair'


export const fixThenCreateRepair = (fixBody, createBody, repairId, carId, repairTypeId, dataLocation) => {
  return dispatch => {
    return dispatch(fixRepair(fixBody, repairId, carId, repairTypeId, dataLocation))
      .then(
        res => {
        return dispatch(createRepair(createBody, dataLocation))
      })
  }
};

export default fixThenCreateRepair;