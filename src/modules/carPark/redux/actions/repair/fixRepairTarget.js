import {
  FIX_REPAIR_TARGET_REQUEST,
  FIX_REPAIR_TARGET_SUCCESS,
  FIX_REPAIR_TARGET_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const fixRepairTargetRequest = (request) => {
  return {
    type: FIX_REPAIR_TARGET_REQUEST,
    request
  }
};

const fixRepairTargetSuccess = (data) => {
  return {
    type: FIX_REPAIR_TARGET_SUCCESS,
    data
  }
};
const fixRepairTargetFailure = (error) => {

  return {
    type: FIX_REPAIR_TARGET_FAILURE,
    error
  }
};

export const fixRepairTarget = (repairId, bodyData) => {
  let request = '/repairs/'+repairId+'/target';

  //const stringify = JSON.stringify(bodyData)

  const body = {
    data: {
      repair: bodyData
    }
  };

  return dispatch => {
    dispatch(fixRepairTargetRequest(request));

    return carsApiProvider.patch(request, body)
      .then(
        res => dispatch(fixRepairTargetSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(fixRepairTargetFailure(error))
        }
      )
  }
};

export default fixRepairTarget;
