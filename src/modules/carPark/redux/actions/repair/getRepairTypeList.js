import {
  GET_REPAIR_TYPE_LIST_REQUEST,
  GET_REPAIR_TYPE_LIST_SUCCESS,
  GET_REPAIR_TYPE_LIST_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getRepairTypeListRequest = (request) => {
  return {
    type: GET_REPAIR_TYPE_LIST_REQUEST,
    request
  }
};

const getRepairTypeListSuccess = (data) => {

  return {
    type: GET_REPAIR_TYPE_LIST_SUCCESS,
    data
  }
};
const getRepairTypeListFailure = (error) => {

  return {
    type: GET_REPAIR_TYPE_LIST_FAILURE,
    error
  }
};



export const getRepairTypeList = () => {

  let request = '/repairs/types';

  return dispatch => {
    dispatch(getRepairTypeListRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getRepairTypeListSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getRepairTypeListFailure(error))
        }
      )
  }
};

export default getRepairTypeList;
