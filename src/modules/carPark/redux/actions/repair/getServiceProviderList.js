import {
  GET_SERVICE_PROVIDER_LIST_REQUEST,
  GET_SERVICE_PROVIDER_LIST_SUCCESS,
  GET_SERVICE_PROVIDER_LIST_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getServiceProviderListRequest = (request) => {
  return {
    type: GET_SERVICE_PROVIDER_LIST_REQUEST,
    request
  }
};

const getServiceProviderListSuccess = (data) => {

  return {
    type: GET_SERVICE_PROVIDER_LIST_SUCCESS,
    data
  }
};
const getServiceProviderListFailure = (error) => {

  return {
    type: GET_SERVICE_PROVIDER_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/servicesproviders?'+filter
  }

  return '/servicesproviders'
};


export const getServiceProviderList = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getServiceProviderListRequest(request));
    return carsApiProvider.get(request)
      .then(
        res => dispatch(getServiceProviderListSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getServiceProviderListFailure(error))
        }
      )
  }
};

export default getServiceProviderList;
