import {
  CREATE_REPAIR_REQUEST,
  CREATE_REPAIR_SUCCESS,
  CREATE_REPAIR_FAILURE,
  CREATE_CURRENTCAR_REPAIR_SUCCESS,
  CREATE_ALERT_REPAIR_SUCCESS,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const createRepairRequest = (request) => {
  return {
    type: CREATE_REPAIR_REQUEST,
    request
  }
};

const createRepairSuccess = (data, dataLocation) => {
  if(dataLocation === "currentCarRepair"){
    return {
      type: CREATE_CURRENTCAR_REPAIR_SUCCESS,
      data
    }
  }
  else if(dataLocation === "repairList"){
    return {
      type: CREATE_REPAIR_SUCCESS,
      data
    }
  }
  else{
     return {
      type: CREATE_ALERT_REPAIR_SUCCESS,
      data
    }
  }
};


const createRepairFailure = (error) => {

  return {
    type: CREATE_REPAIR_FAILURE,
    error
  }
};



export const createRepair = (bodyData, dataLocation) => {
  let request = '/repairs';
  
  const body = {
    data: {
      repair: bodyData
    }
  };

  return dispatch => {
    dispatch(createRepairRequest(request));

    return carsApiProvider.post(request, body)
      .then(
        res => dispatch(createRepairSuccess(res.data.data, dataLocation)),
        error => {
          dispatch(displayError(error));
          dispatch(createRepairFailure(error))
        }
      )
  }
};

export default createRepair;
