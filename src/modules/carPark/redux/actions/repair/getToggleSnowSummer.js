import {
    GET_TOGGLE_SNOW_SUMMER_REQUEST,
    GET_TOGGLE_SNOW_SUMMER_SUCCESS,
    GET_TOGGLE_SNOW_SUMMER_FAILURE,
  } from '../../types';
  import {
    carsApiProvider
  } from '../../../../../utils/api/providers';
  import displayError from '../../../../../redux/actions/displayError';

  import getCar from '../car/getCar';
  import getCarRepairs from '../car/getCarRepairs';
  
  const getToggleSnowSummerRequest = (request) => {
    return {
      type: GET_TOGGLE_SNOW_SUMMER_REQUEST,
      request
    }
  };
  
  const getToggleSnowSummerSuccess = (data) => {
  
    return {
      type: GET_TOGGLE_SNOW_SUMMER_SUCCESS,
      data
    }
  };
  const getToggleSnowSummerFailure = (error) => {
  
    return {
      type: GET_TOGGLE_SNOW_SUMMER_FAILURE,
      error
    }
  };
  
  const getRequest = (carId, idTire) => {

    if(idTire === 1){
      return `/cars/${carId}/snowtires/snow`
    }
    else{
      return `/cars/${carId}/snowtires/summer`
    }
      
  };
  
  
  export const getToggleSnowSummer = (carId, idTire) => {
  
    let request = getRequest(carId, idTire);

    const body = {
      data:{

      }
    }
  
    return dispatch => {
      dispatch(getToggleSnowSummerRequest(request));
      return carsApiProvider.patch(request, body)
        .then(
          res => {
            dispatch(getToggleSnowSummerSuccess(res.data.data));
            dispatch(getCarRepairs(carId));
            dispatch(getCar(carId));
          },
          error => {
            dispatch(displayError(error));
            dispatch(getToggleSnowSummerFailure(error))
          }
        )
    }
  };
  
  export default getToggleSnowSummer;
  