import {
  GET_ALERT_REQUEST,
  GET_ALERT_SUCCESS,
  GET_ALERT_FAILURE,
} from '../../types';
import {
  carsApiProvider
} from '../../../../../utils/api/providers';
import displayError from '../../../../../redux/actions/displayError';

const getAlertRequest = (request) => {
  return {
    type: GET_ALERT_REQUEST,
    request
  }
};

const getAlertSuccess = (data) => {

  return {
    type: GET_ALERT_SUCCESS,
    data
  }
};
const getAlertFailure = (error) => {

  return {
    type: GET_ALERT_FAILURE,
    error
  }
};



export const getAlert = () => {
  let request = '/repairs/stats';

  return dispatch => {
    dispatch(getAlertRequest(request));

    return carsApiProvider.get(request)
      .then(
        res => dispatch(getAlertSuccess(res.data.data)),
        error => {
          dispatch(displayError(error));
          dispatch(getAlertFailure(error))
        }
      )
  }
};

export default getAlert;
