import {
  GET_UPCAR_LIST_REQUEST,
  GET_UPCAR_LIST_SUCCESS,
  GET_UPCAR_LIST_FAILURE,
  PATCH_UPCAR_REQUEST,
  PATCH_UPCAR_SUCCESS,
  PATCH_UPCAR_FAILURE,
  DELETE_UPCAR_REQUEST,
  DELETE_UPCAR_SUCCESS,
  DELETE_UPCAR_FAILURE,
  GET_UPCAR_STATS_REQUEST,
  GET_UPCAR_STATS_SUCCESS,
  GET_UPCAR_STATS_FAILURE,
  GET_RETARDS_UPCAR_REQUEST,
  GET_RETARDS_UPCAR_SUCCESS,
  GET_RETARDS_UPCAR_FAILURE,
  DISPATCH_MAIL_REQUEST,
  DISPATCH_MAIL_SUCCESS,
  DISPATCH_MAIL_FAILURE
} from '../types'

const initialState = {
  upCarList: null,
  upCarStats: null,
  lastRequest: null,
  retardsUpcars: null,
  error: null,
};

export const upCarReducer = (state = initialState, action) => {
  const {data,type,request,error} = action;

  switch (type) {

    case GET_UPCAR_LIST_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_UPCAR_LIST_SUCCESS:
      return {
        ...state,
        upCarList: data,
      };

    case GET_UPCAR_LIST_FAILURE:
      return {
        ...state,
        error
      };

    case PATCH_UPCAR_REQUEST:
      return {
        ...state,
        lastRequest: request,
        upCarList: state.upCarList.filter( (item, index) => item.kmStatementId !== action.id)
      };

    case PATCH_UPCAR_SUCCESS:
      return {
        ...state,
      };

    case PATCH_UPCAR_FAILURE:
      return {
        ...state,
        error
      };

    case DELETE_UPCAR_REQUEST:
      return {
        ...state,
        lastRequest: request,
        upCarList: state.upCarList.filter( (item, index) => item.kmStatementId !== action.id)
      };

    case DELETE_UPCAR_SUCCESS:
      return {
        ...state,
      };

    case DELETE_UPCAR_FAILURE:
      return {
        ...state,
        error
      };

    case GET_UPCAR_STATS_REQUEST:
      return {
        ...state,
        lastRequest: request,
      };

    case GET_UPCAR_STATS_SUCCESS :
      return {
        ...state,
        upCarStats: data,
      };

    case GET_UPCAR_STATS_FAILURE:
      return {
        ...state,
        error
      };
    case GET_RETARDS_UPCAR_REQUEST:
      return {
        ...state,
        lastRequest: request,
      };

    case GET_RETARDS_UPCAR_SUCCESS :
      return {
        ...state,
        retardsUpcars: data,
      };

    case GET_RETARDS_UPCAR_FAILURE:
      return {
        ...state,
        error
      };
    case DISPATCH_MAIL_REQUEST:
      return {
        ...state,
        lastRequest: request,
      };

    case DISPATCH_MAIL_SUCCESS :
      return {
        ...state,
      };

    case DISPATCH_MAIL_FAILURE:
      return {
        ...state,
        error
      };
      
    default:
      return state;
  }
};

export default upCarReducer;