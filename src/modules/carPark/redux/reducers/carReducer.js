import {
  GET_CAR_LIST_REQUEST,
  GET_CAR_LIST_SUCCESS,
  GET_CAR_LIST_FAILURE,
  GET_CAR_REQUEST,
  GET_CAR_SUCCESS,
  GET_CAR_FAILURE,
  GET_CAR_STATS_REQUEST,
  GET_CAR_STATS_SUCCESS,
  GET_CAR_STATS_FAILURE,
  GET_CAR_BRAND_REQUEST,
  GET_CAR_BRAND_SUCCESS,
  GET_CAR_BRAND_FAILURE,
  GET_CAR_MODEL_REQUEST,
  GET_CAR_MODEL_SUCCESS,
  GET_CAR_MODEL_FAILURE,
  ADD_CAR_REQUEST,
  ADD_CAR_SUCCESS,
  ADD_CAR_FAILURE,
  PATCH_CAR_REQUEST,
  PATCH_CAR_SUCCESS,
  PATCH_CAR_FAILURE,
  RESET_CURRENTCAR,
  GET_KM_HISTORY_REQEST,
  GET_KM_HISTORY_SUCCESS,
  GET_KM_HISTORY_FAILURE,
} from '../types'

const initialState = {
  carList: null,
  carStats: null,
  currentCar: null,
  carStatsLoaded: false,
  carListLoaded: false,
  lastRequest: null,
  error: null,
  brand: null,
  model: null,
  kmHistory: null,
  loaderIn: false
};

export const carReducer = (state = initialState, action) => {
  const {data,type,request,error} = action;

  switch (type) {

    case GET_CAR_LIST_REQUEST:
      return {
        ...state,
        carListLoaded: false,
        lastRequest: request
      };

    case GET_CAR_LIST_SUCCESS:
      return {
        ...state,
        carList: data,
        carListLoaded: true,
      };

    case GET_CAR_LIST_FAILURE:
      return {
        ...state,
        carListLoaded: false,
        error
      };

    case GET_CAR_REQUEST:
      return {
        ...state,
        lastRequest: request,
        loaderIn: true
      };

    case GET_CAR_SUCCESS:
      return {
        ...state,
        currentCar: {
          ...state.currentCar,
          ...data
        },
        loaderIn: false
      };

    case GET_CAR_FAILURE:
      return {
        ...state,
        currentCar: null,
        error
      };

    case GET_CAR_STATS_REQUEST:
      return {
        ...state,
        carStatsLoaded: false,
        lastRequest: request
      };

    case GET_CAR_STATS_SUCCESS:
      return {
        ...state,
        carStats:  data,
        carStatsLoaded: true,
      };

    case GET_CAR_STATS_FAILURE:
      return {
        ...state,
        carStatsLoaded: false,
        error
      };

    case GET_CAR_BRAND_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_CAR_BRAND_SUCCESS:
      return {
        ...state,
        brand:  data,
      };

    case GET_CAR_BRAND_FAILURE:
      return {
        ...state,
        error
      };

    case GET_CAR_MODEL_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_CAR_MODEL_SUCCESS:
      return {
        ...state,
        model:  data,
      };

    case GET_CAR_MODEL_FAILURE:
      return {
        ...state,
        error
      };

    case ADD_CAR_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case ADD_CAR_SUCCESS:
      return {
        ...state,
      };

    case ADD_CAR_FAILURE:
      return {
        ...state,
        error
      };


    case PATCH_CAR_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case PATCH_CAR_SUCCESS:
      return {
        ...state,
        currentCar: data,
      };

    case PATCH_CAR_FAILURE:
      return {
        ...state,
        error
      };

    case RESET_CURRENTCAR:
      return {
        ...state,
        currentCar: null,
      };
    case GET_KM_HISTORY_REQEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_KM_HISTORY_SUCCESS:
      return {
        ...state,
        kmHistory: data,
      };

    case GET_KM_HISTORY_FAILURE:
      return {
        ...state,
        error
      };

    default:
      return state;
  }
};

export default carReducer;