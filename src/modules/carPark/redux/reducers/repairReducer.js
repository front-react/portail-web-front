import {
  GET_REPAIR_LIST_REQUEST,
  GET_REPAIR_LIST_SUCCESS,
  GET_REPAIR_LIST_FAILURE,
  GET_CAR_REPAIR_REQUEST,
  GET_CAR_REPAIR_SUCCESS,
  GET_CAR_REPAIR_FAILURE,
  GET_REPAIR_TYPE_LIST_REQUEST,
  GET_REPAIR_TYPE_LIST_SUCCESS,
  GET_REPAIR_TYPE_LIST_FAILURE,
  GET_SERVICE_PROVIDER_LIST_REQUEST,
  GET_SERVICE_PROVIDER_LIST_SUCCESS,
  GET_SERVICE_PROVIDER_LIST_FAILURE,
  CREATE_REPAIR_REQUEST,
  CREATE_REPAIR_SUCCESS,
  CREATE_REPAIR_FAILURE,
  FIX_REPAIR_REQUEST,
  FIX_REPAIR_SUCCESS,
  FIX_REPAIR_FAILURE,
  FIX_REPAIR_TARGET_REQUEST,
  FIX_REPAIR_TARGET_SUCCESS,
  FIX_REPAIR_TARGET_FAILURE,
  GET_ALERT_REQUEST,
  GET_ALERT_SUCCESS,
  GET_ALERT_FAILURE,
  RESET_CURRENTCAR,
  CREATE_CURRENTCAR_REPAIR_SUCCESS,
  CREATE_ALERT_REPAIR_SUCCESS,
  FIX_CURRENTCAR_REPAIR_SUCCESS,
  FIX_REPAIR_ALERT_SUCCESS,
  GET_REPAIR_ALERTES_LIST_REQUEST,
  GET_REPAIR_ALERTES_LIST_SUCCESS,
  GET_REPAIR_ALERTES_LIST_FAILURE,
  GET_TOGGLE_SNOW_SUMMER_REQUEST,
  GET_TOGGLE_SNOW_SUMMER_SUCCESS,
  GET_TOGGLE_SNOW_SUMMER_FAILURE

} from '../types'

const initialState = {
  repairList: null,
  repairAlertList: null,
  repairTypeList: null,
  serviceProviderList: null,
  alertCount: null,
  currentCarRepair: null,
};

export const repairReducer = (state = initialState, action) => {
  const {data,type,request,error} = action;

  switch (type) {
    case GET_REPAIR_LIST_REQUEST:
      return {
        ...state,
        repairListLoaded: false,
        lastRequest: request
      };

    case GET_REPAIR_LIST_SUCCESS:
      return {
        ...state,
        repairList: data,
        repairListLoaded: true,
      };

    case GET_REPAIR_LIST_FAILURE:
      return {
        ...state,
        repairListLoaded: false,
        error
      };

      case GET_REPAIR_TYPE_LIST_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_REPAIR_TYPE_LIST_SUCCESS:
      return {
        ...state,
        repairTypeList: data,
      };

    case GET_REPAIR_TYPE_LIST_FAILURE:
      return {
        ...state,
        error
      };
    case GET_CAR_REPAIR_REQUEST:
      return {
        ...state,
      };

    case GET_CAR_REPAIR_SUCCESS:
      return {
        ...state,
        currentCarRepair: data
      };

    case GET_CAR_REPAIR_FAILURE:
      return {
        ...state,
        currentCar: null,
        error
      };

    case GET_SERVICE_PROVIDER_LIST_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_SERVICE_PROVIDER_LIST_SUCCESS:
      return {
        ...state,
        serviceProviderList: data,
      };

    case GET_SERVICE_PROVIDER_LIST_FAILURE:
      return {
        ...state,
        error
      };

      case CREATE_REPAIR_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case CREATE_REPAIR_SUCCESS:
      return {
        ...state,
        repairList: [
          data,
          ...state.repairList,
        ]
      };


    case CREATE_ALERT_REPAIR_SUCCESS:
      return {
        ...state,
        repairList: [
          data,
          ...state.repairList,
        ]
      };

    case CREATE_REPAIR_FAILURE:
      return {
        ...state,
        error
      };

      case FIX_REPAIR_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case FIX_REPAIR_SUCCESS:
      return {
        ...state,
        repairList: [
          data,
          ...state.repairList.filter( (item, index) => item.repairId !== data.repairId),
        ],
      };

    case FIX_REPAIR_ALERT_SUCCESS:
      return {
        ...state,
        repairList: [
          data,
          ...state.repairAlertList.filter( (item, index) => item.repairId !== data.repairId),
        ],
      };

    case FIX_CURRENTCAR_REPAIR_SUCCESS:
      return {
        ...state,
        currentCarRepair: [
          data,
          ...state.currentCarRepair.filter( (item, index) => item.repairId !== data.repairId),
        ],
      };

    case FIX_REPAIR_FAILURE:
      return {
        ...state,
        error
      };

      case FIX_REPAIR_TARGET_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case FIX_REPAIR_TARGET_SUCCESS:
      return {
        ...state,
        currentCarRepair: [
          ...state.currentCarRepair.filter((item, index) => item.repairId !== data.repairId),
          data,
        ],
      };

    case FIX_REPAIR_TARGET_FAILURE:
      return {
        ...state,
        error
      };

      case GET_ALERT_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

      case GET_ALERT_SUCCESS:
        return {
          ...state,
          alertCount: data.inAlert
        };

      case GET_ALERT_FAILURE:
        return {
          ...state,
          error
        };

      case GET_REPAIR_ALERTES_LIST_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case RESET_CURRENTCAR:
      return {
        ...state,
        currentCarRepair: null,
      };

    case CREATE_CURRENTCAR_REPAIR_SUCCESS:
      return {
        ...state,
        currentCarRepair: [
          data,
          ...state.currentCarRepair,
        ]
      };

      case GET_REPAIR_ALERTES_LIST_SUCCESS:
        return {
          ...state,
          repairAlertList: data
        };

      case GET_REPAIR_ALERTES_LIST_FAILURE:
        return {
          ...state,
          error
        };
      case GET_TOGGLE_SNOW_SUMMER_REQUEST:
        return {
          ...state,
          lastRequest: request
        };
  
      case GET_TOGGLE_SNOW_SUMMER_SUCCESS:
          return {
            ...state,
          };
  
      case GET_TOGGLE_SNOW_SUMMER_FAILURE:
          return {
            ...state,
            error
          };

    default:
      return state;
  }
};

export default repairReducer;
