import {
  GET_BOT_ECODRIVER_REQUEST,
  GET_BOT_ECODRIVER_SUCCESS,
  GET_BOT_ECODRIVER_FAILURE,
  GET_TOP_ECODRIVER_REQUEST,
  GET_TOP_ECODRIVER_SUCCESS,
  GET_TOP_ECODRIVER_FAILURE,
  GET_LIST_ECODRIVER_REQUEST,
  GET_LIST_ECODRIVER_SUCCESS,
  GET_LIST_ECODRIVER_FAILURE,
  GET_ECODRIVER_SUCCESS,
  GET_ECODRIVER_REQUEST,
  GET_ECODRIVER_FAILURE,
  RESET_ECODRIVER,
} from "../types";

const initialState = {
  top: null,
  bot: null,
  ecoDriverList: null,
  currentEcoDriver: null,
  lastRequest: null,
  error: null,
};

export const ecoDrivingReducer = (state = initialState, action) => {
  const { data, type, request, error } = action;

  switch (type) {
    case GET_BOT_ECODRIVER_REQUEST:
      return {
        ...state,
        lastRequest: request,
      };
    case GET_BOT_ECODRIVER_SUCCESS:
      return {
        ...state,
        bot: data,
      };
    case GET_BOT_ECODRIVER_FAILURE:
      return {
        ...state,
        error,
      };

    case GET_TOP_ECODRIVER_REQUEST:
      return {
        ...state,
        lastRequest: request,
      };
    case GET_TOP_ECODRIVER_SUCCESS:
      return {
        ...state,
        top: data,
      };
    case GET_TOP_ECODRIVER_FAILURE:
      return {
        ...state,
        error,
      };

    case GET_LIST_ECODRIVER_REQUEST:
      return {
        ...state,
        lastRequest: request,
      };
    case GET_LIST_ECODRIVER_SUCCESS:
      return {
        ...state,
        ecoDriverList: data,
      };
    case GET_LIST_ECODRIVER_FAILURE:
      return {
        ...state,
        error,
      };
    case GET_ECODRIVER_REQUEST:
      return {
        ...state,
        lastRequest: request,
      };
    case GET_ECODRIVER_SUCCESS:
      return {
        ...state,
        currentEcoDriver: data,
      };
    case GET_ECODRIVER_FAILURE:
      return {
        ...state,
        error,
      };
    case RESET_ECODRIVER:
      return {
        ...state,
        currentEcoDriver: null,
      };
    default:
      return state;
  }
};

export default ecoDrivingReducer;
