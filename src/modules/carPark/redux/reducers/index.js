import {combineReducers} from 'redux';
import assignmentReducer from './assignmentReducer';
import repairReducer from './repairReducer';
import carReducer from './carReducer';
import ecoDrivingReducer from './ecoDrivingReducer';
import upCarReducer from './upCarReducer';

const carParkReducer = combineReducers({
  car: carReducer,
  assignment: assignmentReducer,
  repair: repairReducer,
  ecoDriving: ecoDrivingReducer,
  upCar: upCarReducer,
});

export default carParkReducer;
