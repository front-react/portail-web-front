import {
  CREATE_ASSIGNMENT_REQUEST,
  CREATE_ASSIGNMENT_SUCCESS,
  CREATE_ASSIGNMENT_FAILURE,
  RESTITUTION_REQUEST,
  RESTITUTION_SUCCESS,
  RESTITUTION_FAILURE,
  GET_CAR_AVAILABLE_REQUEST,
  GET_CAR_AVAILABLE_SUCCESS,
  GET_CAR_AVAILABLE_FAILURE,
  GET_ASSIGNMENT_LIST_REQUEST,
  GET_ASSIGNMENT_LIST_SUCCESS,
  GET_ASSIGNMENT_LIST_FAILURE,
  GET_PERSON_ASSIGNMENT_REQUEST,
  GET_PERSON_ASSIGNMENT_SUCCESS,
  GET_PERSON_ASSIGNMENT_FAILURE,
  CREATE_FILE_TEMPLATING_REQUEST,
  CREATE_FILE_TEMPLATING_SUCCESS,
  CREATE_FILE_TEMPLATING_FAILURE,
  RESET_ASSIGNMENT,
} from '../types'

const initialState = {
  assignmentList: null,
  personForAssignment: {
    person: null,
    assignment: null,
  },
  fileLoading: false,
  personForTransaction: {
    person: null,
    assignment: null,
  },
  lastRequest: null,
  carAvailable: null,
  error: null,
};

export const assignmentReducer = (state = initialState, action) => {
  const {data,type,request,error} = action;

  switch (type) {

    case GET_ASSIGNMENT_LIST_REQUEST:
      return {
        ...state,
        assignmentListLoaded: false,
        lastRequest: request
      };

    case GET_ASSIGNMENT_LIST_SUCCESS:
      return {
        ...state,
        assignmentList: data,
        assignmentListLoaded: true,
      };


    case GET_ASSIGNMENT_LIST_FAILURE:
      return {
        ...state,
        assignmentListLoaded: false,
        error
      };


    case GET_CAR_AVAILABLE_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_CAR_AVAILABLE_SUCCESS:
      return {
        ...state,
        carAvailable: data,
      };

    case GET_CAR_AVAILABLE_FAILURE:
      return {
        ...state,
        carListLoaded: false,
        error
      };

    case CREATE_ASSIGNMENT_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case CREATE_ASSIGNMENT_SUCCESS:
      return {
        ...state,
        [action.to]: {
          ...state[action.to],
          assignment: [
            ...state[action.to].assignment,
            data,
          ]
        },
        carAvailable: state.carAvailable.filter( (item, index) => item.carId !== data.carId)
      };

    case CREATE_ASSIGNMENT_FAILURE:
      return {
        ...state,
        assignmentListLoaded: false,
        error
      };

    case RESTITUTION_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case RESTITUTION_SUCCESS:
      return {
        ...state,
        [action.from]: {
          ...state[action.from],
          assignment:  state[action.from].assignment.filter( (item, index) => item.carId !== data.carId)
        },
        carAvailable: [
          ...state.carAvailable,
          data
        ]
      };

    case RESTITUTION_FAILURE:
      return {
        ...state,
        assignmentListLoaded: false,
        error
      };

    case GET_PERSON_ASSIGNMENT_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_PERSON_ASSIGNMENT_SUCCESS:
      return {
        ...state,
        ...data
      };

    case GET_PERSON_ASSIGNMENT_FAILURE:
      return {
        ...state,
        assignmentListLoaded: false,
        error
      };

      case CREATE_FILE_TEMPLATING_REQUEST:
      return {
        ...state,
        fileLoading: true,
        lastRequest: request
      };

      case CREATE_FILE_TEMPLATING_SUCCESS:
        return {
          ...state,
          fileLoading: false,
          createFile: data,
        };


      case CREATE_FILE_TEMPLATING_FAILURE:
        return {
          ...state,
          error
        };




    case RESET_ASSIGNMENT:
      return {
        ...state,
        assignmentList: null,
        personForAssignment: {
          person: null,
          assignment: null,
        },
        personForTransaction: {
          person: null,
          assignment: null,
        },
        carAvailable: null,
        filterAssignmentList: '',
      };

    default:
      return state;
  }
};

export default assignmentReducer;