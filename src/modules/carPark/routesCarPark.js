import React from 'react';
import {Route, IndexRedirect, IndexRoute} from 'react-router';
import {
  CarPark,
  DashBoardContainer,
  CarListContainer,
  CarProfileContainer,
  AssignmentListContainer,
  CarAssignmentContainer,
  RepairListContainer,
  RepairAlertListContainer,
  AddCarContainer,
  UpCarListContainer,
  EcoDrivingContainer,
  KmHistoryContainer,
  DelayUpcarsContainer
} from './containers';


export default(
  <Route name="Parc auto" path="parcauto" component={CarPark}>
    <IndexRedirect to="dashboard" />
    <Route name="Dashboard" path="dashboard" component={DashBoardContainer}/>
    <Route path="cars">
      <IndexRoute name="Liste des véhicules" component={CarListContainer} />
      <Route name="Ajouter un nouveau vehicule" path="add" component={AddCarContainer}/>
      <Route name="Fiche véhicule" path=":id" component={CarProfileContainer}/>
    </Route>
    <Route path="assignment">
      <IndexRoute name="liste des attributions" component={AssignmentListContainer} />
      <Route name="Attribution/Restitution" path="add" component={CarAssignmentContainer}/>
    </Route>
    <Route path="alertesRepairs">
      <IndexRoute name="liste des alertes en cours" component={RepairAlertListContainer} />
    </Route>
    <Route path="repairs">
      <IndexRoute name="liste des réparations" component={RepairListContainer} />
    </Route>
    <Route path="upcar">
      <IndexRoute name="liste des UPCARS à valider" component={UpCarListContainer} />
    </Route>
    <Route path="ecodriving">
      <IndexRoute name="Liste EcoDrinvings à afficher" component={EcoDrivingContainer} />
    </Route>
    <Route path="km-history">
      <Route name="Historique des UPCARS par véhicule" path=":id" component={KmHistoryContainer} />
    </Route>
    <Route path="retardsUpcar">
      <IndexRoute name="liste des retards Upcars" component={DelayUpcarsContainer}/>
    </Route>
  </Route>
)