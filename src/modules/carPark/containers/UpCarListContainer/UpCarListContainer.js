import React, {Component} from 'react'
import {connect} from 'react-redux';
import getUpCarList from '../../redux/actions/upcar/getUpCarList';
import patchUpCar from '../../redux/actions/upcar/patchUpCar';
import deleteUpCar from '../../redux/actions/upcar/deleteUpCar';
import { UpCarList } from '../../components';
import { Loader } from '../../../../components';

const mapStateToProps = (state) => {

  return {
    upCarList: state.carPark.upCar.upCarList,
    validationPersonId: state.auth.user.personId || state.auth.user.idPersonne,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUpCarList: (filter) => {dispatch(getUpCarList(filter))},
    patchUpCar: (id, statusId, validationPersonId, comments) => {dispatch(patchUpCar(id, statusId, validationPersonId, comments))},
    deleteUpCar: (id) => {dispatch(deleteUpCar(id))},
  }
};

class UpCarListContainer extends Component {

  state = {
    open: false,
    upCarIdForm: null,
  }

  componentDidMount(){
    this.props.getUpCarList('_filter(kmStatementStatusId-like)=%255%25&_limit=100000');
  }

  changeUpCarStatus = (upCarId, statusId) => {
    this.props.patchUpCar(upCarId, statusId, this.props.validationPersonId);
    this.setState({
      open: false,
    });
  };

  deleteUpCar = (upCarId) => {
    this.props.deleteUpCar(upCarId);
  };

  openDialog = (upCarIdDial) => {
    this.setState({
      open: true,
      upCarIdForm: upCarIdDial
    });
  };

  closeDialog = () => {
    this.setState({open: false});
  };

  handleFormSubmit = (formData) => {

    const comments = formData.comments;
    const statusIdForm = 4;
    const upCarIdState = this.state.upCarIdForm;
    this.props.patchUpCar(upCarIdState, statusIdForm, this.props.validationPersonId, comments);
  }
  render() {
    const { upCarList } = this.props;

    if(upCarList != null){
      return (
          <UpCarList 
          upCarList={upCarList} 
          changeUpCarStatus={this.changeUpCarStatus} 
          deleteUpCar={this.deleteUpCar} 
          dialogIsOpen={this.state.open}
          openDialog={this.openDialog}
          closeDialog={this.closeDialog}
          handleFormSubmit={this.handleFormSubmit}
          />
        );
    }
    return <Loader />
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(UpCarListContainer);