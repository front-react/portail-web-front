import React, {Component} from 'react'
import {connect} from 'react-redux';
import getAssignmentList from '../../redux/actions/assignment/getAssignmentList';
import { AssignmentList } from '../../components';
import { Loader } from '../../../../components';
import getReleasetransactionCarAndTemplating from '../../redux/actions/assignment/getReleasetransactionCarAndTemplating';


const mapStateToProps = (state) => {
  return {
    assignmentList: state.carPark.assignment.assignmentList,
    fileLoading: state.carPark.assignment.fileLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAssignmentList: (filter) => {dispatch(getAssignmentList(filter))},
    getReleasetransactionCarAndTemplating: (carid, nom, prenom, ville, restitution, attibution) => {dispatch(getReleasetransactionCarAndTemplating(carid, nom, prenom, ville, restitution, attibution))}
  }
};

class AssignmentListContainer extends Component {

  state = {
    attributionIdClick: null
  }


  componentDidMount(){
    this.props.getAssignmentList();
  }

  filterAssignmentList = (filterValue, field) => {
    this.props.getAssignmentList('_filter('+field+'-like)=%25'+filterValue+'%25&_limit=51')
  };

  getCurrentAsssignment = ( carId, nom, prenom, ville, restitution, attibution, attributionId) => {

      this.props.getReleasetransactionCarAndTemplating(carId, nom, prenom, ville, restitution, attibution );

    
    this.setState({
      attributionIdClick: attributionId
    });

    
  }
  
  render() {
    const { assignmentList, fileLoading } = this.props;

    if(assignmentList != null){
      return (<AssignmentList assignmentList={assignmentList} filterAssignmentList={this.filterAssignmentList} getCurrentAsssignment={this.getCurrentAsssignment} fileLoading={fileLoading} attributionIdClick={this.state.attributionIdClick} />);
    }
    return (
      <Loader />
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AssignmentListContainer);