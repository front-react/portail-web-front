import React, {Component} from 'react'
import {connect} from 'react-redux';
import getCar from '../../redux/actions/car/getCar';
import getCarRepairs from '../../redux/actions/car/getCarRepairs';
import resetCurrentCar from '../../redux/actions/car/resetCurrentCar';
import getToggleSnowSummer from '../../redux/actions/repair/getToggleSnowSummer';
import patchCar from '../../redux/actions/car/patchCar';
import { CarProfile } from '../../components';
import { Loader } from '../../../../components';
import moment from 'moment';

import createRepair from '../../redux/actions/repair/createRepair';

const mapStateToProps = (state) => {
  return {
    currentCar: state.carPark.car.currentCar,
    currentCarRepair: state.carPark.repair.currentCarRepair,
    loaderIn: state.carPark.car.loaderIn
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCar: (id) => {dispatch(getCar(id))},
    getCarRepairs: (id) => {dispatch(getCarRepairs(id))},
    patchCar: (data, id) => {dispatch(patchCar(data, id))},
    resetCurrentCar: () => {dispatch(resetCurrentCar())},
    getToggleSnowSummer: (carId, idTire) => {dispatch(getToggleSnowSummer(carId, idTire))},
    createRepair: (createBody, repairId, dataLocation) => {dispatch(createRepair(createBody, repairId, dataLocation))},
  }
};

class CarProfileContainer extends Component {

  state = {
    dialogIsOpen: false,
    repairType: null,
    togglePneu: null,
    togglePneuArriere: false,
    togglePneuAvant: false,
    toggleParrHiver: false,
    togglePavHiver: false,
    toggleDisqueAvant: false,
    toggleMachoireArr: false,
    toggleFreinAvant: false
  };

  componentDidMount(){
    const carId = this.props.params.id;
    this.props.getCar(carId);
    this.props.getCarRepairs(carId);
  }

  componentWillUnmount(){
    this.props.resetCurrentCar()
  }

  handleFormSubmit = (formData) => {
    formData.outDate = moment().format('DD-MM-YYYY');
    this.props.patchCar(formData, this.props.currentCar.carId);
    this.setState({dialogIsOpen: false});
  };

  openDialog = () => {
    this.setState({dialogIsOpen: !this.state.dialogIsOpen});
  };

  InitRepairType = (type) => {
    this.setState({
      repairType: type,
      togglePneu: this.props.currentCar.withSnowTire
    })
  }

  intialisationReparation = (repairId, repairInfo, car) => {
      // on initialise la réparation dans les autres cas
      if(repairId !== "2" || repairId !== "10"){
        const createBody = {
          repairTypeId: repairId,
          carId: car.carId,
          counterKm: "0",
          counterReferenceKm: car.currentKm
        };
        const dataLocation = "currentCarRepair";
        this.props.createRepair(createBody, dataLocation);
      }
      else{
        // on initialise la réparation dans le cas d'un contôle technique et d'un contrôle anti-pollution
        const createBody = {
          repairTypeId: repairId,
          carId: car.carId,
          counterDays: "0",
          counterDaysReference: moment().format("YYYY-MM-DD")
        };
        const dataLocation = "currentCarRepair";
        this.props.createRepair(createBody, dataLocation);
      } 
  }

  toggleSnowSummer = () => {
    if(this.state.togglePneu === "0"){
      this.setState({togglePneu: "1"}); // on passe en hiver
      const carId = this.props.params.id;
      this.props.getToggleSnowSummer(carId, 1);
    }
    else{
      this.setState({togglePneu: "0"}); // on passe en été
      const carId = this.props.params.id;
      this.props.getToggleSnowSummer(carId, 0);
    }
  }

  onExpandChange = (newExpandedState, repairTypeId) => {

    switch(repairTypeId) 
    {
      case "6":
        this.setState({
          togglePneuArriere: false,
          togglePneuAvant: newExpandedState,
          toggleParrHiver: false,
          togglePavHiver: false,
        })
        break;
      case "7":
        this.setState({
          togglePneuArriere: newExpandedState,
          togglePneuAvant: false,
          toggleParrHiver: false,
          togglePavHiver: false,
        })
        break;
      case "8":
        this.setState({
          togglePneuArriere: false,
          togglePneuAvant: false,
          toggleParrHiver: false,
          togglePavHiver: newExpandedState,
        })
        break;
      case "9":
        this.setState({
          togglePneuArriere: false,
          togglePneuAvant: false,
          toggleParrHiver: newExpandedState,
          togglePavHiver: false,
        })
        break;
      case "3":
        this.setState({
          toggleDisqueAvant: false,
          toggleMachoireArr: false,
          toggleFreinAvant: newExpandedState
        })
        break;
      case "4":
        this.setState({
          toggleDisqueAvant: false,
          toggleMachoireArr: newExpandedState,
          toggleFreinAvant: false
        })
        break;
      case "5":
        this.setState({
          toggleDisqueAvant: newExpandedState,
          toggleMachoireArr: false,
          toggleFreinAvant: false
        })
        break;
      default:
        this.setState({
          togglePneuArriere: false,
          togglePneuAvant: false,
          toggleParrHiver: false,
          togglePavHiver: false,
          toggleDisqueAvant: false,
          toggleMachoireArr: false,
          toggleFreinAvant: false
        })
    }
  }


  render() {
    const {currentCar, currentCarRepair, loaderIn} = this.props;
    
    const {dialogIsOpen} = this.state;

    if(currentCar  !== null && currentCarRepair  !== null){
      
      return (
        <span>
          <CarProfile
            car={currentCar}
            repair={currentCarRepair}
            openDialog={this.openDialog}
            handleFormSubmit={this.handleFormSubmit}
            dialogIsOpen={dialogIsOpen}
            repairType={this.state.repairType}
            InitRepairType={this.InitRepairType}
            intialisationReparation={this.intialisationReparation}
            toggleSnowSummer={this.toggleSnowSummer}
            togglePneu={this.state.togglePneu}
            onExpandChange={this.onExpandChange}
            togglePneuArriere={this.state.togglePneuArriere}
            togglePneuAvant={this.state.togglePneuAvant}
            toggleParrHiver={this.state.toggleParrHiver}
            togglePavHiver={this.state.togglePavHiver}
            toggleDisqueAvant={this.state.toggleDisqueAvant}
            toggleMachoireArr={this.state.toggleMachoireArr}
            toggleFreinAvant={this.state.toggleFreinAvant}
            loaderIn={loaderIn}
          />
        </span>
      );
    }

    return <Loader />
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(CarProfileContainer);