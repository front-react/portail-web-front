import React, {Component} from 'react';
import {connect} from 'react-redux';
import { AddRepair } from '../../components';
import getCarList from '../../redux/actions/car/getCarList';
import getRepairTypeList from '../../redux/actions/repair/getRepairTypeList';
import getServiceProviderList from '../../redux/actions/repair/getServiceProviderList';
import getPersonList from '../../../rh/redux/actions/person/getPersonList';
import fixThenCreateRepair from '../../redux/actions/repair/fixThenCreateRepair';
import createRepair from '../../redux/actions/repair/createRepair';
import moment from 'moment';

import getRepairAlertList from '../../redux/actions/repair/getRepairAlertList';

const mapStateToProps = (state) => {
  return {
    carList: state.carPark.car.carList,
    personList: state.rh.person.personList,
    repairTypeList: state.carPark.repair.repairTypeList,
    serviceProviderList: state.carPark.repair.serviceProviderList,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCarList: (filter) => {dispatch(getCarList(filter))},
    getRepairTypeList: () => {dispatch(getRepairTypeList())},
    getServiceProviderList: (filter) => {dispatch(getServiceProviderList(filter))},
    getPersonList: (filter) => {dispatch(getPersonList(filter))},
    createRepair: (createBody, dataLocation) => {dispatch(createRepair(createBody, dataLocation))},
    fixThenCreateRepair: (fixBody, createBody, repairId, carId, repairTypeId, dataLocation) => {
      dispatch(fixThenCreateRepair(fixBody, createBody, repairId, carId, repairTypeId, dataLocation))
    },
    getRepairAlertList: (filter) => {dispatch(getRepairAlertList(filter))},
  }
};

class AddRepairContainer extends Component {

    state = {
      open: false,
    };

  componentDidMount(){
    if(this.props.serviceProviderList === null){
      this.props.getServiceProviderList('_limit=5');
    }
    if(this.props.carList === null){
      this.props.getCarList('_limit=5');
    }
    if(this.props.personList === null){
      this.props.getPersonList('_limit=5');
    }
    if(this.props.repairTypeList === null){
      this.props.getRepairTypeList();
    }
  }

  handleFormSubmit = (formData) => {

    const currentKm = this.props.currentKm
    const counterKm = currentKm - formData.repairKm
    //On crée une réparation dans le cas autre 
    if(formData.repairTypeId === "12"){
      const createBody = {
        repairTypeId: formData.repairTypeId,
        repairStatusId: "5",
        carId: formData.carId,
        repairDate: moment(formData.repairDate).format('DD-MM-YYYY'),
        repairKm: formData.repairKm,
        counterKm: counterKm,
        lastControlKm: formData.repairKm,
        personId: formData.personId,
        lastControlDate: moment(formData.repairDate).format('DD-MM-YYYY'),
        serviceProviderId: formData.serviceProviderId,
        comments: formData.comments,
        dtPrice: formData.dtPrice,
        vat: formData.dtPrice/100*20
      };
      this.props.createRepair(createBody, this.props.dataLocation);
      this.props.getRepairAlertList('_filter(repairStatusId-like)=%252%25');
      
    }
    else{
      if(formData.repairTypeId === '2' || formData.repairTypeId === '10'){
        //initialiser la reparation en gérant le cas des dates contôle technique et contrôle anti-pollution
        const currentDay = moment().format('YYYY-MM-DD');
        const lastControlDate = moment(formData.repairDate).format('YYYY-MM-DD');
    
        const date1 = new Date(currentDay);
        const date2 = new Date(lastControlDate);
    
        let timeDiff = date1.getTime() - date2.getTime();
        let counterDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

        const fixBody = {
          repairDate: moment(formData.repairDate).format('DD-MM-YYYY'),
          repairKm: formData.repairKm,
          counterDays: counterDays,
          personId: formData.personId,
          serviceProviderId: formData.serviceProviderId,
          comments: formData.comments,
          dtPrice: formData.dtPrice,
          vat: formData.dtPrice/100*20
        };
        // initialiser les réparation dans le cas de réparation sur les km
        const createBody = {
          repairTypeId: formData.repairTypeId,
          carId: formData.carId,
          counterDays: counterDays,
          lastControlDate: moment(formData.repairDate).format('DD-MM-YYYY'),
          lastControlKm: formData.repairKm,
          counterDaysReference: moment().format("YYYY-MM-DD") //la date d'aujourd'hui
        };
        this.props.fixThenCreateRepair(fixBody, createBody, this.props.repairId.repairId, formData.carId, formData.repairTypeId, this.props.dataLocation);
        this.props.getRepairAlertList('_filter(repairStatusId-like)=%252%25');
      }
      else{

        const fixBody = {
          repairDate: moment(formData.repairDate).format('DD-MM-YYYY'),
          repairKm: formData.repairKm,
          personId: formData.personId,
          serviceProviderId: formData.serviceProviderId,
          comments: formData.comments,
          dtPrice: formData.dtPrice,
          vat: formData.dtPrice/100*20
        };
        // initialiser les réparation dans le cas de réparation sur les km
        const createBody = {
          repairTypeId: formData.repairTypeId,
          carId: formData.carId,
          counterKm: counterKm, 
          lastControlDate: moment(formData.repairDate).format('DD-MM-YYYY'),
          lastControlKm: formData.repairKm,
          counterReferenceKm: currentKm
        };
        this.props.fixThenCreateRepair(fixBody, createBody, this.props.repairId.repairId, formData.carId, formData.repairTypeId, this.props.dataLocation);
        this.props.getRepairAlertList('_filter(repairStatusId-like)=%252%25');
      }
      

      
    }
    
   this.setState({open: false});
  };

  openDialog = () => {

    this.setState({open: true});
  };

  closeDialog = () => {
    this.setState({open: false});
  };

  render() {
    return <AddRepair
        handleFormSubmit={this.handleFormSubmit}
        openDialog={this.openDialog}
        closeDialog={this.closeDialog}
        dialogIsOpen={this.state.open}
        getCarList={this.props.getCarList}
        getServiceProviderList={this.props.getServiceProviderList}
        getPersonList={this.props.getPersonList}
        carList={this.props.carList}
        personList={this.props.personList}
        repairTypeList={this.props.repairTypeList}
        serviceProviderList={this.props.serviceProviderList}
        carId={this.props.carId || null}
        carName={this.props.carName || null}
        personId={this.props.personId || null}
        personName={this.props.personName || null}
        repairTypeId={this.props.repairTypeId || null}
      />
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddRepairContainer);
