import React, {Component} from 'react';
import {connect} from 'react-redux';
import { FixRepairTarget } from '../../components';
import fixRepairTarget from '../../redux/actions/repair/fixRepairTarget';
import getCarRepairs from '../../redux/actions/car/getCarRepairs';
import moment from 'moment'

const mapDispatchToProps = (dispatch) => {
  return {
    fixRepairTarget: (repairId, bodyData) => {dispatch(fixRepairTarget(repairId, bodyData))},
    getCarRepairs: (id) => {dispatch(getCarRepairs(id))},
  }
};

class FixRepairTargetContainer extends Component {
  state = {
    open: false,
  };

  //Action pour Décaler une réparation
  handleFormSubmit = (formData, repairId) => {

    const { repair, currentKm } = this.props;

    const counterKm = currentKm - formData.lastControlKm;
    const targetKm = formData.addToTargetKm;
    
    const currentDay = moment().format('YYYY-MM-DD');
    const lastControlDate = moment(formData.lastControlDate).format('YYYY-MM-DD');

    const date1 = new Date(currentDay);
    const date2 = new Date(lastControlDate);

    let timeDiff = date1.getTime() - date2.getTime();
    let counterDays = Math.ceil(timeDiff / (1000 * 3600 * 24));


    //const counterDays = currentDay - lastControlDay;
    const targetDays = formData.addToTargetDays;
    // Décale une réparation dans les autres cas
    if(repairId === "2" || repairId === "10"){
      const bodyData = {
        lastControlDate:  moment(formData.lastControlDate).format('DD-MM-YYYY'),
        targetDays: targetDays || null,
        counterDays: counterDays || null,
        counterDaysReference: moment().format('DD-MM-YYYY'),
        lastControlKm: formData.lastControlKm,
        comments: formData.comments
      };
      this.props.fixRepairTarget(repair.repairId, bodyData);
      this.setState({open: false});
    }
    else{
      // Décale une réparation dans les autres contrôle technique et contrôle anti-pollution
      const bodyData = {
        lastControlDate: moment(formData.lastControlDate).format('DD-MM-YYYY'),
        lastControlKm: formData.lastControlKm,
        targetKm: targetKm || null,
        counterKm: counterKm || null,
        counterReferenceKm: currentKm,
        comments: formData.comments
      };
      
      this.props.fixRepairTarget(repair.repairId, bodyData);
      this.setState({open: false});
    }
  };

  openDialog = () => {
    this.setState({open: true});
    this.props.getCarRepairs(this.props.repair.carId);
  };

  closeDialog = () => {
    this.setState({open: false});
  };

  render() {
    return <FixRepairTarget
      handleFormSubmit={this.handleFormSubmit}
      openDialog={this.openDialog}
      closeDialog={this.closeDialog}
      dialogIsOpen={this.state.open}
      repair={this.props.repair}
    />
  }
}


export default connect(null, mapDispatchToProps)(FixRepairTargetContainer);