import React, { Component } from "react";
import { connect } from "react-redux";
import getListEcodriver from "../../redux/actions/ecodriving/getListEcodriver";
import { getEcodriver } from "../../redux/actions/ecodriving/getEcodriver";
import { resetEcodriver } from "../../redux/actions/ecodriving/getEcodriver";
import { CarInfoEcoDriving } from "../../components";

const mapStateToProps = state => {
  return {
    ecoDriverList: state.carPark.ecoDriving.ecoDriverList,
    currentEcoDriver: state.carPark.ecoDriving.currentEcoDriver,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getListEcodriver: filter => {
      dispatch(getListEcodriver(filter));
    },
    getEcodriver: filter => {
      dispatch(getEcodriver(filter));
    },
    resetEcodriver: filter => {
      dispatch(resetEcodriver(filter));
    },
  };
};

class CarInfoEcoDrivingContainer extends Component {
  componentDidMount() {
    const field = "carId";
    const filterValue = this.props.carId;
    this.props.getListEcodriver();
    this.props.getEcodriver(
      "_filter(" + field + "-like)=%25" + filterValue + "%25",
    );
  }

  componentWillUnmount() {
    this.props.resetEcodriver();
  }

  render() {
    const { ecoDriverList, currentEcoDriver } = this.props;
    if (ecoDriverList !== null && currentEcoDriver !== null) {
      if (currentEcoDriver !== undefined) {
        const { ecoDrivingNote, classement } = currentEcoDriver;
        const totalEcoDriver = ecoDriverList.length;
        return (
          <CarInfoEcoDriving
            classement={classement}
            ecoDrivingNote={ecoDrivingNote}
            totalEcoDriver={totalEcoDriver}
          />
        );
      }
    }
    return null;
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(
  CarInfoEcoDrivingContainer,
);
