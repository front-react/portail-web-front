import React, {Component} from 'react';
import {connect} from 'react-redux';
import getCarStats from '../../redux/actions/car/getCarStats';
import getBotEcodriver from '../../redux/actions/ecodriving/getBotEcodriver';
import getTopEcodriver from '../../redux/actions/ecodriving/getTopEcodriver';
import getAlert from '../../redux/actions/repair/getAlert';
import { DashBoard } from '../../components';
import { Loader } from '../../../../components';
import getCarAvailable from '../../redux/actions/car/getCarAvailable';
import getUpCarStats from '../../redux/actions/upcar/getUpCarStats';
import getRetardsUpcars from '../../redux/actions/upcar/getRetardsUpcars';

const mapStateToProps = (state) => {
  return {
    carAvailable: state.carPark.assignment.carAvailable,
    carStats: state.carPark.car.carStats,
    carStatsLoaded: state.carPark.car.carStatsLoaded,
    topEcodriver: state.carPark.ecoDriving.top,
    botEcodriver: state.carPark.ecoDriving.bot,
    alertCount: state.carPark.repair.alertCount,
    upCarStats: state.carPark.upCar.upCarStats,
    retardsUpcars: state.carPark.upCar.retardsUpcars,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCarStats: () => {dispatch(getCarStats())},
    getBotEcodriver: () => {dispatch(getBotEcodriver())},
    getTopEcodriver: () => {dispatch(getTopEcodriver())},
    getAlert: () => {dispatch(getAlert())},
    getCarAvailable: () => {dispatch(getCarAvailable())},
    getUpCarStats: () => {dispatch(getUpCarStats())},
    getRetardsUpcars: (id) => {dispatch(getRetardsUpcars(id))},
  }
};


class DashBoardContainer extends Component {

  componentDidMount() {
    this.props.getCarStats();
    this.props.getBotEcodriver();
    this.props.getTopEcodriver();
    this.props.getAlert();
    this.props.getCarAvailable();
    this.props.getUpCarStats();
    this.props.getRetardsUpcars();
  };


  render() {
    const { carStatsLoaded, carStats, topEcodriver, botEcodriver, alertCount, carAvailable, upCarStats, retardsUpcars} = this.props;
    if(carStatsLoaded && botEcodriver != null && topEcodriver != null && alertCount != null && carAvailable != null && upCarStats != null && retardsUpcars != null){
      return <DashBoard
        carAvailable={carAvailable}
        carStats={carStats}
        topEcodriver={topEcodriver}
        botEcodriver={botEcodriver}
        alertCount={alertCount}
        upCarStats={upCarStats}
        retardsUpcars={retardsUpcars}
      />
    }
    return (
      <Loader />
    )
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(DashBoardContainer);