import React, {Component} from 'react'
import {connect} from 'react-redux';
import getCarList from '../../redux/actions/car/getCarList';
import { CarList } from '../../components';
import { Loader } from '../../../../components';

const mapStateToProps = (state) => {

  return {
    carList: state.carPark.car.carList,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCarList: (filter) => {dispatch(getCarList(filter))},
  }
};

class CarListContainer extends Component {

  componentDidMount(){
    this.props.getCarList('_limit=50');
  }

  filterCarList = (filterValue, field) => {
    this.props.getCarList('_filter('+field+'-like)=%25'+filterValue+'%25&_limit=50')
  };

  render() {
    const { carList } = this.props;

    if(carList != null){
      return (<CarList carList={carList} filterCarList={this.filterCarList} />);
    }
    return <Loader />
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(CarListContainer);