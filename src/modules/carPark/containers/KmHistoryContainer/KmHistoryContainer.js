import React, {Component} from 'react';
import {connect} from 'react-redux';

import { Loader } from '../../../../components';

import getkmstatementsHistory from '../../redux/actions/car/getkmstatementsHistory';
import {KmHistory} from '../../components';


const mapStateToProps = (state) => {
  return {
    kmHistory: state.carPark.car.kmHistory
  }
};


const mapDispatchToProps = (dispatch) => {
  return {
    getkmstatementsHistory: (filter) => {dispatch(getkmstatementsHistory(filter))}
  }
};

class KmHistoryContainer extends Component {

  componentDidMount(){
    this.props.getkmstatementsHistory(`_filter(carId-%3D)=${this.props.params.id}`);
  }

  render() {
    const {kmHistory} = this.props;

    
    if(kmHistory !== null){
      return( <KmHistory kmHistory={kmHistory} /> )
    }
    return (<Loader />)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(KmHistoryContainer);