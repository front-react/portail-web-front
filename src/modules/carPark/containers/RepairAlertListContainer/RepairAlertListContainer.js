import React, {Component} from 'react'
import {connect} from 'react-redux';

import { Loader } from '../../../../components';

import getRepairAlertList from '../../redux/actions/repair/getRepairAlertList';
import { RepairAlertList } from '../../components';

import getCarList from '../../redux/actions/car/getCarList';
import getRepairTypeList from '../../redux/actions/repair/getRepairTypeList';
import getServiceProviderList from '../../redux/actions/repair/getServiceProviderList';
import getPersonList from '../../../rh/redux/actions/person/getPersonList';


const mapStateToProps = (state) => {
  return {
    repairAlertList: state.carPark.repair.repairAlertList,
    carList: state.carPark.car.carList,
    personList: state.rh.person.personList,
    repairTypeList: state.carPark.repair.repairTypeList,
    serviceProviderList: state.carPark.repair.serviceProviderList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRepairAlertList: (filter) => {dispatch(getRepairAlertList(filter))},
    getCarList: (filter) => {dispatch(getCarList(filter))},
    getRepairTypeList: () => {dispatch(getRepairTypeList())},
    getServiceProviderList: (filter) => {dispatch(getServiceProviderList(filter))},
    getPersonList: (filter) => {dispatch(getPersonList(filter))},
  }
};

class RepairAlertListContainer extends Component {

  componentDidMount(){
    this.props.getRepairAlertList('_filter(repairStatusId-like)=%252%25');
    this.props.getServiceProviderList('_limit=5');
    this.props.getCarList('_limit=5');
    this.props.getPersonList('_limit=5');
    this.props.getRepairTypeList(); 
  }

  filterAlertList = (filterValue, field) => {
    this.props.getRepairAlertList('_filter('+field+'-like)=%25'+filterValue+'%25')
  };
  
  render() {
      const { repairAlertList, carList, personList, repairTypeList, serviceProviderList } = this.props;
      if(repairAlertList != null && carList != null && personList != null && repairTypeList != null && serviceProviderList != null){
          return(
              <RepairAlertList 
                repairAlertList={repairAlertList} 
                filterAlertList={this.filterAlertList}
              />
          );
      }
      else{
        return(
            <Loader />
        );
      }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(RepairAlertListContainer);