import React, {Component} from 'react';
import {connect} from 'react-redux';
import getCarAvailable from '../../redux/actions/car/getCarAvailable';
import createAssignment from '../../redux/actions/assignment/createAssignment';
import getPersonAssignment from '../../redux/actions/assignment/getPersonAssignment';
import getPersonList from '../../../rh/redux/actions/person/getPersonList';
import resetAssignment from '../../redux/actions/assignment/resetAssignment';
import restitution from '../../redux/actions/assignment/restitution';
import transaction from '../../redux/actions/assignment/transaction';
import { CarAssignment } from '../../components';
import moment from 'moment';
import { Loader } from '../../../../components';

const mapStateToProps = (state) => {
  return {
    carAvailable: state.carPark.assignment.carAvailable,
    personForAssignment: state.carPark.assignment.personForAssignment,
    personForTransaction: state.carPark.assignment.personForTransaction,
    personList: state.rh.person.personList
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCarAvailable: () => {dispatch(getCarAvailable())},
    createAssignment: (assignmentBody, to) => {dispatch(createAssignment(assignmentBody, to))},
    getPersonList: (filter) => {dispatch(getPersonList(filter))},
    getPersonAssignment: (person, typePerson) => {dispatch(getPersonAssignment(person, typePerson))},
    restitution: (restitutionBody, attributionId,  from) => {dispatch(restitution(restitutionBody, attributionId, from))},
    transaction: (restitutionBody, attributionId, from, assignmentBody, to) => {
      dispatch(transaction(restitutionBody, attributionId, from, assignmentBody, to))
    },
    resetAssignment: () => {dispatch(resetAssignment())},
  }
};

class CarAssignmentContainer extends Component {
  state = {
    open: false,
    carId: null,
    assignmentType: null,
    to: null,
    from: null,
  };

  componentDidMount() {
    this.props.getPersonList('_limit=5');
    this.props.getCarAvailable();
  };

  componentWillUnmount() {
    this.props.resetAssignment();
  };

  searchPerson = (personId) => {
    this.props.getPersonList('_filter(personFullName-like)=%25'+personId+'%25&_limit=5')
  };

  searchCarPerson = (personId, typePerson) => {
    this.props.getPersonAssignment(personId, typePerson)
  };

  handleFormSubmit = (formData) => {
    const { assignmentType, to, from } =   this.state;

    if(assignmentType === 'assignment') {
      const assignmentBody = {
        carId: formData.carId,
        personId: formData.personId,
        attributionDate: moment(formData.attributionDate).format('DD-MM-YYYY'),
        attributionPersonId: formData.connectedPersonId
      };
      this.props.createAssignment(assignmentBody, to)
    }
    else if(assignmentType === 'restitution'){
      const attribution = this.props[from].assignment.filter((attribution) =>{
        return attribution.carId === formData.carId
      });
      const restitutionBody = {
        restitutionDate: moment(formData.restitutionDate).format('DD-MM-YYYY'),
        restitutionPersonId: formData.connectedPersonId
      };
      this.props.restitution(restitutionBody, attribution[0].attributionId, from)
    }
    else if(assignmentType === 'transaction'){
      const attribution = this.props[from].assignment.filter((attribution) =>{
        return attribution.carId === formData.carId
      });
      const restitutionBody = {
        restitutionDate: moment(formData.restitutionDate).format('DD-MM-YYYY'),
        restitutionPersonId: formData.connectedPersonId
      };
      const assignmentBody = {
        carId: formData.carId,
        personId: formData.personId,
        attributionDate: moment(formData.attributionDate).format('DD-MM-YYYY'),
        attributionPersonId: formData.connectedPersonId
      };

      this.props.transaction(restitutionBody, attribution[0].attributionId, from, assignmentBody, to)
    }

    this.setState({carId: null});
    this.setState({assignmentType: null});
    this.setState({open: false});
  };

  openDialog = (carId, event) => {
    const { from, to } = event;
    if(from === 'carAvailable'){
      this.setState({assignmentType: 'assignment'});
    }
    else if(to === 'carAvailable'){
      this.setState({assignmentType: 'restitution'});
    }
    else{
      this.setState({assignmentType: 'transaction'});
    }


    if(to !== 'carAvailable' && from !== 'carAvailable'){
      this.setState({to: to});
      this.setState({from: from});
    }
    else{
      this.setState({to: 'personForAssignment'});
      this.setState({from: 'personForAssignment'});
    }

    this.setState({open: true});
    this.setState({carId: carId});
  };

  closeDialog = () => {
    this.setState({open: false});
  };

  render() {
    const { carAvailable, personForAssignment, personList, personForTransaction} = this.props;

    if(carAvailable != null){
      return <CarAssignment
        carAvailable={carAvailable}
        searchPerson={this.searchPerson}
        searchCarPerson={this.searchCarPerson}
        handleFormSubmit={this.handleFormSubmit}
        personList={personList}
        personForAssignment={personForAssignment}
        personForTransaction={personForTransaction}
        openDialog={this.openDialog}
        closeDialog={this.closeDialog}
        dialogIsOpen={this.state.open}
        assignmentType={this.state.assignmentType}
        to={this.state.to}
        from={this.state.from}
        carId={this.state.carId}
      />
    }
    return (
      <Loader />
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CarAssignmentContainer);