import React, { Component } from "react";
import { connect } from "react-redux";
import { AddCar } from "../../components";
import getCarBrand from "../../redux/actions/car/getCarBrand";
import getCarModel from "../../redux/actions/car/getCarModel";
import addCar from "../../redux/actions/car/addCar";
import { Loader } from "../../../../components";
//import moment from 'moment'
import { Step, Stepper, StepLabel } from "material-ui/Stepper";
//import RaisedButton from "material-ui/RaisedButton";
//import FlatButton from "material-ui/FlatButton";
import AddButtonRedirect from "../../components/AddButtonRedirect";

const mapStateToProps = state => {
  return {
    modelList: state.carPark.car.model,
    brandList: state.carPark.car.brand,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCarModel: () => {
      dispatch(getCarModel());
    },
    getCarBrand: () => {
      dispatch(getCarBrand());
    },
    addCar: body => {
      dispatch(addCar(body));
    },
  };
};

class AddCarContainer extends Component {
  state = {
    finished: false,
    stepIndex: 0,
  };

  componentDidMount() {
    this.props.getCarBrand();
    this.props.getCarModel();
  }

  handleFormSubmit = formData => {
    //formData.purchaseDate = moment(formData.purchaseDate).format('DD-MM-YYYY');
    //formData.circulationDate = moment(formData.circulationDate).format('DD-MM-YYYY');
    formData.currentKm = formData.purchaseKm;
    this.props.addCar(formData);
    this.setState({ stepIndex: 1 });
  };

  handleNext = () => {
    const { stepIndex } = this.state;
    this.setState({
      stepIndex: stepIndex + 1,
      finished: stepIndex >= 2,
    });
  };

  handlePrev = () => {
    const { stepIndex } = this.state;
    if (stepIndex > 0) {
      this.setState({ stepIndex: stepIndex - 1 });
    }
  };

  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <AddCar
            handleFormSubmit={this.handleFormSubmit}
            brandList={this.props.brandList}
            modelList={this.props.modelList}
          />
        );
      case 1:
        return <AddButtonRedirect />;
      case 2:
        return "This is the bit I really care about!";
      default:
        return "You're a long way from home sonny jim!";
    }
  }

  render() {
    const { brandList, modelList } = this.props;
    const { stepIndex } = this.state;

    if (brandList !== null && modelList !== null) {
      return (
        <div style={{ width: "100%", maxWidth: 700, margin: "auto" }}>
          <Stepper activeStep={stepIndex}>
            <Step>
              <StepLabel>Select campaign settings</StepLabel>
            </Step>
            <Step>
              <StepLabel>Create an ad group</StepLabel>
            </Step>
            <Step>
              <StepLabel>Create an ad</StepLabel>
            </Step>
          </Stepper>
          <div>
            <div>
              <div>
                {this.getStepContent(stepIndex)}
              </div>
              {/* <div style={{ marginTop: 12 }}>
                <FlatButton
                  label="Back"
                  disabled={stepIndex === 0}
                  onTouchTap={this.handlePrev}
                  style={{ marginRight: 12 }}
                />
                <RaisedButton
                  label={stepIndex === 2 ? "Finish" : "Next"}
                  primary={true}
                  onTouchTap={this.handleNext}
                />
              </div> */}
            </div>
          </div>
        </div>
      );
    }
    return <Loader />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCarContainer);
