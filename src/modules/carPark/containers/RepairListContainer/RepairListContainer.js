import React, {Component} from 'react'
import {connect} from 'react-redux';
import getRepairList from '../../redux/actions/repair/getRepairList';
import { RepairList } from '../../components';
import { Loader } from '../../../../components';


const mapStateToProps = (state) => {
  return {
    repairList: state.carPark.repair.repairList,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRepairList: (filter) => {dispatch(getRepairList(filter))},
  }
};

class RepairListContainer extends Component {



  componentDidMount(){
    this.props.getRepairList();
  };


  filterRepairList = (filterValue, field) => {
    this.props.getRepairList('_filter('+field+'-like)=%25'+filterValue+'%25&_limit=50')
  };

  render() {
    const { repairList } = this.props;
    if(repairList != null ){
      return (<RepairList
        repairList={repairList}
        filterRepairList={this.filterRepairList}
      />);
    }
    return (
      <Loader />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RepairListContainer);