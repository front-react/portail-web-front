import React, {Component} from 'react';
import {connect} from 'react-redux'
import Paper from 'material-ui/Paper';

const AlertRouting = {
  boxAlert: {
    width: '100%',
    height: '600px',
    display: 'flex',
    backgroundColor: 'red'
  },
  textAlert: {
    width: '100%',
    color: '#ffffff',
    fontWeigth:'700',
    fontSize: '30px',
    textAlign: 'center',
    paddingTop: '50px',
  }
    
};

const mapStateToProps = (state) => {
  return {
    user: state.auth.user,
  }
};

class CarPark extends Component {

  render() {
    const { user } = this.props;

    const carPark = user.applications.find((application) => application.applicationId === "145");

    return (
      <div>
        {carPark 
          ? <span>{this.props.children}</span>
          : 
          <Paper 
            style={AlertRouting.boxAlert}
          >
            <p style={AlertRouting.textAlert}>Vous me pouvez pas accèder au CarPark</p>
          </Paper>
        }
      </div>
    );
  }
}
export default connect(mapStateToProps, null)(CarPark)