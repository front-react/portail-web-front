import React, {Component} from 'react'
import {connect} from 'react-redux';
import getRetardsUpcars from '../../redux/actions/upcar/getRetardsUpcars';
import { DelayUpcars } from '../../components';
import { Loader } from '../../../../components';

const mapStateToProps = (state) => {

  return {
    retardsUpcars: state.carPark.upCar.retardsUpcars,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getRetardsUpcars: (id) => {dispatch(getRetardsUpcars(id))},
  }
};

class DelayUpcarsContainer extends Component {

  componentDidMount(){
    this.props.getRetardsUpcars();
  }
  state = {
    upCarDate: 7
  }

  onDragStop = (event, value) => {
    this.setState({upCarDate: value});
    this.props.getRetardsUpcars(value)
  };

  handleSlider = (event, value) => {
    this.setState({upCarDate: value});
  }

  render() {
    const { retardsUpcars } = this.props;

    if(retardsUpcars !== null){
      return (
      <DelayUpcars 
        retardsUpcars={retardsUpcars} 
        onDragStop={this.onDragStop} 
        handleSlider={this.handleSlider}
        currentUpcarDate={this.state.upCarDate}
      />
      );
    }
    return <Loader />
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(DelayUpcarsContainer);