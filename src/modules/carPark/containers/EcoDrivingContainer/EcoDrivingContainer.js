import React, {Component} from 'react';
import {connect} from 'react-redux';

import { Loader } from '../../../../components';

import getListEcodriver from '../../redux/actions/ecodriving/getListEcodriver';
import {EcoDriverList} from '../../components';


const mapStateToProps = (state) => {
  return {
    ecoDriverList: state.carPark.ecoDriving.ecoDriverList
  }
};


const mapDispatchToProps = (dispatch) => {
  return {
    getListEcodriver: (filter) => {dispatch(getListEcodriver(filter))}
  }
};

class EcoDrivingContainer extends Component {

  componentDidMount(){
    this.props.getListEcodriver();
  }

  filterListEcodriver = (filterValue, field) => {
    this.props.getListEcodriver('_filter('+field+'-like)=%25'+filterValue+'%25')
  };

  render() {
    const {ecoDriverList} = this.props;
    
    if(ecoDriverList != null){
      return( <EcoDriverList ecoDriverList={ecoDriverList} filterListEcodriver={this.filterListEcodriver} /> )
    }
    return (<Loader />)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(EcoDrivingContainer);