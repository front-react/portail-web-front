import React, { Component } from "react";
import { connect } from "react-redux";
import { PersonInfoHeader } from "../../components";
import { Loader } from "../../../../components";
import getPerson from "../../redux/actions/person/getPerson";
import refreshPerson from "../../redux/actions/person/refreshPerson";

const mapStateToProps = (state, props) => {
  return {
    person: state.rh.person.currentPerson,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPerson: id => {
      dispatch(getPerson(id));
    },
    refreshPerson: () => {
      dispatch(refreshPerson());
    },
  };
};

class PersonInfoHeaderContainer extends Component {
  
  componentDidMount() {
    const id = this.props.personId;
    this.props.getPerson(id);
  }
  componentWillUnmount() {
    this.props.refreshPerson();
  }

  render() {
    const { person } = this.props;

    if (person !== null) {
      return <PersonInfoHeader person={person} />;
    }
    return <Loader />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  PersonInfoHeaderContainer,
);
