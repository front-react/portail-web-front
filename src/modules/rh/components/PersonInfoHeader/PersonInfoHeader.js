import React from 'react'
import Paper from 'material-ui/Paper';
//import {Tabs, Tab} from 'material-ui/Tabs';
import {grey200, grey500, orange500} from "material-ui/styles/colors"
import Avatar from 'material-ui/Avatar';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider'

const styles = {
    userProfile: {
      marginBottom: '40px',
      width: '33%',
    },
    PaperPerson: {
      height: '350px',
      width: '100%',
      // display: 'flex',
      alignItems: 'center',
      padding: '10px',
      fontSize: '16px'
    },    
    PaperPersonProfile: {
      alignItems: 'center',
      width: '100%',
    },
    PaperPersonInformation: {
      width: '100%',
      marginBottom: '10px',
      flexDirection: 'column',
      display: 'inline-block'
    },
    PaperPersonInformationName: {
      fontSize: '28px',
      marginBottom: '5px',
      display: 'block'
    },
    PaperPersonInformationJob: {
      fontSize: '20px',
      color: '#757575',
      marginBottom: '10px',
    },
    PaperPersonInformationContactPro: {
      alignItems: 'center',
      fontSize: '16px',
      color: '#757575',
      display: 'flex',
      marginBottom: '3px',
    },
    contactIcon: {
      color:"#757575",
      width: '16px',
      height: '16px',
      marginRight: '5px'
    },
    PaperPersonPerso: {
      paddingLeft: '15px',
      width: '100%',
      height: '120px',
      flexDirection: 'column',
      justifyContent: 'center',
    },
    PaperPersonPersoLine: {
      marginBottom: '10px',
      display: 'block'
    },
    PaperPersonPersoLabel: {
      fontWeight: '600',
      width: '150px'
    },
    avatar: {
      marginRight: '20px',
    },
    tabsProfile: {
      marginTop: 20,
      textAlign: 'center'
    },
    tabItemContainerProfile: {
      backgroundColor: grey200,
      borderBottom: '1px solid '+grey500 ,
    },
    tabContainerProfile: {
      marginTop: 20,
    },
    tabProfile: {
      color: grey500,
    },
    tabButtonProfile: {
      textTransform: 'none',
      fontWeight: '300',
    },
    inkBar: {
      backgroundColor: orange500
    },
    subheader: {
      lineHeight: '30px',
      fontSize: '25px',
      fontWeight: '700',
      marginBottom: '15px'
    },
  };
  
  const random = () => {
    return Math.floor(Math.random() * 99) + 1;
  };

  const PersonInfoHeader = ({person}) => {
    
    return (
    <div style={styles.userProfile}>
        <Paper style={styles.PaperPerson}>
          <Subheader style={styles.subheader}>Information Générales</Subheader>
          <Divider />
            <div style={styles.PaperPersonProfile}>
              <div style={styles.tabsProfile} >
                <Avatar
                    src={"https://randomuser.me/api/portraits/men/" + random() + ".jpg"}
                    size={180}
                    style={styles.avatar}
                />
                <div style={styles.PaperPersonInformation}>
                    <span style={styles.PaperPersonInformationName}>{person.personFullName}</span>
                </div>
              </div>
              
            </div>
        </Paper>
      </div>
    );
  }
  

  export default PersonInfoHeader