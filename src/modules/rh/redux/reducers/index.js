import {combineReducers} from 'redux';
import personReducer from './personReducer';

const rhReducer = combineReducers({
  person: personReducer,
});

export default rhReducer;
