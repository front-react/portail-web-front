import {
  GET_PERSON_LIST_REQUEST,
  GET_PERSON_LIST_SUCCESS,
  GET_PERSON_LIST_FAILURE,
  GET_PERSON_REQUEST,
  GET_PERSON_SUCCESS,
  GET_PERSON_FAILURE,
} from '../types'

const initialState = {
  personList: null,
  lastRequest: null,
  currentPerson: null,
  error: null,
};

export const personReducer = (state = initialState, action) => {
  const {data,type,request,error} = action;

  switch (type) {
    case GET_PERSON_LIST_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_PERSON_LIST_SUCCESS:
      return {
        ...state,
        personList: data,
      };


    case GET_PERSON_LIST_FAILURE:
      return {
        ...state,
        error
      };

    case GET_PERSON_REQUEST:
      return {
        ...state,
        lastRequest: request
      };

    case GET_PERSON_SUCCESS:
      return {
        ...state,
        currentPerson: data,
      };


    case GET_PERSON_FAILURE:
      return {
        ...state,
        error
      };
    
    default:
      return state;
  }
};

export default personReducer;