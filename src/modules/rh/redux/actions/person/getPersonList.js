import {
  GET_PERSON_LIST_REQUEST,
  GET_PERSON_LIST_SUCCESS,
  GET_PERSON_LIST_FAILURE,
} from '../../types';
import {
  rhApiProvider
} from '../../../../../utils/api/providers';

const getPersonListRequest = (request) => {
  return {
    type: GET_PERSON_LIST_REQUEST,
    request
  }
};

const getPersonListSuccess = (data) => {

  return {
    type: GET_PERSON_LIST_SUCCESS,
    data
  }
};
const getPersonListFailure = (error) => {

  return {
    type: GET_PERSON_LIST_FAILURE,
    error
  }
};

const getRequest = (filter) => {
  if(filter != null) {
    return '/persons?'+filter
  }

  return '/persons'
};


export const getPersonList = (filter = null) => {

  let request = getRequest(filter);

  return dispatch => {
    dispatch(getPersonListRequest(request));
    return rhApiProvider.get(request)
      .then(
        res => dispatch(getPersonListSuccess(res.data.data)),
        error => {
          dispatch(getPersonListFailure(error))
        }
      )
  }
};

export default getPersonList;
