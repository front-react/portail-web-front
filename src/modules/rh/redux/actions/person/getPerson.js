import {
  GET_PERSON_REQUEST,
  GET_PERSON_SUCCESS,
  GET_PERSON_FAILURE,
} from '../../types';
import {
  rhApiProvider
} from '../../../../../utils/api/providers';

const getPersonRequest = (request) => {
  return {
    type: GET_PERSON_REQUEST,
    request
  }
};

const getPersonSuccess = (data) => {

  return {
    type: GET_PERSON_SUCCESS,
    data
  }
};
const getPersonFailure = (error) => {

  return {
    type: GET_PERSON_FAILURE,
    error
  }
};

export const getPerson = (id) => {

  let request =  '/persons/'+id;

  return dispatch => {
    dispatch(getPersonRequest(request));
    return rhApiProvider.get(request)
      .then(
        res => dispatch(getPersonSuccess(res.data.data)),
        error => {
          dispatch(getPersonFailure(error))
        }
      )
  }
};

export default getPerson;
