import { GET_PERSON_SUCCESS, GET_PERSON_REQUEST } from "../../types";

const getRefreshPersonRequest = (request) => {
  return {
    type: GET_PERSON_REQUEST,
    request
  }
};

const getRefreshPersonSuccess = data => {
  return {
    type: GET_PERSON_SUCCESS,
    data,
  };
};

export const refreshPerson = () => {
  return dispatch => {
    dispatch(getRefreshPersonRequest(null));
    dispatch(getRefreshPersonSuccess(null));
  };
};

export default refreshPerson;
