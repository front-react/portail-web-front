/* eslint-disable */
import {
  reportingReducer
} from '../redux/reducers/reportingReducer'
import {
  GET_LIST_VISITES_REQUEST,
  GET_LIST_VISITES_SUCCESS,
  GET_LIST_VISITES_FAILURE,
  GET_ONE_VISITE_REQUEST,
  GET_ONE_VISITE_SUCCESS,
  GET_ONE_VISITE_FAILURE,
  GET_LIST_ENSEIGNES_REQUEST,
  GET_LIST_ENSEIGNES_SUCCESS,
  GET_LIST_ENSEIGNES_FAILURE,
  GET_LIST_PERSONNES_REQUEST,
  GET_LIST_PERSONNES_SUCCESS,
  GET_LIST_PERSONNES_FAILURE,
  GET_LIST_CDNC_REQUEST,
  GET_LIST_CDNC_SUCCESS,
  GET_LIST_CDNC_FAILURE,
} from '../redux/actions/types';

describe('mainReducer', function() {

  it('should return the initial state', function() {
    expect(reportingReducer(undefined, {})).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: '',
    });
  });

  //============= GET_LIST_VISITES =============
  it('should react to an action with the type GET_LIST_VISITES_REQUEST', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_VISITES_REQUEST,
      request: "request"
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: 'request',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_VISITES_SUCCESS', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_VISITES_SUCCESS,
      data: [{
          object: "object"
        },
        {
          object: "object"
        }
      ]
    })).toEqual({
      visites: [{
        object: "object"
      }, {
        object: "object"
      }],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_VISITES_FAILURE', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_VISITES_FAILURE,
      error: 'error'
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: 'error',
    })
  });

  //============= GET_ONE_VISITE =============
  it('should react to an action with the type GET_ONE_VISITE_REQUEST', function() {
    expect(reportingReducer(undefined, {
      type: GET_ONE_VISITE_REQUEST,
      request: "request"
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: 'request',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_ONE_VISITE_SUCCESS', function() {
    expect(reportingReducer(undefined, {
      type: GET_ONE_VISITE_SUCCESS,
      data: {
        object: "object",
        object: "object"
      }
    })).toEqual({
      visites: [],
      visite: {
        object: "object",
        object: "object"
      },
      isFetching: false,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_ONE_VISITE_FAILURE:', function() {
    expect(reportingReducer(undefined, {
      type: GET_ONE_VISITE_FAILURE,
      error: 'error'
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: 'error',
    })
  });


  //============= GET_LIST_ENSEIGNES =============
  it('should react to an action with the type GET_LIST_ENSEIGNES_REQUEST', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_ENSEIGNES_REQUEST,
      request: "request"
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: 'request',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_ENSEIGNES_SUCCESS', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_ENSEIGNES_SUCCESS,
      data: [{
        object: "object"
      }, {
        object: "object"
      }]
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [{
        object: "object"
      }, {
        object: "object"
      }],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_ENSEIGNES_FAILURE:', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_ENSEIGNES_FAILURE,
      error: 'error'
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: 'error',
    })
  });


  //============= GET_LIST_PERSONNES =============
  it('should react to an action with the type GET_LIST_PERSONNES_REQUEST', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_PERSONNES_REQUEST,
      request: "request"
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: 'request',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_PERSONNES_SUCCESS', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_PERSONNES_SUCCESS,
      data: [{
        object: "object"
      }, {
        object: "object"
      }]
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [{
        object: "object"
      }, {
        object: "object"
      }],
      cdncDataFilter: [],
      request: '',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_PERSONNES_FAILURE:', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_PERSONNES_FAILURE,
      error: 'error'
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: 'error',
    })
  });



  //============= GET_LIST_CDNC_REQUEST =============
  it('should react to an action with the type GET_LIST_CDNC_REQUEST', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_CDNC_REQUEST,
      request: "request"
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: 'request',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_CDNC_SUCCESS', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_CDNC_SUCCESS,
      data: [{
        object: "object"
      }, {
        object: "object"
      }]
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [{
        object: "object"
      }, {
        object: "object"
      }],
      request: '',
      requestError: '',
    })
  });

  it('should react to an action with the type GET_LIST_CDNC_FAILURE:', function() {
    expect(reportingReducer(undefined, {
      type: GET_LIST_CDNC_FAILURE,
      error: 'error'
    })).toEqual({
      visites: [],
      visite: {},
      isFetching: true,
      enseignesDataFilter: [],
      personnesDataFilter: [],
      cdncDataFilter: [],
      request: '',
      requestError: 'error',
    })
  });


});
