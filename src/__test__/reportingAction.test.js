/* eslint-disable */
jest.unmock('../redux/actions/reporting/getAllVisitesActions');
jest.unmock('../redux/actions/reporting/getOneVisiteActions');
jest.unmock('../redux/actions/reporting/getAllEnseignesActions');
jest.unmock('../redux/actions/reporting/getAllPersonnesActions');
jest.unmock('../redux/actions/reporting/getAllCdncActions');
jest.unmock('redux-mock-store');
jest.unmock('redux-thunk');
jest.unmock('axios');
jest.unmock('moxios');
import {
  reportingApi
} from '../services/api/url';
import {
  reportingApiProvider
} from '../services/api/providers';
import * as getAllVisitesActions from '../redux/actions/reporting/getAllVisitesActions'
import * as getOneVisitesActions from '../redux/actions/reporting/getOneVisiteActions'
import * as getAllEnseignesActions from '../redux/actions/reporting/getAllEnseignesActions'
import * as getAllPersonnesActions from '../redux/actions/reporting/getAllPersonnesActions'
import * as getAllCdncActions from '../redux/actions/reporting/getAllCdncActions'
import * as type from '../redux/actions/types';
import thunk from 'redux-thunk'
import axios from 'axios'
import moxios from 'moxios'
import configureMockStore from 'redux-mock-store'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Repoting Actions', () => {
  beforeEach(function() {
    moxios.install(reportingApiProvider)
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
  });

  afterEach(function() {
    moxios.uninstall()
  });


  it('return data for the list of visites', (done) => {
    const expectedActions = [{
        type: type.GET_LIST_VISITES_REQUEST,
        request: '/clients/11/visites'
      },
      {
        type: type.GET_LIST_VISITES_SUCCESS,
        data: "data",
      }
    ];
    const store = mockStore({
      data: ""
    });

    store.dispatch(getAllVisitesActions.getAllVisites())
    moxios.wait(function() {
      let request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: {
          data: "data",
          schema: "schema",
        }
      }).then(function() {
        expect(store.getActions()).toEqual(expectedActions)
        done()
      })
    })
  })


  it('return data for one visites', (done) => {

    const visite = {
      idVisite: "386541"
    }
    const expectedActions = [
      {
        type: type.GET_ONE_VISITE_REQUEST,
        request: '/visites/' + visite.idVisite + '/resultats'
      },
      {
        type: type.GET_ONE_VISITE_SUCCESS,
        data: {
          information: visite,
          resultats: "data"
        },
      }
    ];
    const store = mockStore({
      information: {},
      resultats: {}
    });

    store.dispatch(getOneVisitesActions.getOneVisites(visite, false))
    moxios.wait(function() {
      let request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: {
          data: "data",
          schema: "schema",
        }
      }).then(function() {
        expect(store.getActions()).toEqual(expectedActions)
        done()
      })
    })
  })



  it('return data for enseignes', (done) => {
    const idClient = 0;
    const expectedActions = [
      {
        type: type.GET_LIST_ENSEIGNES_REQUEST,
        request: '/clients/' + idClient +'/enseignes'
      },
      {
        type: type.GET_LIST_ENSEIGNES_SUCCESS,
        data: {
          enseignes: "data"
        },
      }
    ];
    const store = mockStore({
      enseignes: {}
    });

    store.dispatch(getAllEnseignesActions.getAllEnseignes(idClient))
    moxios.wait(function() {
      let request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: {
          enseignes: "data",
        }
      }).then(function() {
        expect(store.getActions()).toEqual(expectedActions)
        done()
      })
    })
  })


  it('return data personnes names', (done) => {
    const idClient = 0;
    const expectedActions = [
      {
        type: type.GET_LIST_PERSONNES_REQUEST,
        request: '/clients/' + idClient +'/personnes?_sort=nom'
      },
      {
        type: type.GET_LIST_PERSONNES_SUCCESS,
        data: {
          personnes: "data"
        },
      }
    ];
    const store = mockStore({
      personnes: {}
    });

    store.dispatch(getAllPersonnesActions.getAllPersonnes(idClient))
    moxios.wait(function() {
      let request = moxios.requests.mostRecent()
      request.respondWith({
        status: 200,
        response: {
          personnes: "data",
        }
      }).then(function() {
        expect(store.getActions()).toEqual(expectedActions)
        done()
      })
    })
  })


    it('return data CDNC ', (done) => {
      const idClient = 0;
      const expectedActions = [
        {
          type: type.GET_LIST_CDNC_REQUEST,
          request: '/clients/' + idClient +'/cdncs?_limit=10000&_sort=nomCdnc'
        },
        {
          type: type.GET_LIST_CDNC_SUCCESS,
          data: {
            cdnc: "data"
          },
        }
      ];
      const store = mockStore({
        cdnc: {}
      });

      store.dispatch(getAllCdncActions.getAllCdnc(idClient))
      moxios.wait(function() {
        let request = moxios.requests.mostRecent()
        request.respondWith({
          status: 200,
          response: {
            cdnc: "data",
          }
        }).then(function() {
          expect(store.getActions()).toEqual(expectedActions)
          done()
        })
      })
    })


})
