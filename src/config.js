let config = {
  api: {},
};

config.env = "stage";
config.api.authApiEndpoint = "http://10.33.100.40/auth/public";
config.api.reportingApiEndpoint = "http://10.33.100.40/api-reporting/public";
config.api.authorizationApiEndpoint =
  "http://10.33.100.40/authorization-api/public";
config.api.carsApiEndpoint = "http://10.33.100.40/car-park-api/public/";
config.api.AssignmentApiEndpoint =
  "https://api.upsell.fr/file-templating-api/public/";
config.api.rhApiEndpoint = "http://10.33.100.40/rh-api/public/";
config.store = "./configureStore.dev";

if (process.env.NODE_ENV === "production") {
  config.env = "production";
  config.api.authApiEndpoint = "https://api.upsell.fr/auth/public";
  config.api.reportingApiEndpoint =
    "https://api.upsell.fr/api-reporting/public";
  config.api.authorizationApiEndpoint =
    "https://api.upsell.fr/authorization-api/public";
  config.api.carsApiEndpoint = "https://api.upsell.fr/car-park-api/public/";
  config.api.rhApiEndpoint = "https://api.upsell.fr/rh-api/public/";

  config.store = "./configureStore.dev";
}
export default config;

/*
 config.env = 'dev';
 config.api.authApiEndpoint = 'http://192.168.1.10/auth/public';
 config.api.reportingApiEndpoint = 'http://192.168.1.10/api-reporting/public';
 config.api.authorizationApiEndpoint = 'http://192.168.1.10/authorization-api/public';
 config.api.carsApiEndpoint = 'http://192.168.1.10/car-park-api/public/';
 config.store = './configureStore.dev';
 */
