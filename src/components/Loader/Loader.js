import React from 'react'
import CircularProgress from "material-ui/CircularProgress"

const style = {
  divloader: {
    height: '100%',
  },
  loader: {
    position: 'absolute',
    margin: 'auto',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  }
}

const Loader = () => {

  return (
    <div style={style.divloader}>
      <CircularProgress style={style.loader} size={80}/>
    </div>
  )

}

export default Loader