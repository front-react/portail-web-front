import React from 'react'
import SearchIcon from 'material-ui/svg-icons/action/search'
import {grey800} from 'material-ui/styles/colors'
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';

const styles = {
  seacrBarContainer: {
    //padding: '10px',
  },
  searchBar: {
    width: '190px',
    height: '36px',
    display: 'flex',
    alignItems: 'center',
    padding: '0 5px 0 5px'
  },

  searchIcon: {
    marginRight: 5
  },
  textField: {
    border: 'none'
  },
  textFieldHint: {
    fontWeight: 500
  }
};



const SearchBar = ({onChange, hintText = "Recherche", iconSearch}) => {

  const Icon = iconSearch || SearchIcon;

  return (
    <div style={styles.seacrBarContainer}>
      <Paper style={styles.searchBar}>
        <Icon color={grey800} fontSize={"25px"} style={styles.searchIcon}/>
        <TextField
          onChange={(event,newValue) => { onChange(newValue.split(" ").join("%25")) }}
          underlineShow={false}
          style={styles.textField}
          hintStyle={styles.textFieldHint}
          hintText={hintText}/>
      </Paper>
    </div>
  )
};

export default SearchBar
