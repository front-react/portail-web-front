import React from 'react'
import SearchIcon from 'material-ui/svg-icons/action/search'
import {grey400} from 'material-ui/styles/colors'
import AutoComplete from 'material-ui/AutoComplete';
import Paper from 'material-ui/Paper';

const styles = {
  searchBarContainer: {
    padding: '10px',
  },
  searchBar: {
    width: '250px',
    height: '36px',
    display: 'flex',
    alignItems: 'center',
    padding: '0 5px 0 5px'
  },

  searchIcon: {
    marginRight: 5
  },
  textField: {
    border: 'none'
  },
  textFieldHint: {
    fontWeight: 500
  }
};

const SearchBarAutoComplete = ({onNewRequest, onUpdateInput, data, fullWidth, value, textField, valueField}) => {
  const dataSourceConfig = {
    text: textField,
    value: valueField
  };
  const fullWidthValue = fullWidth || false;

  return (
    <div style={styles.searchBarContainer}>
      <Paper style={styles.searchBar}>
        <SearchIcon color={grey400} style={styles.searchIcon}/>
        <AutoComplete
          onNewRequest={onNewRequest}
          onUpdateInput={onUpdateInput}
          dataSource={data}
          style={styles.textField}
          hintStyle={styles.textFieldHint}
          hintText="Recherche"
          filter={AutoComplete.caseInsensitiveFilter}
          dataSourceConfig={dataSourceConfig}
          fullWidth={fullWidthValue}
          maxSearchResults={10}
          value={value}
          underlineShow={false}
        />

      </Paper>
    </div>
  )
};

export default SearchBarAutoComplete
