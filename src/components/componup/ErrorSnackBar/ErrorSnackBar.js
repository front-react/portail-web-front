import React from 'react';
import Snackbar from 'material-ui/Snackbar';

const ErrorSnackBar = ({onRequestClose, message, open}) => {
  const styles = {
    snackBar: {
      top: 0,
      bottom: 'auto',
      left: (window.innerWidth - 288) / 2,
      transform: message ?
        'translate3d(0, 0, 0)' :
        `translate3d(0, -50px, 0)`
    },
    snackBarBody: {
      height: '60px',
      backgroundColor: '#F44336',
      display: 'flex',
      alignItems: 'center',
    },
  };

  return (
    <Snackbar
      open={open}
      message={message}
      action={"Fermer"}
      autoHideDuration={10000000}
      onRequestClose={onRequestClose}
      onActionTouchTap={onRequestClose}
      style={styles.snackBar}
      bodyStyle={styles.snackBarBody}

    />
  )
};

export default ErrorSnackBar