import React from 'react'

const styles = {
  title: {
    fontWeight: '200',
    fontSize: '48px',
    marginBottom: '20px',
  },
};

const ProfileTitle = ({title}) => {

  return (
    <div style={styles.title}>
      {title}
    </div>
  )
};

export default ProfileTitle