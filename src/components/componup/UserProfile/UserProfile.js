import React from 'react'
import Avatar from 'material-ui/Avatar';

const styles = {
  userProfile: {
    display: 'flex',
    flexDirection: 'row',
  },
  userInformation: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginLeft: '10px',
  },
  name: {
    fontSize: '20px',
  }
};

const UserProfile = ({picture, name, job}) => {

  return (
    <span style={styles.userProfile}>
      <span>
        <Avatar size={48} src={picture} />
      </span>
      <span style={styles.userInformation}>
        <span style={styles.name}>{name}</span>
        <span>{job}</span>
      </span>
    </span>
  )
};

export default UserProfile