import React from 'react'
import Paper from 'material-ui/Paper'
import { darkerColor } from '../../../utils/style/color';
import FaBug from 'react-icons/lib/fa/bug';
import Divider from 'material-ui/Divider';

const SimpleStatsData = ({hexaColor, icon, dataNumber, dataLegend, sizeImg, height, width, numberData, numberLegend, titleLabel}) => {

  const mainColor = hexaColor || 'black';
  const secondColor = darkerColor(mainColor, -0.3);
  const Icon = icon || FaBug;
  const displayedDataNumber = dataNumber;
  const displayedDataLegend = dataLegend;
  
  const styles = {
    paper: {
      color: 'white',
      width: width,
      height: height,
      marginRight: '5px',
      marginBottom: '15px',
      fontSize: '24px',
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      textAlign: 'center',
    },
    iconSpan: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '50%',
      height: '100%',
      backgroundColor: mainColor,
    },
    dataSpan: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      width: '50%',
      height: '100%',
      backgroundColor: secondColor,
    },
    dataNumber: {
      fontSize: numberData,
      color: 'white'
    },
    dataLegend: {
      fontSize: numberLegend,
      color: 'white'
    },
    colorIcon : {
      color: 'white',
      height: sizeImg,
      width: sizeImg,
    },
    displayState: {
      display: 'block',
      width: '100%',
      paddingTop: '6%'
    },
    PaperDivTitle: {
      width: '100%',
      height: '20%',
      backgroundColor: 'white',
      color: secondColor,
      padding: '10px',
      fontWeight: '700',
      fontSize: '32px',
      display: 'flex'
    },
    PaperTitle: {
      fontSize: '24px'
    },
    boxFlex: {
      display: 'flex',
    },
    svgIcon : {
      color: secondColor,
      height: '25px',
      width: '25px',
      paddingRight: '10px'
    },
    styleLink: {
      textDecoration: 'none',
      display: 'block',
      width: '100%',
      height: '100%',
      paddingTop: '6%'
    }
  };


  return (
    <Paper style={styles.paper}>
      <Divider />
      <div style={styles.iconSpan}>
        <div style={styles.displayState}><Icon  style={styles.colorIcon} /></div>
      </div>

      <div style={styles.dataSpan}>
        <div style={styles.dataNumber}>{displayedDataNumber}</div>
        <div style={styles.dataLegend}>{displayedDataLegend}</div>
      </div>
    </Paper>
  )
};

export default SimpleStatsData