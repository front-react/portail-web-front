import React from 'react'
import Paper from 'material-ui/Paper'
import { darkerColor } from '../../../utils/style/color';
import FaBug from 'react-icons/lib/fa/bug';

const SimpleDataView = ({hexaColor, icon, dataNumber, dataLegend}) => {
  const mainColor = hexaColor || 'black';
  const secondColor = darkerColor(mainColor, -0.3);
  const Icon = icon || FaBug;
  const displayedDataNumber = dataNumber || 'errors on dataNumber';
  const displayedDataLegend = dataLegend || 'errors on dataLegend';
  const styles = {
    paper: {
      color: 'white',
      height: 80,
      width: 200,
      textAlign: 'center',
      display: 'flex',
    },
    iconSpan: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '50%',
      height: '100%',
      backgroundColor: mainColor,
    },
    dataSpan: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      width: '50%',
      height: '100%',
      backgroundColor: secondColor,
    },
    dataNumber: {
      fontSize: '26px',
    },
    dataLegend: {
      fontSize: '16px',
    },
  };


  return (
    <Paper style={styles.paper}>
      <div style={styles.iconSpan}>
        <Icon size={38}/>
      </div>
      <div style={styles.dataSpan}>
        <span style={styles.dataNumber}>{displayedDataNumber}</span>
        <span style={styles.dataLegend}>{displayedDataLegend}</span>
      </div>
    </Paper>
  )
};

export default SimpleDataView