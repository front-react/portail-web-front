/* THIS IS THE FUTUR UPSELL'S COMPONENT LIBRAIRIE */

import SimpleDataView from './SimpleDataView/SimpleDataView'
import SearchBar from './SearchBar/SearchBar'
import SearchBarAutoComplete from './SearchBarAutoComplete/SearchBarAutoComplete'
import ProfileTitle from './ProfileTitle/ProfileTitle'
import UserProfile from './UserProfile/UserProfile'
import ErrorSnackBar from './ErrorSnackBar/ErrorSnackBar'
import SimpleStatsData from './SimpleStatsData/SimpleStatsData'


export {
  SimpleDataView,
  SearchBar,
  SearchBarAutoComplete,
  ProfileTitle,
  UserProfile,
  ErrorSnackBar,
  SimpleStatsData
}