import {orange500, grey800, grey400} from 'material-ui/styles/colors'


const imgNumber = Math.floor(Math.random() * 4) + 1;
const img = require('./bg_login/'+imgNumber+'.jpg');


const styles = {
  mainDiv: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    background: 'url(' + img + ')',
    backgroundSize: 'cover',
    display: 'flex',
    flexDirection: 'row'
  },
  loginDiv: {
    marginLeft: '70%',
    marginTop: '8%',
  },

  loginFormDiv: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  contentRightdiv: {
    marginTop: '15%',
    marginRight: 'auto',
    marginLeft: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center'
  },
  up: {
    color: orange500,
    fontWeight: 'bold'
  },
  sell: {
    fontFamily: 'Roboto',
    color: grey800,
    fontSize: '4em',
    textAlign: 'center',
    marginBottom: '80px'
  },
  welcomeMessage: {
    width: '100%',
    fontFamily: 'Roboto',
    color: "White",
    textAlign: 'center'
  },
  fieldDiv: {
    flex: 2,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  row: {
    width: 'auto'
  },
  field: {
    color: 'white',
    textAlign: 'center'
  },
  fieldInput: {
    color: 'white',
  },
  fieldLabel: {
    color: 'white',
  },
  fieldLabelFocus: {
    color: orange500,
  },
  buttonlogin: {
    backgroundColor: 'transparent',
    marginTop: '40px',
    width: '150px',
    fontWeight: 300
  },
  labelbuttonlogin: {
    fontWeight: 400,
    textTransform: 'none'
  },
  versionDiv: {
    marginTop: 'auto',
    marginBottom: '5px',
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontSize: '0.6em',
    fontWeight: 300,
    color: grey400
  },
  snackBar: {
    top: '0',
  },
  snackBarBody: {
    height: '60px',
    backgroundColor: '#F44336',
    display: 'flex',
    alignItems: 'center',
  },
};

export default styles