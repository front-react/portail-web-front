import React from 'react'
import {Field, reduxForm} from 'redux-form'
import {TextField} from 'redux-form-material-ui'
import RaisedButton from 'material-ui/RaisedButton';
import {orange500} from 'material-ui/styles/colors'
import styles from './stylesLoginForm'
import Snackbar from 'material-ui/Snackbar';


export const LoginForm = (props) => {
  const {handleFormSubmit, handleSubmit, open, message, handleRequestClose} = props;

  return (
    <div style={styles.mainDiv}>
      <div style={styles.loginDiv}>
        <div style={styles.loginFormDiv}>
          <div style={styles.contentRightdiv}>
             <span style={styles.sell}>
              <span style={styles.up}>Up</span>Sell</span>
            <span style={styles.welcomeMessage}>Bienvenue sur le Portail Upsell,</span>
            <span style={styles.welcomeMessage}>veuillez vous connectez pour accéder à vos services :</span>

            <form onSubmit={handleSubmit(handleFormSubmit)}>
              <div style={styles.fieldDiv}>
                <Field
                  name="username"
                  style={styles.field}
                  floatingLabelStyle={styles.fieldLabel}
                  floatingLabelFocusStyle={styles.fieldLabelFocus}
                  inputStyle={styles.fieldInput}
                  component={TextField}
                  type="text"
                  errorText={message}
                  floatingLabelText="Entrez votre identifiant"/>
                <Field
                  name="password"
                  style={styles.field}
                  floatingLabelStyle={styles.fieldLabel}
                  floatingLabelFocusStyle={styles.fieldLabelFocus}
                  inputStyle={styles.fieldInput}
                  component={TextField}
                  type="password"
                  errorText={message}
                  floatingLabelText="Entrez votre mot de passe"/>
                <RaisedButton label="Connexion" type="submit" labelStyle={styles.labelbuttonlogin}
                              style={styles.buttonlogin} backgroundColor={orange500}/>
              </div>
            </form>
          </div>
          <div style={styles.versionDiv}>
            <span>Portail v3.0.0</span>
          </div>
        </div>
      </div>
      <Snackbar
        open={open}
        message={message || ''}
        autoHideDuration={100000}
        onRequestClose={handleRequestClose}
        style={styles.snackBar}
        bodyStyle={styles.snackBarBody}
      />
    </div>
  )
};
export default reduxForm({
  form: 'loginFrom',
})(LoginForm);
