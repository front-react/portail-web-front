import React from "react";
import { ComponentLoader } from "../../../components";

const withLoader = Component => ({ isLoading, ...rest }) =>
  isLoading ? <ComponentLoader /> : <Component {...rest} />;

export default withLoader;
