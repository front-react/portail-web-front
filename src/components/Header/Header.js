import React from "react";
import IconButton from "material-ui/IconButton";
import IconBack from "material-ui/svg-icons/navigation/arrow-back";
import Avatar from "material-ui/Avatar";
import DropDownMenu from "material-ui/DropDownMenu";
import { grey600 } from "material-ui/styles/colors";
import "./style.css";

const Header = ({
  clients,
  username,
  currentClient,
  location,
  menuItems,
  prevLocation,
  handleChange,
}) => {
  const imgprofile = require("./images/profil.png");

  const styles = {
    dropDownLabel: {
      padding: 0,
      color: grey600,
    },
    dropDownInline: {
      border: "none",
    },
    dropDownIcon: {
      color: grey600,
    },
  };

  return (
    <div className="header">
      <div className="headerGradient">
        <div className="navigation">
          <IconButton
            className="backButton"
            onClick={() => {
              prevLocation();
            }}
          >
            <IconBack />
          </IconButton>

          <span className="pageTitle">{location}</span>
        </div>
        <div className="profile">
          <div className="profileInformation">
            <span className="profileInformationName">{username}</span>
            <DropDownMenu
              value={currentClient.idClient}
              onChange={value => handleChange(value)}
              labelStyle={styles.dropDownLabel}
              iconStyle={styles.dropDownIcon}
              underlineStyle={styles.dropDownInline}
            >
              {menuItems}
            </DropDownMenu>
          </div>
          <div className="divAvatar">
            <Avatar src={imgprofile} size={50} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
