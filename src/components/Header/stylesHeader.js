import { grey600 } from "material-ui/styles/colors";

export const styles = {
  header: {},
  headerGradiant: {},
  navigation: {},
  profile: {},
  profileInformation: {},
  profileInformationName: {},
  dropDownLabel: {
    padding: 0,
    color: grey600,
  },
  dropDownInline: {
    border: "none",
  },
  dropDownIcon: {
    color: grey600,
  },
  divAvatar: {},
  backButton: {
    padding: "0",
    verticalAlign: "middle",
  },
  backButtonIcon: {},
  pageLocation: {},
  pageTitle: {},
  test: {
    color: "white",
    textDecoration: "none",
  },
};

export default styles;
