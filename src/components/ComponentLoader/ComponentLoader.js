import React from "react";
import CircularProgress from "material-ui/CircularProgress";

const style = {
  divloader: {
    position: "relative",
    minHeight: 100,
    height: "100%",
    width: 100,
    margin: "auto",
  },
  loader: {
    position: "absolute",
    left: "20%",
    top: "20%",
  },
};

const ComponentLoader = () => {
  return (
    <div style={style.divloader}>
      <CircularProgress style={style.loader} size={80} color="lightgrey" />
    </div>
  );
};

export default ComponentLoader;
