import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import {syncHistoryWithStore} from 'react-router-redux';
import Perf from 'react-addons-perf';
import {browserHistory} from 'react-router';
import configureStore from './redux/store';
import Root from './containers/Root/Root';
import initAuth from './redux/actions/initAuth';
import config from './config';

if(config.env === 'stage' || config.env === 'develop') {
 //https://facebook.github.io/react/docs/perf.html
 Perf.start();
}

//Needed for onTouchTap
//http://stackoverflow.com/a/34015469/988941

const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

store.dispatch(initAuth());

moment.locale('fr');

const rootEl = document.getElementById('root');

ReactDOM.render(
  <Root store={store} history={history}/>,
  rootEl
);



if (module.hot) {
  module.hot.accept('./containers/Root/Root', () => {
    let NextApp = require('./containers/Root/Root').default;
    ReactDOM.render(
      <NextApp />,
      document.getElementById('root')
    );
  });
}

if(config.env === 'stage' || config.env === 'develop') {
  Perf.stop();
  Perf.printInclusive(Perf.getLastMeasurements());
}
