const required = value => (value == null || ''  ? "Oups ! Vous n'avez pas rempli ce champ !" : undefined);

const requiredInt = value => (value == null || '')
  ? "Oups ! Vous n'avez pas rempli ce champ !"
  : (/^\d+$/.test(value))
    ? undefined
    : "Oups ! Pas de lettre ou de charactère spécial !";

const requiredFloat = value => (value == null || '')
  ? "Oups ! Vous n'avez pas rempli ce champ !"
  : (/^\d+(,\d{1,2})?$/.test(value)) || (/^\d+(.\d{1,2})?$/.test(value))
    ? undefined
    : "Oups ! Pas de lettre ou de charactère spécial  ou format non valide!";

export {
  requiredFloat,
  required,
  requiredInt,
}