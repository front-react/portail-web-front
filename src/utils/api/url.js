import config from '../../config'

export const authApi = {
  endpoint: config.api.authApiEndpoint
};
export const reportingApi = {
  endpoint: config.api.reportingApiEndpoint
};
export const authorizationApi = {
  endpoint: config.api.authorizationApiEndpoint
};
export const carsApi = {
  endpoint: config.api.carsApiEndpoint
};
export const AssignmentApi = {
  endpoint: config.api.AssignmentApiEndpoint
};

export const rhApi = {
  endpoint: config.api.rhApiEndpoint
};
