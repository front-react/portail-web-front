export const urlFilterConstructor = (filterOption) => {
  let urlFilter= '';
  for(let [key, value] of filterOption)
  {
    if(value != null){
      let filter = '_filter('+key+'-%3D)='+value+'&';
      urlFilter += filter;
    }
  }

  return urlFilter
};

export const urlSortingConstructor = (colId, sortType) => {
  let urlsort ='';
  if(colId != null){
    if (sortType == 'asc') {
      urlsort = '_sort='+colId
    }
    else if (sortType == 'desc') {
      urlsort += '_sort=-'+colId
    }
  }

  return urlsort
};

export const urlParametersConstructor = (params) => {
  let url ="&";
  let paramsFilters = urlFilterConstructor(Object.entries(params.filter));
  let paramsSorting = urlSortingConstructor(params.sort.idCol, params.sort.type);
  url += paramsFilters;
  url += paramsSorting;

  return url
};



