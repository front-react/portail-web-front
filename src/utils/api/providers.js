import axios from 'axios';
import {
  authApi,
  reportingApi,
  authorizationApi,
  carsApi,
  AssignmentApi,
  rhApi,
} from './url';

const authApiProvider = axios.create({
  baseURL: authApi.endpoint,
});

const reportingApiProvider = axios.create({
  baseURL: reportingApi.endpoint
});

const authorizationApiProvider = axios.create({
  baseURL: authorizationApi.endpoint
});

const carsApiProvider = axios.create({
  baseURL: carsApi.endpoint
});
const AssignmentApiProvider = axios.create({
  baseURL: AssignmentApi.endpoint
});
AssignmentApiProvider.defaults.headers.post['Content-Type'] = 'application/json';
AssignmentApiProvider.defaults.responseType = 'arraybuffer';

const rhApiProvider = axios.create({
  baseURL: rhApi.endpoint
});


const setAuthorization = (token) => {
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
};

const unSetAuthorization = () => {
  delete axios.defaults.headers.common.Authorization
};


export {
  authApiProvider,
  reportingApiProvider,
  authorizationApiProvider,
  carsApiProvider,
  AssignmentApiProvider,
  rhApiProvider,
  setAuthorization,
  unSetAuthorization,

}