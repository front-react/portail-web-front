export const darkerColor = (colorHexa, lum) => {
  lum = lum || 0;
  // validate colorHexa string
  colorHexa = String(colorHexa).replace(/[^0-9a-f]/gi, '');
  if (colorHexa.length < 6) {
    colorHexa = colorHexa[0]+colorHexa[0]+colorHexa[1]+colorHexa[1]+colorHexa[2]+colorHexa[2];
  }
  // convert to decimal and change luminosity
  let rgb = "#", c, i;
  for (i = 0; i < 3; i++) {
    c = parseInt(colorHexa.substr(i*2,2), 16);
    c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
    rgb += ("00"+c).substr(c.length);
  }

  return rgb;
};

export const getStripedStyle = (index) => {
  return { background: index % 2 ? '#fafafa' : 'white' };
};