import React from 'react';
import {Route, IndexRoute} from 'react-router';
import {UserAuthWrapper} from 'redux-auth-wrapper';
import {App, LoginPage, Layout, Home} from './containers';
import routesCarPark from './modules/carPark/routesCarPark';
import routesAdmin from './modules/admin/routesAdmin';
import routesUser from './modules/admin/routesUser';

/**
 * Verify if user is authenticated
 */
const IsAuthenticated = UserAuthWrapper({
  authSelector: (state) => {
    return state.auth
  },
  failureRedirectPath: '/login',
  wrapperDisplayName: 'IsAuthenticated',
  predicate: (auth) => {
    return auth.authenticated
  }
});

export default(
  <Route path="/" component={App}>
    <Route component={IsAuthenticated(Layout)}>
      <IndexRoute name="HomePage" component={Home}/>
      {routesCarPark}
      {routesAdmin}
      {routesUser}
    </Route>
    <Route path="/login" component={LoginPage}/>
  </Route>
)